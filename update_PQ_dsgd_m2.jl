include("./c_wrappers.jl")

function update_PQ_dsgd_m2(args)
    
    # unpack arguments 
    cid::Int64 = args[1]
    if cid == -1
        return 0,0,0,0
    end

    #burnin::Bool,
    local_blk_id::Int64,
    Ps::Array{Float64,2}, 
    Qs::Array{Float64,2}, 
    As::Array{Float64,1}, 
    Bs::Array{Float64,1},
    prec_Pk::Array{Float64,1},
    prec_Qk::Array{Float64,1},
    prec_A::Float64,
    prec_B::Float64,
    prec_r::Float64,
    eps::Float64,
    m::Int64,
    rndlen::Int64,
    mean_rate::Float64 = args[2:end]
    
    ################################################
    # Main SGLD 
    update_local_sgd_m2!(local_blk_id, Ps, Qs, As, Bs, prec_Pk, prec_Qk, prec_A, prec_B, prec_r, eps, m, rndlen, mean_rate)
    
    return Ps, Qs, As, Bs

end

function update_local_sgd_m2!(local_blk_id::Int64,
                              P::Array{Float64,2}, Q::Array{Float64,2}, 
                              A::Array{Float64,1}, B::Array{Float64,1},
                              prec_P::Array{Float64,1}, prec_Q::Array{Float64,1},   
                              prec_A::Float64, prec_B::Float64, prec_r::Float64, 
                              eps::Float64, m::Int64, rndlen::Int64, mean_rate::Float64
                              )
    sz_Xs = size(_train_blks[local_blk_id]::Array{Float64,2}, 1)
    K = size(P,1) 
    ka = _Ns_scale_blk[local_blk_id]::Float64 * prec_r * (_sz_trainset::Int64 / m) # _Ns_scale = Ns * S / N
    n_worker = _para["S"]::Int64
    hastep = 0.5 * eps
    sqrtstep = sqrt(eps)

    grad_sum_P = zeros(Float64, size(P))
    grad_sum_Q = zeros(Float64, size(Q))
    grad_sum_A = zeros(Float64, size(A))
    grad_sum_B = zeros(Float64, size(B))

    if sz_Xs < m
        @printf("m is larger than sz_Xs, so reduced to sz_Xs\n")
        m = sz_Xs
    end
    
    sampling_type = "w_replacement"
    error = Array(Float64, m)

    inv_pr_pick_user = _inv_pr_pick_user_blks[local_blk_id]
    inv_pr_pick_item = _inv_pr_pick_item_blks[local_blk_id]

    for iter in 1:rndlen
        mini_batch = _train_blks[local_blk_id][rand_samp(sz_Xs, m, sampling_type), :]
        uu, ii, rr = int(mini_batch[:,1]), int(mini_batch[:,2]), mini_batch[:,3]
        ux, ix     = unique(uu), unique(ii)
        c_comp_error!(error, rr, mean_rate, P, Q, A, B, uu, ii, m, K)
        c_comp_grad_sum!(error, uu, ii, P, Q, A, B, grad_sum_P, grad_sum_Q, grad_sum_A, grad_sum_B, m, K)
        c_update_para_sgd!(ux, P, grad_sum_P, prec_P, inv_pr_pick_user, ka, hastep, K)
        c_update_para_sgd!(ix, Q, grad_sum_Q, prec_Q, inv_pr_pick_item, ka, hastep, K)
        c_update_para_sgd!(ux, A, grad_sum_A, prec_A, inv_pr_pick_user, ka, hastep, 1)
        c_update_para_sgd!(ix, B, grad_sum_B, prec_B, inv_pr_pick_item, ka, hastep, 1)
    end
    nothing 
end


#function comp_grad_sum!(error::Array{Float64,1},
                        #uu::Array{Int64,1},ii::Array{Int64,1},
                        #Ps::Array{Float64,2}, Qs::Array{Float64,2}, 
                        #As::Array{Float64,1}, Bs::Array{Float64,1},
                        #grad_sum_Ps::Array{Float64,2}, grad_sum_Qs::Array{Float64,2},
                        #grad_sum_As::Array{Float64,1}, grad_sum_Bs::Array{Float64,1}
                        #) 
    #uu_n, ii_n, err_n = 0, 0, 0
    #for n = 1:length(error)
        #@inbounds uu_n, ii_n, err_n = uu[n], ii[n], error[n]
        #@inbounds grad_sum_Ps[:,uu_n] += err_n * Qs[:,ii_n]
        #@inbounds grad_sum_Qs[:,ii_n] += err_n * Ps[:,uu_n]
        #@inbounds grad_sum_As[uu_n]   += err_n
        #@inbounds grad_sum_Bs[ii_n]   += err_n
    #end
    #return nothing
#end



