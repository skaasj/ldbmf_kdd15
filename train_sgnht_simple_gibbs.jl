using Debug
using Datetime
using Devectorize
using Distributions
using NumericExtensions
include("utils/mf_common.jl")
include("utils/utils.jl")

function train_sgnht_simple_gibbs{T}(trainset::Array{T,2}, testset::Array{T,2}, 
                                     n_users::Int64, n_items::Int64, 
                                     n_elem_row::Array{Int64,1}, n_elem_col::Array{Int64,1}, 
                                     P::Array{Float64,2}, Q::Array{Float64,2}, 
                                     para::Dict{ASCIIString, Any})
#function train_sgnht_simple_gibbs{T}(trainset::Array{T,2}, testset::Array{T,2}, 
                                     #n_users::Int64, n_items::Int64, 
                                     #n_elem_row::Array{Int64,1}, n_elem_col::Array{Int64,1}, 
                                     #P::Array{Float64,2}, Q::Array{Float64,2}, 
                                     #etaSGNHT::Float64, alSGNHT::Float64, sz_batch::Int64=3000, 
                                     #round_len::Int64=200, 
                                     #maxiter=100, burnin=100, itv_save=Inf, 
                                     #itv_test=1, maxsec=Inf)
 
    para["algo"] = "SGNHT-Full-Gibbs"
    etaSGNHT = para["etaSGNHT"]
    alSGNHT = para["alSGNHT"]
    stepsz::Float64 = para["stepsz"] 
    sz_batch::Int64 = para["sz_batch"] 
    round_len::Int64 = para["round_len"]
    burnin::Int64 = para["burnin"]
    maxiter = para["maxiter"]
    maxsec = para["maxsec"] 
    itv_save = para["itv_save"]
    itv_test = para["itv_test"]
    
    println(para)
    @printf("Running %s (v0.04)\n", para["algo"])  
                                     
    # initialization
    logs = Log2(zeros(Int64,0), zeros(Float64,0), zeros(Float64,0), para)
    const min_rate = float(minimum(trainset[:,3]))
    const max_rate = float(maximum(trainset[:,3]))
    const mean_rate = mean(trainset[:,3])
    const sz_trainset = size(trainset)[1]
    const sz_testset = size(testset)[1]
    const K = size(P,1)

    iter = 1        # iter
    avg_cnt = 1  # counts number of averaged predicions
    ts = 0.0     # timestamp
    best_rmse = 100.0
    avg_pred = zeros(Float64,sz_testset)
    
    cur_logfile = get_str_id2(para)
    log_dir = "00_results_SGNHT_f_gibbs"

    # initial hyperparams
    alpha = 2.0   # r_ui ~ N(Pu'Qi, alpha^inv)
    alpha0, beta0 = 1.0, 1.0
    var_Pu = rand(InverseGamma(alpha0, beta0), K)
    var_Qi = rand(InverseGamma(alpha0, beta0), K)
    const alpha_P = alpha0 + 0.5 * n_users
    const alpha_Q = alpha0 + 0.5 * n_items
    
    mom_P = sqrt(etaSGNHT) * randn(K, n_users)
    mom_Q = sqrt(etaSGNHT) * randn(K, n_items)

    while iter <= maxiter && ts < maxsec
    
        tic()
        ########################################################################
        # Sample P and Q using SGLD
        P, Q, mom_P, mom_Q, alSGNHT = sample_sgnht_simple_gibbs(trainset, P, Q, mom_P, mom_Q, var_Pu, var_Qi, alpha, etaSGNHT, alSGNHT, n_elem_row, n_elem_col, sz_batch, mean_rate, round_len)
        # Sample hyperparams
        if rem(iter, 1) == 0
            var_Pu, var_Qi = sample_hyper(P, Q, var_Pu, var_Qi, alpha_P, alpha_Q, beta0, K)
        end
        iter += 1
        ########################################################################
        ts += toq();

        # Compute RMSE
        if mod(iter, itv_test) == 0
            avg_rmse_t, cur_rmse_t, avg_pred = rmse_avg(testset, iter, P, Q, avg_pred, avg_cnt, min_rate, max_rate, mean_rate, burnin)
            if iter > burnin 
                avg_cnt += 1
            end
            @printf("%d, %.4f (%.4f) (%.2fs) vPu:%.4f, vQi:%.5f", iter, avg_rmse_t, cur_rmse_t, ts, mean(var_Pu), mean(var_Qi))
            if iter > burnin && avg_rmse_t < best_rmse 
                best_rmse = avg_rmse_t
                @printf("(**)\n")
            else @printf("\n") 
            end
            push!(logs.iter, iter)
            push!(logs.ts, ts)
            push!(logs.rmse, avg_rmse_t)
            para["iter"] = iter; para["ts"] = ts; para["best"] = best_rmse
            logs.para = para

        end

        # Save logs file
        if mod(iter, itv_save) == 0
            @printf("saving loggings ... ")
            para["iter"] = iter; para["ts"] = ts
            cur_logfile = save_logs3(para, logs, log_dir, cur_logfile)
            @printf("done\n")
            if ts > maxsec
                break
            end
        end
    end
    logs, P, Q
end


function sample_hyper(P::Array{Float64,2}, Q::Array{Float64,2}, 
                      var_Pu::Array{Float64,1}, var_Qi::Array{Float64,1},
                      alpha_P::Float64, alpha_Q::Float64, beta0::Float64, K::Int64)
    sse_P = sum(sqr(broadcast(-, P, mean(P,2))),2)
    beta_P = beta0 + 0.5 * sse_P
    sse_Q = sum(sqr(broadcast(-, Q, mean(Q,2))),2)
    beta_Q = beta0 + 0.5 * sse_Q
    for k in 1:K 
        @inbounds var_Pu[k] = rand(InverseGamma(alpha_P, beta_P[k]))
        @inbounds var_Qi[k] = rand(InverseGamma(alpha_Q, beta_Q[k]))
    end
    return var_Pu, var_Qi
end


function sample_sgnht_simple_gibbs{T}(trainset::Array{T,2}, 
                                      P::Array{Float64,2}, Q::Array{Float64,2},  
                                      mom_P::Array{Float64,2}, mom_Q::Array{Float64,2},
                                      var_Pu::Array{Float64,1}, var_Qi::Array{Float64,1},   
                                      alpha::Float64, etaSGNHT::Float64, alSGNHT::Float64,
                                      n_elem_row::Array{Int64,1}, n_elem_col::Array{Int64,1}, 
                                      sz_batch::Int64, mean_rate::Float64, round_len::Int64=200)
    const sz_trainset = size(trainset)[1]
    const K = size(P,1)
    grad_sum_P = zeros(Float64, K, n_users)
    grad_sum_Q = zeros(Float64, K, n_items)
    const ka = alpha * (sz_trainset / sz_batch)
    const halfstepsz = 0.5 * etaSGNHT
    const sqrtstepsz = sqrt(2 * alSGNHT * etaSGNHT)
    const D = (n_users + n_items) * K
    for iter in 1:round_len
        @inbounds batch = trainset[rand_samp(sz_trainset, sz_batch, "w_replacement"), :]
        fill!(grad_sum_P, 0.0)
        fill!(grad_sum_Q, 0.0)
        uu, ii, rr = batch[:,1], batch[:,2], batch[:,3]
        error = (rr - mean_rate) - vec(sum(P[:,uu] .* Q[:,ii],1))
        grad_Pu = broadcast(*, error', Q[:,ii])
        grad_Qi = broadcast(*, error', P[:,uu])
        for ix = 1:sz_batch
            @inbounds grad_sum_P[:,uu[ix]] += grad_Pu[:,ix]
            @inbounds grad_sum_Q[:,ii[ix]] += grad_Qi[:,ix]
        end
        Prior_P = - broadcast(*, P, var_Pu)
        Prior_Q = - broadcast(*, Q, var_Qi)
        noiseP, noiseQ = randn(K, n_users), randn(K, n_items)
        @devec grad_P = etaSGNHT .* (ka .* grad_sum_P .+ Prior_P) .+ sqrtstepsz .* noiseP 
        @devec grad_Q = etaSGNHT .* (ka .* grad_sum_Q .+ Prior_Q) .+ sqrtstepsz .* noiseQ  
        @devec mom_P = mom_P .* (1 .- alSGNHT) .+ grad_P     
        @devec mom_Q = mom_Q .* (1 .- alSGNHT) .+ grad_Q 
        @devec P = P .+ mom_P
        @devec Q = Q .+ mom_Q
        #sumsqr_momP = sumsqr(mom_P)
        #sumsqr_momQ = sumsqr(mom_Q)
        #alSGNHT = alSGNHT + (sumsqr_momP + sumsqr_momQ) / D - etaSGNHT 
        alSGNHT = alSGNHT + (sum(sqr(mom_P)) + sum(sqr(mom_Q))) / D - etaSGNHT 
    end
    return P, Q, mom_P, mom_Q, alSGNHT
end


# Make this parallel 
function sumsqr(Mat::Array{Float64,2})
    K, N = size(Mat)
    a::Float64 = 0.0
    for i in 1:K
        for j in 1:N
            val::Float64 = Mat[i,j]
            a += val * val
        end
    end
    return a
end


function rmse_avg{T}(testset::Array{T,2}, iter::Int64, P::Array{Float64,2}, Q::Array{Float64,2}, 
                     avg_pred::Array{Float64,1}, avg_cnt::Int64, min_rate::Float64, 
                     max_rate::Float64, mean_rate::Float64, burnin::Int64)
    avg_rmse = 0.0
    rr = testset[:,3]
    # dot products
    pred = vec(sum(P[:,int(testset[:,1])] .* Q[:,int(testset[:,2])], 1)) + mean_rate
    # filter out of range predicts
    pred[pred .< min_rate] = min_rate
    pred[pred .> max_rate] = max_rate
    if iter > burnin
        avg_pred = (1 - 1/avg_cnt) * avg_pred + 1/avg_cnt * pred
        avg_rmse = sqrt(sum(sqr(rr - avg_pred))/size(testset,1))
    end
    # compute RMSE
    cur_rmse = sqrt(sum(sqr(rr - pred))/size(testset,1))
    return avg_rmse, cur_rmse, avg_pred
end


function get_str_id(algo, iter::Int64, sz_batch::Int64, burnin::Int64, round_len::Int64, best_rmse::Float64)
    now_str = replace(replace(string(now(Datetime.PST)), ":",""),"-","")[1:end-4]
    now_str = "$now_str""_np"string(nprocs()-1)"_K"string(K)"_stsz"@sprintf("%.7f",etaSGNHT)"_szbatch"string(sz_batch)"_roundlen"string(round_len)"_burnin"string(burnin)"_""$algo""_rmse"@sprintf("%.4f",best_rmse)"_t""$iter"
    now_str = replace(now_str,".","_")
    now_str
end
