include("./c_wrappers.jl")

function sample_PQ_gibbs_m1!{T}(model::Dict{ASCIIString,Any}, trainset::Array{T,2}, 
                                u_idx::Array{Array{Int64,1},1}, i_idx::Array{Array{Int64,1},1}, 
                                mean_rate::Float64, itv_gibbs::Int64)
    P::Array{Float64,2}    = model["P"]
    Q::Array{Float64,2}    = model["Q"]
    al::Float64            = model["prec_r"]
    mu_p::Array{Float64,1} = model["mu_p"]
    mu_q::Array{Float64,1} = model["mu_q"]
    La_p::Array{Float64,2} = model["La_p"]
    La_q::Array{Float64,2} = model["La_q"]
    n_users = size(P,2)
    n_items = size(Q,2)
    K = size(P,1)
    R = trainset[:,3] - mean_rate

    for gibbs = 1:itv_gibbs
        for i = 1:n_items
            @inbounds tr_i = trainset[i_idx[i],:]
            @inbounds pp = P[:,tr_i[:,1]]
            covar = inv((La_q + al * pp * pp'))
            @inbounds mean_q = covar * (al * pp * R[i_idx[i]] + La_q * mu_q)
            @inbounds Q[:,i] = chol(covar)' * randn(K) + mean_q
        end
        for u = 1:n_users
            @inbounds tr_u = trainset[u_idx[u],:]
            @inbounds qq = Q[:,tr_u[:,2]]
            covar = inv((La_p + al * qq * qq'))
            @inbounds mean_p = covar * (al * qq * R[u_idx[u]] + La_p * mu_p)
            @inbounds P[:,u] = chol(covar)' * randn(K) + mean_p
        end    
    end
    return nothing
end


#function update_P!{T}(trainset::Array{T,2}, P::Array{Float64,2}, Q::Array{Float64,2}, 
                       #u_idx::Array{Array{Int64,1},1}, R_AB::Array{Float64,1}, 
                       #al::Float64, la_P::Array{Float64,2}, K::Int64)
    #n_users = size(P,2)
    #for u = 1:n_users
        ##@inbounds u_ratings = trainset[u_idx[u],:]
        #@inbounds Qu = Q[:,trainset[u_idx[u],:][:,2]]
        #@inbounds covar = inv(la_P + al * Qu * Qu')
        #@inbounds mu = covar * (al * Qu * R_AB[u_idx[u]])
        #@inbounds P[:,u] = chol(covar)' * randn(K) + mu
    #end 
    #return nothing
#end

#function update_Q!{T}(trainset::Array{T,2}, P::Array{Float64,2}, Q::Array{Float64,2}, 
                       #i_idx::Array{Array{Int64,1},1}, R_AB::Array{Float64,1}, 
                       #al::Float64, la_Q::Array{Float64,2}, K::Int64)
    #n_items = size(Q,2)
    #for i = 1:n_items
        ##@inbounds i_ratings = trainset[i_idx[i],:]
        #@inbounds Pi = P[:,trainset[i_idx[i],:][:,1]]
        #@inbounds covar = inv(la_Q + al * Pi * Pi')
        #@inbounds mu = covar * (al * Pi * R_AB[i_idx[i]])
        #@inbounds Q[:,i] = chol(covar)' * randn(K) + mu
    #end
    #return nothing
#end


#function update_A!(A::Array{Float64,1}, XX::Array{Float64,1}, u_idx::Array{Array{Int64,1},1},
                    #al::Float64, la_A::Float64)
    #for u = 1:length(A)
        #@inbounds va = 1 / (la_A + al * length(u_idx[u]))
        #@inbounds mu = va * al * sum(XX[u_idx[u]])
        #@inbounds A[u] = mu + sqrt(va) * randn()
    #end
    #return nothing
#end


#function update_B!(B::Array{Float64,1}, XX::Array{Float64,1}, i_idx::Array{Array{Int64,1},1},
                   #al::Float64, la_B::Float64)
    #for i = 1:length(B)
        #@inbounds va = 1 / (la_B + al * length(i_idx[i]))
        #@inbounds mu = va * al * sum(XX[i_idx[i]])
        #@inbounds B[i] = mu + sqrt(va) * randn()
    #end
    #return nothing
#end


#function update_P2!{T}(trainset::Array{T,2}, P::Array{Float64,2}, Q::Array{Float64,2}, 
                       #u_idx::Array{Array{Int64,1},1}, R_AB::Array{Float64,1}, 
                       #al::Float64, la_P::Array{Float64,2}, K::Int64)
    #n_users = size(P,2)
    #Prec = Array(Float64,K,K)
    #mu = Array(Float64,K)

    #for u = 1:n_users
        ##@inbounds u_ratings = trainset[u_idx[u],:]
        #@inbounds Qu = Q[:,trainset[u_idx[u],:][:,2]]
        #@inbounds comp_Prec!(Prec, la_P, al, Qu, K, n_users)
        #@inbounds covar = inv(Prec)
        ##@inbounds covar = inv(la_P + al * Qu * Qu')
        #@inbounds mu = covar * (al * Qu * R_AB[u_idx[u]])
        #@inbounds P[:,u] = chol(covar)' * randn(K) + mu
    #end 
    #return nothing
#end


#function comp_Prec!(Prec, la_P, al, Qu, K, D)
    #for k1 = 1:K
        #for k2 = 1:K
            #a = 0.0
            #for i = 1:D
                #@inbounds a += Qu[k1,i] * Qu[k2,i]
            #end
            #@inbounds Prec[k1,k2] = la_P[k1,k2] + al * a
        #end
    #end
#end


