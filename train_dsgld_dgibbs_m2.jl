using Debug
using Devectorize
using Distributions
include("./utils/rng.jl")
include("./utils/utils.jl")  
include("./utils/mf_common.jl")
@everywhere include("../22_v0.06_dev/sample_hyper_gibbs_m2.jl")
@everywhere include("../22_v0.06_dev/sample_PQ_hyper_dsgld_dgibbs_m2.jl")

function train_dsgld_dgibbs_m2{T}(models::Array{Dict{ASCIIString,Any},1},
                                  preds::Array{Dict{ASCIIString,Any},1},
                                  para::Dict{ASCIIString,Any},
                                  trainset::Array{T,2}, testset::Array{T,2},
                                  localized_uid_s::Array{Array{Int64,1},1}, 
                                  localized_iid_s::Array{Array{Int64,1},1},
                                  n_overlap_row::Array{Int16,1},
                                  n_overlap_col::Array{Int16,1}
                                  )
    para["algo"] = "DS-DG-2"

    @assert rem(para["itvhyp"], para["S"]) == 0

    # unpack para
    n_chain::Int64  = para["C"]
    n_worker::Int64 = para["S"]
    n_group::Int64  = para["G"]
    mxiter::Int64   = para["mxiter"]
    n_users::Int64  = size(models[1]["P"],2)
    n_items::Int64  = size(models[1]["Q"],2)
    
    println(para)
    @printf("Running %s\n", para["algo"])
    log_dir = "00_results_"para["algo"]
    cur_logfile = get_str_id2(para)
    @printf("%s\n",cur_logfile)
    
    logs = Dict{ASCIIString,Any}()
    logs["iter"] = Int64[]
    logs["rmse"] = Float64[]
    logs["ts"]   = Float64[]

    # precomputation of some constants
    sz_testset = size(testset,1)
    min_rate   = float(minimum(trainset[:,3])) 
    max_rate   = float(maximum(trainset[:,3]))
    mean_rate  = float(mean(trainset[:,3]))

    # pre-allocate 
    avg_rmse = 0.0  
    best_rmse = 5.0
    avg_cur_rmse = 5.0
    round = 0
    ts = 0.0
    is_burnin = true
    
    p_results = cell(n_worker)
    
    #worker_group = get_worker_group(n_worker, n_group)
    #models, preds = clone_models(models, preds, n_chain)
    
    chain_order = update_chain_order(n_worker, n_chain)
    
    while round < mxiter
        
        tic()
        ###########################################

        round += 1

        chain_order = update_chain_order(chain_order)
        p_param     = pack_param_mcmc(round, chain_order, models, localized_uid_s, localized_iid_s, para, is_burnin, mean_rate)
        
        # perform parallel updates
        #p_results = pmap(p_update_a_round, p_param)
        i = 1
        nextidx() = (idx=i; i+=1; idx)
        @sync begin
            for p in procs()[2:end]
                @async begin 
                    idx = nextidx()
                    p_results[idx] = remotecall_fetch(p, sample_PQ_hyper_dsgld_dgibbs_m2, p_param[idx])
                end
            end
        end

        # update models from received packets
        for s in 1:n_worker
            c = chain_order[s]
            if c != -1
                models[c]["P"][:,localized_uid_s[s]] = p_results[s][1]
                models[c]["Q"][:,localized_iid_s[s]] = p_results[s][2]
                models[c]["A"][localized_uid_s[s]]   = p_results[s][3] 
                models[c]["B"][localized_iid_s[s]]   = p_results[s][4] 
            end
        end

        # update hyper-parameters
        if !is_burnin && mod(round, para["itvhyp"]) == 0
            @time update_hyper_gibbs_m2!(models, para["C"])
        end

        ###########################################
        ts += toq()
        
        ################################
        # compute test RMSE
        if mod(round, para["itvtst"]) == 0
            tic()
            params = param_p_rmse_avg(models, preds, n_worker, round, min_rate, max_rate, mean_rate, is_burnin)
            p_results = pmap(p_rmse_avg, params)

            for m in 1:length(models)
                preds[m]["avg_rmse"] = p_results[m][1]
                preds[m]["cur_rmse"] = p_results[m][2]
                preds[m]["avg_pred"] = p_results[m][3]
                preds[m]["avg_cnts"] = p_results[m][4]
            end
            
            # avg rmse over chains
            if is_burnin == false
                avg_pred = sum([pred["avg_pred"] for pred in preds]) / length(preds)
                error = testset[:,3] - avg_pred
                avg_rmse = sqrt(sum(error .* error) / size(testset,1))
                push!(logs["rmse"], avg_rmse)
            else
                push!(logs["rmse"], preds[1]["cur_rmse"])
            end
            
            @printf("pr_P:%.0f, pr_Q:%.0f, pr_A:%.1f, pr_B:%.1f, pr_r:%.2f\n", 
                mean(models[c]["prec_P"]), mean(models[c]["prec_Q"]), models[c]["prec_A"], models[c]["prec_B"], models[c]["prec_r"]) 
            best_rmse = print_results(avg_rmse, best_rmse, preds, round, ts, is_burnin)
            @printf " [%.2fs]\n" toq()

            push!(logs["iter"], round)
            push!(logs["ts"], ts)
            para["t"] = round; para["ts"] = ts; para["best"]= best_rmse
            logs["para"] = para
            
            # udpate is_burnin
            avg_cur_rmse = mean([preds[c]["cur_rmse"] for c in length(models)])
            if is_burnin == true
                is_burnin = (avg_cur_rmse > para["bthresh"])
                if is_burnin == false
                    @printf("burnin finished\n")
                end
            end
        end
        
        ################################
        # save to file
        if mod(round, para["itvsv"]) == 0
            @printf("saving logs ... ")
            para["t"] = round
            para["ts"] = ts
            cur_logfile = save_logs4(logs, log_dir, cur_logfile)
            @printf("done.\n")
        end
    end
    return models
end


function update_hyper_gibbs_m2!(models::Array{Dict{ASCIIString,Any},1}, C::Int64)
    for c = 1:C
        sample_hyper_gibbs_m2!(models[c])
    end
end

#function compute_sse{T}(uu::Array{T,1}, ii::Array{T,1}, rr::Array{T,1}, 
                        #P::Array{Float64,2}, Q::Array{Float64,2}, 
                        #A::Array{Float64,1}, B::Array{Float64,1})
    ##uu, ii = dataset[:,1], dataset[:,2]
    ##pred = vec(sum(model["P"][:,uu] .* model["Q"][:,ii], 1)) + model["A"][uu] + model["B"][ii]
    #sse = 0.0
    #for n=1:length(rr)
        #sse += sqr(rr[n] - ((P[:,uu[n]]' * Q[:,ii[n]])[1] + A[uu[n]] + B[ii[n]]))
    #end
    #return sse
#end

#function sample_hyper!(prec_P::Array{Float64,1}, prec_Q::Array{Float64,1},
                       #prec_A::Float64, precB::Float64,
                       #P::Array{Float64,2}, Q::Array{Float64,2}, 
                       #A::Array{Float64,1}, B::Array{Float64,1}, 
                       #al0::Float64, bt0::Float64)
#function sample_hyper!(model::Dict{ASCIIString, Any})

    #P = model["P"]
    #Q = model["Q"]
    #A = model["A"]
    #B = model["B"]

    #K, n_users = size(P)
    #n_items = size(Q,2)

    ##sse_P = sum(sqr(broadcast(-, P, mean(P,2))),2)
    ##sse_Q = sum(sqr(broadcast(-, Q, mean(Q,2))),2) 
    #sse_P = sum(P .* P, 2)
    #sse_Q = sum(Q .* Q, 2)
    #beta_P = model["bt0"] + 0.5 * sse_P
    #beta_Q = model["bt0"] + 0.5 * sse_Q
    #for k in 1:K
        #@inbounds model["prec_P"][k] = 1/rand(InverseGamma(model["al0"] + 0.5 * n_users, beta_P[k]))
        #@inbounds model["prec_Q"][k] = 1/rand(InverseGamma(model["al0"] + 0.5 * n_items, beta_Q[k]))
    #end
    
    ## sample bias precisions
    ##sse_A = sum(sqr(A - mean(A)))
    ##sse_B = sum(sqr(B - mean(B)))
    #sse_A = sum(A .* A)
    #sse_B = sum(B .* B)
    #model["prec_A"]  = 1/rand(InverseGamma(model["al0"] + 0.5 * n_users, model["bt0"] + 0.5 * sse_A))
    #model["prec_B"]  = 1/rand(InverseGamma(model["al0"] + 0.5 * n_items, model["bt0"] + 0.5 * sse_B))
    
    #return nothing
#end


#function clone_models(models::Array{Dict{ASCIIString,Any},1}, 
                      #preds::Array{Dict{ASCIIString,Any},1},
                      #n_chain::Int64)
    #n_group = length(models)
    #n_chain_to_add = n_chain - n_group
    #m = 1
    #while n_chain_to_add > 0
        #new_model = deepcopy(models[m])
        #new_pred  = deepcopy(preds[m])
        #push!(models, new_model)
        #push!(preds,  new_pred)
        #m = mod(m+1, n_group); if m == 0; m = n_group; end
        #n_chain_to_add -= 1
    #end
    #return models, preds
#end



#function get_worker_group(n_worker::Int64, n_group::Int64)
    #worker_group = [zeros(Int64,0) for g in 1:n_group]
    #wid = 1
    #while wid <= n_worker
        #gid = 1
        #while gid <= n_group
            #push!(worker_group[gid], wid)
            #wid += 1
            #gid += 1
        #end
    #end
    #return worker_group
#end



function print_results(avg_rmse::Float64, best_rmse::Float64, 
                       preds::Array{Dict{ASCIIString,Any},1}, 
                       round::Int64, ts::Float64, is_burnin::Bool)
    # avg over all parallel chains
    str = @sprintf("r:%d, ", round)
    @printf("%-8s", str)
    str = @sprintf("%.4f (%.2fs) ", avg_rmse, ts)
    @printf("%-20s", str)
    @printf("[ ")
    for pred in preds; @printf("%.4f ", pred["cur_rmse"]); end
    @printf("] ")
    if !is_burnin && avg_rmse < best_rmse
        best_rmse = avg_rmse
        @printf("(**)")
    end
    return best_rmse
end



function param_p_rmse_avg(
                          models::Array{Dict{ASCIIString, Any},1}, 
                          preds::Array{Dict{ASCIIString, Any},1},
                          n_worker::Int64,
                          round::Int64,
                          min_rate::Float64, max_rate::Float64, 
                          mean_rate::Float64, is_burnin::Bool
                          )
    param = cell(n_worker)
    for s in 1:length(models)
        param[s] = (1, round, models[s], preds[s]["avg_pred"], preds[s]["avg_cnts"], min_rate, max_rate, mean_rate, is_burnin)
    end
    s = length(models) + 1
    while s <= n_worker
        param[s] = -1
        s += 1
    end
    return param
end



function pack_param_optm(round::Int64,
                         worker_group::Array{Array{Int64,1},1},
                         models::Array{Dict{ASCIIString,Any},1},
                         localized_uid_s::Array{Array{Int64,1},1}, 
                         localized_iid_s::Array{Array{Int64,1},1}, 
                         g_para::Dict{ASCIIString,Any})
    n_worker = sum([length(group) for group in worker_group])
    param = cell(n_worker)
    round <= g_para["burnin"] ? burnin = true : burnin = false;
    for g in 1:length(worker_group)
        for s in worker_group[g]
            param[s] = (1,
                        burnin, 
                        models[g]["P"][:,localized_uid_s[s]], 
                        models[g]["Q"][:,localized_iid_s[s]],
                        models[g]["A"][localized_uid_s[s]], 
                        models[g]["B"][localized_iid_s[s]],
                        models[g]["prec_P"],
                        models[g]["prec_Q"],
                        models[g]["prec_A"],
                        models[g]["prec_B"],
                        models[g]["prec_r"],
                        para["eps"],
                        para["m"],
                        para["rndlen"],
                        para["itvhyp"]
                       )
        end
    end
    return param
end



function pack_param_mcmc(round::Int64,
                         chain_order::Array{Int64,1}, 
                         models::Array{Dict{ASCIIString,Any},1},
                         localized_uid_s::Array{Array{Int64,1},1}, 
                         localized_iid_s::Array{Array{Int64,1},1}, 
                         g_para::Dict{ASCIIString,Any},
                         is_burnin::Bool, mean_rate::Float64)
    n_worker = length(chain_order)
    param = cell(n_worker)
    for s in 1:n_worker
        c = chain_order[s]
        if c == -1  # there is no chain to assign to the worker s
            param[s] = c
        else
            param[s] = (
                        c,
                        is_burnin,
                        models[c]["P"][:,localized_uid_s[s]], 
                        models[c]["Q"][:,localized_iid_s[s]],
                        models[c]["A"][localized_uid_s[s]], 
                        models[c]["B"][localized_iid_s[s]],
                        models[c]["prec_P"],
                        models[c]["prec_Q"],
                        models[c]["prec_A"],
                        models[c]["prec_B"],
                        models[c]["prec_r"],
                        para["eps"],
                        para["m"],
                        para["rndlen"],
                        mean_rate,
                        para["itvhyp"]
                       )
        end
    end
    return param
end



function update_worker_group(worker_group::Array{Array{Int64,1},1})
    n_group = length(worker_group)
    gids = [1:n_group]
    unshift!(gids, pop!(gids))
    return worker_group[gids]
end



function update_chain_order(n_worker::Int64, n_chain::Int64)
    # chain_order[wid] = cid, '-1' means no chain assigned
    # e.g., [3, 2, 5, -1, -1, 1, -1, 4] => 8 workers running 4 chains
    chain_order = fill!(Array(Int64, n_worker), -1) 
    chain_order[randperm(n_worker)[1:n_chain]] = 1:n_chain
    return chain_order
end



function update_chain_order(chain_order::Array{Int64,1})
    # Shifts S/2 workers to the right. Scheduling algorithm is tightly
    # related to the data partitioning and augmenting. Due to augmentation,
    # duplicated data points are supposed to be seen at several workers. 
    # And this would affect mixing. Assuming that the augmented sets of R(n) 
    # are from R(n+1), R(n+2), ..., R(n-1), and so on, the best mixing would
    # be obtained by shifing S/2 workers so that duplicated ratings are seen
    # neither too soon nor too late.
    #for i in 1:div(length(chain_order),2) 
        #unshift!(chain_order, pop!(chain_order))
    #end
    unshift!(chain_order, pop!(chain_order))
    return chain_order
end





function pcopy_inv_pr_pick(inv_pr_pick_user::Array{Float64,1}, 
                           inv_pr_pick_item::Array{Float64,1},
                           localized_uid_s::Array{Array{Int64,1},1}, 
                           localized_iid_s::Array{Array{Int64,1},1})
    @printf("pcopy inv_pr_pick started ... ")
    for (s,p) in enumerate(procs()[2:end])
        remotecall_fetch(p, (arg)->(global _inv_pr_pick_user = arg), inv_pr_pick_user[localized_uid_s[s]])
        remotecall_fetch(p, (arg)->(global _inv_pr_pick_item = arg), inv_pr_pick_item[localized_iid_s[s]])
    end
    @printf("done\n")
end



function compute_bias_corrector(N::Int64, sz_batch::Int64, 
                                n_user::Int64, n_item::Int64,
                                localized_uid_s::Array{Array{Int64,1},1},
                                localized_iid_s::Array{Array{Int64,1},1},
                                Us::Array{Array{Int64,1},1}, 
                                Is::Array{Array{Int64,1},1},
                                n_elem_row_s::Array{Array{Int64,1},1}, 
                                n_elem_col_s::Array{Array{Int64,1},1})
    n_worker = length(Us)
    Pr_us = zeros(Float64, n_user, n_worker)
    Pr_is = zeros(Float64, n_item, n_worker)
    Ns = Array(Int64, n_worker)
    for s in 1:n_worker
        Ns[s] = sum(n_elem_row_s[s])
    end
    for s in 1:n_worker
        for (ix,uid) in enumerate(localized_uid_s[s])
            Pr_us[uid,s] = 1 - (1 - n_elem_row_s[s][ix] / Ns[s]) ^ sz_batch
        end 
        for (ix,iid) in enumerate(localized_iid_s[s])
            Pr_is[iid,s] = 1 - (1 - n_elem_col_s[s][ix] / Ns[s]) ^ sz_batch
        end
    end
    Pr_u = vec(sum(Pr_us,2)) ./ n_worker
    Pr_i = vec(sum(Pr_is,2)) ./ n_worker
    inv_pr_pick_user = 1 ./ Pr_u
    inv_pr_pick_item = 1 ./ Pr_i
    return inv_pr_pick_user, inv_pr_pick_item
end

function compute_bias_corrector(N::Int64, sz_batch::Int64, n_elem_row::Array{Int64,1}, n_elem_col::Array{Int64,1})
    inv_pr_pick_user = 1 ./ (1 - (1 - float(n_elem_row/N)) .^ sz_batch)
    inv_pr_pick_item = 1 ./ (1 - (1 - float(n_elem_col/N)) .^ sz_batch)
    return inv_pr_pick_user, inv_pr_pick_item
end

