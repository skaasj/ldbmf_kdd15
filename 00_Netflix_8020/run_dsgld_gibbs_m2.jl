include("../utils/types.jl")
include("../utils/mf_common.jl")
include("../utils/utils.jl")
include("../utils/master_utils.jl")
include("../train_dsgld_gibbs_m2.jl")

srand(1234)
trainset = float64(trainset)
testset = float64(testset)
gc()

# init params 
para = Dict{ASCIIString, Any}()
para["eps0"]    = 8.5e-6
para["tau"]     = 10000
para["ka"]      = 0.51
para["S"]       = 9
para["C"]       = 9
para["K"]       = 100
#para["eps"]     = 9e-6
para["m"]       = 50000
para["aug"]     = 0.0     # aug is in a range of [0,1] where 0 = N/S and 1 = N.
para["rndlen"]  = 50
para["bthresh"] = 0.86
para["rP"]      = 2.0 
para["rQ"]      = 2.0  
para["rA"]      = 2.0 
para["rB"]      = 2.0 
para["bt0"]     = 700.0
para["iscale"]  = 0.01
para["mxiter"]  = 999999
para["itvsv"]   = 100
para["itvtst"]  = 10
para["itvhyp"]  = 300  # sample hyper-params per 'itvhyp' rounds

f_stepsz(t)     = para["eps0"] .* ((1 + t./para["tau"]).^(-para["ka"]))

@time worker_ids = launch_workers(para["S"]);

@everywhere include("../utils/worker_utils.jl")
@everywhere include("../sample_PQ_dsgld_m2.jl")
@everywhere include("../utils/rng.jl")

srand(1)
println("Netflix 8020 - D-SGLD")

pre_init_workers(size(trainset,1), para)

##################################################
# Preprare parameter server to run
@time blk_rng, blk_mbr = partition_matrix(trainset, n_users, n_items, para["S"], 1, para["S"]); # 256s
@time Xs_idx, Ns, aug_count = augment_blocks(blk_mbr, size(trainset,1), para["S"], para["aug"]); # 5s
@time pcopy_shards(trainset, Xs_idx, aug_count); # 54s

sz_train   = size(trainset,1)
min_rate   = float(minimum(trainset[:,3])) 
max_rate   = float(maximum(trainset[:,3]))
mean_rate  = float(mean(trainset[:,3]))
#trainset = 0
#gc()

@time pcopy_testset(testset, para["C"]); # 11s
test_r = deepcopy(testset[:,3]);

## localize Xs
@time p_result = pmap(p_localize_Xs, [[] for s in 1:para["S"]]); # 224s
map_uid_g2l_s = [p_result[s][1] for s in 1:para["S"]];
map_iid_g2l_s = [p_result[s][2] for s in 1:para["S"]];
n_elem_row_s  = [p_result[s][5]::Array{Int64,1} for s in 1:para["S"]];
n_elem_col_s  = [p_result[s][6]::Array{Int64,1} for s in 1:para["S"]];
@time localized_uid_s, Us = get_convert_orders(map_uid_g2l_s);
@time localized_iid_s, Is = get_convert_orders(map_iid_g2l_s);

#@time n_overlap_row, n_overlap_col = compute_n_overlap(n_users, n_items, Us, Is);
@time inv_pr_pick_user, inv_pr_pick_item = compute_bias_corrector(sz_train, para["m"], n_users, n_items, localized_uid_s, localized_iid_s, Us, Is, n_elem_row_s, n_elem_col_s);
@time pcopy_inv_pr_pick(inv_pr_pick_user, inv_pr_pick_item, localized_uid_s, localized_iid_s);

#map_uid_g2l_s = 0
#map_iid_g2l_s = 0
#n_elem_row_s = 0
#n_elem_col_s = 0
#gc()

function post_init_workers(n_worker::Int64, Ns::Array{Int64,1}, N::Int64)
    Ns_scale = Array(Float64, n_worker)
    for (s,p) in enumerate(procs()[2:end])
        Ns_scale[s] = Ns[s] * n_worker
        remotecall_fetch(p, (arg)->(global _Ns_scale = arg::Float64), Ns[s] * n_worker / N)
    end
end

post_init_workers(para["S"], Ns, sz_train);


###################################################
# init models
models0 = Array(Dict{ASCIIString, Any}, para["C"])
for c in 1:length(models0)
    model = Dict{ASCIIString, Any}()
    model["P"]       = rand(para["K"], n_users) * para["iscale"]
    model["Q"]       = rand(para["K"], n_items) * para["iscale"]
    model["A"]       = randn(n_users) * para["iscale"]
    model["B"]       = randn(n_items) * para["iscale"]
    model["al0"]     = 1.0
    model["bt0"]     = para["bt0"]
    model["prec_P"]  = ones(para["K"]) * para["rP"]
    model["prec_Q"]  = ones(para["K"]) * para["rQ"]
    model["prec_A"]  = para["rA"]
    model["prec_B"]  = para["rB"]
    model["prec_r"]  = 2.0  
    models0[c] = model
end

preds0 = Array(Dict{ASCIIString, Any}, para["C"])
for c in 1:length(preds0)
    pred = Dict{ASCIIString, Any}()
    pred["avg_rmse"] = 10.0
    pred["cur_rmse"] = 10.0
    pred["avg_pred"] = zeros(Float64, size(testset,1))
    pred["avg_cnts"] = 0
    preds0[c] = pred
end
###################################################
# Start learning
models = deepcopy(models0); preds  = deepcopy(preds0);
train_dsgld_gibbs_m2(models0, preds0, para, test_r, localized_uid_s, localized_iid_s, min_rate, max_rate, mean_rate, f_stepsz);
#train_dsgld_gibbs_m2(models, preds, para, trainset, testset, localized_uid_s, localized_iid_s);
#train_dsgld_gibbs_m2(models0, preds0, para, testset, localized_uid_s, localized_iid_s, min_rate, max_rate, mean_rate);

