include("../utils/types.jl")
include("../utils/data_loader.jl")
include("../utils/mf_common.jl")
include("../utils/utils.jl")
include("../utils/master_utils.jl")

trainset = float64(trainset)
testset = float64(testset)
gc()

n_worker = 9

EC2_deployed = contains(gethostname(), "ip-")
if EC2_deployed # running on EC2
    @printf("Running on Amazon EC2\n")
    include("../../../run/config.jl")
    include("../../../run/addprocs_headnode.jl")
    @everywhere cd("/home/ubuntu/00_dbmf/src/005_ML_10M/")
else
    @printf("Running on localhost\n")
    @time worker_ids = launch_workers(n_worker);
end

include("../train_ddsgld_gibbs_m2.jl")
@everywhere include("../sample_PQ_ddsgld_m2.jl")
@everywhere include("../utils/worker_utils.jl")
@everywhere include("../utils/rng.jl")

# init params 
para = Dict{ASCIIString, Any}()
para["eps0"]    = 3e-6
para["tau"]     = 500
para["ka"]      = 0.51
para["S"]       = n_worker              
para["K"]       = 30
para["C"]       = int(sqrt(n_worker))
para["G"]       = 1           
para["rP"]      = 2.0 
para["rQ"]      = 2.0  
para["rA"]      = 2.0 
para["rB"]      = 2.0  
para["bt0"]     = 300.0
para["nrb"]     = 3      # number of row blocks
para["ncb"]     = 3      # number of column blocks
para["m"]       = 50000
para["aug"]     = 0.0      
para["rndlen"]  = 50
para["bthresh"] = 0.85
para["submean"] = true
para["iscale"]  = 0.01
para["mxiter"]  = 999999
para["itvsv"]   = 100
para["itvtst"]  = 10
para["itvhyp"]  = 50     # sample hyper-params per 'itvhyp' rounds

# stepsize function
f_stepsz(t)     = para["eps0"] .* ((1 + t./para["tau"]).^(-para["ka"]))

sz_trainset = size(trainset,1)
sz_testset  = size(testset,1)
min_rate = float(minimum(trainset[:,3])) 
max_rate = float(maximum(trainset[:,3]))
para["submean"] ? mean_rate = float(mean(trainset[:,3])) : mean_rate = 0.0
#test_rr = deepcopy(testset[:,3])

srand(1)
println("Netflix 8020 - DSGLD-3x3")
n_block = para["nrb"] * para["ncb"]

@time blk_rng, blk_mbr = partition_matrix(trainset, n_users, n_items, para["nrb"], para["ncb"], n_worker);
@time wkr2blk_map = assign_blocks_to_workers(para["nrb"], para["ncb"], n_worker);
@time pcopy_blocks(trainset, blk_mbr, wkr2blk_map, n_worker);

## free memory
trainset = 0
blk_mbr = 0
gc()

@time pcopy_testset(testset, para["C"])
#testset = 0
#gc()

if !isdefined(:blk_info)
    @time p_result = pmap(p_localize_idx, [[] for s in 1:n_worker]);
    @time blk_info = get_blk_info(p_result, wkr2blk_map);
else
    @printf("Skip p_localize_idx. Already localized.\n")
end

@time inv_pr_pick_user, inv_pr_pick_item = comp_inv_pr_pick(blk_info, n_users, n_items, para["m"]);
@time pcopy_inv_pr_pick(inv_pr_pick_user, inv_pr_pick_item, blk_info, wkr2blk_map);

#inv_pr_pick_user = 0
#inv_pr_pick_item = 0
#gc()

@time post_init_ddsgld(blk_info, sz_trainset);

###################################################
# init models
models0 = Array(Dict{ASCIIString, Any}, para["C"])
for c in 1:length(models0)
    model = Dict{ASCIIString, Any}()
    model["P"]       = rand(para["K"], n_users) * para["iscale"]
    model["Q"]       = rand(para["K"], n_items) * para["iscale"]
    model["A"]       = randn(n_users) * para["iscale"]
    model["B"]       = randn(n_items) * para["iscale"]
    model["al0"]     = 1.0
    model["bt0"]     = para["bt0"]
    model["prec_P"]  = ones(para["K"]) * para["rP"]
    model["prec_Q"]  = ones(para["K"]) * para["rQ"]
    model["prec_A"]  = para["rA"]
    model["prec_B"]  = para["rB"]
    model["prec_r"]  = 2.0  
    models0[c] = model
end

preds0 = Array(Dict{ASCIIString, Any}, para["C"])
for c in 1:length(preds0)
    pred = Dict{ASCIIString, Any}()
    pred["avg_rmse"] = 10.0
    pred["cur_rmse"] = 10.0
    pred["avg_pred"] = zeros(Float64, sz_testset)
    pred["avg_cnts"] = 0
    preds0[c] = pred
end

# 4x4, 16-wkr-4-chain
#blk_groups = Array[[1,6,11,16] [2,7,12,13] [3,8,9,14] [4,5,10,15]]
# 3x3, 9-wkr-3-chain
#blk_groups = Array[[1,5,9] [2,6,7] [3,4,8]]
blk_groups = gen_ortho_blk_groups(para["C"])

###################################################
## Start learning
#models0 = train_ddsgld_gibbs_m2(models0, preds0, para, min_rate, max_rate, mean_rate, test_rr, blk_info, blk_groups);
models = deepcopy(models0); preds  = deepcopy(preds0);
train_ddsgld_gibbs_m2(models, preds, para, min_rate, max_rate, mean_rate, testset, blk_info, blk_groups, f_stepsz);

