include("../utils/types.jl")

# Initialize parmeter
srand(1)
println("Netflix8020 BPMF")

para = Dict{ASCIIString, Any}()
para["K"]       = 30
#para["rP"]      = 2.0 
#para["rQ"]      = 2.0  
#para["rA"]      = 2.0 
#para["rB"]      = 2.0 
#para["bt0"]     = 1.0
#para["bthresh"] = 1.08
para["mxiter"]  = 9999999
para["mxsec"]   = 9999999
#para["iscale"]  = 0.0001
para["itvsv"]   = 1
para["itvtst"]  = 1


# model
#model0 = Dict{ASCIIString,Any}()
#model0["P"] = rand(para["K"], n_users) * para["iscale"]
#model0["Q"] = rand(para["K"], n_items) * para["iscale"]
#model0["A"] = randn(n_users) * 0.01
#model0["B"] = randn(n_items) * 0.01
#model0["al0"] = 1.0
#model0["bt0"] = para["bt0"]   # important to supress inv_pr_pick_user
#model0["prec_r"] = 2.0
#model0["prec_P"] = ones(para["K"]) * para["rP"]
#model0["prec_Q"] = ones(para["K"]) * para["rQ"]
#model0["prec_A"] = para["rA"]
#model0["prec_B"] = para["rB"]

#user_index = load("/scratch/DATA/YahooMusic/141228_user_index.jld")["user_index"]
#item_index = load("/scratch/DATA/YahooMusic/141228_item_index.jld")["item_index"]
include("../utils/data_loader.jl")
user_index, item_index = build_user_item_index_map(trainset, n_users, n_items)


include("../train_gibbs_gibbs_m2.jl");
model = load("/scratch/DATA/netflix/8020set_m2_map_k30.jd")["model"];
model["bt0"] = 1.0
para["itv_gibbs"] = 1
model = train_gibbs_gibbs_m2(trainset, testset, model, para, user_index, item_index);

