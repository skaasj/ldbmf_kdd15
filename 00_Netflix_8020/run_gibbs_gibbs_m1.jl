include("../utils/types.jl")
include("../train_gibbs_gibbs_m1.jl")

# Initialize parmeter
srand(1)
println("Netflix 8020")

## params 
para = Dict{ASCIIString, Any}()
para["K"] = 30
para["itvtst"] = 1
para["itvsv"] = 1
para["mxiter"] = Inf
para["mxsec"] = 9990000


@time model = load("./map_net8020_k30_m1.jd")["model"];
#@time model = load("./map_net8020_k60_m1.jl")["model"];
#@time model = load("./map_net8020_k100_m1.jl")["model"];

include("../utils/data_loader.jl")
@time user_index, item_index = build_user_item_index_map(trainset, n_users, n_items);

## run BPMF
para["mxiter"] = 999999
para["bthresh"] = 0.95
para["itv_gibbs"] = 1
train_gibbs_gibbs_m1(trainset, testset, model, para, user_index, item_index);

