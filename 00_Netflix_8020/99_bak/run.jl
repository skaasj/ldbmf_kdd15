@everywhere include("types.jl")
include("data.jl")

srand(1)

# Load dataset 
#trainset, testset, n_users, n_items, N_elem_row, N_elem_col = load_movielens_10M()

use_exiting_indexing = true
if use_exiting_indexing    
    using HDF5, JLD
    @printf("Loading indexes ... ")
    #ret = load("00_data/ml_1m_idx.jld")
    ret = load("00_data/ml_10m_idx.jld")
    user_idx = ret["user_idx"]
    item_idx = ret["item_idx"]
    @printf("done\n")
else
    @printf("Indexing ... ")
    user_idx, item_idx = indexing2(trainset, n_users, n_items)
    @printf("done\n")
end


# Initialize parmeter
K = 30 # latent feature dimension
#stepsz = 0.005 # for ml-1M
stepsz = 5*1e-5
lambda = 150.0
sz_batch = 100000


use_exiting_map = false
if !use_exiting_map
    # Random initial state
    P0 = randn(K, n_users)
    Q0 = randn(K, n_items)
    @printf("Start PMF\n")
    P = copy(P0)
    Q = copy(Q0)
    include("train_pmf.jl")
    #stepsz = 2 * 1e-4 # current best setting
    logs, P, Q = train_pmf(trainset, testset, n_users, n_items, P, Q, stepsz, lambda, sz_batch, 
            minimum(trainset[:,3]), maximum(trainset[:,3]), 50, int(1e+9), 10, int(1e+9), 0.7);
    @printf("PMF finished\n")
else
    using HDF5, JLD
    @printf("Loading existing MAP ... ")
    ret = load("00_data/map_pmf_K10_lam3_stsz6e_4.jld")
    P, Q = ret["P"], ret["Q"]
    @printf("done\n")
end


## Start BPMF
#include("train_bpmf.jl")
#logs, Pb, Qb = train_bpmf(trainset, testset, user_idx, item_idx, n_users, n_items, P, Q, minimum(trainset[:,3]), maximum(trainset[:,3]), 500, int(1e+9), 1, int(1e+9));
