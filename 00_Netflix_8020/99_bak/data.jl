using ProgressMeter
using Debug
    
function load_movielens_dataset2(ratings_filepath, users_filepath, items_filepath, n_users, n_items)
    ratings = (Rating)[]
    #ratings = zeros(Float64,0,3)
    #ratings = Array((Int,Int,Float64),0)
    item_id_to_index = Dict{String, Int64}()
    user_id_to_index = Dict{String, Int64}()
    n_elem_row = zeros(Int64, n_users)
    n_elem_col = zeros(Int64, n_items)
    ratings_count = int(split(readall(`wc -l $(ratings_filepath)`), " ")[1])
    progress = Progress(ratings_count, 1, "Loading ratings ")
    open(ratings_filepath, "r") do ratings_file
       for line in eachline(ratings_file)
           user_id, item_id, rating, timestamp = split(line, "::")
           # item = original_id_to_movie[item_id]
           user_index = get(user_id_to_index, user_id, 0)
           if user_index == 0   # unseen user
                   user_index = user_id_to_index[user_id] = length(user_id_to_index) + 1
           end
           item_index = get(item_id_to_index, item_id, 0)
           if item_index == 0   # unseen item
                   item_index = item_id_to_index[item_id] = length(item_id_to_index) + 1
           end
           #ratings = vcat(ratings, (int(user_index), int(item_index), float(rating)))
           ratings = push!(ratings, Rating(user_index, item_index, float(rating)))
           n_elem_row[user_index] += 1
           n_elem_col[item_index] += 1
           next!(progress)
        end
    end
    @assert n_users == length(user_id_to_index)
    @assert n_items == length(item_id_to_index)
    
    # generate Features
    #features_item = gen_item_features(items_filepath, item_id_to_index)
    #features_user = gen_user_features(users_filepath, user_id_to_index)
    #features = Features(features_item, features_user)
    train_set, test_set = split_ratings2(ratings, 0.10)
    # rr = RatingSet(train_set, test_set, user_id_to_index, item_id_to_index)
    train_set, test_set, n_users, n_items, n_elem_row, n_elem_col
end

function load_movielens_dataset3(ratings_filepath)
    #ratings = Array((Int,Int,Float64),0)
    item_id_to_index = Dict{String, Int64}()
    user_id_to_index = Dict{String, Int64}()
    #n_elem_row = zeros(Int64, n_users)
    #n_elem_col = zeros(Int64, n_items)
    n_elem_row = Int64[]
    n_elem_col = Int64[]
    ratings_count = int(split(readall(`wc -l $(ratings_filepath)`), " ")[1])
    ratings = zeros(Float64,ratings_count,3)
    progress = Progress(ratings_count, 1, "Loading ratings ")
    line_num = 0
    open(ratings_filepath, "r") do ratings_file
        for line in eachline(ratings_file)
            line_num += 1
            user_id, item_id, rating, timestamp = split(line, "::")
            # item = original_id_to_movie[item_id]
            user_index = get(user_id_to_index, user_id, 0)
            if user_index == 0   # unseen user
                user_index = user_id_to_index[user_id] = length(user_id_to_index) + 1
                push!(n_elem_row, 0)
            end
            item_index = get(item_id_to_index, item_id, 0)
            if item_index == 0   # unseen item
                item_index = item_id_to_index[item_id] = length(item_id_to_index) + 1
                push!(n_elem_col, 0)
            end
            ratings[line_num,:] = [float(user_index), float(item_index), float(rating)]'
            #ratings = vcat(ratings, [float(user_index), float(item_index), float(rating)]')
            #ratings = push!(ratings, Rating(user_index, item_index, float(rating)))
            n_elem_row[user_index] += 1
            n_elem_col[item_index] += 1
            next!(progress)
        end
    end
    #@assert n_users == length(user_id_to_index)
    #@assert n_items == length(item_id_to_index)
    n_users = length(user_id_to_index)
    n_items = length(item_id_to_index)
    train_set, test_set = split_ratings3(ratings, 0.10)
    train_set, test_set, n_users, n_items, n_elem_row, n_elem_col
end

function make_dict_list(trainset::Array{Float64,2}, n_users, n_items)
    sz_trainset = size(trainset)[1]
    user2items = Dict{Int64, Array{Int64,1}}()
    item2users = Dict{Int64, Array{Int64,1}}()
    for ix = 1:sz_trainset
        user, item, rate = trainset[ix,:]
        user in keys(user2items) ? push!(user2items[user], item) : user2items[user] = Int64[]
        item in keys(item2users) ? push!(item2users[item], user) : item2users[item] = Int64[]
    end
    @assert length(user2items) == n_users
    @assert lenght(item2users) == n_items
    return user2items, item2users
end


## locate users and items in training set
#function indexing(trainset::Float2D, n_users::Int64, n_items::Int64)
    #users_at_trainset = Array(Array{Int64,1},0)
    #items_at_trainset = Array(Array{Int64,1},0)
    #progress = Progress(n_users + n_items, 1, "Indexing ...")
    #for user in 1:n_users
        #push!(users_at_trainset, find(trainset[:,1] .== user))
        #next!(progress)
    #end
    #for item in 1:n_items
        #push!(items_at_trainset, find(trainset[:,2] .== item))
        #next!(progress)
    #end
    #return users_at_trainset, items_at_trainset
#end


function indexing(trainset::Array{Float64,2}, n_users::Int64, n_items::Int64)
    sz_trainset = size(trainset)[1]
    users_at_trainset = [Int64[] for user in 1:n_users]
    items_at_trainset = [Int64[] for item in 1:n_items]
    progress = Progress(sz_trainset, 1, "Indexing ...")
    for ix in 1:sz_trainset
        u::Int64, i::Int64, r::Float64 = trainset[ix,:]
        push!(users_at_trainset[u], ix)
        push!(items_at_trainset[i], ix)
        next!(progress)
    end
    return users_at_trainset, items_at_trainset
end


function load_movielens_dataset(ratings_filepath, users_filepath, items_filepath)
    users = zeros(Int64,0)
    items = zeros(Int64,0)
    rates = zeros(Float64,0)
    ts = zeros(Float64,0)
    item_id_to_index = Dict{String, Int64}()
    user_id_to_index = Dict{String, Int64}()
    ratings_count = int(split(readall(`wc -l $(ratings_filepath)`), " ")[1])
    progress = Progress(ratings_count, 1, "Loading ratings ")
    open(ratings_filepath, "r") do ratings_file
        for line in eachline(ratings_file)
            user_id, item_id, rating, timestamp = split(chomp(line), "::")
            user_index = get(user_id_to_index, user_id, 0)
            if user_index == 0
                user_index = user_id_to_index[user_id] = length(user_id_to_index) + 1
            end
            item_index = get(item_id_to_index, item_id, 0)
            if item_index == 0
                item_index = item_id_to_index[item_id] = length(item_id_to_index) + 1
            end
            push!(users, int(user_index))
            push!(items, int(item_index))
            push!(rates, float(rating))
            push!(ts, float(timestamp))
            next!(progress)
        end
    end
    n_users = length(user_id_to_index)
    n_items = length(item_id_to_index)
    dataset = Dataset(users, items, rates, ts, n_users, n_items, ratings_count)
    shuffle_rowcol!(dataset)
    # generate Features
    trainset, testset = split_ratings(dataset, 0.10)
    dataset = 0 # free memory
    trainset, testset, n_users, n_items
end

function shuffle_rowcol!(mat)
    @printf("shuffling rows & cols ... ")
    # shuffle items
    rnd_idx = randperm(mat.n_items)
    for (i,item) in enumerate(mat.items)
        mat.items[i] = rnd_idx[item]
    end
    # shuffle users
    rnd_idx = randperm(mat.n_users)
    for (i,user) in enumerate(mat.users)
        mat.users[i] = rnd_idx[user]
    end
    @printf("done.\n")
end



function split_ratings(dataset::Dataset, testset_ratio=0.10)
    n_instance = length(dataset.users)
    
    shuffled_idx = randperm(n_instance)
    dataset.users = dataset.users[shuffled_idx]
    dataset.items = dataset.items[shuffled_idx]
    dataset.rates = dataset.rates[shuffled_idx]
    dataset.ts = dataset.ts[shuffled_idx]
    
    n_trainset = floor(n_instance * (1-testset_ratio))
    n_testset = dataset.size - n_trainset

    tr_users = dataset.users[1:n_trainset]
    tr_items = dataset.items[1:n_trainset]
    tr_rates = dataset.rates[1:n_trainset]
    tr_ts = dataset.ts[1:n_trainset]
    trainset = Dataset(tr_users, tr_items, tr_rates, tr_ts, dataset.n_users, dataset.n_items, n_trainset)

    ts_users = dataset.users[n_trainset+1:end]
    ts_items = dataset.items[n_trainset+1:end]
    ts_rates = dataset.rates[n_trainset+1:end]
    ts_ts = dataset.ts[n_trainset+1:end]
    testset = Dataset(ts_users, ts_items, ts_rates, ts_ts, dataset.n_users, dataset.n_items, n_testset)
    
    trainset, testset
end



function split_ratings2(ratings, testset_ratio=0.10)
    seen_users = Set()
    seen_items = Set()
    train_set = (Rating)[]
    test_set = (Rating)[]
    #train_set = Array(typeof(ratings))
    #test_set = Array(typeof(ratings))
    shuffled = shuffle(ratings)
    @inbounds for rating in shuffled
        user::Int64, item::Int64, rate::Float64 = rating.user, rating.item, rating.rate
        if in(user, seen_users) && in(item, seen_items) && length(test_set) < testset_ratio * length(shuffled)
            push!(test_set, rating)
            #test_set = vcat(test_set, rating)
        else
            push!(train_set, rating)
            #train_set = vcat(train_set, rating)
        end
        push!(seen_users, user)
        push!(seen_items, item)
    end
    train_set, test_set
end

function split_ratings3(ratings, testset_ratio=0.10)
    # do shuffle!
    ratings = ratings[randperm(size(ratings)[1]),:]
    n_trainset = floor(size(ratings)[1] * (1-testset_ratio))
    trainset = deepcopy(ratings[1:n_trainset,:])
    testset = deepcopy(ratings[n_trainset+1:end,:])
    return trainset, testset
end

function gen_user_features(file_path, user_id_to_index)
    # zip 
    zip_ids = Dict{String, Int32}()
    open(file_path, "r") do user_file
        for line in eachline(user_file)
            zip_str = split(chomp(line), "::")[5]
            if !haskey(zip_ids, zip_str)
                zip_ids[zip_str] = length(zip_ids) + 1
            end
        end
    end
    zip_count = length(zip_ids)
    MAX_OCCUPATION_ID = 21
    feature_dim = 2 + MAX_OCCUPATION_ID + zip_count # gender (1), age (1), occupation(21), zip_count
    num_valid_users = length(user_id_to_index);
    user_features = zeros(Int32, num_valid_users, feature_dim)     
    
    progress = Progress(num_valid_users, 1, "Generating User Features ")
    user_file = open(file_path, "r")
    for line in eachline(user_file)
        user_id, gender, age, occupation, zip = split(chomp(line), "::")
        if !haskey(user_id_to_index, user_id) continue    
        end
        user_index = user_id_to_index[user_id]
        user_features[user_index, 1] = (gender == "M" ? 1 : 0)  # man or female
        user_features[user_index, 2] = int(age)
        user_features[user_index, 2 + int(occupation) + 1] = 1
        user_features[user_index, 2 + MAX_OCCUPATION_ID + zip_ids[zip]] = 1
        next!(progress)
    end
    close(user_file)
    user_features
end



function gen_item_features(file_path, item_id_to_index)
    GENRE_COUNT = 18
    ITEM_FEATURE_DIM = GENRE_COUNT + 1
    num_valid_items = length(item_id_to_index)
    item_features = zeros(Int32, num_valid_items, ITEM_FEATURE_DIM)
    
    progress = Progress(num_valid_items, 1, "Generating Item Features ")
    item_file = open(file_path, "r") 
    genre_ids = Dict{String, Int32}()
    for line in eachline(item_file)
        item_id, title_year_str, genre_str = split(chomp(line), "::")
        if !haskey(item_id_to_index, item_id) continue
        end
        item_index = item_id_to_index[item_id]
        year = int(title_year_str[end-4:end-1])
        # genre
        genre_feat = zeros(Int32, GENRE_COUNT)
        genres = split(genre_str, "|")
        for genre in genres
            if !haskey(genre_ids, genre)
                genre_ids[genre] = length(genre_ids) + 1
            end
            genre_feat[genre_ids[genre]] = 1
        end
        item_features[item_index, 1] = year 
        item_features[item_index, 2:end] = genre_feat
        next!(progress)
    end
    close(item_file)
    item_features
end



function load_movielens_1M()
    ratings_filepath = "../00_data/00_ml_1m/ratings.dat"
    #items_filepath = "../00_data/00_ml_1m/movies.dat"
    #users_filepath = "../00_data/00_ml_1m/users.dat"
    #n_users, n_items = 6040, 3706
    load_movielens_dataset3(ratings_filepath)
end

function load_movielens_10M()
    ratings_filepath = "../00_data/01_ml_10m/ratings.dat"
    #items_filepath = "../00_data/01_ml_10m/movies.dat"
    #users_filepath = "../00_data/01_ml_10m/users.dat"
    load_movielens_dataset3(ratings_filepath)
end


function compute_n_elem(trainset::Array{Int64,2}, n_users, n_items)
    sz_trainset = size(trainset)[1]
    n_elem_row = zeros(Int64, n_users)
    n_elem_col = zeros(Int64, n_items)
    for ix in 1:sz_trainset
        u, i, r = trainset[ix,:]
        n_elem_row[u] += 1
        n_elem_col[i] += 1
    end
    return n_elem_row, n_elem_col
end

using HDF5, JLD
function load_netflix()
    trainset = load("/scratch/DATA/netflix/01_processed/trainset.jld")["trainset"]
    validset = load("/scratch/DATA/netflix/01_processed/validset.jld")["validset"]
    n_elem = load("/scratch/DATA/netflix/01_processed/n_elem_row_cols.jld")
    n_elem_row, n_elem_col = n_elem["n_elem_row"], n_elem["n_elem_col"]
    trainset = trainset[randperm(size(trainset)[1]), :] # shuffle
    n_users, n_items = 480189, 17770
    #n_elem_row, n_elem_col = compute_n_elem(trainset, n_users, n_items)
    return trainset, validset, n_users, n_items, n_elem_row, n_elem_col
end

function load_netflix_train8020()

    @printf("Loading a 80/20 split of trainset ... ")
    trainset = load("/scratch/DATA/netflix/01_processed/train80.jld")["trainset"]
    validset = load("/scratch/DATA/netflix/01_processed/train20.jld")["validset"]
    n_elem = load("/scratch/DATA/netflix/01_processed/n_elem_row_cols_train8020.jld")
    n_elem_row, n_elem_col = n_elem["n_elem_row"], n_elem["n_elem_col"]
    n_users, n_items = 480189, 17770
    @printf("Done\n")
    #allset = load("/scratch/DATA/netflix/01_processed/trainset.jld")["trainset"]
    #sz_allset = size(allset)[1]
    #sz_validset = int(sz_allset * 0.2)
    #sz_trainset = sz_allset - sz_validset
    
    #rand_idx = randperm(sz_allset)
    #validset_idx = rand_idx[1:sz_validset]
    #trainset_idx = rand_idx[sz_validset+1:end]
    #validset = allset[validset_idx,:]
    #trainset = allset[trainset_idx,:]

    #n_users, n_items = 480189, 17770
    #sorted_trainset = allset[sort(trainset_idx),:]
    #n_elem_row, n_elem_col = compute_n_elem(sorted_trainset, n_users, n_items)

    return trainset, validset, n_users, n_items, n_elem_row, n_elem_col
end
