function rmse(P, Q, testset::Dataset)
    sse = 0.0
    n_testset = length(testset.users)
    for idx = 1:n_testset
        u = testset.users[idx]    
        i = testset.items[idx]    
        r = testset.rates[idx]
        pred = dot(P[:,u], Q[:,i])
        if pred < 1.; pred = 1. 
        elseif pred > 5.; pred = 5. 
        end 
        a = r.rate - pred
        sse += a*a # for speed, '^2' is avoided
        #sse += (r - pred) * (r - pred) # for speed, '^2' is avoided
    end
    sqrt(sse/n_testset)
end

function rmse{T}(P, Q, testset::Array{T,2}, min_rate::Float64=0, max_rate::Float64=5, mean_rate::Float64=0)
    Puu = P[:,testset[:,1]]
    Qii = Q[:,testset[:,2]]
    # dot products
    pred = vec(sum(Puu .* Qii, 1)) + mean_rate
    # filter out of range predicts
    pred[pred .< min_rate] = min_rate
    pred[pred .> max_rate] = max_rate
    # compute RMSE
    return sqrt(sum(sqr(testset[:,3] - pred))/size(testset,1))
end

#function rmse(P, Q, testset, min_rate::Float64=0, 
              #max_rate::Float64=5, mean_rate::Float64=0)
    #sse = 0.0
    #n_testset = size(testset,1)
    #for idx = 1:n_testset
        #u, i::Int64, r::Int64 = testset[idx,:]
        #pred = dot(P[:,u], Q[:,i]) + mean_rate
        #if pred < min_rate; pred = min_rate
        #elseif pred > max_rate; pred = max_rate
        #end
        #sse += sqr(r - pred) # for speed, '^2' is avoided
    #end 
    #return sqrt(sse/n_testset)
#end

function rmse(P, Q, testset::Array{Rating,1}, range_min, range_max)
    sse = 0.0
    for r in testset
        pred = dot(P[:,r.user], Q[:,r.item])
        if pred < range_min; pred = range_min 
        elseif pred > range_max; pred = range_max
        end
        a = r.rate - pred
        sse += a*a # for speed, '^2' is avoided
    end
    sqrt(sse/length(testset))
end



# Refer to D-SGLD paper
function annealed_stepsize(t; epsilon=0.05, tau0=1000, kappa=0.7)
    return epsilon * ((1 + t./tau0).^(-kappa))
end



# copy data to all workers 
function pcopy_trainset(trainset::Array{Float64,2})
    @everywhere l_trainset = Array{Float64,2}
    # line below is a function which assigns the dat passed
    # to the predefined global variable l_trainset. This is
    # executed at eachch worker by remotecall_fetch()
    @everywhere copy_trainset(dat) = (global l_trainset; l_trainset = dat) 
    for p in procs()
        remotecall_fetch(p, copy_trainset, trainset)
    end
    println("trainset loaded to workers")
end

# copy data to all workers 
function pcopy_trainset2(trainset::Array{Rating,1})
    @everywhere l_trainset = Array{Rating,1}
    # line below is a function which assigns the dat passed
    # to the predefined global variable l_trainset. This is
    # executed at eachch worker by remotecall_fetch()
    @everywhere copy_trainset(dat) = (global l_trainset; l_trainset = dat) 
    for p in procs()
        remotecall_fetch(p, copy_trainset, trainset)
    end
    println("trainset loaded to workers")
end

# copy data to all workers 
function pcopy_trainset(trainset::Dataset)
    @everywhere l_trainset = Dataset
    # line below is a function which assigns the dat passed
    # to the predefined global variable l_trainset. This is
    # executed at eachch worker by remotecall_fetch()
    @everywhere copy_trainset(dat) = (global l_trainset; l_trainset = dat) 
    for p in procs()
        remotecall_fetch(p, copy_trainset, trainset)
    end
    println("trainset loaded to workers")
end


# copy data to all workers 
function pcopy_testset(testset::Array{Float64,2})
    @everywhere l_testset = Array{Float64,2}
    @everywhere copy_testset(dat) = (global l_testset; l_testset = dat) 
    for p in procs()
        remotecall_fetch(p, copy_testset, testset)
    end
    println("testset loaded to workers")
end

# copy data to all workers 
function pcopy_testset2(testset::Array{Rating,1})
    @everywhere l_testset = Array{Rating,1}
    @everywhere copy_testset(dat) = (global l_testset; l_testset = dat) 
    for p in procs()
        remotecall_fetch(p, copy_testset, testset)
    end
    println("testset loaded to workers")
end
# copy data to all workers 
function pcopy_testset(testset::Dataset)
    @everywhere l_testset = Dataset
    @everywhere copy_testset(dat) = (global l_testset; l_testset = dat) 
    for p in procs()
        remotecall_fetch(p, copy_testset, testset)
    end
    println("testset loaded to workers")
end

# copy data to all workers 
function pcopy_blockmembership(blockmembership)
    @everywhere l_blockmembeship = Array{Array{Int64,1},1}
    @everywhere copy_blockmembership(dat) = (global l_blockmembership; l_blockmembership = dat) 
    for p in procs()
        remotecall_fetch(p, copy_blockmembership, blockmembership)
    end
    println("blockmembership loaded to workers")
end



# Used to compute block_id given block row and column. 
# Square block structure (n_row_block = n_col_block) is assumed.
function rowcol2seqid(row, col, n_colblk)
    @assert col <= n_colblk 
    id = (row-1)*n_colblk + col
end


function block_membership(trainset::Array{Float64,2}, bounds)
    n_rowblk, n_colblk = size(bounds)
    n_blks = n_rowblk * n_colblk
    blockmembership = Array(Array{Int64,1}, n_blks) 
    for blk = 1:n_blks
        blockmembership[blk] = zeros(Int64,0)
    end
    i::Int64 = 1
    j::Int64 = 1
    for idx in 1:size(trainset)[1]
        user, item = trainset[idx,[1,2]]
        #user = trainset[idx].user
        #item = trainset[idx].item
        for r = 1:n_rowblk
            if user >= bounds[r,1].rowbegin && user <= bounds[r,1].rowend
                i = r
                break
            end
        end
        for c = 1:n_colblk
            if item >= bounds[1,c].colbegin && item <= bounds[1,c].colend
                j = c
                break
            end
        end
        blk_id = rowcol2seqid(i, j, n_colblk)
        push!(blockmembership[blk_id], idx)
    end
    blockmembership
end
function block_membership2(trainset::Array{Rating,1}, bounds)

    n_rowblk, n_colblk = size(bounds)
    n_blks = n_rowblk * n_colblk
    blockmembership = Array(Array{Int64,1}, n_blks) 
    for blk = 1:n_blks
        blockmembership[blk] = zeros(Int64,0)
    end
    i::Int64 = 1
    j::Int64 = 1
    for idx in 1:length(trainset)
        user = trainset[idx].user
        item = trainset[idx].item
        for r = 1:n_rowblk
            if user >= bounds[r,1].rowbegin && user <= bounds[r,1].rowend
                i = r
                break
            end
        end
        for c = 1:n_colblk
            if item >= bounds[1,c].colbegin && item <= bounds[1,c].colend
                j = c
                break
            end
        end
        blk_id = rowcol2seqid(i, j, n_colblk)
        push!(blockmembership[blk_id], idx)
    end
    blockmembership
end


function block_membership(trainset::Dataset, bounds)

    n_rowblk, n_colblk = size(bounds)
    n_blks = n_rowblk * n_colblk
    blockmembership = Array(Array{Int64,1}, n_blks) 
    for blk = 1:n_blks
        blockmembership[blk] = zeros(Int64,0)
    end
    i::Int64 = 1
    j::Int64 = 1
    for idx in 1:trainset.size
        user = trainset.users[idx]
        item = trainset.items[idx]
        for r = 1:n_rowblk
            if user >= bounds[r,1].rowbegin && user <= bounds[r,1].rowend
                i = r
                break
            end
        end
        for c = 1:n_colblk
            if item >= bounds[1,c].colbegin && item <= bounds[1,c].colend
                j = c
                break
            end
        end
        blk_id = rowcol2seqid(i, j, n_colblk)
        push!(blockmembership[blk_id], idx)
    end
    blockmembership
end



# Divide the matrix into non-overlapping blocks and return
# the bounds of each blocks. Both row and column is divided 
# into nproc() partitions so that in total there exists 
# nproc()^2 blocks.
function indep_block_bounds(n_users, n_items, n_partition)
    user_range = int(linspace(1, n_users+1, n_partition+1))
    item_range = int(linspace(1, n_items+1, n_partition+1))
    bounds = Array(Bound, n_partition, n_partition)
    for r = 1:n_partition, c = 1:n_partition
            bounds[r,c] = Bound(user_range[r], user_range[r+1]-1, item_range[c], item_range[c+1]-1)
    end
    bounds
end


function indep_block_bounds(n_rows, n_cols, blocks_dim)
    (n_blk_rows, n_blk_cols) = blocks_dim
    row_range = int(linspace(1, n_rows+1, n_blk_rows+1))
    col_range = int(linspace(1, n_cols+1, n_blk_cols+1))
    bounds = Array(Bound, n_blk_rows, n_blk_cols)
    for r = 1:n_blk_rows, c = 1:n_blk_cols
        bounds[r,c] = Bound(row_range[r], row_range[r+1]-1, col_range[c], col_range[c+1]-1)
    end
    bounds
end



# Assign a block to each process. Blocks must be independent, 
# meaning that any pair of blocks do not share any row or column.
@debug function assign_blocks(t, n_workers, algo)

    @assert t >= 1

    if algo == "MATAVG"
        blk_ids = zeros(Int64, n_workers)
        n_blocks = n_workers * n_workers
        tmp = mod(t, n_blocks) 
        blk_id = tmp == 0 ? n_blocks : tmp
        fill!(blk_ids, blk_id)
    elseif algo == "DSGD"
    # Independent blocks are selected by shifting right 
    # the diagonal blocks
        blk_ids = zeros(Int64, 0)
        for p = 1:n_workers
            shift = mod(t-1, n_workers) 
            col = mod(p+shift, n_workers)
            col = col == 0 ? n_workers : col
            blk_id = rowcol2seqid(p, col, n_workers)
            push!(blk_ids, blk_id)
        end
    elseif algo == "DSGD_R"
    # Independent blocks are selected at random
        blk_ids = zeros(Int64, 0)
        block_cols = randperm(n_workers)
        for p = 1:n_workers
            blk_id = rowcol2seqid(p, block_cols[p], n_workers)
            push!(blk_ids, blk_id)
        end
    end
    @bp
    return blk_ids
end



# Return the minibatch size of each block. Each block uses 
# random samples of minibatch size. Default minibatch size is
# n_ratings / nprocs()**2. If this number is larger than the
# total number of ratings in a block due to the irregularity
# in the number of ratings in the blocks, then all ratings in 
# the block is used.
function get_epoch_lens(blockmembership, n_ratings, block_sample_ratio)
    epoch_len = zeros(Int64, length(blockmembership))
    n_workers = nprocs() - 1
    algo = "block_propotional"
    #algo = "global_epoch_len"
    if algo == "block_propotional"
        # batch size is proportional to the block size
        for (i,block) in enumerate(blockmembership)
            epoch_len[i] = int(length(block) * block_sample_ratio)
        end
    elseif algo == "global_epoch_len"
        # batch size is global constant. But when the block size
        # is smaller then the global batch size, the batch size
        # is set to be equal to the block size
        default_epoch_len::Int64 = div(n_ratings * block_sample_ratio, n_workers * n_workers)
        for (i,block) in enumerate(blockmembership)
            n_blk_ratings = length(block)
            epoch_len[i] = default_epoch_len > n_blk_ratings ? n_blk_ratings : default_epoch_len
        end
    else
        error("Undefined algorithm type")
    end
    epoch_len
end
