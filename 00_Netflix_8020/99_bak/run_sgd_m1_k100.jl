include("../utils/types.jl")
include("../train_sgld_gibbs_m1.jl")

# Initialize parmeter
srand(1)
println("Netflix 8020 - SGD-m1")

## params 
para = Dict{ASCIIString, Any}()
para["K"]       = 100
#para["eps"]     = 9e-6
para["m"]       = 50000
para["rndlen"]  = 25
para["map"]     = true
para["tg_rmse"] = 0.865
#para["bthresh"] = 0.1
para["la0_p"]   = 2.0
para["la0_q"]   = 2.0
para["submean"] = true
para["mxiter"]  = 999999
para["mxsec"]   = 999999
para["iscale"]  = 0.01
para["itvsv"]   = 100
para["itvtst"]  = 10
para["itvhyp"]  = 30      

para["eps"]     = 9e-6
para["tau0"]    = 50
para["kappa"]   = 0.51
f_stepsz(t)     = para["eps"] .* ((1 + t./para["tau0"]).^(-para["kappa"]))

# model
model0 = Dict{ASCIIString,Any}()
model0["P"]         = randn(para["K"], n_users) * para["iscale"]
model0["Q"]         = randn(para["K"], n_items) * para["iscale"]
model0["mu_p"]      = zeros(para["K"])
model0["mu_q"]      = zeros(para["K"])   
model0["mu0_p"]     = zeros(para["K"])
model0["mu0_q"]     = zeros(para["K"])
model0["La_p"]      = para["la0_p"] * eye(para["K"])
model0["La_q"]      = para["la0_q"] * eye(para["K"])
model0["bt0_p"]     = 2.0
model0["bt0_q"]     = 2.0
model0["prec_r"]    = 2.0

#model0 = load("./map_netval_k30_m1.jd")["model"]

model = deepcopy(model0)
train_sgld_gibbs_m1(trainset, testset, model, para, trn_n_elem_row, trn_n_elem_col, f_stepsz)

