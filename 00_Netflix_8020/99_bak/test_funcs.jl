using NumericExtensions
using Debug

function rmse_forloop(mat::Array{Float64,2}, mm::Array{Float64,2})
    sz = size(mat)[1]
    ans = zeros(sz)
    for i in 1:sz
        ans[i] = sum(sqr(mat[i,:] .- mm[i,:]))
    end
    return ans

end

function rmse_vector(mat::Array{Float64,2}, ans::Array{Float64,2})
    return sum(sqr(mat .- ans),2)
end

N = 100000
d = 1000
M = randn(N,d);
mean = randn(N,d);

@time a = rmse_forloop(M, mean);
@time b = rmse_vector(M, mean);








