using Debug
using Datetime
using Devectorize
using NumericExtensions
include("mf_common.jl")
include("utils.jl")
include("rng.jl")

function train_sgld_gibbs(trainset, testset, n_users, n_items, N_elem_row, N_elem_col, P, Q, stepsz;sz_batch=3000, maxiter=100, burnin=100, itv_save=Inf, itv_test=1, maxsec=Inf)

    algo = "SGLD-Gibbs"
    @assert itv_save > itv_test
    @printf("Running %s \n", algo)

    # initialization
    logs = Log(zeros(Int64,0), zeros(Float64,0), zeros(Float64,0))
    const min_rate = minimum(trainset[:,3])
    const max_rate = maximum(trainset[:,3])
    const mean_rate = mean(trainset[:,3])
    const sz_trainset = size(trainset)[1]
    const sz_testset = size(testset)[1]
    #do_mean_subtract ? (const mean_rate = mean(trainset[:,3])) : (const mean_rate = 0.0)

    #alpha0 = 1; beta0 = 1;
    #var_Pu = rand_inverse_gamma(alpha0, beta0, n_users)
    #var_Qi = rand_inverse_gamma(alpha0, beta0, n_items)
    t = 1        # iter
    avg_cnt = 1  # counts number of averaged predicions
    ts = 0.0     # timestamp

    best_rmse = 100.0
    cur_str_id = get_str_id(algo, t, sz_batch, burnin, best_rmse)
    avg_pred = zeros(Float64,sz_testset)

    alpha = 2.0   # r_ui ~ N(Pu'Qi, alpha^inv)
    WI_p = eye(K)
    WI_q = eye(K)
    mu0_p = zeros(K)
    mu0_q = zeros(K)
    bt0_p = 2.0
    bt0_q = 2.0
    df_p = float(K)
    df_q = float(K)

    round_length = 1

    #while t <= maxiter
    while t <= maxiter && ts < maxsec
    
        tic()
        ########################################################################
        ## sample item params
        x_bar = vec(mean(Q,2))
        S_bar = cov(Q')
        WI_post = inv(inv(WI_q) + n_items/1 * S_bar + n_items * bt0_q * (mu0_q - x_bar) * (mu0_q - x_bar)'/(1 * (bt0_q + n_items)))
        WI_post = (WI_post + WI_post)'/2
        df_qmost = df_q + n_items
        Lam_Q = rand(Wishart(df_qmost, WI_post))
        mu_temp = (bt0_q * mu0_q + n_items * x_bar) / (bt0_q + n_items)
        lam = chol(inv((bt0_q + n_items) * Lam_Q)); lam = lam'
        mu_q = lam * randn(K) + mu_temp

        ## sample user params
        x_bar = vec(mean(P,2))
        S_bar = cov(P')
        WI_post = inv(inv(WI_p) + n_users/1 * S_bar + n_users * bt0_p * (mu0_p - x_bar) * (mu0_p - x_bar)'/(1 * (bt0_p + n_users)))
        WI_post = (WI_post + WI_post)'/2
        df_pmost = df_p + n_users
        Lam_P = rand(Wishart(df_pmost, WI_post))
        mu_temp = (bt0_p * mu0_p + n_users * x_bar) / (bt0_p + n_users)
        lam = chol(inv((bt0_p + n_users) * Lam_P)); lam = lam'
        mu_p = lam * randn(K) + mu_temp

        # SGLD sampling for P and Q
        P, Q  = sample_sgld(trainset, P, Q, mu_p, mu_q, Lam_P, Lam_Q, alpha, stepsz, N_elem_row, N_elem_col, 
                            sz_batch, mean_rate, round_length, "w_replacement")
        ########################################################################
        ts += toq();

        # Compute RMSE
        if mod(t, itv_test) == 0
            avg_rmse_t, cur_rmse_t, avg_pred = rmse_avg(t, P, Q, avg_pred, avg_cnt, testset, min_rate, max_rate, mean_rate, burnin)
            if t > burnin; avg_cnt += 1; end
            @printf("%d, %.4f (%.4f) (%.2fs) ", t, avg_rmse_t, cur_rmse_t, ts)
            if avg_rmse_t < best_rmse 
                best_rmse = avg_rmse_t
                @printf("(**)\n")
            else @printf("\n") 
            end
            tic()
            push!(logs.iter, t)
            push!(logs.ts, ts)
            push!(logs.rmse, avg_rmse_t)
            ts += toq()
        end

        # Save logs file
        if mod(t, itv_save) == 0
            @printf("saving loggings ... ")
            cur_str_id = save_logs(logs, cur_str_id, t, algo, best_rmse)  
            @printf("done.\n")
            if ts > maxsec
                break
            end
        end
        t += 1
    end
    logs, P, Q
end



# Gibbs sampling for Normal-Inverse-Gamma conjugate posterior 
function sample_hyper(Pu, var_Pu, n_elem_u, alpha0, beta0)
    # Updated alpha
    alpha = alpha0 + 0.5 * n_elem_u
    # Update beta 
    sse = sumsqdiff(Pu, mean(Pu))
    #sse = sum(sqr(Pu - mean(Pu)))
    beta  = beta0 + 0.5 * sse
    # sample from posterior
    var_Pu_new = rand_inverse_gamma(alpha, beta)
    return var_Pu_new
end


#function sample_sgld2(trainset::Array{Int64,2}, P::Array{Float64,2}, Q::Array{Float64,2}, 
                     #var_Pu::Array{Float64,1}, var_Qi::Array{Float64,1}, stepsz::Float64, 
                     #N_elem_row::Array{Int64,1}, N_elem_col::Array{Int64,1}, sz_batch::Int64,
                     #mean_rate::Float64,
                     #; round_length::Int64=200, sample_method::ASCIIString="w_replacement")
    
    #sz_trainset = size(trainset)[1]
    #K = size(P,1)
    ##grad_sum_P = Dict{Int64,Array{Float64,1}}()
    ##grad_sum_Q = Dict{Int64,Array{Float64,1}}()
    #grad_sum_P = zeros(Float64, K, n_users)
    #grad_sum_Q = zeros(Float64, K, n_items)
    ## shuffle trainset
    #if sample_method == "wo_replacement"
        #suffled_trainset = trainset[randperm(sz_trainset), :]
    #end
    #next_start_idx = 1
    ## sample over a single round
    #for t in 1:round_length
        #if sample_method == "wo_replacement"
            #batch, next_start_idx = get_next_batch(suffled_trainset, next_start_idx, sz_batch)
        #else    
            ## w_replacement
            #@inbounds batch = trainset[rand_samp(sz_trainset, sz_batch, sample_method), :]
            ##batch_idx, next_start_idx = get_next_batch_idx(next_start_idx, sz_batch, sz_trainset)
            ##batch = trainset[batch_idx,:]
        #end
        ## update grad sum
        #uu, ii, rr = batch[:,1], batch[:,2], batch[:,3]
        #Puu, Qii, rr = P[:,batch[:,1]], Q[:,batch[:,2]], batch[:,3]
        #pred = vec(sum(sqr(Puu .* Qii), 1))
        #error = (rr - mean_rate) - pred
        #grad_Puu = broadcast(+, error', Qii)
        #grad_Qii = broadcast(+, error', Puu)
        #fill!(grad_sum_P, 0.0)
        #fill!(grad_sum_Q, 0.0)
        #for ix in 1:sz_batch
            #grad_sum_P[:,uu[ix]] += grad_Puu[:,ix] 
            #grad_sum_Q[:,ii[ix]] += grad_Qii[:,ix]
        #end
        #P = P + 0.5 * stepsz * ((sz_trainset / sz_batch) * grad_sum_P - broadcast(*, var_Pu', P)) 
            #+ sqrt(stepsz) * randn(K, n_users)
        #Q = Q + 0.5 * stepsz * ((sz_trainset / sz_batch) * grad_sum_Q - broadcast(*, var_Qi', Q)) 
            #+ sqrt(stepsz) * randn(K, n_items)
    #end
    #return P, Q
#end



# sample by sgld
function sample_sgld(trainset::Array{Int64,2}, P::Array{Float64,2}, Q::Array{Float64,2}, 
                     mu_p::Array{Float64,1}, mu_q::Array{Float64,1}, Lam_P::Array{Float64,2}, Lam_Q::Array{Float64,2},
                     alpha::Float64, stepsz::Float64, N_elem_row::Array{Int64,1}, N_elem_col::Array{Int64,1}, 
                     sz_batch::Int64,
                     mean_rate::Float64,
                     round_length::Int64=200, sample_method::ASCIIString="w_replacement")
    
    sz_trainset = size(trainset)[1]
    K = size(P,1)
    #grad_sum_P = Dict{Int64,Array{Float64,1}}()
    #grad_sum_Q = Dict{Int64,Array{Float64,1}}()
    grad_sum_P = zeros(Float64, K, n_users)
    grad_sum_Q = zeros(Float64, K, n_items)
    
    # shuffle trainset
    if sample_method == "wo_replacement"
        suffled_trainset = trainset[randperm(sz_trainset), :]
    end
    next_start_idx = 1

    const ka = alpha * (sz_trainset / sz_batch)

    # sample over a single round
    for t in 1:round_length
        if sample_method == "wo_replacement"
            batch, next_start_idx = get_next_batch(suffled_trainset, next_start_idx, sz_batch)
        else    
            # w_replacement
            @inbounds batch = trainset[rand_samp(sz_trainset, sz_batch, sample_method), :]
            #batch_idx, next_start_idx = get_next_batch_idx(next_start_idx, sz_batch, sz_trainset)
            #batch = trainset[batch_idx,:]
        end
        #empty!(grad_sum_P)
        #empty!(grad_sum_Q)
        fill!(grad_sum_P, 0.0)
        fill!(grad_sum_Q, 0.0)
        # update grad sum
        for i in 1:sz_batch
            @inbounds user, item, rate = batch[i,:]
            @inbounds error = (rate - mean_rate) - dot(P[:,user], Q[:,item])
            @devec grad_Pu = error .* Q[:,item]
            @devec grad_Qi = error .* P[:,user]
            @inbounds grad_sum_P[:,user] += grad_Pu
            @inbounds grad_sum_Q[:,item] += grad_Qi
            #user in keys(grad_sum_P) ? grad_sum_P[user] += grad_Pu : grad_sum_P[user] = grad_Pu
            #item in keys(grad_sum_Q) ? grad_sum_Q[item] += grad_Qi : grad_sum_Q[item] = grad_Qi
        end

        #seen_users = Set(keys(grad_sum_P))
        #seen_items = Set(keys(grad_sum_Q))
        #unseen_users = setdiff(Set(1:n_users), seen_users)
        #unseen_items = setdiff(Set(1:n_items), seen_items)

        Prior_P = - Lam_P * broadcast(-, P, mu_p)
        Prior_Q = - Lam_Q * broadcast(-, Q, mu_q)

        for user in 1:n_users
            #prior = - Lam_P * (P[:,user] - mu_p)
            @devec grad = ka .* grad_sum_P[:,user] .+ Prior_P[:,user]
            noise = randn(K)
            @devec P[:,user] += stepsz./2 .* grad .+ sqrt(stepsz) .* noise
        end
        for item in 1:n_items
            #prior = - Lam_Q * (Q[:,item] - mu_q)
            @devec grad = ka .* grad_sum_Q[:,item] .+ Prior_Q[:,item]
            noise = randn(K)
            @devec Q[:,item] += stepsz./2 .* grad .+ sqrt(stepsz) .* noise
        end
        #for user in unseen_users
            #prior = - Lam_P * (P[:,user] - mu_p)
            #noise = randn(K)
            #@devec P[:,user] += stepsz./2 .* prior .+ sqrt(stepsz) .* noise
        #end
        #for item in unseen_items
            #prior = - Lam_Q * (Q[:,item] - mu_q)
            #noise = randn(K)
            #@devec Q[:,item] += stepsz./2 .* prior .+ sqrt(stepsz) .* noise
        #end
        ## Update P
        #for (user, grad_sum_Pu) in grad_sum_P
            #prior = Lam_P * (P[:,user] - mu_p)
            #@devec grad = alpha .* (sz_trainset ./ sz_batch) .* grad_sum_Pu .- prior
            #noise = randn(K)
            #@devec P[:,user] += stepsz./2 .* grad .+ sqrt(stepsz) .* noise
        #end
        ## Update Q
        #for (item, grad_sum_Qi) in grad_sum_Q
            #prior = Lam_Q * (Q[:,item] - mu_q)
            #@devec grad = alpha .* (sz_trainset ./ sz_batch) .* grad_sum_Qi .- prior
            #noise = randn(K)
            #@devec Q[:,item] += stepsz./2 .* grad .+ sqrt(stepsz) .* noise
        #end
    end
    return P, Q
end


function get_next_batch_idx(start_idx::Int64, sz_batch::Int64, sz_total::Int64)
    end_idx = start_idx + sz_batch - 1;
    if end_idx < sz_total
        batch_idx = [start_idx:end_idx]
        new_start_idx = end_idx + 1
    elseif end_idx == sz_total
        batch_idx = [start_idx:end_idx]
        new_start_idx = 1
    else 
        offset = end_idx - sz_total
        batch_idx = vcat([start_idx:sz_total], [1:offset])
        new_start_idx = offset + 1
    end
    @assert length(batch_idx) == sz_batch
    return batch_idx, new_start_idx
end


function get_next_batch(trainset, start_idx, sz_batch)
    sz_trainset = size(trainset)[1]
    end_idx = start_idx + sz_batch - 1;
    if end_idx <= sz_trainset
        batch = trainset[start_idx:end_idx, :]
    else 
        offset = end_idx - sz_trainset
        batch = vcat(trainset[start_idx:end, :], trainset[1:offset-1, :])
    end
    @assert size(batch)[1] == sz_batch
    return batch, end_idx + 1
end


function rmse_avg(t::Int64, P, Q, avg_pred, avg_cnt, testset, 
                  min_rate::Int64, max_rate::Int64, mean_rate::Float64, burnin::Int64)
    rr = testset[:,3]
    # dot products
    pred = vec(sum(P[:,testset[:,1]] .* Q[:,testset[:,2]], 1)) + mean_rate
    # filter out of range predicts
    pred[pred .< min_rate] = min_rate
    pred[pred .> max_rate] = max_rate
    if t > burnin
        avg_pred = (1 - 1/avg_cnt) * avg_pred + 1/avg_cnt * pred
        avg_rmse = sqrt(sum(sqr(rr - avg_pred))/size(testset,1))
    end
    # compute RMSE
    cur_rmse = sqrt(sum(sqr(rr - pred))/size(testset,1))
    return avg_rmse, cur_rmse, avg_pred
end


#function rmse_avg(t::Int64, P, Q, avg_pred, avg_cnt, testset, min_rate::Int64, max_rate::Int64, mean_rate::Float64, burnin::Int64)
    #sse = 0.0
    #avg_sse = 0.0
    #sz_testset = size(testset)[1]
    #for i in 1:sz_testset
        #user::Int, item::Int, rate::Int64 = testset[i,:]
        #pred = dot(P[:,user], Q[:,item]) + mean_rate
        #if pred < min_rate; pred = min_rate 
        #elseif pred > max_rate; pred = max_rate
        #end
        #if t > burnin
            #avg_pred[i] = (1 - 1/avg_cnt) * avg_pred[i] + 1/avg_cnt * pred
            #avg_sse += sqr(rate - avg_pred[i])
        #end
        #sse += sqr(rate - pred)  
    #end
    #cur_rmse = sqrt(sse/sz_testset)
    #avg_rmse = sqrt(avg_sse/sz_testset)
    #return avg_rmse, cur_rmse, avg_pred
#end



function get_str_id(algo, t::Int64, sz_batch::Int64, burnin::Int64, best_rmse::Float64)
    now_str = replace(replace(string(now(Datetime.PST)), ":",""),"-","")[1:end-4]
    now_str = "$now_str""_np"string(nprocs()-1)"_K"string(K)"_stsz"string(stepsz)"_szbatch"string(sz_batch)"_burnin"string(burnin)"_""$algo""_rmse"@sprintf("%.4f",best_rmse)"_t""$t"
    now_str = replace(now_str,".","_")
    now_str
end

