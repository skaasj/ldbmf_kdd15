
# Check if 'filename' exists in the 'path'
function file_exist(path, filename)
    n = length(filter(x->(x==filename), readdir(path))) 
    if n == 0
        return false
    elseif n == 1
        return true
    else
        error("must not reach here")
    end
end



using HDF5, JLD
# Save logs into a new log file and delete old log file.
# The filename changes only for current iteration
function save_logs(logs, cur_str_id, new_t, algo)
    if !file_exist(".","00_results")    # include utils.jl
        run(`mkdir 00_results`)
    end
    # Apply current iteration to the new filename
    new_str_id = cur_str_id[1:rsearch(cur_str_id,'_')]"t""$new_t"
    # Save new log
    save("./00_results/$new_str_id.jd", "logs", logs)
    # Delete old log
    run(`rm -f 00_results/$cur_str_id.jd`)
    new_str_id
end

function save_logs(logs, cur_str_id, new_t, algo, best_error)
    if !file_exist(".","00_results")    # include utils.jl
        run(`mkdir 00_results`)
    end
    # Apply current iteration to the new filename
    new_str_id = cur_str_id[1:rsearch(cur_str_id,"_rmse")[1]]"rmse_"@sprintf("%.4f",best_error)"_t""$new_t"
    # Save new log
    save("./00_results/$new_str_id.jd", "logs", logs)
    # Delete old log
    run(`rm -f 00_results/$cur_str_id.jd`)
    new_str_id
end
