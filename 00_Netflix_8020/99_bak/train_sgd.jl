using Devectorize
using HDF5, JLD
using Datetime
include("mf_common.jl")
include("utils.jl")

function train_sgd(trainset, testset, P, Q, λ, ϵ, momentum, maxiter=100, itv_test=10, itv_save=100, maxsec=Inf)

    algo = "SGD"
    logs = Log(zeros(Int64,0), zeros(Float64,0), zeros(Float64,0))
    const K = size(P,1)
    t = 1
    ts = 0.0
    max_epoch = maxiter
    cur_str_id = get_str_id(algo, t, K, λ, ϵ)
    const min_rate = float(minimum(trainset[:,3]))
    const max_rate = float(maximum(trainset[:,3]))
    const mean_rate = mean(trainset[:,3])
    const sz_trainset = size(trainset)[1]
    const n_users = size(P)[2]
    const n_items = size(Q)[2]
    Pu = zeros(Float64,K,1)
    grad_Pu_inc = zeros(K, n_users)
    grad_Qi_inc = zeros(K, n_items)

    idx = 0
    while t <= max_epoch && ts < maxsec
        
        tic()
        ###################################################################
        # Train 1 Epoch
        #for j = 1:div(sz_trainset,20)
        for j = 1:div(sz_trainset, 20)
            #idx = int(ceil(rand()*sz_trainset))
            idx = mod(idx += 1, sz_trainset); if idx == 0; idx = sz_trainset; end
            u::Int64, i::Int64, r::Int64 = trainset[idx,:]
            error::Float64 = (r - mean_rate) - dot(P[:,u], Q[:,i]) 
            @devec grad_Pu_inc[:,u] = momentum .* grad_Pu_inc[:,u] .+ ϵ .* (error .* Q[:,i] .- λ .* P[:,u])
            @devec grad_Qi_inc[:,i] = momentum .* grad_Qi_inc[:,i] .+ ϵ .* (error .* P[:,u] .- λ .* Q[:,i])
            @devec Pu = P[:,u] .+ grad_Pu_inc[:,u]
            @devec Q[:,i] = Q[:,i] .+ grad_Qi_inc[:,i]
            P[:,u] = copy(Pu)
        end
        ###################################################################
        ts += toq()

        # Compute RMSE and store logs
        if mod(t, itv_test) == 0
            rmse_t = rmse(P, Q, testset, min_rate, max_rate, mean_rate)
            @printf("epoch:%d, rmse: %f (%.2f sec.) \n", t, rmse_t, ts)
            if rmse_t <= 0.9215
                @printf("stop at rmse: %f \n", rmse_t)
                break
            end
            tic()
            push!(logs.iter, t)
            push!(logs.ts, ts)
            push!(logs.rmse, rmse_t)
            ts += toq()
        end
        
        # Save logs to file
        if mod(t, itv_save) == 0
            @printf("saving logs ... ")
            cur_str_id = save_logs(logs, cur_str_id, t, algo)   # include utils.jl
            @printf("done.\n")
            if ts > maxsec
                break
            end
        end       
        t += 1

    end
    logs, P, Q
end


function get_str_id(algo, t, K, λ, ϵ)
    now_str = replace(replace(string(Datetime.now(Datetime.PST)), ":",""),"-","")[1:end-4]
    now_str = "$now_str""_K"string(K)"_stsz"string(ϵ)"_reg"string(λ)"_""$algo""_t""$t"
    now_str = replace(now_str,".","_")
    now_str
end

