include("types.jl")

# Initialize parmeter
srand(1)

K = 30 # latent feature dimension
init_factor = 0.001
P0 = rand(K, n_users) * init_factor
Q0 = rand(K, n_items) * init_factor

P = copy(P0);
Q = copy(Q0);
include("train_sgd.jl")
λ = 0.005
ϵ = 2 * 1e-3
momentum = 0.9

maxiter = 500
itv_test = 1
itv_save = 100
maxsec = Inf

println("momentum:", momentum, " stepsz:", ϵ, " lambda:", λ, " init_factor:", init_factor)
logs_s, Ps, Qs = train_sgd(trainset, validset, P, Q, λ, ϵ, momentum, maxiter, itv_test, itv_save, maxsec)

