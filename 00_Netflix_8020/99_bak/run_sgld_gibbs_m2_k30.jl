include("../utils/types.jl")
include("../train_sgld_gibbs_m2.jl")

# Initialize parmeter
srand(1)
println("Netflix 8020 - SGLD-M2")

## params 
para = Dict{ASCIIString, Any}()
para["eps0"]    = 6e-6
para["tau"]     = 10
para["ka"]      = 0.51
para["K"]       = 100
para["m"]       = 50000
para["rndlen"]  = 50
para["submean"] = true
para["map"]     = true
para["tg_rmse"] = 0.00
if para["map"] 
    para["bthresh"] = 0.00
else
    para["bthresh"] = 0.85
end
para["bt0"]     = 300.0
para["rP"]      = 5.0 
para["rQ"]      = 5.0  
para["rA"]      = 5.0 
para["rB"]      = 5.0 
para["mxiter"]  = 9999999
para["mxsec"]   = 9999999
para["iscale"]  = 0.01
para["itvsv"]   = 100
para["itvtst"]  = 20
para["itvhyp"]  = 50      # sample hyper-params per 'itvhyp' rounds

# Stepsize decreasing function
f_stepsz(t)     = para["eps0"] .* ((1 + t./para["tau"]).^(-para["ka"]))

# model
model0 = Dict{ASCIIString,Any}()
model0["P"] = rand(para["K"], n_users) * para["iscale"]
model0["Q"] = rand(para["K"], n_items) * para["iscale"]
model0["A"] = randn(n_users) * 0.01
model0["B"] = randn(n_items) * 0.01
model0["al0"] = 1.0
model0["bt0"] = para["bt0"]   # important to supress inv_pr_pick_user
model0["prec_r"] = 2.0
model0["prec_P"] = ones(para["K"]) * para["rP"]
model0["prec_Q"] = ones(para["K"]) * para["rQ"]
model0["prec_A"] = para["rA"]
model0["prec_B"] = para["rB"]

model = deepcopy(model0)
model = train_sgld_gibbs_m2(trainset, testset, model, para, trn_n_elem_row, trn_n_elem_col, f_stepsz)

