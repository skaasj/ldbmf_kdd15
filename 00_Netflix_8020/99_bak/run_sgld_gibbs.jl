include("types.jl")

# Initialize parmeter
srand(1)
stepsz = 9 * 1e-6 # current best setting is 3 * 1e-5
println("stepsz:", stepsz)
K = 30 # latent feature dimension

start_from_map = false
if start_from_map 
    using HDF5, JLD
    ret = load("./00_metadata/map_k30_netflix_rmse0925.jld")
    P = ret["P"]
    Q = ret["Q"]
    println("Map loaded and start from MAP")
else
    P0 = rand(K, n_users) * 0.001
    Q0 = rand(K, n_items) * 0.001
    P = copy(P0)
    Q = copy(Q0)
end

include("train_sgld_gibbs.jl")
logs_m, Pm, Qm = train_sgld_gibbs(trainset, validset, n_users, n_items, n_elem_row, n_elem_col, P, Q, stepsz, sz_batch=40000, burnin=0, maxiter=150000, maxsec=Inf, itv_test=1, itv_save=10);

