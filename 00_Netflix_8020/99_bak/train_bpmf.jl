using Debug
using Datetime
using Distributions
using Devectorize
include("mf_common.jl")
include("utils.jl")
include("rng.jl")

function train_bpmf(trainset::Array{Float64,2}, testset::Array{Float64,2}, 
                    user_idx::Array{Array{Int64,1},1}, item_idx::Array{Array{Int64,1},1}, 
                    P::Array{Float64,2}, Q::Array{Float64,2}, 
                    max_sample::Int64=100, itv_save::Int64=100000, 
                    itv_test::Int64=10, maxsec::Int64=50000)

    algo = "BPMF"
    @assert itv_save > itv_test
    @printf("Running %s \n", algo)

    # initialization
    logs = Log(zeros(Int64,0), zeros(Float64,0), zeros(Float64,0))
    const sz_trainset = size(trainset)[1]
    const sz_testset = size(testset)[1]
    const min_rate = minimum(trainset[:,3])
    const max_rate = maximum(trainset[:,3])
    const n_feat = size(P)[1]
    const mean_rating = mean(trainset[:,3])
    const n_users = size(P)[2]
    const n_items = size(Q)[2]
    t = 1        # iter
    cur_str_id = get_str_id(algo, t)
    avg_pred = zeros(Float64,sz_testset)
    
    # init parameters
    beta = 2.
    mu_p = zeros(n_feat)
    mu_q = zeros(n_feat)
    al_p = eye(n_feat)
    al_q = eye(n_feat)

    WI_p = eye(n_feat);
    bt0_p = 2.
    df_p = n_feat
    mu0_p = zeros(n_feat)

    WI_q = eye(n_feat);
    bt0_q = 2.
    df_q = n_feat
    mu0_q = zeros(n_feat)

    # apply what's leant from PMF
    mu_p  = vec(mean(P,2))
    invcov_p = inv(cov(P'))
    mu_q  = vec(mean(Q,2))
    invcov_q = inv(cov(Q'))

    start_idx = 1
    n_sample = 0
    avg_cnt = 0  # counts number of averaged predicions
    ts = 0.0     # timestamp

    println("start sampling ...")
    while n_sample <= max_sample

        tic()
        n_sample += 1

        ##################################################
        # Sample from item hyperparams
        ##################################################
        x_bar = vec(mean(Q,2))
        S_bar = cov(Q')
        WI_post = inv(inv(WI_q) + n_items/1 * S_bar + 
            n_items * bt0_q * (mu0_q - x_bar) * (mu0_q - x_bar)'/(1 * (bt0_q + n_items)))
        WI_post = (WI_post + WI_post') / 2
        df_qmost = df_q + n_items
        wishdist = Wishart(df_qmost, WI_post)
        al_q = rand(wishdist)
        mu_temp = (bt0_q * mu0_q + n_items * x_bar) / (bt0_q + n_items)
        lam = chol(inv((bt0_q + n_items) * al_q)); lam = lam'
        mu_q = lam * randn(n_feat) + mu_temp

        ##################################################
        # Sample from user hyperparams
        ##################################################
        x_bar = vec(mean(P,2))
        S_bar = cov(P')
        WI_post = inv(inv(WI_p) + n_users/1*S_bar + 
            n_users*bt0_p*(mu0_p - x_bar) * (mu0_p - x_bar)'/(1*(bt0_p + n_users)))
        WI_post = (WI_post + WI_post') / 2
        df_pmost = df_p + n_users
        wishdist = Wishart(df_pmost, WI_post)
        al_p = rand(wishdist)
        mu_temp = (bt0_p * mu0_p + n_users * x_bar) / (bt0_p + n_users)
        lam = chol(inv((bt0_p + n_users) * al_p)); lam = lam'
        mu_p = lam * randn(n_feat) + mu_temp

        ##################################################
        # Gibbs Sampling for user and item vectors 
        ##################################################
        for gibbs = 1:2
            for item = 1:n_items
                @inbounds tr_item = trainset[item_idx[item],:]
                @inbounds pp = P[:,tr_item[:,1]]
                rr = tr_item[:,3] - mean_rating
                covar = inv((al_q + beta * pp * pp'))
                mean_q = covar * (beta * pp * rr + al_q * mu_q)
                lam = chol(covar); lam = lam'
                @inbounds Q[:,item] = lam * randn(n_feat) + mean_q
            end
            for user = 1:n_users
                @inbounds tr_user = trainset[user_idx[user],:]
                @inbounds qq = Q[:,tr_user[:,2]]
                rr = tr_user[:,3] - mean_rating
                covar = inv((al_p + beta * qq * qq'))
                mean_p = covar * (beta * qq * rr + al_p * mu_p)
                lam = chol(covar); lam = lam'
                @inbounds P[:,user] = lam * randn(n_feat) + mean_p
            end
        end
        ts += toq()

        # Compute RMSE
        if mod(n_sample, itv_test) == 0
            avg_cnt += 1
            avg_rmse_t, cur_rmse_t = rmse_avg(P, Q, avg_pred, avg_cnt, testset, mean_rating, min_rate, max_rate)
            @printf("%d, %f (%f) (%.2fs) \n", n_sample, avg_rmse_t, cur_rmse_t, ts)
            tic()
            push!(logs.iter, n_sample)
            push!(logs.ts, ts)
            push!(logs.rmse, avg_rmse_t)
            ts += toq()
        end
        
        # Save logs file
        if mod(n_sample, itv_save) == 0
            #@printf("\nsaving loggings ... ")
            #cur_str_id = save_logs(logs, cur_str_id, t, algo)  
            #@printf("done.\n")
            #if ts > maxsec; break
            #end
        end
    end
    logs, P, Q
end



function rmse_avg(P, Q, avg_pred, avg_cnt, testset, mean_rating::Float64, min_rate::Float64, max_rate::Float64)
    sse = 0.0
    avg_sse = 0.0
    sz_testset = size(testset)[1]
    for i in 1:sz_testset
        user::Int, item::Int, rate::Float64 = testset[i,:]
        @inbounds pred = dot(P[:,user], Q[:,item]) + mean_rating
        if pred < min_rate 
            pred = min_rate 
        elseif pred > max_rate
            pred = max_rate
        end
        @inbounds avg_pred[i] = (1-1/avg_cnt) * avg_pred[i] + 1/avg_cnt * pred
        sse += sqr(rate - pred) # for speed, '^2' is avoided
        avg_sse += sqr(rate - avg_pred[i])
    end
    cur_rmse = sqrt(sse/sz_testset)
    avg_rmse = sqrt(avg_sse/sz_testset)
    return avg_rmse, cur_rmse
end


function get_str_id(algo, t)
    now_str = replace(replace(string(Datetime.now(Datetime.PST)), ":",""),"-","")[1:end-4]
    now_str = "$now_str""_np"string(nprocs()-1)"_K"string(K)"_stsz"string(stepsz)"_""$algo""_t""$t"
    now_str = replace(now_str,".","_")
    now_str
end

