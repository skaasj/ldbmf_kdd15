## Requirement ##

Julia 0.3.2

## Running scripts ##

m1 is a model without bias terms a and b in eqn. 31. And, m2 is a model with the bias terms.

* load_netflix_8020.jl => load data
* run_ddsgld_gibbs_m2.jl => DSGLD-S
* run_dsgd_m2.jl => DSGD
* run_dsgld_gibbs_m2.jl => DSGLD-C
* run_find_map_m2.jl => find map using SGD for initialization of BPMF
* run_gibbs_gibbs_m1.jl => BPMF m1
* run_gibbs_gibbs_m2.jl => BPMF m2
* run_sgld_gibbs_m1.jl => SGLD m1
* run_sgld_gibbs_m2.jl => SGLD m2

## Running Netflix Experiments ##

    $ cd 00_Netflix_8020
    $ julia
    julia> include("load_netflix_8020.jl")
    julia> include("run_ddsgld_gibbs_m2.jl")

## Running Yahoo Experiments ##

    $ cd 00_Yahoo
    $ julia
    julia> include("load_y700m.jl")
    julia> include("run_ddsgld_gibbs_m2.jl")

## Updating C wrappers ##

Run compile_clib.sh if you modify c functions.
    
    $ compile_clib.sh