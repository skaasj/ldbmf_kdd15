include("../utils/types.jl")
include("../utils/data_loader.jl")
using HDF5, JLD
v = load("/scratch/DATA/movielens/10M/141225_10m.jld")
trainset        = v["trainset"];   
testset         = v["validset"];
n_users         = v["n_users"];
n_items         = v["n_items"];
trn_n_elem_row  = v["trn_n_elem_row"];
trn_n_elem_col  = v["trn_n_elem_col"];
@printf("Movielens 10M loaded")
