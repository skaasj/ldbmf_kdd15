# Run include("run_load_data.jl") first before this 
include("../22_v0.06_dev/utils/types.jl")
include("../22_v0.06_dev/train_sgd_m2.jl")

# Initialize parmeter
srand(1)
para = Dict{ASCIIString, Any}()
para["K"] = 30
para["eps"] = 0.003
para["iscale"] = 0.1
para["reg"] = 0.005
para["mom"] = 0.9
para["rndlen"] = 1000000
para["itvtst"] = 1
para["itvsv"] = 100
para["mxiter"] = Inf
para["mxsec"] = 9990000

# model
model0 = Dict{ASCIIString,Any}()
model0["P"] = rand(para["K"], n_users) * para["iscale"]
model0["Q"] = rand(para["K"], n_items) * para["iscale"]
model0["A"] = randn(n_users) * 0.01
model0["B"] = randn(n_items) * 0.01
model0["prec_P"] = ones(para["K"]) * para["reg"]
model0["prec_Q"] = ones(para["K"]) * para["reg"]
model0["prec_A"] = para["reg"] * 0.01
model0["prec_B"] = para["reg"] * 0.01

model = deepcopy(model0)
model = train_sgd_m2(trainset, validset, model, para)

