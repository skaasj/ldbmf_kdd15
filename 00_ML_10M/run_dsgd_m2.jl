include("../utils/master_utils.jl")
#include("../25_v0.07_dsgld_mem_opt/utils/types.jl")
#include("../25_v0.07_dsgld_mem_opt/utils/mf_common.jl")
#include("../25_v0.07_dsgld_mem_opt/utils/utils.jl")
#include("../25_v0.07_dsgld_mem_opt/train_ddsgld_gibbs_m2.jl")
#include("../25_v0.07_dsgld_mem_opt/sample_hyper_gibbs_m2.jl")
#include("../25_v0.07_dsgld_mem_opt/utils/master_utils.jl")
n_worker = 16
@time worker_ids = launch_workers(n_worker);

include("../utils/types.jl")
include("../utils/mf_common.jl")
include("../utils/utils.jl")
include("../train_dsgd_m2.jl")
@everywhere include("../update_PQ_dsgd_m2.jl")
@everywhere include("../utils/worker_utils.jl")
@everywhere include("../utils/rng.jl")
#@everywhere include("../25_v0.07_dsgld_mem_opt/utils/worker_utils.jl")
#@everywhere include("../25_v0.07_dsgld_mem_opt/utils/rng.jl")

# init params 
para = Dict{ASCIIString, Any}()
para["S"]       = n_worker              
para["C"]       = 1
para["K"]       = 30
para["rP"]      = 20.0 
para["rQ"]      = 5.0  
para["rA"]      = 2.0 
para["rB"]      = 2.0  
para["nrb"]     = n_worker      # number of row blocks
para["ncb"]     = n_worker      # number of column blocks
para["eps"]     = 9e-7
para["m"]       = 20000
para["rndlen"]  = 30
para["submean"] = true
para["iscale"]  = 0.0001
para["mxiter"]  = 999999
para["itvsv"]   = 200
para["itvtst"]  = 10

sz_trainset = size(trainset,1)
sz_testset  = size(testset,1)
min_rate = float(minimum(trainset[:,3])) 
max_rate = float(maximum(trainset[:,3]))
para["submean"] ? mean_rate = float(mean(trainset[:,3])) : mean_rate = 0.0
#test_rr = deepcopy(testset[:,3])

srand(1)
println("Movielens 10M")
n_block = para["nrb"] * para["ncb"]

@time blk_rng, blk_mbr = partition_matrix(trainset, n_users, n_items, para["nrb"], para["ncb"], n_worker);
@time wkr2blk_map = assign_blocks_to_workers_dsgd(para["nrb"], para["ncb"], n_worker);
@time pcopy_blocks(trainset, blk_mbr, wkr2blk_map, n_worker);

## free memory
#trainset = 0
#blk_mbr = 0
#gc()

@time pcopy_testset(testset);
#testset = 0
#gc()

if !isdefined(:blk_info)
    @time p_result = pmap(p_localize_idx, [[] for s in 1:n_worker]);
    @time blk_info = get_blk_info(p_result, wkr2blk_map);
else
    @printf("Skip p_localize_idx. Already localized.\n")
end

@time inv_pr_pick_user, inv_pr_pick_item = comp_inv_pr_pick(blk_info, n_users, n_items, para["m"]);
@time pcopy_inv_pr_pick(inv_pr_pick_user, inv_pr_pick_item, blk_info, wkr2blk_map);

#inv_pr_pick_user = 0
#inv_pr_pick_item = 0
#gc()

@time post_init_dsgd(blk_info, sz_trainset, wkr2blk_map)

###################################################
# init models
#models0 = Array(Dict{ASCIIString, Any}, para["C"])
#for c in 1:length(models0)
    model = Dict{ASCIIString, Any}()
    model["P"]       = rand(para["K"], n_users) * para["iscale"]
    model["Q"]       = rand(para["K"], n_items) * para["iscale"]
    model["A"]       = randn(n_users) * 0.01
    model["B"]       = randn(n_items) * 0.01
    model["al0"]     = 1.0
    model["bt0"]     = 300.0 
    model["prec_P"]  = ones(para["K"]) * para["rP"]
    model["prec_Q"]  = ones(para["K"]) * para["rQ"]
    model["prec_A"]  = para["rA"]
    model["prec_B"]  = para["rB"]
    model["prec_r"]  = 2.0  
    #models0[c] = model
#end

#preds0 = Array(Dict{ASCIIString, Any}, para["C"])
#for c in 1:length(preds0)
    #pred = Dict{ASCIIString, Any}()
    #pred["avg_rmse"] = 10.0
    #pred["cur_rmse"] = 10.0
    #pred["avg_pred"] = zeros(Float64, sz_testset)
    #pred["avg_cnts"] = 0
    #preds0[c] = pred
#end

# 4x4, 16-wkr-4-chain
#blk_groups = Array[[1,6,11,16] [2,7,12,13] [3,8,9,14] [4,5,10,15]]

function gen_ortho_blk_groups(blk_dim)
    n_blocks = blk_dim * blk_dim
    blk_groups = [Int64[] for d in 1:blk_dim]
    for i=1:blk_dim
        blk_group = Int64[]
        for j=1:blk_dim
            row = j
            col = mod(i+(j-1), blk_dim)
            col == 0 ? col = blk_dim : nothing
            push!(blk_groups[i], rowcol2seqid(row, col, blk_dim))
        end
    end
    return blk_groups
end

blk_groups = gen_ortho_blk_groups(n_worker)

###################################################
## Start learning
#models = deepcopy(models0); preds  = deepcopy(preds0);
#models0 = train_ddsgld_gibbs_m2(models0, preds0, para, min_rate, max_rate, mean_rate, test_rr, blk_info, blk_groups);
###################################################
## Start learning
model_t = deepcopy(model); 
model_t = train_dsgd_m2(model_t, para, min_rate, max_rate, mean_rate, testset, blk_info, blk_groups);

