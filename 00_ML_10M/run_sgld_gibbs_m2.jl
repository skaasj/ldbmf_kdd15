include("../22_v0.06_dev/utils/types.jl")
include("../22_v0.06_dev/train_sgld_gibbs_m2.jl")

# Initialize parmeter
srand(1)
println("Movielens 10M")

## params 
para = Dict{ASCIIString, Any}()
para["K"]       = 30
para["eps"]     = 3e-5
para["m"]       = 20000
para["rndlen"]  = 30
para["bthresh"] = 0.83
para["rP"]      = 2.0 
para["rQ"]      = 2.0  
para["rA"]      = 2.0 
para["rB"]      = 2.0 
para["mxiter"]  = 999999
para["mxsec"]   = 200000
para["iscale"]  = 0.0001
para["itvsv"]   = 200
para["itvtst"]  = 10
para["itvhyp"]  = 10      # sample hyper-params per 'itvhyp' rounds

# model
model0 = Dict{ASCIIString,Any}()
model0["P"] = rand(para["K"], n_users) * para["iscale"]
model0["Q"] = rand(para["K"], n_items) * para["iscale"]
model0["A"] = randn(n_users) * 0.01
model0["B"] = randn(n_items) * 0.01
model0["al0"] = 1.0
model0["bt0"] = 300.0   # important to supress inv_pr_pick_user
model0["prec_r"] = 2.0
model0["prec_P"] = ones(para["K"]) * para["rP"]
model0["prec_Q"] = ones(para["K"]) * para["rQ"]
model0["prec_A"] = para["rA"]
model0["prec_B"] = para["rB"]

model = deepcopy(model0)
model = train_sgld_gibbs_m2(trainset, validset, model, para, trn_n_elem_row, trn_n_elem_col)

