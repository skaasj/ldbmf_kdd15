include("../20_dbmf/utils/types.jl")
include("../20_dbmf/utils/data.jl")

srand(1)
# Load dataset 
#trainset, validset, n_users, n_items, n_elem_row, n_elem_col = load_netflix()
trainset, validset, n_users, n_items, n_elem_row, n_elem_col = load_netflix_train8020()

sz_trainset = size(trainset,1)
sz_validset = size(validset,1)
@assert sum(n_elem_row) == sum(n_elem_col) == sz_trainset
@assert sum(n_elem_row .== 0) == sum(n_elem_col .== 0) == 0
