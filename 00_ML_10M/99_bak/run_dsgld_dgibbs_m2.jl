n_workers = 10
@time worker_ids = launch_workers(para["S"]);

include("../22_v0.06_dev/utils/types.jl")
include("../22_v0.06_dev/utils/mf_common.jl")
include("../22_v0.06_dev/utils/utils.jl")
include("../22_v0.06_dev/utils/master_utils.jl")
include("../22_v0.06_dev/train_dsgld_dgibbs_m2.jl")
#include("../22_v0.06_dev/parameter_server.jl")
@everywhere include("../22_v0.06_dev/utils/worker_utils.jl")
@everywhere include("../22_v0.06_dev/utils/rng.jl")

# init params 
para = Dict{ASCIIString, Any}()
para["S"]       = n_workers                  
para["C"]       = 10                   
para["K"]       = 30
para["G"]       = 1                      
para["eps"]     = 2e-5
para["m"]       = 20000
para["aug"]     = 0.0                    # aug is in a range of [0,1] where 0 = N/S and 1 = N.
para["rndlen"]  = 20
para["bthresh"] = 0.83
para["iscale"]  = 0.0001
#para["mxsec"]   = 999999
para["mxiter"]  = 999999
para["matavg"]  = false
para["itvsv"]   = 100
para["itvtst"]  = 10
para["itvhyp"]  = para["S"] * 5          # sample hyper-params per 'itvhyp' rounds


#@time worker_ids = launch_workers(para["S"]);
#@everywhere include("../22_v0.06_dev/worker_dbmf.jl")
#@everywhere include("../22_v0.06_dev/sample_PQ_hyper_dsgld_dgibbs_m2.jl")

srand(1)
println("Netflix 80/20")

pre_init_workers(size(trainset,1), para)

##################################################
# Preprare parameter server to run
@time blk_rng, blk_mbr = partition_original_set(trainset, n_users, n_items, para["S"], 1, para);
@time Xs_idx, Ns, aug_counts = augment_blocks(blk_mbr, size(trainset,1), para["S"], para["aug"]);
@time pcopy_shards(trainset, Xs_idx, aug_counts);
@time pcopy_testset(validset)

## localize Xs
@time p_result = pmap(p_localize_Xs, [[] for s in 1:para["S"]]);
map_uid_g2l_s = [p_result[s][1] for s in 1:para["S"]];
map_iid_g2l_s = [p_result[s][2] for s in 1:para["S"]];
n_elem_row_s  = [p_result[s][5]::Array{Int64,1} for s in 1:para["S"]];
n_elem_col_s  = [p_result[s][6]::Array{Int64,1} for s in 1:para["S"]];
@time localized_uid_s, Us = get_convert_orders(map_uid_g2l_s);
@time localized_iid_s, Is = get_convert_orders(map_iid_g2l_s);


@time n_overlap_row, n_overlap_col = compute_n_overlap(n_users, n_items, Us, Is)
@time inv_pr_pick_user, inv_pr_pick_item = compute_bias_corrector(size(trainset,1), para["m"], n_users, n_items, localized_uid_s, localized_iid_s, Us, Is, n_elem_row_s, n_elem_col_s);
@time pcopy_inv_pr_pick(inv_pr_pick_user, inv_pr_pick_item, localized_uid_s, localized_iid_s);


function post_init_workers(n_worker::Int64, Ns::Array{Int64,1}, N::Int64)
    Ns_scale = Array(Float64, n_worker)
    for (s,p) in enumerate(procs()[2:end])
        Ns_scale[s] = Ns[s] * n_worker
        remotecall_fetch(p, (arg)->(global _Ns_scale = arg::Float64), Ns[s] * n_worker / N)
    end
end

post_init_workers(para["S"], Ns, size(trainset,1))


###################################################
# init models
models0 = Array(Dict{ASCIIString, Any}, para["C"])
for c in 1:length(models0)
    model = Dict{ASCIIString, Any}()
    model["P"]       = rand(para["K"], n_users) * para["iscale"]
    model["Q"]       = rand(para["K"], n_items) * para["iscale"]
    model["A"]       = randn(n_users) * 0.01 
    model["B"]       = randn(n_items) * 0.01 
    model["al0"]     = 1.0
    model["bt0"]     = 300.0 
    model["prec_P"]  = ones(para["K"]) * 2.0
    model["prec_Q"]  = ones(para["K"]) * 2.0
    model["prec_A"]  = 2.0 
    model["prec_B"]  = 2.0 
    model["prec_r"]  = 2.0  
    models0[c] = model
end

preds0 = Array(Dict{ASCIIString, Any}, para["C"])
for c in 1:length(preds0)
    pred = Dict{ASCIIString, Any}()
    pred["avg_rmse"] = 10.0
    pred["cur_rmse"] = 10.0
    pred["avg_pred"] = zeros(Float64, size(validset,1))
    pred["avg_cnts"] = 0
    preds0[c] = pred
end
###################################################
## Start learning
models = deepcopy(models0); preds  = deepcopy(preds0);
models = train_dsgld_dgibbs_m2(models, preds, para, trainset, validset, localized_uid_s, localized_iid_s, n_overlap_row, n_overlap_col);

