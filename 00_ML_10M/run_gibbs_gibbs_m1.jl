include("../utils/types.jl")
include("../train_gibbs_gibbs_m1.jl")

# Initialize parmeter
srand(1)
println("Movielens 10M")

## params 
para = Dict{ASCIIString, Any}()
para["K"] = 30
para["eps"] = 0.002
para["iscale"] = 0.1
#para["reg"] = 0.01
para["init_La"] = 0.01
para["mom"] = 0.9
para["rndlen"] = 500000
para["itvtst"] = 1
para["itvsv"] = 100
para["mxiter"] = Inf
para["mxsec"] = 9990000

model0 = Dict{ASCIIString,Any}()
model0["P"]         = randn(para["K"], n_users) * para["iscale"]
model0["Q"]         = randn(para["K"], n_items) * para["iscale"]
model0["mu_p"]      = zeros(para["K"])
model0["mu_q"]      = zeros(para["K"])   
model0["La_p"]      = para["init_La"] * eye(para["K"])
model0["La_q"]      = para["init_La"] * eye(para["K"])
model0["La_p_chol"] = chol(model0["La_p"])
model0["La_q_chol"] = chol(model0["La_q"])
model0["mu0_p"]     = zeros(para["K"])
model0["mu0_q"]     = zeros(para["K"])
model0["bt0_p"]     = 2.0
model0["bt0_q"]     = 2.0
model0["prec_r"]    = 2.0
## model
#model0 = Dict{ASCIIString,Any}()
#model0["P"] = rand(para["K"], n_users) * para["iscale"]
#model0["Q"] = rand(para["K"], n_items) * para["iscale"]
#model0["A"] = randn(n_users) * 0.01
#model0["B"] = randn(n_items) * 0.01
#model0["al0"] = 1.0
#model0["bt0"] = 1.0   # important to supress inv_pr_pick_user
#model0["prec_r"] = 2.0
#model0["prec_P"] = ones(para["K"]) * para["reg"]
#model0["prec_Q"] = ones(para["K"]) * para["reg"]
#model0["prec_A"] = para["reg"] * 0.01
#model0["prec_B"] = para["reg"] * 0.01

# run SGD to find a MAP
model = deepcopy(model0)
include("../train_sgd_m1.jl")
#para["mxiter"] = 20 #opt = 20
para["mxiter"] = 100
train_sgd_m1(trainset, testset, model, para)
#model = load("map_sgd_k30_m1.jl")["model"]

include("../utils/data_loader.jl")
user_index, item_index = build_user_item_index_map(trainset, n_users, n_items)

## run BPMF
para["mxiter"] = 999999
para["bthresh"] = 0.95
para["itv_gibbs"] = 2
model = train_gibbs_gibbs_m1(trainset, testset, model, para, user_index, item_index)

