include("../utils/types.jl")
include("../train_sgd2_mom_m1.jl")

# Initialize parmeter
srand(1)
println("Movielens 10M")

## params 
para = Dict{ASCIIString, Any}()
para["K"]       = 30
para["eps"]     = 1e-10
para["mom"]     = 0.9
para["m"]       = 20000
para["rndlen"]  = 20
para["bthresh"] = 0.83
para["init_La"] = 0.000001
para["submean"] = true
para["mxiter"]  = 999999
para["mxsec"]   = 200000
para["iscale"]  = 0.1
para["itvsv"]   = 200
para["itvtst"]  = 10
para["itvhyp"]  = 20       # sample hyper-params per 'itvhyp' rounds
para["map"]     = false

# model
model0 = Dict{ASCIIString,Any}()
model0["P"]         = randn(para["K"], n_users) * para["iscale"]
model0["Q"]         = randn(para["K"], n_items) * para["iscale"]
model0["mu_p"]      = zeros(para["K"])
model0["mu_q"]      = zeros(para["K"])   
model0["mu0_p"]     = zeros(para["K"])
model0["mu0_q"]     = zeros(para["K"])
model0["La_p"]      = para["init_La"] * eye(para["K"])
model0["La_q"]      = para["init_La"] * eye(para["K"])
model0["La_p_chol"] = chol(model0["La_p"])
model0["La_q_chol"] = chol(model0["La_q"])
model0["bt0_p"]     = 2.0
model0["bt0_q"]     = 2.0
model0["prec_r"]    = 2.0

model = deepcopy(model0)
model = train_sgd2_mom_m1(trainset, testset, model, para, trn_n_elem_row, trn_n_elem_col)

