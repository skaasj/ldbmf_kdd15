include("../22_v0.06_dev/utils/types.jl")
include("../22_v0.06_dev/train_sgld_austMH_m2.jl")

# Initialize parmeter
srand(1)
println("Movielens 10M")

## params 
para = Dict{ASCIIString, Any}()
para["K"]       = 30
para["eps"]     = 3e-5
para["m"]       = 20000
para["rndlen"]  = 10
para["bthresh"] = 0.86
para["mxiter"]  = 999999
para["mxsec"]   = 200000
para["iscale"]  = 0.0001
para["itvsv"]   = 200
para["itvtst"]  = 10
para["itvhyp"]  = 10       # sample hyper-params per 'itvhyp' rounds
para["map"]     = false

# paras for austerity
aust_para = Dict{ASCIIString, Any}()
aust_para["tthresh"]  = 1.2815516  # eps_MH = 0.1  ==> tthresh = 1.2815516
aust_para["sz_batch"] = 500
#aust_para["init_La"]  = 3
#aust_para["eps_mu_p"] = 1e-4
#aust_para["eps_mu_q"] = 1e-3
#aust_para["eps_La_p"] = 6e-10
#aust_para["eps_La_q"] = 3e-12

#aust_para["init_La"]  = 1e-6
#aust_para["eps_mu_p"] = 0.5
#aust_para["eps_mu_q"] = 1.0
#aust_para["eps_La_p"] = 6e-7
#aust_para["eps_La_q"] = 3e-10

# model
model0 = Dict{ASCIIString,Any}()
model0["P"] = rand(para["K"], n_users) * para["iscale"]
model0["Q"] = rand(para["K"], n_items) * para["iscale"]
model0["A"] = randn(n_users) * 0.01
model0["B"] = randn(n_items) * 0.01
model0["al0"] = 1.0
model0["bt0"] = 1000.0   # important to supress inv_pr_pick_user
model0["prec_r"] = 2.0
model0["prec_P"] = ones(para["K"]) * 2.0
model0["prec_Q"] = ones(para["K"]) * 2.0
model0["prec_A"] = 2.0
model0["prec_B"] = 2.0

model = deepcopy(model0)
model = train_sgld_austMH_m2(trainset, validset, model, para, aust_para, trn_n_elem_row, trn_n_elem_col)

