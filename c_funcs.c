#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "./c_matrix.h"
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/*
 *double drand()   [> uniform distribution, (0..1] <]
 *{
 *    return (rand()+1.0)/(RAND_MAX+1.0);
 *}
 *
 *double random_normal()  [> normal distribution, centered on 0, std dev 1 <]
 *{
 *    return sqrt(-2*log(drand())) * cos(2*M_PI*drand());
 *}
 *
 */

double dot_product(double* a, double* b, int n)
{
    double result = 0.0;
    for (int i = 0; i < n; i++) {
        result += a[i] * b[i];
    }
    return result;
}


//@time R_AB = R - A[uu] - B[ii]
//@time c_comp_R_AB_bpmf!(R_AB, R, A, B, uu, ii);
void c_comp_R_AB_bpmf(double* R_AB, double* R, double* A, double* B, int* uu, int* ii, int N)
{
    for (int i=0; i < N; i++) 
        R_AB[i] = R[i] - A[uu[i]-1] - B[ii[i]-1];
}

//#@time XX = C - B[ii]
void c_comp_XX_bpmf(double* XX, double* C, double* B, int* ii, int N)
{
    for (int i=0; i < N; i++)
        XX[i] = C[i] - B[ii[i]-1];
}

void c_comp_C_bpmf(double* C, double* R, double* P, double* Q, 
                   int* uu, int* ii, int N, int D)
{
    int idx_u, idx_i;
    for (int i = 0; i < N; i++)
    {
        idx_u = D * (uu[i] - 1);
        idx_i = D * (ii[i] - 1);
        C[i] = R[i] - dot_product(&P[idx_u], &Q[idx_i], D);
    }
}

void c_comp_error(double* error, double* rr, double mean_rate, double* P, double* Q, double* A, double* B, 
                  int* uu, int* ii, int size, int D)
{
    int idx_u, idx_i;
    /*double dotval;*/
    for (int i = 0; i < size; i++) 
    {
        idx_u = D * (uu[i] - 1);
        idx_i = D * (ii[i] - 1);
        error[i] = (rr[i] - mean_rate) - (dot_product(&P[idx_u], &Q[idx_i], D) + A[uu[i]-1] + B[ii[i] - 1]);
    }
}

void c_comp_error_m1(double* error, double* rr, double mean_rate, double* P, double* Q, 
                  int* uu, int* ii, int size, int D)
{
    int idx_u, idx_i;
    /*double dotval;*/
    for (int i = 0; i < size; i++) 
    {
        idx_u = D * (uu[i] - 1);
        idx_i = D * (ii[i] - 1);
        error[i] = (rr[i] - mean_rate) - dot_product(&P[idx_u], &Q[idx_i], D);
    }
}


void c_comp_grad_sum(double* error, int* uu, int* ii, 
                     double* P, double* Q, double* A, double* B, 
                     double* g_sum_P, double* g_sum_Q, 
                     double* g_sum_A, double* g_sum_B, int m, int D)
{
    int uu_i, ii_i, uu_i_d_idx, ii_i_d_idx;
    double err_i;
    for (int i = 0; i < m; i++) {
        uu_i  = uu[i] - 1;
        ii_i  = ii[i] - 1;
        err_i = error[i];
        for (int d = 0; d < D; d++) {
            uu_i_d_idx = D * uu_i + d;
            ii_i_d_idx = D * ii_i + d;
            g_sum_P[uu_i_d_idx] = g_sum_P[uu_i_d_idx] + (err_i * Q[ii_i_d_idx]);
            g_sum_Q[ii_i_d_idx] = g_sum_Q[ii_i_d_idx] + (err_i * P[uu_i_d_idx]);
        }
        g_sum_A[uu_i] += err_i;
        g_sum_B[ii_i] += err_i;
    }
}

void c_comp_grad_sum_m1(double* error, int* uu, int* ii, double* P, double* Q, double* g_sum_P, double* g_sum_Q, int m, int D)
{
    int uu_i, ii_i, uu_i_d_idx, ii_i_d_idx;
    double err_i;
    for (int i = 0; i < m; i++) {
        uu_i  = uu[i] - 1;
        ii_i  = ii[i] - 1;
        err_i = error[i];
        for (int d = 0; d < D; d++) {
            uu_i_d_idx = D * uu_i + d;
            ii_i_d_idx = D * ii_i + d;
            g_sum_P[uu_i_d_idx] = g_sum_P[uu_i_d_idx] + (err_i * Q[ii_i_d_idx]);
            g_sum_Q[ii_i_d_idx] = g_sum_Q[ii_i_d_idx] + (err_i * P[uu_i_d_idx]);
        }
    }
}


/*
 *void c_comp_grad_sum_pmf_m1(double* error, int* uu, int* ii, double* P, double* Q, double* g_sum_P, double* g_sum_Q, int m, int D)
 *{
 *    int uu_i, ii_i, uu_i_d_idx, ii_i_d_idx;
 *    double err_i;
 *    for (int i = 0; i < m; i++) {
 *        uu_i  = uu[i] - 1;
 *        ii_i  = ii[i] - 1;
 *        err_i = error[i];
 *        for (int d = 0; d < D; d++) {
 *            uu_i_d_idx = D * uu_i + d;
 *            ii_i_d_idx = D * ii_i + d;
 *            g_sum_P[uu_i_d_idx] = g_sum_P[uu_i_d_idx] + (err_i * Q[ii_i_d_idx]);
 *            g_sum_Q[ii_i_d_idx] = g_sum_Q[ii_i_d_idx] + (err_i * P[uu_i_d_idx]);
 *        }
 *    }
 *}
 */

void c_update_para(int* ux, double* P, double* g_sum_P, double* prec_P, double* inv_pr_pick, double* rands,
                   double ka, double halfstepsz, double sqrtstepsz, int len_ux, int dim)
{
    int idx_u;
    int rnd_idx = 0;
    double inv_pr_pick_i;
    for (int i = 0; i < len_ux; i++) {
        idx_u = dim * (ux[i] - 1);
        inv_pr_pick_i = inv_pr_pick[ux[i]-1];
        for (int k = 0; k < dim; k++) {
            P[idx_u + k] += halfstepsz * (ka * g_sum_P[idx_u + k] - prec_P[k] * inv_pr_pick_i * P[idx_u + k]) + sqrtstepsz * rands[rnd_idx++];            
            g_sum_P[idx_u + k] = 0.0;  // initialize!
        }
    }
}

void c_update_para_m1(int* ux, double* P, double* g_sum_P, double* mu_p, double* La_p, double* inv_pr_pick, double* rands, double ka, double halfstepsz, double sqrtstepsz, int len_ux, int dim, int batch_sz)
{
    int idx_u;
    int rnd_idx = 0;
    double grad_prior_ik;
    double inv_pr_pick_i;
    for (int i = 0; i < len_ux; i++) {
        idx_u = dim * (ux[i] - 1);
        inv_pr_pick_i = inv_pr_pick[ux[i]-1];
        for (int k = 0; k < dim; k++) {
            grad_prior_ik = 0.0;
            for (int j = 0; j < dim; j++) {
                grad_prior_ik += La_p[j*dim + k] * (P[idx_u + j] - mu_p[j]);
            }
            P[idx_u + k] += (halfstepsz * (ka * g_sum_P[idx_u + k] - inv_pr_pick_i * grad_prior_ik) + sqrtstepsz * rands[rnd_idx++]) ; 
            g_sum_P[idx_u + k] = 0.0;  // initialize!
        }
    }
}


void c_update_para_sgd(int* ux, double* P, double* g_sum_P, double* prec_P, double* inv_pr_pick, 
                       double ka, double halfstepsz, int len_ux, int dim)
{
    int idx_u; 
    double inv_pr_pick_i;
    for (int i = 0; i < len_ux; i++) {
        idx_u = dim * (ux[i] - 1);
        inv_pr_pick_i = inv_pr_pick[ux[i]-1];
        for (int k = 0; k < dim; k++) {
            P[idx_u + k] += halfstepsz * (ka * g_sum_P[idx_u + k] - prec_P[k] * inv_pr_pick_i * P[idx_u + k]);
            g_sum_P[idx_u + k] = 0.0;  // initialize!
        }
    }
}

void c_update_para_sgd_m1(int* ux, double* P, double* g_sum_P, double* mu_p, double* La_p, double* inv_pr_pick, double ka, double halfstepsz, int len_ux, int dim, int batch_sz)
{
    int idx_u; 
    /*double grad_prior_ik;*/
    double inv_pr_pick_i;
    for (int i = 0; i < len_ux; i++) {
        idx_u = dim * (ux[i] - 1);
        inv_pr_pick_i = inv_pr_pick[ux[i]-1];
        for (int k = 0; k < dim; k++) {
            /*
             *grad_prior_ik = 0;
             *for (int j = 0; j < dim; j++) {
             *    grad_prior_ik += La_p[j*dim + k] * (P[idx_u + j] - mu_p[j]);
             *}
             */
            /*P[idx_u + k] += halfstepsz * (ka * g_sum_P[idx_u + k] - inv_pr_pick_i * grad_prior_ik) ;  */
            P[idx_u + k] += halfstepsz * (ka * g_sum_P[idx_u + k] - inv_pr_pick_i * La_p[k*dim + k] * (P[idx_u + k] - mu_p[k])) ;  
            g_sum_P[idx_u + k] = 0.0;  // initialize!
        }
    }
}

/*
 *void c_update_para_sgd_m1_(int* ux, double* P, double* g_sum_P, double* grad_prior_P, double* inv_pr_pick, double ka, double halfstepsz, int len_ux, int dim, int batch_sz)
 *{
 *    int idx_u, idx_uk;
 *    [>double grad_prior_ik;<]
 *    double inv_pr_pick_i;
 *    for (int i = 0; i < len_ux; i++) {
 *        idx_u = dim * (ux[i] - 1);
 *        inv_pr_pick_i = inv_pr_pick[ux[i]-1];
 *        for (int k = 0; k < dim; k++) {
 *            [>grad_prior_ik = 0;<]
 *            for (int j = 0; j < dim; j++) {
 *                grad_prior_ik += La_p[j*dim + k] * (P[idx_u + j] - mu_p[j]);
 *            }
 *            idx_uk = idx_u + k;
 *            P[idx_uk] += halfstepsz * (ka * g_sum_P[idx_uk] - inv_pr_pick_i * grad_prior_P[idx_uk]);  
 *            g_sum_P[idx_uk] = 0.0;  // initialize!
 *        }
 *    }
 *}
 */


void c_update_para_sgd_mom_m1(int* ux, double* P, double* g_mom_P, double* g_sum_P, double* mu_p, double* La_p, double ka, double halfstepsz, int len_ux, int dim, int batch_sz, float mom_rate)
{
    int idx_u; 
    double grad_prior_ik;
    /*double inv_pr_pick_i;*/
    for (int i = 0; i < len_ux; i++) {
        idx_u = dim * (ux[i] - 1);
        /*inv_pr_pick_i = inv_pr_pick[ux[i]-1];*/
        for (int k = 0; k < dim; k++) {
            grad_prior_ik = 0;
            for (int j = 0; j < dim; j++) {
                grad_prior_ik += La_p[j*dim + k] * (P[idx_u + j] - mu_p[j]);
            }
            // update gradient momentum 
            g_mom_P[idx_u + k] = mom_rate * g_mom_P[idx_u + k] + halfstepsz * (ka * g_sum_P[idx_u + k] - grad_prior_ik);  
            // update parameter
            P[idx_u + k] += g_mom_P[idx_u + k];
            g_sum_P[idx_u + k] = 0.0;  // initialize!
        }
    }
}


void c_rmse_avg_m2(double* testset, double* P, double* Q, double* A, double* B, double* avg_pred, int iter, double min_rate, double max_rate, double mean_rate, int is_burnin, int sz_testset, int dim, double* avg_rmse, double* cur_rmse, int* avg_cnt)
{
    double r_i, p_i, tmp;
    int u_i, i_i;

    if (!is_burnin) avg_cnt[0] += 1;

    for (int i = 0; i < sz_testset; i++) {
        u_i = testset[i] - 1;
        i_i = testset[sz_testset + i] - 1;
        r_i = testset[2*sz_testset + i];
        p_i = dot_product(&P[dim * u_i], &Q[dim * i_i], dim) + A[u_i] + B[i_i] + mean_rate;
        if (p_i < min_rate) p_i = min_rate;
        if (p_i > max_rate) p_i = max_rate;
        if (!is_burnin) {
            avg_pred[i] = (1 - 1./avg_cnt[0]) * avg_pred[i] + 1./avg_cnt[0] * p_i;
            tmp = r_i - avg_pred[i];
            avg_rmse[0] += tmp * tmp;
        } 
        else {
            avg_pred[i] = p_i;
        }
        tmp = r_i - p_i;
        cur_rmse[0] += tmp * tmp;
    }
    avg_rmse[0] = sqrt(avg_rmse[0] / sz_testset);
    cur_rmse[0] = sqrt(cur_rmse[0] / sz_testset);
}


void c_rmse_avg_m1(double* testset, double* P, double* Q, double* avg_pred, int iter, double min_rate, double max_rate, double mean_rate, int is_burnin, int sz_testset, int dim, double* avg_rmse, double* cur_rmse, int* avg_cnt)
{
    double r_i, p_i, tmp;
    int u_i, i_i;

    if (!is_burnin) avg_cnt[0] += 1;

    for (int i = 0; i < sz_testset; i++) {
        u_i = testset[i] - 1;
        i_i = testset[sz_testset + i] - 1;
        r_i = testset[2*sz_testset + i];
        p_i = dot_product(&P[dim * u_i], &Q[dim * i_i], dim) + mean_rate;
        if (p_i < min_rate) p_i = min_rate;
        if (p_i > max_rate) p_i = max_rate;
        if (!is_burnin) {
            avg_pred[i] = (1 - 1./avg_cnt[0]) * avg_pred[i] + 1./avg_cnt[0] * p_i;
            tmp = r_i - avg_pred[i];
            avg_rmse[0] += tmp * tmp;
        } 
        else {
            avg_pred[i] = p_i;
        }
        tmp = r_i - p_i;
        cur_rmse[0] += tmp * tmp;
    }
    avg_rmse[0] = sqrt(avg_rmse[0] / sz_testset);
    cur_rmse[0] = sqrt(cur_rmse[0] / sz_testset);
}
