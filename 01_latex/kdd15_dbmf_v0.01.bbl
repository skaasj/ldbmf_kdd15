\begin{thebibliography}{10}

\bibitem{yahoomusic_r2}
R2 - yahoo! music user ratings of songs with artist, album, and genre meta
  information, v. 1.0 (1.4 gbyte and 1.1 gbyte).
\newblock http://webscope.sandbox.yahoo.com/.

\bibitem{adams10incorporating}
R.~Adams, G.~Dahl, and I.~Murray.
\newblock Incorporating side information in probabilistic matrix factorization
  with gaussian processes.
\newblock In {\em Proceedings of the 26th Conference on Uncertainty in
  Artificial Intelligence}, 2010.

\bibitem{AhnKorattikaraWelling12}
S.~Ahn, A.~Korattikara, and M.~Welling.
\newblock Bayesian posterior sampling via stochastic gradient fisher scoring.
\newblock In {\em International Conference on Machine Learning}, 2012.

\bibitem{AhnShaWel14}
S.~Ahn, B.~Shahbaba, and M.~Welling.
\newblock Distributed stochastic gradient mcmc.
\newblock In {\em International Conference on Machine Learning (ICML)}, 2014.

\bibitem{netflix}
J.~Bennett and S.~Lanning.
\newblock The netflix prize.
\newblock {\em In KDD Cup and Workshop in conjunction with KDD}, 2007.

\bibitem{bez14julia}
J.~Bezanson, A.~Edelman, S.~Karpinski, and V.~B. Shah.
\newblock Julia: {A} fresh approach no numerical computing.
\newblock {\em CoRR}, 2014.
\newblock http://dblp.uni-trier.de/rec/bib/journals/corr/BezansonEKS14.

\bibitem{tianqi14sghmc}
T.~Chen, E.~Fox, and C.~Guestrin.
\newblock Stochastic gradient hamiltonian monte carlo.
\newblock In {\em International Conference on Machine Learning (ICML)}, 2014.

\bibitem{dingfang14sgnht}
N.~Ding, Y.~Fang, R.~Babbush, C.~Chen, R.~Skeel, and H.~Neven.
\newblock Bayesian sampling using stochastic gradient thermostats.
\newblock In {\em Advances in Neural Information Processing Systems (NIPS)},
  2014.

\bibitem{dror12kddcup11}
G.~Dror, N.~Koenigstein, Y.~Koren, and M.~Weimer.
\newblock The yahoo! music dataset and kdd-cup'11.
\newblock In {\em Proceedings of KDD-Cup 2011 competition}, 2012.

\bibitem{Duane87}
S.~Duane, A.~Kennedy, B.~Pendleton, and D.~Roweth.
\newblock Hybrid monte carlo.
\newblock {\em Physics letters B}, 195(2):216--222, 1987.

\bibitem{GemanGeman84}
S.~Geman and D.~Geman.
\newblock Stochastic relaxation, {G}ibbs distributions, and the {B}ayesian
  restoration of images.
\newblock {\em IEEE Transactions on Pattern Analysis and Machine Intelligence},
  6:721--741, 1984.

\bibitem{gemulla11large}
R.~Gemulla, E.~Nijkamp, P.~Haas, and Y.~Sismanis.
\newblock Large-scale matrix factorization with distributed stochastic gradient
  descent.
\newblock In {\em Proceedings of the 17th ACM SIGKDD international conference
  on Knowledge discovery and data mining}, 2011.

\bibitem{GirolamiCalderhead10}
M.~Girolami and B.~Calderhead.
\newblock Riemann manifold langevin and hamiltonian monte carlo.
\newblock {\em Journal of the Royal Statistical Society B}, 73 (2):1--37, 2010.

\bibitem{hall10mapreduce}
K.~B. Hall, S.~Gilpin, and G.~Mann.
\newblock Mapreduce/bigtable for distributed optimization.
\newblock In {\em NIPS LCCC Workshop}, 2010.

\bibitem{korattikara14austerity}
A.~Korattikara, Y.~Chen, and M.~Welling.
\newblock Austerity in mcmc land: Cutting the metropolis-hastings budget.
\newblock In {\em International Conference on Machine Learning (ICML)}, 2014.

\bibitem{koren09mf}
Y.~Koren, R.~Bell, and C.~Volinsky.
\newblock Matrix factorization techniques for recommender systems.
\newblock In {\em IEEE Computer}, 2009.

\bibitem{li14communication}
M.~Li, D.~Andersen, A.~Smola, and K.~Yu.
\newblock Communication efficient distributed machine learning with the
  parameter server.
\newblock In {\em Advances in Neural Information Processing Systems}, 2014.

\bibitem{Li13parameter}
M.~Li, L.~Zhou, Z.~Yang, A.~Li, F.~Xia, D.~Andersen, and A.~Smola.
\newblock Parameter server for distributed machine learning.
\newblock In {\em Big Learning NIPS Workshop}, 2013.

\bibitem{mann09effcient}
G.~Mann, R.~McDonald, M.~Mohri, N.~Silberman, and D.~Walker.
\newblock Efficient large-scale distributed training of conditional maximum
  entropy models.
\newblock In {\em Neural Information Processing Systems}, 2009.

\bibitem{mcdonald10distributed}
R.~McDonald, K.~Hall, and G.~Mann.
\newblock Distributed training strategies for the structured perceptron.
\newblock In {\em HLT}, 2010.

\bibitem{andriy07pmf}
A.~Mnih and R.~Salakhutdinov.
\newblock Probabilistic matrix factorization.
\newblock In {\em Advances in Neural Information Processing Systems}, 2007.

\bibitem{Neal93}
R.~Neal.
\newblock Probabilistic inference using markov chain monte carlo methods.
\newblock Technical Report CRG-TR-93-1, University of Toronto, Computer
  Science, 1993.

\bibitem{Neal11}
R.~Neal.
\newblock Mcmc using hamiltonian dynamics.
\newblock In S.~Brooks, A.~Gelman, G.~Jones, and X.~Meng, editors, {\em
  Handbook of Markov Chain Monte Carlo}. Chapman\&Hall/CRC, 2011.

\bibitem{niu2011hogwild}
F.~Niu, B.~Recht, C.~R{\'e}, and S.~J. Wright.
\newblock Hogwild!: A lock-free approach to parallelizing stochastic gradient
  descent.
\newblock {\em arXiv preprint arXiv:1106.5730}, 2011.

\bibitem{PatTeh13sgrld}
S.~Patterson and Y.~W. Teh.
\newblock Stochastic gradient riemannian langevin dynamics on the probability
  simplex.
\newblock In {\em Advances in Neural Information Processing Systems}, 2013.

\bibitem{PorteousAsuncionWelling10}
I.~Porteous, A.~Ascuncion, and M.~Welling.
\newblock Bayesian matrix factorization with side information and dirichlet
  process mixtures.
\newblock In {\em AAAI Conference on Artificial Intelligence}, 2010.

\bibitem{recht13parallel}
B.~Recht and C.~Re.
\newblock Parallel stochastic gradient algorithms for large-scale matrix
  completion.
\newblock In {\em Mathematical Programming Computation}, 2013.

\bibitem{rossky78mala}
P.~Rossky, J.~Doll, and H.~Friedman.
\newblock Brownian dynamics as smart monte carlo simulation.
\newblock In {\em The Journal of Chemical Physics}, 1978.

\bibitem{ruslan08bpmf}
R.~Salakhutdinov and A.~Mnih.
\newblock Bayesian probabilistic matrix factorization using markov chain monte
  carlo.
\newblock In {\em Proceedings of the 25th International Conference on Machine
  Learning (ICML)}, 2008.

\bibitem{christina12distributed}
C.~Teflioudi, F.~Makari, and R.~Gemulla.
\newblock Distributed matrix completion.
\newblock In {\em IEEE 12th International Conference on Data Mining}, 2012.

\bibitem{WellingTeh11}
M.~Welling and Y.~W. Teh.
\newblock Bayesian learning via stochastic gradient langevin dynamics.
\newblock In {\em International Conference on Machine Learning (ICML)}, 2011.

\bibitem{zhuang13fast}
Y.~Zhuang, W.~S. Chin, Y.~C. Juan, and C.~J. Lin.
\newblock A fast parallel sgd for matrix factorizatio in shared memory systems.
\newblock In {\em Proceedings of the 7th ACM conference on Recommender
  systems}, 2013.

\bibitem{zinkevich10psgd}
M.~Zinkevich, M.~Weimer, and A.~Smola.
\newblock Parallelized stochastic gradient descent.
\newblock In {\em Neural Information Processing Systems}, 2010.

\end{thebibliography}
