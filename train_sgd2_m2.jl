using Debug
using Datetime
using Devectorize
using Distributions
using NumericExtensions
include("./utils/types.jl")
include("./utils/utils.jl")
include("./utils/mf_common.jl")
include("./update_PQ_sgd2_m2.jl")
include("./c_wrappers.jl")

function train_sgd2_m2{T}(trainset::Array{T,2}, 
                          testset::Array{T,2}, 
                          model::Dict{ASCIIString,Any},
                          para::Dict{ASCIIString, Any},
                          n_elem_row::Array{Int64,1}, 
                          n_elem_col::Array{Int64,1} 
                          )
    para["algo"] = "SGD2-2"

    eps::Float64    = para["eps"] 
    m::Int64        = para["m"] 
    rndlen::Int64   = para["rndlen"]
    mxiter::Int64   = para["mxiter"]
    mxsec::Int64    = para["mxsec"] 
    itvsv::Int64    = para["itvsv"]
    itvtst::Int64   = para["itvtst"]
    n_users = size(model["P"],2)
    n_items = size(model["Q"],2)

    println(para)
    @printf("Running %s\n", para["algo"])
    @assert itvsv >= itvtst
    log_dir = "00_results_"para["algo"]
    #log_dir = "/scratch/sungjin/00_projects/00_dbmf/004_neflix_train8020/00_results_"para["algo"]
    cur_logfile = get_str_id2(para)
    #logs = Log2(zeros(Int64,0), zeros(0), zeros(0), zeros(0,0), zeros(0,0), para)
    logs = Dict{ASCIIString,Any}()
    logs["iter"] = Int64[]
    logs["rmse"] = Float64[]
    logs["ts"]   = Float64[]

    # initialization
    min_rate    = float(minimum(trainset[:,3]))
    max_rate    = float(maximum(trainset[:,3]))
    para["submean"] ? mean_rate = mean(trainset[:,3]) : mean_rate = 0.0
    sz_trainset = size(trainset)[1]
    sz_testset  = size(testset)[1]
    K           = size(model["P"],1)

    if rndlen > sz_trainset
        rndlen = sz_trainset
        para["rndlen"] = sz_trainset
        @printf("rndlen is truncated to sz_trainset\n")
    end

    #P_star, Q_star = model["P"], model["Q"]

    round = 1     # round
    ts = 0.0     # timestamp
    best_rmse = 100.0
    preds = zeros(Float64,sz_testset)
    
    inv_pr_pick_user = 1 ./ (1 - (1 - n_elem_row ./ size(trainset,1)) .^ para["m"])
    inv_pr_pick_item = 1 ./ (1 - (1 - n_elem_col ./ size(trainset,1)) .^ para["m"])

    is_burnin = true

    while round <= mxiter && ts < mxsec
    
        tic()
        ######################################################################## 
        
        update_PQ_sgd2_m2!(model, trainset, para, mean_rate, inv_pr_pick_user, inv_pr_pick_item) 
        
        ########################################################################
        ts += toq();
        # Compute RMSE and store logs
        if mod(round, itvtst) == 0

            avg_rmse_t, cur_rmse_t, avg_cnt = c_rmse_avg_m2!(testset, round, model["P"], model["Q"], model["A"], model["B"], preds, 0, min_rate, max_rate, mean_rate, true)
            
            #cur_rmse_t = rmse_m1(model["P"], model["Q"], model["A"], model["B"], model["prec_P"], model["prec_Q"], model["prec_A"], model["prec_B"], testset, min_rate, max_rate, mean_rate)
            @printf("t:%d, rmse:%.4f (%.2f sec.) ", round, cur_rmse_t, ts)
            if cur_rmse_t < best_rmse
                best_rmse = cur_rmse_t
                print("(**)")
            end
            println()
            
            push!(logs["iter"], round)
            push!(logs["ts"], ts)
            push!(logs["rmse"], cur_rmse_t)

            para["iter"] = round; 
            para["ts"]   = ts; 
            para["best"] = best_rmse

            logs["para"] = para 
            #logs["pred"] = preds
        end
        
        # Save logs to file
        if mod(round, itvsv) == 0
            @printf("saving logs ... ")
            para["iter"] = round;  para["ts"] = ts
            cur_logfile = save_logs4(logs, log_dir, cur_logfile)
            @printf("done.\n")
            if ts > mxsec
                @printf("mxsec completed!\n")
                return
            end
            if logs["rmse"][end] <= para["bthresh"]
                @printf("bthresh reached!\n")
                return
            end
        end   
        
        round += 1

    end
    return nothing
end


