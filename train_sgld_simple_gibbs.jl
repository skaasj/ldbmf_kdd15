using Debug
using Datetime
using Devectorize
using Distributions
using NumericExtensions
include("utils/utils.jl")
include("utils/mf_common.jl")

function train_sgld_simple_gibbs{T}(trainset::Array{T,2}, testset::Array{T,2}, 
                                    n_users::Int64, n_items::Int64, 
                                    n_elem_row::Array{Int64,1}, n_elem_col::Array{Int64,1}, 
                                    P::Array{Float64,2}, Q::Array{Float64,2}, 
                                    para::Dict{ASCIIString, Any})
    para["algo"] = "SGLD_Simple_Gibbs"
    stepsz::Float64 = para["stepsz"] 
    sz_batch::Int64 = para["sz_batch"] 
    round_len::Int64 = para["round_len"]
    burnin::Int64 = para["burnin"]
    maxiter = para["maxiter"]
    maxsec = para["maxsec"] 
    itv_save = para["itv_save"]
    itv_test = para["itv_test"]
    
    println(para)
    @printf("Running %s (v0.04)\n", para["algo"])
    @assert itv_save >= itv_test
    log_dir = "00_results_"para["algo"]
    cur_logfile = get_str_id2(para)

    # initialization
    logs = Log2(zeros(Int64,0), zeros(Float64,0), zeros(Float64,0), P, Q, para)
    const min_rate = float(minimum(trainset[:,3]))
    const max_rate = float(maximum(trainset[:,3]))
    const mean_rate = mean(trainset[:,3])
    const sz_trainset = size(trainset)[1]
    const sz_testset = size(testset)[1]
    const K = size(P,1)

    iter = 1     # iter
    avg_cnt = 1  # counts number of averaged predicions
    ts = 0.0     # timestamp
    best_rmse = 100.0
    avg_pred = zeros(Float64,sz_testset)


    # initial hyperparams
    alpha = 2.0   # r_ui ~ N(Pu'Qi, alpha^inv)
    alpha0, beta0 = 1.0, 1.0
    prec_P = rand(InverseGamma(alpha0, 1/beta0), K)
    prec_Q = rand(InverseGamma(alpha0, 1/beta0), K)
    const alpha_P = alpha0 + 0.5 * n_users
    const alpha_Q = alpha0 + 0.5 * n_items

    while iter <= maxiter && ts < maxsec
    
        tic()
        ########################################################################
        # Sample P and Q using SGLD
        P, Q  = sample_sgld_diag_gibbs(trainset, P, Q, prec_P, prec_Q, alpha, stepsz, n_elem_row, n_elem_col, sz_batch, mean_rate, round_len)
        # Sample hyperparams
        if rem(iter, 1) == 0
            prec_P, prec_Q = sample_hyper(P, Q, prec_P, prec_Q, alpha_P, alpha_Q, beta0, K)
        end
        ########################################################################
        ts += toq();

        # Compute RMSE
        if mod(iter, itv_test) == 0
            avg_rmse_t, cur_rmse_t, avg_pred = rmse_avg(testset, iter, P, Q, avg_pred, avg_cnt, min_rate, max_rate, mean_rate, burnin)
            if iter > burnin 
                avg_cnt += 1
            end
            @printf("%d, %.4f (%.4f) (%.2fs) vPu:%.4f, vQi:%.5f", iter, avg_rmse_t, cur_rmse_t, ts, mean(1 ./ prec_P), mean(1 ./ prec_Q))
            if iter > burnin && avg_rmse_t < best_rmse 
                best_rmse = avg_rmse_t
                @printf("(**)\n")
            else @printf("\n") 
            end
            push!(logs.iter, iter)
            push!(logs.ts, ts)
            push!(logs.rmse, avg_rmse_t)
            para["iter"] = iter; para["ts"] = ts; para["best"] = best_rmse
            logs.para = para
        end

        # Save logs file
        if mod(iter, itv_save) == 0
            @printf("saving loggings ... ")
            para["iter"] = iter; 
            para["ts"]   = ts
            cur_logfile  = save_logs3(para, logs, log_dir, cur_logfile)
            @printf("done\n")
            if ts > maxsec
                break
            end
        end

        iter += 1
    end
    logs, P, Q
end


function sample_sgld_diag_gibbs{T}(trainset::Array{T,2}, 
                                   P::Array{Float64,2}, Q::Array{Float64,2},  
                                   prec_P::Array{Float64,1}, prec_Q::Array{Float64,1},   
                                   alpha::Float64, stepsz::Float64, 
                                   n_elem_row::Array{Int64,1}, n_elem_col::Array{Int64,1}, 
                                   sz_batch::Int64, mean_rate::Float64, round_len::Int64=200)
    sz_trainset = size(trainset)[1]
    K = size(P,1)
    grad_sum_P = zeros(Float64, K, n_users)
    grad_sum_Q = zeros(Float64, K, n_items)
    const ka = alpha * (sz_trainset / sz_batch)
    const halfstepsz = 0.5 * stepsz
    const sqrtstepsz = sqrt(stepsz)
    for iter in 1:round_len
        @inbounds batch = trainset[rand_samp(sz_trainset, sz_batch, "w_replacement"), :]
        fill!(grad_sum_P, 0.0)
        fill!(grad_sum_Q, 0.0)
        uu, ii, rr = batch[:,1], batch[:,2], batch[:,3]
        error = (rr - mean_rate) - vec(sum(P[:,uu] .* Q[:,ii],1))
        grad_Pu = broadcast(*, error', Q[:,ii])
        grad_Qi = broadcast(*, error', P[:,uu])
        for ix = 1:sz_batch
            @inbounds grad_sum_P[:,uu[ix]] += grad_Pu[:,ix]
            @inbounds grad_sum_Q[:,ii[ix]] += grad_Qi[:,ix]
        end
        Prior_P = - broadcast(*, P, prec_P)
        Prior_Q = - broadcast(*, Q, prec_Q)
        noiseP, noiseQ = randn(K, n_users), randn(K, n_items)
        @devec P = P .+ halfstepsz .* (ka .* grad_sum_P .+ Prior_P) .+ sqrtstepsz .* noiseP 
        @devec Q = Q .+ halfstepsz .* (ka .* grad_sum_Q .+ Prior_Q) .+ sqrtstepsz .* noiseQ  
    end
    return P, Q
end


function sample_hyper(P::Array{Float64,2}, Q::Array{Float64,2}, 
                      prec_P::Array{Float64,1}, prec_Q::Array{Float64,1},
                      alpha_P::Float64, alpha_Q::Float64, beta0::Float64, K::Int64)
    sse_P = sum(sqr(broadcast(-, P, mean(P,2))),2)
    beta_P = beta0 + 0.5 * sse_P
    sse_Q = sum(sqr(broadcast(-, Q, mean(Q,2))),2)
    beta_Q = beta0 + 0.5 * sse_Q
    for k in 1:K 
        @inbounds prec_P[k] = rand(InverseGamma(alpha_P, 1/beta_P[k]))
        @inbounds prec_Q[k] = rand(InverseGamma(alpha_Q, 1/beta_Q[k]))
    end
    return prec_P, prec_Q
end


