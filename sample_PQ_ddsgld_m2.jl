include("./c_wrappers.jl")

function sample_PQ_ddsgld_m2(args)
    
    # unpack arguments 
    cid::Int64 = args[1]
    if cid == -1
        return 0,0,0,0
    end

    burnin::Bool,
    local_blk_id::Int64,
    Ps::Array{Float64,2}, 
    Qs::Array{Float64,2}, 
    As::Array{Float64,1}, 
    Bs::Array{Float64,1},
    prec_Pk::Array{Float64,1},
    prec_Qk::Array{Float64,1},
    prec_A::Float64,
    prec_B::Float64,
    prec_r::Float64,
    eps::Float64,
    m::Int64,
    rndlen::Int64,
    mean_rate::Float64,
    itvhyp::Int64 = args[2:end]
    
    ################################################
    # Main SGLD 
    update_local_sgld_m2!(local_blk_id, Ps, Qs, As, Bs, prec_Pk, prec_Qk, prec_A, prec_B, prec_r, eps, m, rndlen, mean_rate, burnin)
    # sample hyper parameters using Asusterity-MH
    #@printf("done\n")
    return Ps, Qs, As, Bs

end

function update_local_sgld_m2!(local_blk_id::Int64,
                               P::Array{Float64,2}, Q::Array{Float64,2}, 
                               A::Array{Float64,1}, B::Array{Float64,1},
                               prec_P::Array{Float64,1}, prec_Q::Array{Float64,1},   
                               prec_A::Float64, prec_B::Float64, prec_r::Float64, 
                               eps::Float64, m::Int64, rndlen::Int64, mean_rate::Float64,
                               burnin::Bool)
    sz_Xs = size(_train_blks[local_blk_id]::Array{Float64,2}, 1)
    K = size(P,1) 
    ka = _Ns_scale::Float64 * prec_r * (_sz_trainset::Int64 / m) # _Ns_scale = Ns * S / N
    n_worker = _para["S"]::Int64
    halfstep = 0.5 * eps
    sqrtstep = sqrt(eps)

    grad_sum_P = zeros(Float64, size(P))
    grad_sum_Q = zeros(Float64, size(Q))
    grad_sum_A = zeros(Float64, size(A))
    grad_sum_B = zeros(Float64, size(B))

    if sz_Xs < m
        @printf("m is larger than sz_Xs, so reduced to sz_Xs\n")
        m = sz_Xs
    end
    
    sampling_type = "w_replacement"
    error = Array(Float64, m)

    inv_pr_pick_user = _inv_pr_pick_user_blks[local_blk_id]
    inv_pr_pick_item = _inv_pr_pick_item_blks[local_blk_id]

    for iter in 1:rndlen

        mini_batch = _train_blks[local_blk_id][rand_samp(sz_Xs, m, sampling_type), :]
        uu, ii, rr = int(mini_batch[:,1]), int(mini_batch[:,2]), mini_batch[:,3]
        ux, ix     = unique(uu), unique(ii)
        c_comp_error!(error, rr, mean_rate, P, Q, A, B, uu, ii, m, K)
        c_comp_grad_sum!(error, uu, ii, P, Q, A, B, grad_sum_P, grad_sum_Q, grad_sum_A, grad_sum_B, m, K)

        if burnin
            c_update_para_sgd!(ux, P, grad_sum_P, prec_P, inv_pr_pick_user, ka, halfstep, K)
            c_update_para_sgd!(ix, Q, grad_sum_Q, prec_Q, inv_pr_pick_item, ka, halfstep, K)
            c_update_para_sgd!(ux, A, grad_sum_A, prec_A, inv_pr_pick_user, ka, halfstep, 1)
            c_update_para_sgd!(ix, B, grad_sum_B, prec_B, inv_pr_pick_item, ka, halfstep, 1)
        else
            c_update_para!(ux, P, grad_sum_P, prec_P, inv_pr_pick_user, ka, halfstep, sqrtstep, K)
            c_update_para!(ix, Q, grad_sum_Q, prec_Q, inv_pr_pick_item, ka, halfstep, sqrtstep, K)
            c_update_para!(ux, A, grad_sum_A, prec_A, inv_pr_pick_user, ka, halfstep, sqrtstep, 1)
            c_update_para!(ix, B, grad_sum_B, prec_B, inv_pr_pick_item, ka, halfstep, sqrtstep, 1)
        end
        #error = (rr - mean_rate) - (vec(sum(Ps[:,uu] .* Qs[:,ii], 1)) + As[uu] + Bs[ii])
        #comp_grad_sum!(error, uu, ii, Ps, Qs, As, Bs, grad_sum_Ps, grad_sum_Qs, grad_sum_As, grad_sum_Bs)
        ## compute prior gradient
        #Ps[:,ux] += halfstep * (ka * grad_sum_Ps[:,ux] - (prec_Pk * _inv_pr_pick_user[ux]') .* Ps[:,ux]) 
                 #+  sqrtstep * randn(K, length(ux))   
        #Qs[:,ix] += halfstep * (ka * grad_sum_Qs[:,ix] - (prec_Qk * _inv_pr_pick_item[ix]') .* Qs[:,ix])
                 #+  sqrtstep * randn(K, length(ix))  
        #As[ux]   += halfstep * (ka * grad_sum_As[ux]   - (prec_A  * _inv_pr_pick_user[ux])  .* As[ux])
                 #+  sqrtstep * randn(length(ux))
        #Bs[ix]   += halfstep * (ka * grad_sum_Bs[ix]   - (prec_B  * _inv_pr_pick_item[ix])  .* Bs[ix])
                 #+  sqrtstep * randn(length(ix))
        ## prepare for next update
        #grad_sum_Ps[:,ux] = fill!(grad_sum_Ps[:,ux], 0.0)
        #grad_sum_Qs[:,ix] = fill!(grad_sum_Qs[:,ix], 0.0)
        #grad_sum_As[ux]   = fill!(grad_sum_As[ux],   0.0)
        #grad_sum_Bs[ix]   = fill!(grad_sum_Bs[ix],   0.0)
    end
    
    grad_sum_P = 0
    grad_sum_Q = 0
    grad_sum_A = 0
    grad_sum_B = 0
    gc()

    return nothing 
end


function comp_grad_sum!(error::Array{Float64,1},
                        uu::Array{Int64,1},ii::Array{Int64,1},
                        Ps::Array{Float64,2}, Qs::Array{Float64,2}, 
                        As::Array{Float64,1}, Bs::Array{Float64,1},
                        grad_sum_Ps::Array{Float64,2}, grad_sum_Qs::Array{Float64,2},
                        grad_sum_As::Array{Float64,1}, grad_sum_Bs::Array{Float64,1}
                        ) 
    uu_n, ii_n, err_n = 0, 0, 0
    for n = 1:length(error)
        @inbounds uu_n, ii_n, err_n = uu[n], ii[n], error[n]
        @inbounds grad_sum_Ps[:,uu_n] += err_n * Qs[:,ii_n]
        @inbounds grad_sum_Qs[:,ii_n] += err_n * Ps[:,uu_n]
        @inbounds grad_sum_As[uu_n]   += err_n
        @inbounds grad_sum_Bs[ii_n]   += err_n
    end
    return nothing
end



