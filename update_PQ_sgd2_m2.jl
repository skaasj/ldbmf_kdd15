include("./c_wrappers.jl")

function update_PQ_sgd2_m2!{T}(                             
                             model::Dict{ASCIIString,Any},
                             trainset::Array{T,2},
                             para::Dict{ASCIIString,Any},
                             mean_rate::Float64,
                             inv_pr_pick_user::Array{Float64,1},
                             inv_pr_pick_item::Array{Float64,1},
                             eps_t::Float64
                             )
    sz_trainset = size(trainset)[1]
    K = para["K"]
    ka = model["prec_r"] * (sz_trainset / para["m"])
    #halfstepsz = 0.5 * para["eps"]
    halfstepsz = 0.5 * eps_t
    P = model["P"]
    Q = model["Q"]
    A = model["A"]
    B = model["B"]
    prec_r = model["prec_r"]
    prec_P = model["prec_P"]
    prec_Q = model["prec_Q"]
    prec_A = model["prec_A"]
    prec_B = model["prec_B"]
    grad_sum_P = zeros(Float64, size(P))
    grad_sum_Q = zeros(Float64, size(Q))
    grad_sum_A = zeros(Float64, size(A))
    grad_sum_B = zeros(Float64, size(B))
    m = para["m"]

    error = Array(Float64, m)

    for iter in 1:para["rndlen"]

        batch = trainset[rand_samp(sz_trainset, m, "w_replacement"), :]
        
        uu, ii, rr = batch[:,1], batch[:,2], batch[:,3]
        ux, ix = unique(uu), unique(ii)
        
        c_comp_error!(error, rr, mean_rate, P, Q, A, B, uu, ii, m, K)
        c_comp_grad_sum!(error, uu, ii, P, Q, A, B, grad_sum_P, grad_sum_Q, grad_sum_A, grad_sum_B, m, K)
        c_update_para_sgd!(ux, P, grad_sum_P, prec_P, inv_pr_pick_user, ka, halfstepsz, K)
        c_update_para_sgd!(ix, Q, grad_sum_Q, prec_Q, inv_pr_pick_item, ka, halfstepsz, K)
        c_update_para_sgd!(ux, A, grad_sum_A, prec_A, inv_pr_pick_user, ka, halfstepsz, 1)
        c_update_para_sgd!(ix, B, grad_sum_B, prec_B, inv_pr_pick_item, ka, halfstepsz, 1)

        #error = (rr - mean_rate) - (vec(sum(P[:,uu] .* Q[:,ii], 1)) + A[uu] + B[ii])
        #comp_grad_sum!(error, uu, ii, P, Q, A, B, grad_sum_P, grad_sum_Q, grad_sum_A, grad_sum_B)

        ## compute gradinet of the prior
        #P[:,ux] += halfstepsz * (ka * grad_sum_P[:,ux] - (prec_P * inv_pr_pick_user[ux]') .* P[:,ux])
        #Q[:,ix] += halfstepsz * (ka * grad_sum_Q[:,ix] - (prec_Q * inv_pr_pick_item[ix]') .* Q[:,ix]) 
        #A[ux]   += halfstepsz * (ka * grad_sum_A[ux] - prec_A * (inv_pr_pick_user[ux] .* A[ux])) 
        #B[ix]   += halfstepsz * (ka * grad_sum_B[ix] - prec_B * (inv_pr_pick_item[ix] .* B[ix]))

        ## prepare for next update
        #grad_sum_P[:,ux] = fill!(grad_sum_P[:,ux], 0.0)
        #grad_sum_Q[:,ix] = fill!(grad_sum_Q[:,ix], 0.0)
        #grad_sum_A[ux]   = fill!(grad_sum_A[ux],   0.0)
        #grad_sum_B[ix]   = fill!(grad_sum_B[ix],   0.0)
    end
    return nothing
end


function comp_grad_sum2!{T}(err::Array{Float64,1},
                           uu::Array{T,1},ii::Array{T,1},
                           Ps::Array{Float64,2}, Qs::Array{Float64,2},
                           As::Array{Float64,1}, Bs::Array{Float64,1},
                           grad_sum_Ps::Array{Float64,2}, grad_sum_Qs::Array{Float64,2},
                           grad_sum_As::Array{Float64,1}, grad_sum_Bs::Array{Float64,1}
                           )
    G_Ps = broadcast(*, err', Qs[:,ii])
    G_Qs = broadcast(*, err', Ps[:,uu])
    for n = 1:length(err)
        @inbounds grad_sum_Ps[:,uu[n]] += G_Ps[:,n]
        @inbounds grad_sum_Qs[:,ii[n]] += G_Qs[:,n]
        @inbounds grad_sum_As[uu[n]]   += err[n]
        @inbounds grad_sum_Bs[ii[n]]   += err[n]
    end
    return nothing
end


function comp_grad_sum!{T}(error::Array{Float64,1},
                           uu::Array{T,1},ii::Array{T,1},
                           Ps::Array{Float64,2}, Qs::Array{Float64,2},
                           As::Array{Float64,1}, Bs::Array{Float64,1},
                           grad_sum_Ps::Array{Float64,2}, grad_sum_Qs::Array{Float64,2},
                           grad_sum_As::Array{Float64,1}, grad_sum_Bs::Array{Float64,1}
                           )
    uu_n, ii_n, err_n = 0, 0, 0
    for n = 1:length(error)
        @inbounds uu_n, ii_n, err_n = uu[n], ii[n], error[n]
        @inbounds grad_sum_Ps[:,uu_n] += err_n * Qs[:,ii_n]
        @inbounds grad_sum_Qs[:,ii_n] += err_n * Ps[:,uu_n]
        @inbounds grad_sum_As[uu_n]   += err_n
        @inbounds grad_sum_Bs[ii_n]   += err_n
    end
    return nothing
end


