using Debug
using Devectorize
using Distributions
include("./utils/rng.jl")
include("./utils/utils.jl")  
include("./utils/mf_common.jl")

function train_dsgd_m2(model::Dict{ASCIIString,Any},
                       para::Dict{ASCIIString,Any},
                       min_rate::Float64, max_rate::Float64, mean_rate::Float64,
                       testset::Array{Float64,2},
                       blk_info::Array{Any,1},
                       blk_groups::Array{Array{Int64,1},1},
                       f_step::Function
                       )
    #@assert length(models) == para["C"]
                                 
    para["algo"] = "DSGD-2"

    # unpack para   
    n_chain::Int64  = para["C"]
    n_worker::Int64 = para["S"]
    mxiter::Int64   = para["mxiter"]
    n_users::Int64  = size(model["P"],2)
    n_items::Int64  = size(model["Q"],2)
    
    println(para)
    @printf("Running %s\n", para["algo"])
    log_dir = "00_results_"para["algo"]"_K"string(para["K"])
    cur_logfile = get_str_id2(para)
    @printf("%s\n",cur_logfile)
    
    logs = Dict{ASCIIString,Any}()
    logs["iter"]     = Int64[]
    logs["rmse"] = Float64[]
    logs["ts"]       = Float64[]

    # precomputation of some constants
    sz_testset = size(testset,1)
    preds = zeros(Float64,sz_testset)

    # pre-allocate 
    avg_rmse = 0.0  
    best_rmse = 5.0
    avg_cur_rmse = 5.0
    round = 0
    ts = 0.0

    p_results = cell(n_worker)
    p_param = cell(n_worker)
    
    #chain_order = update_chain_order(n_worker, n_chain)
    blkgroup2chain = update_blk_group_order(length(blk_groups), n_chain)

    while round < mxiter
        
        tic()
        ###########################################

        round += 1
        blkgroup2chain = update_blk_group_order(blkgroup2chain)
        eps_t = f_step(round)
        pack_param_dsgd!(p_param, round, blkgroup2chain, blk_groups, model, para, blk_info, n_worker, mean_rate, eps_t)

        # perform parallel updates
        i = 1
        nextidx() = (idx=i; i+=1; idx)
        @sync begin
            for p in procs()[2:end]
                @async begin 
                    idx = nextidx()
                    p_results[idx] = remotecall_fetch(p, update_PQ_dsgd_m2, p_param[idx])
                end
            end
        end
        @everywhere gc()
        reduce_received!(p_results, model, blk_groups, blkgroup2chain)

        #cur_group = find(blkgroup2chain .== 1)
        #for (wid, bid) in enumerate(blk_groups[cur_group[1]])
            #model["P"][:,blk_info[bid]["local_uid"]] = p_results[wid][1] 
            #model["Q"][:,blk_info[bid]["local_iid"]] = p_results[wid][2] 
            #model["A"][blk_info[bid]["local_uid"]]   = p_results[wid][3] 
            #model["B"][blk_info[bid]["local_iid"]]   = p_results[wid][4] 
        #end

        #for (g, blk_group) in enumerate(blk_groups)
            #cid = blkgroup2chain[g]
            #if cid != -1
               #for bid in blk_group
                    #wid = bid
                    #models[cid]["P"][:,blk_info[bid]["local_uid"]] = p_results[wid][1]
                    #models[cid]["Q"][:,blk_info[bid]["local_iid"]] = p_results[wid][2]
                    #models[cid]["A"][blk_info[bid]["local_uid"]]   = p_results[wid][3]
                    #models[cid]["B"][blk_info[bid]["local_iid"]]   = p_results[wid][4]
                #end
            #end
        #end

        ###########################################
        ts += toq()
        gc()
        ################################
        # compute test RMSE
        if mod(round, para["itvtst"]) == 0
            tic()

            avg_rmse_t, cur_rmse_t, avg_cnt = c_rmse_avg_m2!(testset, round, model["P"], model["Q"], model["A"], model["B"], preds, 0, min_rate, max_rate, mean_rate, true)
            gc()

            @printf("t:%d, rmse:%.4f (%.2f sec.), eps_t: %.3e", round, cur_rmse_t, ts, eps_t)
            if cur_rmse_t < best_rmse
                best_rmse = cur_rmse_t
                print(" (**)")
            end
            
            push!(logs["iter"], round)
            push!(logs["ts"], ts)
            push!(logs["rmse"], cur_rmse_t)

            para["iter"] = round; 
            para["ts"]   = ts; 
            para["best"] = best_rmse

            logs["para"] = para 

            @printf " [%.2fs]\n" toq()

            #@everywhere gc()
        end
        
        ################################
        # save to file
        if mod(round, para["itvsv"]) == 0
            @printf("saving logs ... ")
            para["t"] = round
            para["ts"] = ts
            cur_logfile = save_logs4(logs, log_dir, cur_logfile)
            @printf("done.\n")
        end
    end
    return nothing
end


function print_results(avg_rmse::Float64, best_rmse::Float64, 
                       preds::Array{Dict{ASCIIString,Any},1}, 
                       round::Int64, ts::Float64, is_burnin::Bool)
    # avg over all parallel chains
    str = @sprintf("r:%d, ", round)
    @printf("%-8s", str)
    str = @sprintf("%.4f (%.2fs) ", avg_rmse, ts)
    @printf("%-20s", str)
    @printf("[ ")
    for pred in preds; @printf("%.4f ", pred["cur_rmse"]); end
    @printf("] ")
    if !is_burnin && avg_rmse < best_rmse
        best_rmse = avg_rmse
        @printf("(**)")
    end
    return best_rmse
end


function reduce_received!(p_results, model, blk_groups, blkgroup2chain)

    cur_group = find(blkgroup2chain .== 1)
    for (wid, bid) in enumerate(blk_groups[cur_group[1]])
        model["P"][:,blk_info[bid]["local_uid"]] = p_results[wid][1] 
        model["Q"][:,blk_info[bid]["local_iid"]] = p_results[wid][2] 
        model["A"][blk_info[bid]["local_uid"]]   = p_results[wid][3] 
        model["B"][blk_info[bid]["local_iid"]]   = p_results[wid][4] 
        gc()
    end
    #for (g, blk_group) in enumerate(blk_groups)
        #cid = blkgroup2chain[g]
        #if cid != -1
           #for bid in blk_group
                #wid = bid
                #models[cid]["P"][:,blk_info[bid]["local_uid"]] = p_results[wid][1]
                #models[cid]["Q"][:,blk_info[bid]["local_iid"]] = p_results[wid][2]
                #models[cid]["A"][blk_info[bid]["local_uid"]]   = p_results[wid][3]
                #models[cid]["B"][blk_info[bid]["local_iid"]]   = p_results[wid][4]
            #end
        #end
        #gc()
    #end
    return nothing
end




function pack_param_dsgd!(p_param::Array{Any,1},
                          round::Int64,
                          blkgroup2chain::Array{Int64,1}, 
                          blk_groups::Array{Array{Int64,1},1},
                          model::Dict{ASCIIString,Any},
                          para::Dict{ASCIIString,Any},
                          blk_info::Array{Any,1},
                          n_worker::Int64,
                          mean_rate::Float64, 
                          eps_t::Float64)

    cur_group = find(blkgroup2chain .== 1)
    @assert length(cur_group) == 1  # only one ortho-group must be active
    #p_param = cell(n_worker)
    for (wid, bid) in enumerate(blk_groups[cur_group[1]])
        p_param[wid] = (
            1,
            blk_info[bid]["idx_in_wkr"],
            model["P"][:,blk_info[bid]["local_uid"]],
            model["Q"][:,blk_info[bid]["local_iid"]],
            model["A"][blk_info[bid]["local_uid"]],
            model["B"][blk_info[bid]["local_iid"]],
            model["prec_P"],
            model["prec_Q"],
            model["prec_A"],
            model["prec_B"],
            model["prec_r"],
            eps_t,
            para["m"],
            para["rndlen"],
            mean_rate
            )
    end
    gc()
    return nothing
end



function update_blk_group_order(n_blk_group, n_chain)
    blkgroup2chain = fill!(Array(Int64, n_blk_group), -1)
    blkgroup2chain[randperm(n_blk_group)[1:n_chain]] = 1:n_chain
    return blkgroup2chain
end


function update_blk_group_order(blkgroup2chain::Array{Int64,1})
    return unshift!(blkgroup2chain, pop!(blkgroup2chain))
end



#function pcopy_inv_pr_pick(inv_pr_pick_user::Array{Float64,1}, 
                           #inv_pr_pick_item::Array{Float64,1},
                           #localized_uid_s::Array{Array{Int64,1},1}, 
                           #localized_iid_s::Array{Array{Int64,1},1})
    #@printf("pcopy inv_pr_pick started ... ")
    #for (s,p) in enumerate(procs()[2:end])
        #remotecall_fetch(p, (arg)->(global _inv_pr_pick_user = arg), inv_pr_pick_user[localized_uid_s[s]])
        #remotecall_fetch(p, (arg)->(global _inv_pr_pick_item = arg), inv_pr_pick_item[localized_iid_s[s]])
    #end
    #@printf("done\n")
#end


function compute_bias_corrector(N::Int64, sz_batch::Int64, 
                                n_user::Int64, n_item::Int64,
                                localized_uid_s::Array{Array{Int64,1},1},
                                localized_iid_s::Array{Array{Int64,1},1},
                                Us::Array{Array{Int64,1},1}, 
                                Is::Array{Array{Int64,1},1},
                                n_elem_row_s::Array{Array{Int64,1},1}, 
                                n_elem_col_s::Array{Array{Int64,1},1})
    n_worker = length(Us)
    Pr_us = zeros(Float64, n_user, n_worker)
    Pr_is = zeros(Float64, n_item, n_worker)
    Ns = Array(Int64, n_worker)
    for s in 1:n_worker
        Ns[s] = sum(n_elem_row_s[s])
    end
    for s in 1:n_worker
        for (ix,uid) in enumerate(localized_uid_s[s])
            Pr_us[uid,s] = 1 - (1 - n_elem_row_s[s][ix] / Ns[s]) ^ sz_batch
        end 
        for (ix,iid) in enumerate(localized_iid_s[s])
            Pr_is[iid,s] = 1 - (1 - n_elem_col_s[s][ix] / Ns[s]) ^ sz_batch
        end
    end
    Pr_u = vec(sum(Pr_us,2)) ./ n_worker
    Pr_i = vec(sum(Pr_is,2)) ./ n_worker
    inv_pr_pick_user = 1 ./ Pr_u
    inv_pr_pick_item = 1 ./ Pr_i
    return inv_pr_pick_user, inv_pr_pick_item
end


function compute_bias_corrector(N::Int64, sz_batch::Int64, n_elem_row::Array{Int64,1}, n_elem_col::Array{Int64,1})
    inv_pr_pick_user = 1 ./ (1 - (1 - float(n_elem_row/N)) .^ sz_batch)
    inv_pr_pick_item = 1 ./ (1 - (1 - float(n_elem_col/N)) .^ sz_batch)
    return inv_pr_pick_user, inv_pr_pick_item
end



