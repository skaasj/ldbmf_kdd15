using Debug
using Datetime
using Devectorize
using Distributions
include("./utils/types.jl")
include("./utils/utils.jl")
include("./utils/mf_common.jl")
include("./sample_PQ_sgld_m2.jl")
include("./update_PQ_sgd2_m2.jl")
include("./sample_hyper_gibbs_m2.jl")
include("./c_wrappers.jl")

function train_sgld_gibbs_m2{T}(trainset::Array{T,2}, 
                                testset::Array{T,2}, 
                                model::Dict{ASCIIString,Any},
                                para::Dict{ASCIIString, Any},
                                n_elem_row::Array{Int64,1}, 
                                n_elem_col::Array{Int64,1},
                                f_stepsz::Function
                                )
    para["map"] ? para["algo"] = "SGD-m2-MAP" : para["algo"] = "S-G-2"

    #eps::Float64    = para["eps"] 
    m::Int64        = para["m"] 
    rndlen::Int64   = para["rndlen"]
    mxiter::Int64   = para["mxiter"]
    mxsec::Int64    = para["mxsec"] 
    itvsv::Int64    = para["itvsv"]
    itvtst::Int64   = para["itvtst"]
    itvhyp::Int64   = para["itvhyp"]
    n_users = size(model["P"],2)
    n_items = size(model["Q"],2)

    println(para)
    @printf("al0: %f, bt0: %f\n", model["al0"], model["bt0"])
    @printf("Running %s\n", para["algo"])
    @assert itvsv >= itvtst
    log_dir = "00_results_"para["algo"]"_K"string(para["K"])
    #log_dir = "/scratch/sungjin/00_projects/00_dbmf/004_neflix_train8020/00_results_"para["algo"]
    cur_logfile = get_str_id2(para)
    #logs = Log2(zeros(Int64,0), zeros(0), zeros(0), zeros(0,0), zeros(0,0), para)
    logs = Dict{ASCIIString,Any}()
    logs["iter"] = Int64[]
    logs["avg_rmse"] = Float64[]
    logs["cur_rmse"] = Float64[]
    logs["ts"]   = Float64[]

    # initialization
    min_rate    = float(minimum(trainset[:,3]))
    max_rate    = float(maximum(trainset[:,3]))
    #mean_rate   = mean(trainset[:,3])
    para["submean"] ? mean_rate = mean(trainset[:,3]) : mean_rate = 0.0
    
    sz_trainset = size(trainset)[1]
    sz_testset  = size(testset)[1]
    K           = size(model["P"],1)

    if rndlen > sz_trainset
        rndlen = sz_trainset
        para["rndlen"] = sz_trainset
        @printf("rndlen is truncated to sz_trainset\n")
    end

    iter = 1     # iter
    avg_cnt = 0  # counts number of averaged predicions
    ts = 0.0     # timestamp
    best_rmse = 100.0
    cur_rmse_t = 100.0
    avg_pred = zeros(Float64,sz_testset)

    inv_pr_pick_user = 1 ./ (1 - (1 - n_elem_row ./ size(trainset,1)) .^ para["m"])
    inv_pr_pick_item = 1 ./ (1 - (1 - n_elem_col ./ size(trainset,1)) .^ para["m"])
    gc()

    is_burnin = true

    while iter <= mxiter && ts < mxsec
    
        tic()
        ######################################################################## 
        eps_t = f_stepsz(iter)

        if is_burnin
            update_PQ_sgd2_m2!(model, trainset, para, mean_rate, inv_pr_pick_user, inv_pr_pick_item, eps_t)
        else
            sample_PQ_sgld_m2!(model, trainset, para, mean_rate, inv_pr_pick_user, inv_pr_pick_item, eps_t) 
        end
        gc()

        if !is_burnin && rem(iter, itvhyp) == 0
            sample_hyper_gibbs_m2!(model)
        end
        gc()
        
        ########################################################################
        ts += toq();

        # Compute RMSE
        if mod(iter, itvtst) == 0

            avg_rmse_t, cur_rmse_t, avg_cnt = c_rmse_avg_m2!(testset, iter, model["P"], model["Q"], model["A"], model["B"], avg_pred, avg_cnt, min_rate, max_rate, mean_rate, is_burnin)
            gc()
            
            @printf("pr_P:%.1f, pr_Q:%.1f, pr_A:%.1f, pr_B:%.1f\n", mean(model["prec_P"]), mean(model["prec_Q"]), model["prec_A"], model["prec_B"])
            @printf("%d, %.4f (%.4f) (%.2fs), eps: %.3e ", iter, avg_rmse_t, cur_rmse_t, ts, eps_t)
            if !is_burnin && avg_rmse_t < best_rmse 
                best_rmse = avg_rmse_t
                @printf("(**)")
            elseif is_burnin && (cur_rmse_t < best_rmse)
                best_rmse = cur_rmse_t
                @printf("(**)")
            end
            println()

            push!(logs["iter"], iter)
            push!(logs["ts"], ts)
            push!(logs["avg_rmse"], avg_rmse_t)
            push!(logs["cur_rmse"], cur_rmse_t)
            para["iter"] = iter; para["ts"] = ts; para["best"] = best_rmse
            logs["para"] = para

            if is_burnin == true && !para["map"]
                is_burnin = (cur_rmse_t > para["bthresh"])
                if is_burnin == false
                    @printf("burnin finished\n")
                    logs["burnin_ts"] = ts
                end
            end
            if para["map"] && haskey(para, "tg_rmse") && logs["cur_rmse"][end] <= para["tg_rmse"]
                @printf("saving loggings ... \n")
                para["iter"] = iter; 
                para["ts"]   = ts
                cur_logfile  = save_logs4(logs, log_dir, cur_logfile)
                @printf("target rmse reached!\n")
                return nothing
            end
        end
        # Save logs file
        if mod(iter, itvsv) == 0
            @printf("saving loggings ... ")
            para["iter"] = iter; 
            para["ts"]   = ts
            cur_logfile  = save_logs4(logs, log_dir, cur_logfile)
            @printf("done\n")
            if ts > mxsec
                @printf("mxsec reached\n")
                return nothing
            end
            
        end

        iter += 1

    end
end

