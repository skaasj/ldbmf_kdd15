function sample_hyper_austMH_m2!(model::Dict{ASCIIString,Any},
                                 ac_rates::Dict{ASCIIString,Any},
                                 ttest_thresh::Float64,
                                 sz_batch::Int64,
                                 cnt::Int64)
    
    K = size(model["P"],1)
    sz_batch_p, sz_batch_q = 5000, 3000
    mini_user_ids = randperm(n_users)[1:sz_batch_p]
    mini_item_ids = randperm(n_items)[1:sz_batch_q]
    acpt_la_p = [true for k in 1:K]
    acpt_la_q = [true for k in 1:K]
    al0 = model["al0"]
    bt0 = model["bt0"]
    for k in 1:K
        model["prec_P"][k], acpt_la_p[k], ndata_la_pk = 
            austerity_ga(vec(model["P"][k,:]), model["prec_P"][k], al0, bt0, mini_user_ids, ttest_thresh, sz_batch)
        model["prec_Q"][k], acpt_la_q[k], ndata_la_qk = 
            austerity_ga(vec(model["Q"][k,:]), model["prec_Q"][k], al0, bt0, mini_item_ids, ttest_thresh, sz_batch)
    end
    model["prec_A"], acpt_la_a, ndata_la_a = austerity_ga(model["A"], model["prec_A"], al0, bt0, mini_user_ids, ttest_thresh, sz_batch)
    model["prec_B"], acpt_la_b, ndata_la_b = austerity_ga(model["B"], model["prec_B"], al0, bt0, mini_item_ids, ttest_thresh, sz_batch)

    #println(acpt_la_p)
    #println(acpt_la_q)
    #println(acpt_la_a)
    #println(acpt_la_b)
    ac_rates["la_P"] = (1 - 1/cnt) * ac_rates["la_P"] + 1/cnt * acpt_la_p
    ac_rates["la_Q"] = (1 - 1/cnt) * ac_rates["la_Q"] + 1/cnt * acpt_la_q
    ac_rates["la_A"] = (1 - 1/cnt) * ac_rates["la_A"] + 1/cnt * acpt_la_a
    ac_rates["la_B"] = (1 - 1/cnt) * ac_rates["la_B"] + 1/cnt * acpt_la_b

    return nothing
end


@debug function austerity_ga(P::Array{Float64,1}, prec::Float64, al0::Float64, bt0::Float64, mini_ids::Array{Int64,1}, ttest_thresh::Float64, sz_batch::Int64)
    
    n = length(mini_ids)
    N = length(P)
    mini_P = P[mini_ids]
    alph = al0 + 0.5 * N
    beta = bt0 + 0.5 * (N / n) * sum(mini_P .* mini_P)
    #beta = bt0 + 0.5 * (N / n) * sum(sqr(mini_P - mean(mini_P)))

    curr_var = 1 / prec
    prop_var = rand(InverseGamma(alph, beta))
    difflog_prior = - (al0  + 1) * (log(curr_var) - log(prop_var)) - bt0  * (1/curr_var - 1/prop_var)
    difflog_props = - (alph + 1) * (log(prop_var) - log(curr_var)) - beta * (1/prop_var - 1/curr_var)
    difflogp = difflog_prior + difflog_props   

    thresh_MH = (log(rand()) + difflogp) / N

    accept, ndata = AR_aust_MH_1d(thresh_MH, P, 1/prop_var, 1/curr_var, sz_batch, ttest_thresh)
    
    if accept
        prec  = 1/prop_var
    end
    return prec, accept, ndata
end


@debug function AR_aust_MH_1d(thresh_MH::Float64, U::Array{Float64,1}, 
                       prec_::Float64, prec::Float64, 
                       sz_batch::Int64, ttest_thresh::Float64)
    N = length(U);
    randinds = randperm(N)
    sampinds = randinds[1:sz_batch]
    randinds = randinds[sz_batch+1:end]
    n = sz_batch

    y = - 0.5 * prec_ * (U[sampinds] .^ 2) + 0.5 * prec * (U[sampinds] .^ 2)
    mx = mean(y)
    mx2 = (y' * y)[1] / n

    done = false
    accept = false
    while done == false 
        
        sx = sqrt((n/(n-1)) * (mx2 - mx^2))
        s  = sx * sqrt(1 - n/N) / sqrt(n)
        tstat = abs((mx - thresh_MH) / s)

        if tstat > ttest_thresh
            accept = thresh_MH < mx
            done = true
        else
            nmb = min(sz_batch, N - n)
            sampinds = randinds[1:nmb] 
            randinds = randinds[nmb+1:end]
            newn = n + nmb
            #iS_U_muU_ = broadcast(-, U[:,sampinds], mu_)
            #iS_U_muU  = broadcast(-, U[:,sampinds], mu)
            #y = - 0.5 * prec_ .* vec(sum(iS_U_muU_ .^ 2, 1)) + 0.5 * prec .* vec(sum(iS_U_muU .^ 2, 1))
            y = - 0.5 * prec_ * (U[sampinds] .^ 2) + 0.5 * prec * (U[sampinds] .^ 2)

            mx = (mx * n + sum(y)) / newn
            
            if newn == N
                accept = thresh_MH < mx
                done = true
            else
                sumy2 = (y' * y)[1]
                #sumy2 = y' * y
                mx2 = (mx2 * n + sumy2) / newn
            end
            
            n = newn
        end
    end

    return accept, n 
end



#function austerity_ga!(model::Dict{ASCIIString,Any})

    #P = model["P"]
    #Q = model["Q"]
    #n_users, n_items = size(P,2), size(Q,2)
    #sz_batch_p, sz_batch_q = 2000, 1000
    
    #mini_user_inds = randperm(n_users)[1:sz_batch_p]
    #mini_item_inds = randperm(n_items)[1:sz_batch_q]
    #mini_P = P[:,mini_user_inds]
    #mini_Q = Q[:,mini_item_inds]

    ## proposals 
    #alph_P_ = model["al0"] + 0.5 * n_users
    #alph_Q_ = model["al0"] + 0.5 * n_items
    #beta_P_ = model["bt0"] + 0.5 * (n_users / sz_batch_p) * sum(sqr(broadcast(-, mini_P, mean(mini_P,2))), 2)
    #beta_Q_ = model["bt0"] + 0.5 * (n_items / sz_batch_q) * sum(sqr(broadcast(-, mini_Q, mean(mini_Q,2))), 2)

    #for k in 1:K 
        ## update prec_P
        #@inbounds curr_var_k = 1 / model["prec_P"][k] 
        #@inbounds prop_var_k = rand(InverseGamma(alph_P_, beta_P_[k]))
        #difflog_prior = - (model["al0"] + 1) * (log(curr_var_k) - log(prop_var_k)) - model["bt0"] * (1/curr_var_k - 1/prop_var_k)
        #difflog_props = - (alph_P_      + 1) * (log(prop_var_k) - log(curr_var_k)) - beta_P_[k]   * (1/prop_var_k - 1/curr_var_k)
        #difflogp = difflog_prior + difflog_props
        #thresh_MH = (log(rand()) + difflogp) / n_users
        #accept, ndata = AR_MH_approx2(thresh_MH, U, mu_, mu, sz_batch, ttest_thresh)
        #if accept
            #model["prec_P"][k] = 1/propr_var_k
        #end

        ## update prec_Q
        #@inbounds curr_var_k = 1 / model["prec_Q"][k] 
        #@inbounds prop_var_k = rand(InverseGamma(alph_Q_, beta_Q_[k]))
        #difflog_prior = -(model["al0"] + 1) * (log(curr_var_k) - log(prop_var_k)) - model["bt0"] * (1/curr_var_k - 1/prop_var_k)
        #difflog_props = -(alph_Q_      + 1) * (log(prop_var_k) - log(curr_var_k)) - beta_Q_[k]   * (1/prop_var_k - 1/curr_var_k)
        #difflogp = difflog_prior + difflog_props
        #thresh_MH = (log(rand()) + difflogp) / n_items
        #accept, ndata = AR_MH_approx2(thresh_MH, U, mu_, mu, sz_batch, ttest_thresh)
        #if accept
            #model["prec_Q"][k] = 1/propr_var_k
        #end
    #end

    ## update la_a, la_b
    #A = model["A"]
    #B = model["B"]
    #mini_A = A[mini_user_inds]
    #mini_B = B[mini_item_inds]
    #alph_A_ = alph_P_
    #alph_B_ = alph_Q_
    #beta_A_ = model["bt0"] + 0.5 * (n_users / sz_batch_p) * sum(sqr(mini_A - mean(mini_A)))
    #beta_B_ = model["bt0"] + 0.5 * (n_items / sz_batch_q) * sum(sqr(mini_B - mean(mini_B)))

    #curr_var_a = 1 / model["prec_A"]
    #prop_var_a = rand(InverseGamma(alph_A_, beta_A_))
    #logdiff_prior_a = - (model["al0"] + 1) * (log(curr_var_a) - log(prop_var_a)) - model["bt0"] * (1/curr_var_a - 1/prop_var_a)
    #logdiff_props_a = - (alph_A_      + 1) * (log(prop_var_a) - log(curr_var_a)) - beta_A_      * (1/prop_var_a - 1/curr_var_a)
    #logdiffp_a = logdiff_prior_a + logdiff_props_a


    #curr_var_b = 1 / model["prec_B"]
    #prop_var_b = rand(InverseGamma(alph_B_, beta_B_))
    #logdiff_prior_b = - (model["al0"] + 1) * (log(curr_var_b) - log(prop_var_b)) - model["bt0"] * (1/curr_var_b - 1/prop_var_b)
    #logdiff_props_b = - (alph_B_      + 1) * (log(prop_var_b) - log(curr_var_b)) - beta_B_      * (1/prop_var_b - 1/curr_var_b)
    #logdiffp_b = logdiff_prior_b + logdiff_props_b




    ##sse_A = sum(sqr(A - mean(A)))
    ##sse_B = sum(sqr(B - mean(B)))
    ##beta_A = model["bt0"] + 0.5 * sse_A
    ##beta_B = model["bt0"] + 0.5 * sse_B
    ##model["prec_A"]  = 1/rand(InverseGamma(model["al0"] + 0.5 * n_users, beta_A))
    ##model["prec_B"]  = 1/rand(InverseGamma(model["al0"] + 0.5 * n_items, beta_B))

    ##mu_ = mu + step * randn(size(mu,1))
    ##iS_muU_mu0_ = sqrt(bt0) * Lam_chol * (mu_ - mu0)
    ##iS_muU_mu0  = sqrt(bt0) * Lam_chol * (mu  - mu0)
    ##difflogp = - 0.5 * (iS_muU_mu0_' * iS_muU_mu0_)[1] + 0.5 * (iS_muU_mu0' * iS_muU_mu0)[1]
    ##M = float(size(U, 2));
    ##thresh_MH = (log(rand()) - difflogp) / M
    ##accept, ndata = AR_MH_approx(thresh_MH, U, mu_, mu, Lam_chol, Lam_chol, sz_batch, ttest_thresh, isU)
    ##if accept
        ##for i in 1:length(mu); mu[i] = mu_[i]; end
    ##end
    ##return accept, ndata


#end



#function AR_MH_approx(thresh_MH::Float64, U::Array{Float64,2}, 
                      #mu_::Array{Float64,1}, mu::Array{Float64,1}, 
                      #Lam_chol_::Array{Float64,2}, Lam_chol::Array{Float64,2}, 
                      #sz_batch::Int64, ttest_thresh::Float64, isU::Bool)
    #N = size(U,2);
    #randinds = randperm(N)
    #sampinds = randinds[1:sz_batch]
    #randinds = randinds[sz_batch+1:end]
    #n = sz_batch

    #iS_U_muU_ = Lam_chol_ * broadcast(-, U[:,sampinds], mu_)
    #iS_U_muU  = Lam_chol  * broadcast(-, U[:,sampinds], mu)
    #y = - 0.5 * vec(sum(iS_U_muU_ .^ 2, 1)) + 0.5 * vec(sum(iS_U_muU .^ 2, 1))
    #mx = mean(y)
    #mx2 = (y' * y)[1] / n

    #done = false
    #accept = false
    #while done == false 
        
        #sx = sqrt((n/(n-1)) * (mx2 - mx^2))
        #s  = sx * sqrt(1 - n/N) / sqrt(n)
        ##if typeof(s) == Array{Float64,2}
            ##@bp
        ##end
        #tstat = abs((mx - thresh_MH) / s)

        #if tstat > ttest_thresh
            #accept = thresh_MH < mx
            #done = true
        #else
            #nmb = min(sz_batch, N - n)
            #sampinds = randinds[1:nmb] 
            #randinds = randinds[nmb+1:end]
            #newn = n + nmb
            
            #iS_U_muU_ = Lam_chol_ * broadcast(-, U[:,sampinds], mu_)
            #iS_U_muU  = Lam_chol_ * broadcast(-, U[:,sampinds], mu)
            #y = - 0.5 * vec(sum(iS_U_muU_ .^ 2, 1)) + 0.5 * vec(sum(iS_U_muU .^ 2, 1))

            #mx = (mx * n + sum(y)) / newn
            
            #if newn == N
                #accept = thresh_MH < mx
                #done = true
            #else
                #sumy2 = (y' * y)[1]
                #mx2 = (mx2 * n + sumy2) / newn
            #end
            
            #n = newn
        #end
    #end

    #return accept, n 
#end
