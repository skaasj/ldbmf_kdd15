using Debug
using Datetime
using Devectorize
using Distributions
include("./utils/types.jl")
include("./utils/utils.jl")
include("./utils/mf_common.jl")
include("./sample_PQ_gibbs_m1.jl")
include("./sample_hyper_gibbs_m1.jl")
include("./c_wrappers.jl")

function train_gibbs_gibbs_m1{T}(trainset::Array{T,2}, 
                                 testset::Array{T,2}, 
                                 model::Dict{ASCIIString,Any},
                                 para::Dict{ASCIIString, Any},
                                 user_idx::Array{Array{Int64,1},1},
                                 item_idx::Array{Array{Int64,1},1}
                                 )
    para["algo"] = "G-G-1"

    mxiter::Int64   = para["mxiter"]
    mxsec::Int64    = para["mxsec"] 
    itvsv::Int64    = para["itvsv"]
    itvtst::Int64   = para["itvtst"]
    #itvhyp::Int64   = para["itvhyp"]
    n_users = size(model["P"],2)
    n_items = size(model["Q"],2)

    println(para)
    @printf("Running %s\n", para["algo"])
    @assert itvsv >= itvtst
    log_dir = "00_results_"para["algo"]
    #log_dir = "/scratch/sungjin/00_projects/00_dbmf/004_neflix_train8020/00_results_"para["algo"]
    cur_logfile = get_str_id2(para)
    #logs = Log2(zeros(Int64,0), zeros(0), zeros(0), zeros(0,0), zeros(0,0), para)
    logs = Dict{ASCIIString,Any}()
    logs["iter"] = Int64[]
    logs["ts"]   = Float64[]
    logs["cur_rmse"] = Float64[]
    logs["avg_rmse"] = Float64[]

    # initialization
    min_rate    = float(minimum(trainset[:,3]))
    max_rate    = float(maximum(trainset[:,3]))
    mean_rate   = mean(trainset[:,3])
    sz_trainset = size(trainset)[1]
    sz_testset  = size(testset)[1]
    K           = size(model["P"],1)

    iter = 1     # iter
    avg_cnt = 0  # counts number of averaged predicions
    ts = 0.0     # timestamp
    best_rmse = 100.0
    avg_pred = zeros(Float64,sz_testset)

    #is_burnin = !no_burnin
    is_burnin = false  # BPMF -> no burnin (it is done by a separate SGD)

    while iter <= mxiter && ts < mxsec
    
        tic()
        ######################################################################## 

        sample_hyper_gibbs_m1!(model)
        sample_PQ_gibbs_m1!(model, trainset, user_idx, item_idx, mean_rate, para["itv_gibbs"])
        
        ########################################################################
        ts += toq();

        # Compute RMSE
        if mod(iter, itvtst) == 0
            
            avg_rmse_t, cur_rmse_t, avg_cnt = c_rmse_avg_m2!(testset, iter, model["P"], model["Q"], model["A"], model["B"], avg_pred, avg_cnt, min_rate, max_rate, mean_rate, is_burnin)

            #avg_rmse_t, cur_rmse_t, avg_pred = rmse_avg(testset, iter, model["P"], model["Q"], model["A"], model["B"], avg_pred, avg_cnt, min_rate, max_rate, mean_rate, is_burnin)
            
            @printf("pr_P:%d, pr_Q:%d, pr_A:%1.1f, pr_B:%1.1f, pr_r:%1.1f\n", mean(model["prec_P"]), mean(model["prec_Q"]), model["prec_A"], model["prec_B"], model["prec_r"])
            @printf("%d, %.4f (%.4f) (%.2fs) ", iter, avg_rmse_t, cur_rmse_t, ts)

            if !is_burnin && avg_rmse_t < best_rmse 
                best_rmse = avg_rmse_t
                @printf("(**)")
            end
            @printf("\n") 

            push!(logs["iter"], iter)
            push!(logs["ts"], ts)
            push!(logs["avg_rmse"], avg_rmse_t)
            push!(logs["cur_rmse"], cur_rmse_t)
            para["iter"] = iter
            para["ts"]   = ts
            para["best"] = best_rmse
            logs["para"] = para
            
            #if is_burnin == true  # check if burnin is finished
                #is_burnin = (cur_rmse_t > para["bthresh"])
                #if is_burnin == false
                    #@printf("burnin finished\n")
                    #logs["burnin_ts"] = ts
                #end
            #end
        end

        # Save logs file
        if mod(iter, itvsv) == 0
            @printf("saving loggings ... ")
            para["iter"] = iter; 
            para["ts"]   = ts
            cur_logfile  = save_logs4(logs, log_dir, cur_logfile)
            @printf("done\n")
            if ts > mxsec
                break
            end
        end

        iter += 1

    end
end



