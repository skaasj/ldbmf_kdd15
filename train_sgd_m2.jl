using Devectorize
using HDF5, JLD
using Datetime
include("./utils/types.jl")
include("./utils/mf_common.jl")
include("./utils/utils.jl")

function train_sgd_m2{T}(trainset::Array{T,2}, testset::Array{T,2},
                         model::Dict{ASCIIString,Any}, para::Dict{ASCIIString, Any})
    
    para["algo"]  = "SGD-2"
    #reg::Float64  = para["reg"]
    eps::Float64  = para["eps"] 
    mom::Float64  = para["mom"]
    rndlen::Int64 = para["rndlen"]
    mxiter        = para["mxiter"]
    itvtst        = para["itvtst"]
    itvsv         = para["itvsv"]
    mxsec         = para["mxsec"]

    println(para)
    @printf("Running %s\n", para["algo"])
    log_dir = "00_results_"para["algo"]
    cur_logfile = get_str_id2(para)
    #logs = Log2(zeros(Int64,0), zeros(0), zeros(0), zeros(0,0), zeros(0,0), para)
    logs = Dict{ASCIIString,Any}()
    logs["iter"] = Int64[]
    logs["rmse"] = Float64[]
    logs["ts"]   = Float64[]

    P = model["P"]
    Q = model["Q"]
    A = model["A"]
    B = model["B"]
    prec_P = model["prec_P"]
    prec_Q = model["prec_Q"]
    prec_A = model["prec_A"]
    prec_B = model["prec_B"]

    K = size(P,1)
    iter = 1            
    ts = 0.0          

    min_rate    = float(minimum(trainset[:,3]))
    max_rate    = float(maximum(trainset[:,3]))
    mean_rate   = mean(trainset[:,3])
    sz_trainset = size(trainset)[1]
    n_users     = size(P)[2]
    n_items     = size(Q)[2]

    grad_Pu_mom = zeros(K, n_users)
    grad_Qi_mom = zeros(K, n_items)
    grad_Au_mom = zeros(n_users)
    grad_Bi_mom = zeros(n_items)

    Pu = zeros(Float64,K,1)
    
    #best_rmse = rmse(P, Q, testset, min_rate, max_rate, mean_rate)
    best_rmse = rmse_m1(P, Q, A, B, prec_P, prec_Q, prec_A, prec_B, testset, min_rate, max_rate, mean_rate)

    @assert rndlen < sz_trainset

    start_idx = 1
    while iter <= mxiter && ts < mxsec
        
        tic()
        ####################################################################
        
        batch_idx, start_idx = get_next_batch_idx(start_idx, rndlen, sz_trainset)

        sgd_update_1_round!(trainset, batch_idx, P, Q, A, B, prec_P, prec_Q, prec_A, prec_B, grad_Pu_mom, grad_Qi_mom, grad_Au_mom, grad_Bi_mom, mean_rate, mom, eps)

        iter += 1

        ####################################################################
        ts += toq()

        # Compute RMSE and store logs
        if mod(iter, itvtst) == 0
            rmse_t = rmse_m1(P, Q, A, B, prec_P, prec_Q, prec_A, prec_B, testset, min_rate, max_rate, mean_rate)
            @printf("t:%d, rmse:%.4f (%.2f sec.) ", iter, rmse_t, ts)
            if rmse_t < best_rmse
                best_rmse = rmse_t
                print("(**)")
            end
            println()
            #tic()
            push!(logs["iter"], iter)
            push!(logs["ts"], ts)
            push!(logs["rmse"], rmse_t)
            para["iter"] = iter; para["ts"] = ts; para["best"] = best_rmse
            logs["para"] = para 
            #ts += toq()
        end
        
        # Save logs to file
        if mod(iter, itvsv) == 0
            @printf("saving logs ... ")
            para["iter"] = iter;  para["ts"] = ts
            cur_logfile = save_logs4(logs, log_dir, cur_logfile)
            @printf("done.\n")
            if ts > mxsec
                break
            end
        end       
    end
end


function sgd_update_1_round!{T}(trainset::Array{T,2}, batch_idx::Array{Int64,1}, 
                            P::Array{Float64,2}, Q::Array{Float64,2}, 
                            A::Array{Float64,1}, B::Array{Float64,1}, 
                            prec_P::Array{Float64,1}, prec_Q::Array{Float64,1}, 
                            prec_A::Float64, prec_B::Float64, 
                            grad_Pu_mom::Array{Float64,2}, grad_Qi_mom::Array{Float64,2}, 
                            grad_Au_mom::Array{Float64,1}, grad_Bi_mom::Array{Float64,1}, 
                            mean_rate::Float64, mom::Float64, eps::Float64)
    u, i, r, error = 0, 0, 0.0, 0.0
    #N = size(trainset,1)
    #Pu = zeros(K,1)
    for ix in batch_idx
        @inbounds u, i, r = trainset[ix,:]
        @inbounds error = (r - mean_rate) - (dot(P[:,u], Q[:,i]) + A[u] + B[i])

        @inbounds grad_Pu_mom[:,u] = mom * grad_Pu_mom[:,u] + eps * (error * Q[:,i] - broadcast(*, prec_P, P[:,u]))
        @inbounds grad_Qi_mom[:,i] = mom * grad_Qi_mom[:,i] + eps * (error * P[:,u] - broadcast(*, prec_Q, Q[:,i]))
        @inbounds grad_Au_mom[u]   = mom * grad_Au_mom[u]   + eps * (error - prec_A * A[u])
        @inbounds grad_Bi_mom[i]   = mom * grad_Bi_mom[i]   + eps * (error - prec_B * B[i])

        #@inbounds Pu = P[:,u] .+ grad_Pu_mom[:,u]
        @inbounds P[:,u] = P[:,u] + grad_Pu_mom[:,u]
        @inbounds Q[:,i] = Q[:,i] + grad_Qi_mom[:,i] 
        @inbounds A[u]   = A[u]   + grad_Au_mom[u]
        @inbounds B[i]   = B[i]   + grad_Bi_mom[i]
    end
    return nothing
end

