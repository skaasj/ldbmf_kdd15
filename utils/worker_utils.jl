include("../c_wrappers.jl")

function p_localize_idx(args)
    @printf("start p_localize_idx ... ")
    #global _is_localized, _map_uid_g2l, _map_uid_l2g, _map_iid_g2l, _map_iid_l2g
    #if _is_localized == true
        #@printf("already localized\n")
        #return _map_uid_g2l, _map_iid_g2l, _map_uid_l2g, _map_iid_l2g
    #end
    block_info = cell(length(_train_blks))
    for (b,block) in enumerate(_train_blks)
        block_info[b] = localize_block(block)
    end
    @printf("done\n")

    return block_info
end


function localize_block(block)
 
    map_uid_g2l = Dict{Int64, Int64}()   
    #map_uid_l2g = Dict{Int64, Int64}() 
    map_iid_g2l = Dict{Int64, Int64}() 
    #map_iid_l2g = Dict{Int64, Int64}()
    n_elem_row = Int64[]
    n_elem_col = Int64[]
    
    for ix in 1:size(block,1)
        uid_g, iid_g, rate = block[ix,:]
        uid_l = get(map_uid_g2l, uid_g, 0)
        if uid_l == 0 # unseen user
            uid_l = map_uid_g2l[uid_g] = length(map_uid_g2l) + 1
            #map_uid_l2g[uid_l] = uid_g
            push!(n_elem_row, 0)
        end
        iid_l = get(map_iid_g2l, iid_g, 0)
        if iid_l == 0 # unseen item
            iid_l = map_iid_g2l[iid_g] = length(map_iid_g2l) + 1
            #map_iid_l2g[iid_l] = iid_g
            push!(n_elem_col, 0)
        end
        block[ix,:] = [uid_l, iid_l, rate]'
        n_elem_row[uid_l] += 1
        n_elem_col[iid_l] += 1
    end

    blk_info = Dict{ASCIIString, Any}()
    blk_info["map_uid_g2l"] = map_uid_g2l
    blk_info["map_iid_g2l"] = map_iid_g2l
    blk_info["n_elem_row"]  = n_elem_row
    blk_info["n_elem_col"]  = n_elem_col

    return blk_info
end


function p_localize_Xs(args)
    
    @printf("started p_localize_Xs ... ")
    global _is_localized, _map_uid_g2l, _map_uid_l2g, _map_iid_g2l, _map_iid_l2g
    
    if _is_localized == true
        @printf("already localized\n")
        return _map_uid_g2l, _map_iid_g2l, _map_uid_l2g, _map_iid_l2g
    end

    N_Xs = size(_Xs,1)
    n_elem_row = Int64[]
    n_elem_col = Int64[]

    for ix in 1:N_Xs
        uid_g, iid_g, rate = _Xs[ix,:]
        uid_l = get(_map_uid_g2l, uid_g, 0)
        if uid_l == 0 # unseen user
            uid_l = _map_uid_g2l[uid_g] = length(_map_uid_g2l) + 1
            _map_uid_l2g[uid_l] = uid_g
            push!(n_elem_row, 0)
        end
        iid_l = get(_map_iid_g2l, iid_g, 0)
        if iid_l == 0 # unseen item
            iid_l = _map_iid_g2l[iid_g] = length(_map_iid_g2l) + 1
            _map_iid_l2g[iid_l] = iid_g
            push!(n_elem_col, 0)
        end
        _Xs[ix,:] = [uid_l, iid_l, rate]'
        n_elem_row[uid_l] += 1
        n_elem_col[iid_l] += 1
    end

    _is_localized = true
    
    @printf("done\n")
    
    return _map_uid_g2l, _map_iid_g2l, _map_uid_l2g, _map_iid_l2g, n_elem_row, n_elem_col
end



function p_rmse_avg(args)
    cid::Int64 = args[1]
    if cid == -1
        return -1,-1,-1,-1
    end
    iter::Int64, 
    model::Dict{ASCIIString,Any}, 
    avg_pred::Array{Float64,1}, 
    avg_cnt::Int64, 
    min_rate::Float64, 
    max_rate::Float64, 
    mean_rate::Float64, 
    is_burnin::Bool = args[2:end]
    sz_testset = size(_testset::Array{Float64,2}, 1)
    
    avg_rmse = 0.0

    #block_test = false
    #if block_test
        #preds = Array(Float64, sz_testset)
        #predict!(preds, model["P"], model["Q"], model["A"], model["B"], mean_rate)
    #else
        #uu, ii, rr = _testset[:,1], _testset[:,2], _testset[:,3]
        ## NOTE:below operation requires large memory
        #preds = vec(sum(model["P"][:,uu] .* model["Q"][:,ii], 1)) .+ model["A"][uu] .+ model["B"][ii] + mean_rate
    #end
    ## filter out of range predicts
    #preds[preds .< min_rate] = min_rate
    #preds[preds .> max_rate] = max_rate
    #if is_burnin == false
        #avg_cnt += 1
        #avg_pred = (1 - 1/avg_cnt) * avg_pred + 1/avg_cnt * preds
        #avg_rmse = sqrt(sum(sqr(_testset[:,3] - avg_pred))/size(_testset,1))
    #end
    ## compute rmse
    #cur_rmse = sqrt(sum(sqr(_testset[:,3] - preds))/size(_testset,1))

    avg_rmse_t, cur_rmse_t, avg_cnt_t = c_rmse_avg_m2!(_testset, iter, model["P"], model["Q"], model["A"], model["B"], avg_pred, avg_cnt, min_rate, max_rate, mean_rate, is_burnin)
    
    return avg_rmse_t, cur_rmse_t, avg_pred, avg_cnt_t
end

function predict!(preds::Array{Float64,1},
                  P::Array{Float64,2}, Q::Array{Float64,2}, 
                  A::Array{Float64,1}, B::Array{Float64,1},
                  mean_rate::Float64
                  )
    #u, i = 0, 0
    sz_batch = 1000000
    sz_testset = size(_testset,1)
    n_processed = 0
    n_remained = sz_testset
    while n_remained > 0
        if n_remained >= sz_batch
            idx = [n_processed + 1 : n_processed + sz_batch]
            n_processed += sz_batch
            n_remained -= sz_batch
        else
            idx = [n_processed + 1 : n_processed + n_remained]
            n_processed += n_remained
            n_remained -= n_remained
        end
        uu, ii = _testset[idx,1], _testset[idx,2]
        preds[idx] = vec(sum(P[:,uu] .* Q[:,ii],1)) .+ A[uu] .+ B[ii] + mean_rate
    end
    #preds += mean_rate
    return nothing
end

