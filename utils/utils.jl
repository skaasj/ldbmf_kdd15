function get_dcr_stepsize(t, eps0)
    epsilon, tau0, kappa = (0.05, 1000, 0.7)
    stepsz(t) = (epsilon * ((1 + t/tau0)^(-kappa)))
end


# Check if 'filename' exists in the 'path'
function file_exist(path, filename)
    n = length(filter(x->(x==filename), readdir(path))) 
    if n == 0
        return false
    elseif n == 1
        return true
    else
        error("must not reach here")
    end
end


function rand_samp(N, n, method="w_replacement")
    if method == "w_replacement"    
        idx = ceil(rand(n)*N)
    elseif method == "wo_replacement"
        idx = randperm(N)[1:n]
    else
        error("not defined method")
    end
    return idx
end


using HDF5, JLD
# Save logs into a new log file and delete old log file.
## The filename changes only for current iteration
#function save_logs(logs, cur_str_id, new_t, algo)
    #if !file_exist(".","00_results")    # include utils.jl
        #run(`mkdir 00_results`)
    #end
    ## Apply current iteration to the new filename
    #new_str_id = cur_str_id[1:rsearch(cur_str_id,'_')]"t""$new_t"
    ## Save new log
    #save("./00_results/$new_str_id.jd", "logs", logs)
    ## Delete old log
    #run(`rm -f 00_results/$cur_str_id.jd`)
    #new_str_id
#end

#function save_logs(logs, cur_str_id, new_t, algo, best_error)
    #if !file_exist(".","00_results")    # include utils.jl
        #run(`mkdir 00_results`)
    #end
    ## Apply current iteration to the new filename
    #new_str_id = cur_str_id[1:rsearch(cur_str_id,"_rmse")[1]]"rmse_"@sprintf("%.4f",best_error)"_t""$new_t"
    ## Save new log
    #save("./00_results/$new_str_id.jd", "logs", logs)
    ## Delete old log
    #run(`rm -f 00_results/$cur_str_id.jd`)
    #new_str_id
#end


using Datetime, Debug
function get_str_id2(conf::Dict{ASCIIString, Any})
    filename = replace(replace(string(Datetime.now(Datetime.PST)), ":",""),"-","")[1:end-4]
    for (key,val) in conf
        if typeof(val) == Float64
            #if key == "stepsz"
                #val_str = @sprintf("%.8f", val) 
            if key == "best"
                val_str = @sprintf("%.4f", val)
            elseif key == "ts" 
                val_str = @sprintf("%.1f", val)
            elseif key == "max_round" || key == "maxsec"
                val_str = @sprintf("%.1fK", val/1000)
            else
                val_str = string(val)
            end
        else
            val_str = string(val)
        end
        filename = "$filename""_$key"val_str
    end
    return replace(filename, ".", "_")
end


#function save_logs2(logs, filename)
    #if !file_exist(".","00_results")    # include utils.jl
        #run(`mkdir 00_results`)
    #end
    #save("./00_results/$filename.jd", "logs", logs)
    ##println(filename)
#end


function save_logs3(para::Dict{ASCIIString, Any}, logs::Log2, log_dir::ASCIIString, cur_logfile::ASCIIString)
    if !isdir(log_dir)
        run(`mkdir $log_dir`)
    end
    new_logfile = get_str_id2(para)
    save("$log_dir/$new_logfile.jd", "logs", logs)
    run(`rm -f $log_dir/$cur_logfile.jd`)
    cur_logfile = new_logfile
    return new_logfile
end


function save_logs4(logs::Dict{ASCIIString,Any}, log_dir::ASCIIString, cur_logfile::ASCIIString)
    if !isdir(log_dir)
        run(`mkdir $log_dir`)
    end
    new_logfile = get_str_id2(logs["para"])
    save("$log_dir/$new_logfile.jd", "logs", logs)
    run(`rm -f $log_dir/$cur_logfile.jd`)
    cur_logfile = new_logfile
    return new_logfile
end





