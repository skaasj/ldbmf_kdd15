type Rating
    user::Int64
    item::Int64
    rate::Float64
end

type Dataset
    users::Array{Int64,1}
    items::Array{Int64,1}
    rates::Array{Float64,1}
    ts::Array{Float64,1}
    n_users::Int64
    n_items::Int64
    size::Int64
end

type Bound
    rowbegin::Int64
    rowend::Int64
    colbegin::Int64
    colend::Int64
end

type Block
    elems::Array{(Int64,Int64,Float64), 1}
    #users::Array{Int64,1}
    #items::Array{Int64,1}
    #rates::Array{Int64,1}
    #user_start_index::Array{Int64,1}
end

type RatingSet
    train_set::Array{Rating, 1}
    test_set::Array{Rating, 1}
    user_to_index::Dict{String, Int64}
    item_to_index::Dict{String, Int64}
end

type Features
    user::Array{Int32,2}
    item::Array{Int32,2}
end

type Log
    iter::Array{Int64,1}
    ts::Array{Float64,1}
    rmse::Array{Float64,1}
end

type Log2
    iter::Array{Int64,1}
    ts::Array{Float64,1}
    rmse::Array{Float64,1}
    P_star::Array{Float64,2}
    Q_star::Array{Float64,2}
    para::Dict{ASCIIString, Any}
end

# type ItemFeature
#     year::Int64
#     genre::Array{Bool,1}
# end
# 
# type UserFeature
#     gender::Bool
#     age::Int32
#     occupation::Int32
#     zipcode::Int64
# end
