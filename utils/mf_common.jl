function rmse(P, Q, testset::Dataset)
    sse = 0.0
    n_testset = length(testset.users)
    for idx = 1:n_testset
        u = testset.users[idx]    
        i = testset.items[idx]    
        r = testset.rates[idx]
        pred = dot(P[:,u], Q[:,i])
        if pred < 1.; pred = 1. 
        elseif pred > 5.; pred = 5. 
        end 
        a = r.rate - pred
        sse += a*a # for speed, '^2' is avoided
        #sse += (r - pred) * (r - pred) # for speed, '^2' is avoided
    end
    sqrt(sse/n_testset)
end



function rmse{T}(P, Q, testset::Array{T,2}, min_rate::Float64=0, max_rate::Float64=5, mean_rate::Float64=0)
    Puu = P[:,testset[:,1]]
    Qii = Q[:,testset[:,2]]
    # dot products
    pred = vec(sum(Puu .* Qii, 1)) + mean_rate
    # filter out of range predicts
    pred[pred .< min_rate] = min_rate
    pred[pred .> max_rate] = max_rate
    # compute RMSE
    return sqrt(sum(sqr(testset[:,3] - pred))/size(testset,1))
end



function rmse_m1{T}(M::Dict{ASCIIString,Any}, testset::Array{T,2}, min_rate::Float64, max_rate::Float64, mean_rate::Float64)
    uu, ii, rr = testset[:,1], testset[:,2], testset[:,3]
    pred = vec(sum(M["P"][:,uu] .* M["Q"][:,ii],1)) .+ M["A"][uu] .+ M["B"][ii] + mean_rate
    pred[pred .< min_rate] = min_rate
    pred[pred .> max_rate] = max_rate
    rmse = sqrt(sum(sqr(rr - pred)) / size(testset,1))
    return rmse
end

function rmse_m1{T}(P::Array{Float64,2}, Q::Array{Float64,2}, 
                    A::Array{Float64,1}, B::Array{Float64,1}, 
                    prec_P::Array{Float64,1}, prec_Q::Array{Float64,1}, 
                    prec_A::Float64, prec_B::Float64, 
                    testset::Array{T,2}, min_rate::Float64, max_rate::Float64, mean_rate::Float64)
    uu, ii, rr = testset[:,1], testset[:,2], testset[:,3]
    pred = vec(sum(P[:,uu] .* Q[:,ii],1)) + A[uu] + B[ii] + mean_rate
    pred[pred .< min_rate] = min_rate
    pred[pred .> max_rate] = max_rate
    rmse = sqrt(sum(sqr(rr - pred)) / size(testset,1))
    return rmse
end


function rmse_avg{T}(testset::Array{T,2}, iter::Int64, P::Array{Float64,2}, Q::Array{Float64,2}, avg_pred::Array{Float64,1}, avg_cnt::Int64, min_rate::Float64, max_rate::Float64, mean_rate::Float64, is_burnin::Bool)
    avg_rmse = 0.0
    rr = testset[:,3]
    # dot products
    pred = vec(sum(P[:,int(testset[:,1])] .* Q[:,int(testset[:,2])], 1)) + mean_rate
    # filter out of range predicts
    pred[pred .< min_rate] = min_rate
    pred[pred .> max_rate] = max_rate
    if !is_burnin
        avg_pred = (1 - 1/avg_cnt) * avg_pred + 1/avg_cnt * pred
        avg_rmse = sqrt(sum(sqr(rr - avg_pred))/size(testset,1))
    end
    # compute RMSE
    cur_rmse = sqrt(sum(sqr(rr - pred))/size(testset,1))
    return avg_rmse, cur_rmse, avg_pred
end


function rmse_avg{T}(testset::Array{T,2}, iter::Int64, P::Array{Float64,2}, Q::Array{Float64,2}, bias_U::Array{Float64,1}, bias_I::Array{Float64,1}, avg_pred::Array{Float64,1}, avg_cnt::Int64, min_rate::Float64, max_rate::Float64, mean_rate::Float64, is_burnin::Bool)
    avg_rmse = 0.0
    uu, ii, rr = testset[:,1], testset[:,2], testset[:,3]
    # dot products
    pred = vec(sum(P[:,uu] .* Q[:,ii], 1)) .+ bias_U[uu] .+ bias_I[ii] .+ mean_rate
    # filter out of range predicts
    pred[pred .< min_rate] = min_rate
    pred[pred .> max_rate] = max_rate
    if !is_burnin
        avg_pred = (1 - 1/avg_cnt) * avg_pred + 1/avg_cnt * pred
        error = rr - avg_pred
        avg_rmse = sqrt(sum(error .* error)/size(testset,1))
    end
    # compute rmse
    error = rr - pred
    cur_rmse = sqrt(sum(error .* error)/size(testset,1))
    return avg_rmse, cur_rmse, avg_pred
end
#function rmse(P, Q, testset, min_rate::Float64=0, 
              #max_rate::Float64=5, mean_rate::Float64=0)
    #sse = 0.0
    #n_testset = size(testset,1)
    #for idx = 1:n_testset
        #u, i::Int64, r::Int64 = testset[idx,:]
        #pred = dot(P[:,u], Q[:,i]) + mean_rate
        #if pred < min_rate; pred = min_rate
        #elseif pred > max_rate; pred = max_rate
        #end
        #sse += sqr(r - pred) # for speed, '^2' is avoided
    #end 
    #return sqrt(sse/n_testset)
#end

function rmse(P, Q, testset::Array{Rating,1}, range_min, range_max)
    sse = 0.0
    for r in testset
        pred = dot(P[:,r.user], Q[:,r.item])
        if pred < range_min; pred = range_min 
        elseif pred > range_max; pred = range_max
        end
        a = r.rate - pred
        sse += a*a # for speed, '^2' is avoided
    end
    sqrt(sse/length(testset))
end



# Refer to D-SGLD paper
function annealed_stepsize(t; epsilon=0.05, tau0=1000, kappa=0.7)
    return epsilon * ((1 + t./tau0).^(-kappa))
end



## copy data to all workers 
#function pcopy_trainset(trainset::Array{Float64,2})
    #@everywhere _trainset = Array{Float64,2}
    ## line below is a function which assigns the dat passed
    ## to the predefined global variable _trainset. This is
    ## executed at eachch worker by remotecall_fetch()
    #@everywhere copy_trainset(dat) = (global _trainset; _trainset = dat) 
    #for p in procs()
        #remotecall_fetch(p, copy_trainset, trainset)
    #end
    #println("trainset loaded to workers")
#end

## copy data to all workers 
#function pcopy_trainset(trainset::Array{Int64,2})
    #@everywhere _trainset = Array{Int64,2}
    #@everywhere copy_trainset(dat) = (global _trainset; _trainset = dat) 
    #for p in procs()
        #remotecall_fetch(p, copy_trainset, trainset)
    #end
    #println("trainset loaded to workers")
#end

## copy data to all workers 
#function pcopy_trainset2(trainset::Array{Rating,1})
    #@everywhere _trainset = Array{Rating,1}
    ## line below is a function which assigns the dat passed
    ## to the predefined global variable _trainset. This is
    ## executed at eachch worker by remotecall_fetch()
    #@everywhere copy_trainset(dat) = (global _trainset; _trainset = dat) 
    #for p in procs()
        #remotecall_fetch(p, copy_trainset, trainset)
    #end
    #println("trainset loaded to workers")
#end

## copy data to all workers 
#function pcopy_trainset(trainset::Dataset)
    #@everywhere _trainset = Dataset
    ## line below is a function which assigns the dat passed
    ## to the predefined global variable _trainset. This is
    ## executed at eachch worker by remotecall_fetch()
    #@everywhere copy_trainset(dat) = (global _trainset; _trainset = dat) 
    #for p in procs()
        #remotecall_fetch(p, copy_trainset, trainset)
    #end
    #println("trainset loaded to workers")
#end


## copy data to all workers 
#function pcopy_testset(testset::Array{Float64,2})
    #@everywhere _testset = Array{Float64,2}
    #@everywhere copy_testset(dat) = (global _testset; _testset = dat) 
    #for p in procs()
        #remotecall_fetch(p, copy_testset, testset)
    #end
    #println("testset loaded to workers")
#end

## copy data to all workers 
#function pcopy_testset2(testset::Array{Rating,1})
    #@everywhere _testset = Array{Rating,1}
    #@everywhere copy_testset(dat) = (global _testset; _testset = dat) 
    #for p in procs()
        #remotecall_fetch(p, copy_testset, testset)
    #end
    #println("testset loaded to workers")
#end
## copy data to all workers 
#function pcopy_testset(testset::Dataset)
    #@everywhere _testset = Dataset
    #@everywhere copy_testset(dat) = (global _testset; _testset = dat) 
    #for p in procs()
        #remotecall_fetch(p, copy_testset, testset)
    #end
    #println("testset loaded to workers")
#end

# copy data to all workers 
function pcopy_blockmembership(blockmembership)
    @everywhere _blockmembership = Array{Array{Int64,1},1}
    @everywhere copy_blockmembership(dat) = (global _blockmembership; _blockmembership = dat) 
    for p in procs()
        remotecall_fetch(p, copy_blockmembership, blockmembership)
    end
    println("blockmembership loaded to workers")
end



# Used to compute block_id given block row and column. 
# Square block structure (n_row_block = n_col_block) is assumed.
function rowcol2seqid(row, col, n_colblk)
    @assert col <= n_colblk 
    id = (row-1)*n_colblk + col
end


#function block_membership(trainset::Array{Float64,2}, bounds)
    #n_rowblk, n_colblk = size(bounds)
    #n_blks = n_rowblk * n_colblk
    #blockmembership = Array(Array{Int64,1}, n_blks) 
    #for blk = 1:n_blks
        #blockmembership[blk] = zeros(Int64,0)
    #end
    #i::Int64 = 1
    #j::Int64 = 1
    #for idx in 1:size(trainset)[1]
        #user, item = trainset[idx,[1,2]]
        ##user = trainset[idx].user
        ##item = trainset[idx].item
        #for r = 1:n_rowblk
            #if user >= bounds[r,1].rowbegin && user <= bounds[r,1].rowend
                #i = r
                #break
            #end
        #end
        #for c = 1:n_colblk
            #if item >= bounds[1,c].colbegin && item <= bounds[1,c].colend
                #j = c
                #break
            #end
        #end
        #blk_id = rowcol2seqid(i, j, n_colblk)
        #push!(blockmembership[blk_id], idx)
    #end
    #blockmembership
#end

function get_block_members2{T}(trainset::Array{T,2}, bounds)
    n_rowblk, n_colblk = size(bounds)
    n_blks = n_rowblk * n_colblk
    blk_members = Array(Array{Int64,1}, n_rowblk, n_colblk) 
    for r = 1:n_rowblk, c = 1:n_colblk
        blk_members[r,c] = Int64[]
    end
    i::Int64 = 1
    j::Int64 = 1
    for idx in 1:size(trainset,1)
        user, item = trainset[idx,[1,2]]
        for r = 1:n_rowblk
            if user >= bounds[r,1].rowbegin && user <= bounds[r,1].rowend
                i = r
                break
            end
        end
        for c = 1:n_colblk
            if item >= bounds[1,c].colbegin && item <= bounds[1,c].colend
                j = c
                break
            end
        end
        #blk_id = rowcol2seqid(i, j, n_colblk)
        push!(blk_members[i,j], idx)
    end
    return blk_members
end


function get_block_members{T}(trainset::Array{T,2}, bounds)
    n_rowblk, n_colblk = size(bounds)
    n_blks = n_rowblk * n_colblk
    blk_members = Array(Array{Int64,1}, n_blks) 
    for blk = 1:n_blks
        blk_members[blk] = zeros(Int64,0)
    end
    i::Int64 = 1
    j::Int64 = 1
    for idx in 1:size(trainset,1)
        user, item = trainset[idx,[1,2]]
        for r = 1:n_rowblk
            if user >= bounds[r,1].rowbegin && user <= bounds[r,1].rowend
                i = r
                break
            end
        end
        for c = 1:n_colblk
            if item >= bounds[1,c].colbegin && item <= bounds[1,c].colend
                j = c
                break
            end
        end
        blk_id = rowcol2seqid(i, j, n_colblk)
        push!(blk_members[blk_id], idx)
    end
    return blk_members
end


#function get_block_membership(trainset::Dataset, bounds)

    #n_rowblk, n_colblk = size(bounds)
    #n_blks = n_rowblk * n_colblk
    #blockmembership = Array(Array{Int64,1}, n_blks) 
    #for blk = 1:n_blks
        #blockmembership[blk] = zeros(Int64,0)
    #end
    #i::Int64 = 1
    #j::Int64 = 1
    #for idx in 1:trainset.size
        #user = trainset.users[idx]
        #item = trainset.items[idx]
        #for r = 1:n_rowblk
            #if user >= bounds[r,1].rowbegin && user <= bounds[r,1].rowend
                #i = r
                #break
            #end
        #end
        #for c = 1:n_colblk
            #if item >= bounds[1,c].colbegin && item <= bounds[1,c].colend
                #j = c
                #break
            #end
        #end
        #blk_id = rowcol2seqid(i, j, n_colblk)
        #push!(blockmembership[blk_id], idx)
    #end
    #blockmembership
#end



## Divide the matrix into non-overlapping blocks and return
## the bounds of each blocks. Both row and column is divided 
## into nproc() partitions so that in total there exists 
## nproc()^2 blocks.
#function indep_block_ranges(n_users, n_items, n_partition)
    #user_range = int(linspace(1, n_users+1, n_partition+1))
    #item_range = int(linspace(1, n_items+1, n_partition+1))
    #bounds = Array(Bound, n_partition, n_partition)
    #for r = 1:n_partition, c = 1:n_partition
            #bounds[r,c] = Bound(user_range[r], user_range[r+1]-1, item_range[c], item_range[c+1]-1)
    #end
    #bounds
#end


function indep_block_ranges(n_rows, n_cols, n_blk_row, n_blk_col)
    #(n_blk_rows, n_blk_cols) = blocks_dim
    row_range = int(linspace(1, n_rows+1, n_blk_row+1))
    col_range = int(linspace(1, n_cols+1, n_blk_col+1))
    bounds = Array(Bound, n_blk_row, n_blk_col)
    for r = 1:n_blk_row, c = 1:n_blk_col
        bounds[r,c] = Bound(row_range[r], row_range[r+1]-1, col_range[c], col_range[c+1]-1)
    end
    bounds
end



# Assign a block to each process. Blocks must be independent, 
# meaning that any pair of blocks do not share any row or column.
function assign_blocks(t, n_workers, algo)

    @assert t >= 1

    if algo == "MATAVG"
        blk_ids = zeros(Int64, n_workers)
        n_blocks = n_workers * n_workers
        tmp = mod(t, n_blocks) 
        blk_id = tmp == 0 ? n_blocks : tmp
        fill!(blk_ids, blk_id)
    elseif algo == "DSGD"
    # Independent blocks are selected by shifting right 
    # the diagonal blocks
        blk_ids = zeros(Int64, 0)
        for p = 1:n_workers
            shift = mod(t-1, n_workers) 
            col = mod(p+shift, n_workers)
            col = col == 0 ? n_workers : col
            blk_id = rowcol2seqid(p, col, n_workers)
            push!(blk_ids, blk_id)
        end
    elseif algo == "DSGD_R"
    # Independent blocks are selected at random
        blk_ids = zeros(Int64, 0)
        block_cols = randperm(n_workers)
        for p = 1:n_workers
            blk_id = rowcol2seqid(p, block_cols[p], n_workers)
            push!(blk_ids, blk_id)
        end
    end
    return blk_ids
end



# Return the minibatch size of each block. Each block uses 
# random samples of minibatch size. Default minibatch size is
# n_ratings / nprocs()**2. If this number is larger than the
# total number of ratings in a block due to the irregularity
# in the number of ratings in the blocks, then all ratings in 
# the block is used.
function get_epoch_lens(blockmembership, n_ratings, block_sample_ratio)
    epoch_len = zeros(Int64, length(blockmembership))
    n_workers = nprocs() - 1
    algo = "block_propotional"
    #algo = "global_epoch_len"
    if algo == "block_propotional"
        # batch size is proportional to the block size
        for (i,block) in enumerate(blockmembership)
            epoch_len[i] = int(length(block) * block_sample_ratio)
        end
    elseif algo == "global_epoch_len"
        # batch size is global constant. But when the block size
        # is smaller then the global batch size, the batch size
        # is set to be equal to the block size
        default_epoch_len::Int64 = div(n_ratings * block_sample_ratio, n_workers * n_workers)
        for (i,block) in enumerate(blockmembership)
            n_blk_ratings = length(block)
            epoch_len[i] = default_epoch_len > n_blk_ratings ? n_blk_ratings : default_epoch_len
        end
    else
        error("Undefined algorithm type")
    end
    epoch_len
end


function get_next_batch_idx(start_idx::Int64, sz_batch::Int64, sz_total::Int64)
    end_idx = start_idx + sz_batch - 1;
    if end_idx < sz_total
        batch_idx = [start_idx:end_idx]
        new_start_idx = end_idx + 1
    elseif end_idx == sz_total
        batch_idx = [start_idx:end_idx]
        new_start_idx = 1
    else 
        offset = end_idx - sz_total
        batch_idx = vcat([start_idx:sz_total], [1:offset])
        new_start_idx = offset + 1
    end
    @assert length(batch_idx) == sz_batch
    return batch_idx, new_start_idx
end


function get_next_batch(trainset, start_idx, sz_batch)
    sz_trainset = size(trainset)[1]
    end_idx = start_idx + sz_batch - 1;
    if end_idx <= sz_trainset
        batch = trainset[start_idx:end_idx, :]
    else 
        offset = end_idx - sz_trainset
        batch = vcat(trainset[start_idx:end, :], trainset[1:offset-1, :])
    end
    @assert size(batch)[1] == sz_batch
    return batch, end_idx + 1
end


