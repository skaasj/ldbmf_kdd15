function launch_workers(n_worker::Int64)
    @printf("started launch_workers ... ")
    # launch worker processes
    cur_n_worker = nprocs() - 1
    if cur_n_worker < n_worker
        addprocs(n_worker - cur_n_worker)
    elseif cur_n_worker > n_worker
        worker_ids = procs()     # discard master node (id = 1)
        rmprocs(worker_ids[end - (cur_n_worker - n_worker) + 1:end])
    end
    worker_ids = procs()
    @printf("done. %d workers are up: %s\n", n_worker, string(worker_ids))
    return worker_ids
end



function partition_matrix{T}(trainset::Array{T,2}, n_user::Int64, n_item::Int64, block_n_row::Int64, block_n_col::Int64, n_worker::Int64)
    @printf("start partition_matrix ... ")
    @assert mod(block_n_row * block_n_col, n_worker) == 0
    @assert rem(block_n_row, block_n_col) == 0 
    blk_rng = indep_block_ranges(n_user, n_item, block_n_row, block_n_col)
    blk_mbr = get_block_members(trainset, blk_rng)
    @printf("done\n")
    return blk_rng, blk_mbr
end



function pcopy_testset{T}(testset::Array{T,2}, n_model)
    @printf("started pcopy_testset ... ")
    for p in procs()[2:2+n_model-1]
        remotecall_fetch(p, (arg)->(global _testset = arg), testset)
    end
    @everywhere _testset_loaded = true
    @printf("done\n")
end


function init_grad_sum(Us::Array{Array{Int64,1},1},
                       Is::Array{Array{Int64,1},1},
                       K::Int64)
    for (s,p) in enumerate(procs()[2:end])
        remotecall_fetch(p, (arg1,arg2)->(global _grad_sum_P = zeros(arg1, arg2)), K, length(Us[s]))
        remotecall_fetch(p, (arg1,arg2)->(global _grad_sum_Q = zeros(arg1, arg2)), K, length(Is[s]))
        remotecall_fetch(p, (arg1)->(global _grad_sum_A = zeros(arg1)), length(Us[s]))
        remotecall_fetch(p, (arg1)->(global _grad_sum_B = zeros(arg1)), length(Is[s]))
    end
end


function init_grad_mom(Us::Array{Array{Int64,1},1},
                       Is::Array{Array{Int64,1},1},
                       K::Int64)
    for (s,p) in enumerate(procs()[2:end])
        remotecall_fetch(p, (arg1,arg2)->(global _grad_Ps_mom = zeros(arg1, arg2)), K, length(Us[s]))
        remotecall_fetch(p, (arg1,arg2)->(global _grad_Qs_mom = zeros(arg1, arg2)), K, length(Is[s]))
    end
end


function assign_blocks_to_workers(n_row_blk::Int64, n_col_blk::Int64, n_worker::Int64)
    n_blocks = n_row_blk * n_col_blk
    @assert n_blocks == n_worker
    wkr2blk_map = [Int64[w] for w=1:n_worker]
    return wkr2blk_map
end

function assign_blocks_to_workers_dsgd(n_row_blk::Int64, n_col_blk::Int64, n_worker::Int64)
    n_blocks = n_row_blk * n_col_blk
    @assert n_row_blk == n_col_blk == n_worker
    wkr2blk_map = [Int64[(w-1)*n_worker+1: w*n_worker] for w=1:n_worker]
    return wkr2blk_map
end


function pcopy_blocks{T}(trainset::Array{T,2}, 
                         blk_members::Array{Array{Int64,1},1},
                         blk2wkr_map::Array{Array{Int64,1},1},
                         n_worker::Int64)
    @printf("Start pcopy_blocks ... ")
    @sync begin 
        for (w,p) in enumerate(procs()[2:end])
            @async begin
                train_blks_w = [trainset[blk_members[b],:] for b in blk2wkr_map[w]]
                remotecall_fetch(p, (arg)->(global _train_blks = arg), train_blks_w)
            end
        end   
    end
    @printf("done\n")
end


function pcopy_inv_pr_pick(inv_pr_pick_user::Array{Float64,1}, 
                           inv_pr_pick_item::Array{Float64,1},
                           blk_info,
                           wkr_blk_ids
                           )
    @printf("pcopy inv_pr_pick started ... ")
    for (s,p) in enumerate(procs()[2:end])
        blk_ids_for_s = wkr_blk_ids[s]
        inv_pr_pick_user_blks = [inv_pr_pick_user[blk_info[bid]["local_uid"]] for bid in blk_ids_for_s]
        inv_pr_pick_item_blks = [inv_pr_pick_item[blk_info[bid]["local_iid"]] for bid in blk_ids_for_s]
        remotecall_fetch(p, (arg)->(global _inv_pr_pick_user_blks = arg), inv_pr_pick_user_blks)
        remotecall_fetch(p, (arg)->(global _inv_pr_pick_item_blks = arg), inv_pr_pick_item_blks)
    end
    @printf("done\n")
end


function pcopy_inv_pr_pick(inv_pr_pick_user::Array{Float64,1}, 
                           inv_pr_pick_item::Array{Float64,1},
                           localized_uid_s::Array{Array{Int64,1},1}, 
                           localized_iid_s::Array{Array{Int64,1},1})
    @printf("pcopy inv_pr_pick started ... ")
    for (s,p) in enumerate(procs()[2:end])
        remotecall_fetch(p, (arg)->(global _inv_pr_pick_user = arg), inv_pr_pick_user[localized_uid_s[s]])
        remotecall_fetch(p, (arg)->(global _inv_pr_pick_item = arg), inv_pr_pick_item[localized_iid_s[s]])
    end
    @printf("done\n")
end


function comp_inv_pr_pick(blk_info, n_user, n_item, sz_batch)
    n_block = length(blk_info)
    Pr_us = zeros(n_user, n_block)
    Pr_is = zeros(n_item, n_block)
    for (b_id, b_info) in enumerate(blk_info)
        for (ix, uid) in enumerate(b_info["local_uid"])
            Pr_us[uid, b_id] = 1 - (1 - b_info["n_elem_row"][ix] / b_info["Nb"]) ^ sz_batch
        end 
        for (ix, iid) in enumerate(b_info["local_iid"])
            Pr_is[iid, b_id] = 1 - (1 - b_info["n_elem_col"][ix] / b_info["Nb"]) ^ sz_batch
        end
    end
    Pr_u = vec(sum(Pr_us,2)) ./ n_block
    Pr_i = vec(sum(Pr_is,2)) ./ n_block
    return 1 ./ Pr_u, 1 ./ Pr_i
end
#function compute_bias_corrector(N::Int64, sz_batch::Int64, 
                                #n_user::Int64, n_item::Int64,
                                #localized_uid_s::Array{Array{Int64,1},1},
                                #localized_iid_s::Array{Array{Int64,1},1},
                                #n_elem_row_s::Array{Array{Int64,1},1}, 
                                #n_elem_col_s::Array{Array{Int64,1},1})
    #n_worker = length(n_elem_row_s)
    #Pr_us = zeros(Float64, n_user, n_worker)
    #Pr_is = zeros(Float64, n_item, n_worker)
    #Ns = Array(Int64, n_worker)
    #for s in 1:n_worker
        #Ns[s] = sum(n_elem_row_s[s])
    #end
    #for s in 1:n_worker
        #for (ix,uid) in enumerate(localized_uid_s[s])
            #Pr_us[uid,s] = 1 - (1 - n_elem_row_s[s][ix] / Ns[s]) ^ sz_batch
        #end 
        #for (ix,iid) in enumerate(localized_iid_s[s])
            #Pr_is[iid,s] = 1 - (1 - n_elem_col_s[s][ix] / Ns[s]) ^ sz_batch
        #end
    #end
    #Pr_u = vec(sum(Pr_us,2)) ./ n_worker
    #Pr_i = vec(sum(Pr_is,2)) ./ n_worker
    #inv_pr_pick_user = 1 ./ Pr_u
    #inv_pr_pick_item = 1 ./ Pr_i
    #return inv_pr_pick_user, inv_pr_pick_item
#end

function get_blk_info(p_result::Array{Any,1}, wkr2blk_map::Array{Array{Int64,1},1})
    n_block = sum([length(blk_group) for blk_group in wkr2blk_map])
    blk_info = cell(n_block)
    for (wid, result) in enumerate(p_result)
        for (b,blk_id) in enumerate(wkr2blk_map[wid])
            blk_info[blk_id] = result[b]
            blk_info[blk_id]["local_uid"] = get_convert_orders(blk_info[blk_id]["map_uid_g2l"])
            blk_info[blk_id]["local_iid"] = get_convert_orders(blk_info[blk_id]["map_iid_g2l"])
            blk_info[blk_id]["Nb"] = sum(blk_info[blk_id]["n_elem_row"])
            blk_info[blk_id]["host_wid"] = wid
            blk_info[blk_id]["idx_in_wkr"] = b
        end
    end
    return blk_info
end

#function get_blk_info_dsgd(p_result::Array{Any,1}, wkr2blk_map::Array{Array{Int64,1},1})
    #n_block = sum([length(blk_group) for blk_group in wkr2blk_map])
    #blk_info = cell(n_block)
    #for (wid, result) in enumerate(p_result)
        #for (b,blk_id) in enumerate(wkr2blk_map[wid])
            #blk_info[blk_id] = result[b]
            #blk_info[blk_id]["local_uid"] = get_convert_orders(blk_info[blk_id]["map_uid_g2l"])
            #blk_info[blk_id]["local_iid"] = get_convert_orders(blk_info[blk_id]["map_iid_g2l"])
            #blk_info[blk_id]["Nb"] = sum(blk_info[blk_id]["n_elem_row"])
            #blk_info[blk_id]["host_wid"] = wid
            #blk_info[blk_id]["idx_in_wkr"] = b
        #end
    #end
    #return blk_info
#end


function pcopy_shards{T}(trainset::Array{T,2}, 
                         Xs_idx::Array{Array{Int64,1},1},
                         aug_counts)
    
    @printf("Loading shard started ... ")
    #@everywhere _Xs           = Array{Int64,2}
    #@everywhere _aug_count    = Array{Int8,1}    
    #@everywhere copy_shard(dat) = (global _Xs; _Xs = dat)
    #@everywhere copy_ac(dat)    = (global _aug_count; _aug_count = dat)
    #for (s,p) in enumerate(procs()[2:end])
        ##shard_idx = vcatcRs_idx[s], Qs_idx[s])
        #remotecall_fetch(p, copy_shard, trainset[Xs_idx[s],:])
        #remotecall_fetch(p, copy_ac, aug_counts[Xs_idx[s]])
    #end
    for (s,p) in enumerate(procs()[2:end])
        remotecall_fetch(p, (arg)->(global _Xs = arg), trainset[Xs_idx[s],:])
        remotecall_fetch(p, (arg)->(global _aug_count = arg), aug_counts[Xs_idx[s]])
    end
    @printf("done\n")
end


function get_local_train_idx(blk_mbr::Array{Array{Int64,1},1}, sz_trset::Int64, n_worker::Int64, aug_scale::Float64)
    
    @printf("started augment_block... ")
    #n_worker = para["S"]
    #aug_scale = para["aug_scale"]
    n_block = length(blk_mbr)
    @assert mod(n_block, n_worker) == 0
    n_block_per_worker = div(n_block, n_worker)

    ###########################################
    # assign idx of original set R to workers. Below algo. fills a row 
    # and goes down to fill next row from left most
    Rs_idx = [zeros(Int64,0) for s in 1:n_worker]
    sz_Rs = zeros(Int64, n_worker)
    blocks_for_s = Array(Array{Int64,1}, n_worker)
    for s in 1:n_worker
        blocks_for_s[s] = [(s-1)*n_block_per_worker+1: s*n_block_per_worker]
        for b in 1:size(blocks_for_s[s],1)
            Rs_idx[s] = vcat(Rs_idx[s], blk_mbr[blocks_for_s[s][b]])
        end
        #Rs_idx[s] = vcat(blk_mbr[blocks_for_s[s]])
        sz_Rs[s] = length(Rs_idx[s])
    end
    @assert sum(sz_Rs) == sz_trset 
    return Rx_idx, sz_Rs
end


function augment_blocks{Int64}(blk_mbr::Array{Array{Int64,1},1}, sz_trset::Int64, n_worker::Int64, aug_scale::Float64)
    
    @printf("started augment_block... ")
    #n_worker = para["S"]
    #aug_scale = para["aug_scale"]
    n_block = length(blk_mbr)
    @assert mod(n_block, n_worker) == 0
    n_block_per_worker = div(n_block, n_worker)

    ###########################################
    # assign idx of original set R to workers. Below algo. fills a row 
    # and goes down to fill next row from left most
    Rs_idx = [zeros(Int64,0) for s in 1:n_worker]
    sz_Rs = zeros(Int64, n_worker)
    blocks_for_s = Array(Array{Int64,1}, n_worker)
    for s in 1:n_worker
        blocks_for_s[s] = [(s-1)*n_block_per_worker+1: s*n_block_per_worker]
        for b in 1:size(blocks_for_s[s],1)
            Rs_idx[s] = vcat(Rs_idx[s], blk_mbr[blocks_for_s[s][b]])
        end
        #Rs_idx[s] = vcat(blk_mbr[blocks_for_s[s]])
        sz_Rs[s] = length(Rs_idx[s])
    end
    @assert sum(sz_Rs) == sz_trset 

    ##########################################
    sz_Qs = zeros(Int64, n_worker) 
    aug_counts = ones(Int8, sz_trset)
    Qs_idx = [zeros(Int64,0) for s in 1:n_worker]
    Xs_idx = Array(Array{Int64,1}, n_worker)
    N_Xs = zeros(Int64, n_worker)

    if aug_scale > 0.0
        ##########################################
        # compute augmented shard size for each worker
        # so that all worker have same number of ratings (Rs + Qs) if aug_scale > 0.0
        for s in 1:n_worker
            sz_Qs[s] = int(ceil(aug_scale * (sz_trset - maximum(sz_Rs)))) + (maximum(sz_Rs) - sz_Rs[s])
        end
        @assert all([sz_Qs[s] + sz_Rs[s] for s in 1:n_worker]) > 0

        ##########################################
        # allocate augmented set shards
        #Qs_idx = Array(Array{T,1}, n_worker)
        get_next_worker(cur) = (cur = mod(cur + 1, n_worker); 
                                if cur == 0; cur = n_worker; end; 
                                cur)
        for s in 1:n_worker
            # t is target worker to sample Qs_idx
            t = get_next_worker(s)
            #t = mod(s + 1, n_worker); if t == 0; t = n_worker; end 
            n_remain = sz_Qs[s]
            while t != s && n_remain > 0
                if sz_Rs[t] >= n_remain
                    Qs_idx[s] = vcat(Qs_idx[s], Rs_idx[t][1:n_remain])
                    n_remain = 0
                else
                    Qs_idx[s] = vcat(Qs_idx[s], Rs_idx[t])
                    n_remain -= sz_Rs[t]
                    t = get_next_worker(t)
                    #t = mod(t + 1, n_worker); if t == 0; t = n_worker; end
                end
            end
            @assert length(Qs_idx[s]) == sz_Qs[s]
            aug_counts[Qs_idx[s]] += 1
        end
    end
    ##########################################
    # combine Rs and Qs
    for s in 1:n_worker
        Xs_idx[s] = vcat(Rs_idx[s], Qs_idx[s])
        N_Xs[s] = sz_Rs[s] + sz_Qs[s]
    end   
    
    @printf("done\n")
    return Xs_idx, N_Xs, aug_counts
end

function get_convert_orders(map_g2l::Dict{Int64,Int64})
    #localized_idx  = Array{Int64,1}()
    #global_idx     = Array{Int64,1}()
    #localized_idx, global_idx = get_sort_index(map_g2l)
    return get_sort_index(map_g2l)[1]
end


function get_convert_orders(map_g2l_s::Array{Any,1})
    @printf("started get_covert_orders ... ")
    n_worker = length(map_g2l_s)
    localized_idx_s  = Array(Array{Int64,1}, n_worker)
    global_idx_s     = Array(Array{Int64,1}, n_worker)
    for s in 1:n_worker
        localized_idx_s[s], global_idx_s[s] = get_sort_index(map_g2l_s[s])
    end
    @printf("done\n")
    return localized_idx_s, global_idx_s
end


function get_sort_index(map_g2l::Dict{Int64,Int64})
    g_idx = collect(keys(map_g2l))
    l_idx = collect(values(map_g2l))
    localized_idx  = g_idx[sortperm(l_idx)]
    return localized_idx, g_idx
end


function pre_init_workers(sz_trainset::Int64, para::Dict{ASCIIString,Any})
    for p in procs()[2:end]
        remotecall_fetch(p, ()->(global _is_localized = false;
                                 global _map_uid_g2l  = Dict{Int64,Int64}();
                                 global _map_uid_l2g  = Dict{Int64,Int64}();
                                 global _map_iid_g2l  = Dict{Int64,Int64}();
                                 global _map_iid_l2g  = Dict{Int64,Int64}())
                        )
        remotecall_fetch(p, (arg)->(global _sz_trainset = arg), sz_trainset)
        remotecall_fetch(p, (arg)->(global _mattype = arg), typeof(trainset))
        remotecall_fetch(p, (arg)->(global _para = arg), para)
    end
end


#function post_init_workers(blk_info, n_block::Int64, sz_trainset::Int64)
    ##Ns_scale = Array(Float64, n_worker)
    #for (s,p) in enumerate(procs()[2:end])
        ##Ns_scale[s] = Ns[s] * n_worker
        #remotecall_fetch(p, (arg)->(global _Ns_scale = arg::Float64), Ns[s] * n_worker / N)
    #end
#end
function post_init_workers(n_worker::Int64, Ns::Array{Int64,1}, sz_trainset::Int64)
    #Ns_scale = Array(Float64, n_worker)
    for (s,p) in enumerate(procs()[2:end])
        #Ns_scale[s] = Ns[s] * n_worker
        remotecall_fetch(p, (arg)->(global _Ns_scale = arg::Float64), Ns[s] * n_worker / sz_trainset)
    end
end

function post_init_ddsgld(blk_info, sz_trainset::Int64)
    n_block = length(blk_info)
    for (s,p) in enumerate(procs()[2:end])
        remotecall_fetch(p, (arg)->(global _Ns_scale = arg::Float64), 
                                    blk_info[s]["Nb"] * n_block / sz_trainset)
        remotecall_fetch(p, (arg)->(global _sz_trainset = arg), sz_trainset)
        remotecall_fetch(p, (arg)->(global _mattype = arg), typeof(trainset))
        remotecall_fetch(p, (arg)->(global _para = arg), para)
    end
end


function post_init_dsgd(blk_info, sz_trainset::Int64, wkr2blk_map)
    n_block = length(blk_info)
    for (s,p) in enumerate(procs()[2:end])
        remotecall_fetch(p, (arg)->(global _Ns_scale_blk = arg::Array{Float64,1}), 
                                    [(blk_info[b]["Nb"] * n_block / sz_trainset)::Float64 for b in wkr2blk_map[s]])
        remotecall_fetch(p, (arg)->(global _sz_trainset = arg), sz_trainset)
        remotecall_fetch(p, (arg)->(global _mattype = arg), typeof(trainset))
        remotecall_fetch(p, (arg)->(global _para = arg), para)
    end
end


function compute_n_overlap(n_users::Int64, n_items::Int64, 
                           Us::Array{Array{Int64,1},1},
                           Is::Array{Array{Int64,1},1}
                           )
    n_overlap_row = zeros(Int16, n_users)
    n_overlap_col = zeros(Int16, n_items)
    for U in Us
        for u in U
            n_overlap_row[u] += 1
        end
    end
    for I in Is
        for i in I
            n_overlap_col[i] += 1
        end
    end
    return n_overlap_row, n_overlap_col
end


function gen_ortho_blk_groups(blk_dim)
    n_blocks = blk_dim * blk_dim
    blk_groups = [Int64[] for d in 1:blk_dim]
    for i=1:blk_dim
        blk_group = Int64[]
        for j=1:blk_dim
            row = j
            col = mod(i+(j-1), blk_dim)
            col == 0 ? col = blk_dim : nothing
            push!(blk_groups[i], rowcol2seqid(row, col, blk_dim))
        end
    end
    return blk_groups
end

#@everywhere function p_localize_Xs(args)
    
    #@printf("started p_localize_Xs ... ")
    #global _is_localized, _map_uid_g2l, _map_uid_l2g, _map_iid_g2l, _map_iid_l2g
    
    #if _is_localized == true
        #@printf("already localized\n")
        #return _map_uid_g2l, _map_iid_g2l, _map_uid_l2g, _map_iid_l2g
    #end

    #N_Xs = size(_Xs,1)
    #n_elem_row = Int64[]
    #n_elem_col = Int64[]

    #for ix in 1:N_Xs
        #uid_g, iid_g, rate = _Xs[ix,:]
        #uid_l = get(_map_uid_g2l, uid_g, 0)
        #if uid_l == 0 # unseen user
            #uid_l = _map_uid_g2l[uid_g] = length(_map_uid_g2l) + 1
            #_map_uid_l2g[uid_l] = uid_g
            #push!(n_elem_row, 0)
        #end
        #iid_l = get(_map_iid_g2l, iid_g, 0)
        #if iid_l == 0 # unseen item
            #iid_l = _map_iid_g2l[iid_g] = length(_map_iid_g2l) + 1
            #_map_iid_l2g[iid_l] = iid_g
            #push!(n_elem_col, 0)
        #end
        #_Xs[ix,:] = [uid_l, iid_l, rate]'
        #n_elem_row[uid_l] += 1
        #n_elem_col[iid_l] += 1
    #end

    #_is_localized = true
    
    #@printf("done\n")
    
    #return _map_uid_g2l, _map_iid_g2l, _map_uid_l2g, _map_iid_l2g
#end



#@everywhere function p_rmse_avg(args)
    #cid::Int64 = args[1]
    #if cid == -1
        #return -1,-1,-1,-1
    #end
    
    #iter::Int64, 
    #model::Dict{ASCIIString,Any}, 
    #avg_pred::Array{Float64,1}, 
    #avg_cnt::Int64, 
    #min_rate::Float64, 
    #max_rate::Float64, 
    #mean_rate::Float64, 
    #burnin::Int64 = args[2:end]
    
    #P::Array{Float64,2} = model["P"]
    #Q::Array{Float64,2} = model["Q"]
    #A::Array{Float64,1} = model["A"]
    #B::Array{Float64,1} = model["B"]
    
    #avg_rmse = 0.0
    #uu, ii, rr = _Xts[:,1], _Xts[:,2], _Xts[:,3]
    ## dot products
    #pred = vec(sum(P[:,uu] .* Q[:,ii], 1)) .+ A[uu] .+ B[ii] .+ mean_rate
    ## filter out of range predicts
    #pred[pred .< min_rate] = min_rate
    #pred[pred .> max_rate] = max_rate
    #if iter > burnin
        #avg_cnt += 1
        #avg_pred = (1 - 1/avg_cnt) * avg_pred + 1/avg_cnt * pred
        #avg_rmse = sqrt(sum(sqr(rr - avg_pred))/size(_Xts,1))
    #end
    ## compute rmse
    #cur_rmse = sqrt(sum(sqr(rr - pred))/size(_Xts,1))
    #return avg_rmse, cur_rmse, avg_pred, avg_cnt
#end


