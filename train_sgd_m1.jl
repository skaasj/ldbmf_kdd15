using Devectorize
using HDF5, JLD
using Datetime
include("./utils/types.jl")
include("./utils/mf_common.jl")
include("./utils/utils.jl")

function train_sgd_m1{T}(trainset::Array{T,2}, testset::Array{T,2},
                         model::Dict{ASCIIString,Any}, para::Dict{ASCIIString, Any})
    
    para["algo"]  = "SGD-1"
    #reg::Float64  = para["reg"]
    eps::Float64  = para["eps"] 
    mom::Float64  = para["mom"]
    rndlen::Int64 = para["rndlen"]
    mxiter        = para["mxiter"]
    itvtst        = para["itvtst"]
    itvsv         = para["itvsv"]
    mxsec         = para["mxsec"]

    println(para)
    @printf("Running %s\n", para["algo"])
    log_dir = "00_results_"para["algo"]
    cur_logfile = get_str_id2(para)
    #logs = Log2(zeros(Int64,0), zeros(0), zeros(0), zeros(0,0), zeros(0,0), para)
    logs = Dict{ASCIIString,Any}()
    logs["iter"] = Int64[]
    logs["rmse"] = Float64[]
    logs["ts"]   = Float64[]

    P = model["P"]
    Q = model["Q"]
    mu_p = model["mu_p"]
    mu_q = model["mu_q"]
    La_p = model["La_p"]
    La_q = model["La_q"]

    K = size(P,1)
    iter = 1            
    ts = 0.0          

    min_rate    = float(minimum(trainset[:,3]))
    max_rate    = float(maximum(trainset[:,3]))
    mean_rate   = mean(trainset[:,3])
    sz_trainset = size(trainset)[1]
    n_users     = size(P)[2]
    n_items     = size(Q)[2]

    grad_Pu_mom = zeros(K, n_users)
    grad_Qi_mom = zeros(K, n_items)

    Pu = zeros(Float64,K,1)
    
    best_rmse = rmse(P, Q, testset, min_rate, max_rate, mean_rate)
    #best_rmse = rmse_m1(P, Q, A, B, prec_P, prec_Q, prec_A, prec_B, testset, min_rate, max_rate, mean_rate)

    @assert rndlen < sz_trainset

    start_idx = 1
    while iter <= mxiter && ts < mxsec
        
        tic()
        ####################################################################
        
        batch_idx, start_idx = get_next_batch_idx(start_idx, rndlen, sz_trainset)

        sgd_update_1_round!(trainset, batch_idx, P, Q, mu_p, mu_q, La_p, La_q, grad_Pu_mom, grad_Qi_mom, mean_rate, mom, eps)

        iter += 1

        ####################################################################
        ts += toq()

        # Compute RMSE and store logs
        if mod(iter, itvtst) == 0
            #rmse_t = rmse(P, Q, prec_P, prec_Q, prec_A, prec_B, testset, min_rate, max_rate, mean_rate)
            rmse_t = rmse(P, Q, testset, min_rate, max_rate, mean_rate)
            @printf("t:%d, rmse:%.4f (%.2f sec.) ", iter, rmse_t, ts)
            if rmse_t < best_rmse
                best_rmse = rmse_t
                print("(**)")
            end
            println()
            #tic()
            push!(logs["iter"], iter)
            push!(logs["ts"], ts)
            push!(logs["rmse"], rmse_t)
            para["iter"] = iter; para["ts"] = ts; para["best"] = best_rmse
            logs["para"] = para 
            #ts += toq()
        end
        
        # Save logs to file
        if mod(iter, itvsv) == 0
            @printf("saving logs ... ")
            para["iter"] = iter;  para["ts"] = ts
            cur_logfile = save_logs4(logs, log_dir, cur_logfile)
            @printf("done.\n")
            if ts > mxsec
                break
            end
        end       
    end
end


function sgd_update_1_round!{T}(trainset::Array{T,2}, batch_idx::Array{Int64,1}, 
                            P::Array{Float64,2}, Q::Array{Float64,2}, 
                            mu_p::Array{Float64,1}, mu_q::Array{Float64,1},
                            La_p::Array{Float64,2}, La_q::Array{Float64,2}, 
                            grad_Pu_mom::Array{Float64,2}, grad_Qi_mom::Array{Float64,2}, 
                            mean_rate::Float64, mom::Float64, eps::Float64)
    u, i, r, error = 0, 0, 0.0, 0.0
    for ix in batch_idx
        @inbounds u, i, r = trainset[ix,:]
        @inbounds error = (r - mean_rate) - dot(P[:,u], Q[:,i])
        @inbounds grad_prior_Pu = - La_p * (P[:,u] - mu_p)
        @inbounds grad_prior_Qi = - La_q * (Q[:,i] - mu_q)
        @inbounds grad_Pu_mom[:,u] = mom * grad_Pu_mom[:,u] + eps * (2 * error * Q[:,i] + grad_prior_Pu)
        @inbounds grad_Qi_mom[:,i] = mom * grad_Qi_mom[:,i] + eps * (2 * error * P[:,u] + grad_prior_Qi)
        @inbounds P[:,u] = P[:,u] + grad_Pu_mom[:,u]
        @inbounds Q[:,i] = Q[:,i] + grad_Qi_mom[:,i] 
    end
    return nothing
end

