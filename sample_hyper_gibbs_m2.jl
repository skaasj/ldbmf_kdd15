function sample_hyper_gibbs_m2!(model::Dict{ASCIIString,Any})
 
    P::Array{Float64,2} = model["P"]
    Q::Array{Float64,2} = model["Q"]
    A::Array{Float64,1} = model["A"]
    B::Array{Float64,1} = model["B"]

    K, n_users = size(P)
    n_items = size(Q,2)
 
    sumsqr_P = sum(P .* P, 2)
    sumsqr_Q = sum(Q .* Q, 2)
    beta_P = model["bt0"] + 0.5 * sumsqr_P
    beta_Q = model["bt0"] + 0.5 * sumsqr_Q
    for k in 1:K 
        @inbounds model["prec_P"][k] = 1/rand(InverseGamma(model["al0"] + 0.5 * n_users, beta_P[k]))
        @inbounds model["prec_Q"][k] = 1/rand(InverseGamma(model["al0"] + 0.5 * n_items, beta_Q[k]))
    end
    
    ## sample bias precisions
    sumsqr_A = sum(A .* A)
    sumsqr_B = sum(B .* B)
    beta_A = model["bt0"] + 0.5 * sumsqr_A
    beta_B = model["bt0"] + 0.5 * sumsqr_B
    model["prec_A"]  = 1/rand(InverseGamma(model["al0"] + 0.5 * n_users, beta_A))
    model["prec_B"]  = 1/rand(InverseGamma(model["al0"] + 0.5 * n_items, beta_B))
    
    return nothing
end


