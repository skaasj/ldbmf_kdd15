using Debug
using Datetime
using Devectorize
using NumericExtensions
include("utils/mf_common.jl")
include("utils/utils.jl")
include("utils/rng.jl")

function train_sgld_gibbs{T}(trainset::Array{T,2}, testset::Array{T,2}, 
                             n_users::Int64, n_items::Int64, 
                             n_elem_row::Array{Int64,1}, n_elem_col::Array{Int64,1}, 
                             P::Array{Float64,2}, Q::Array{Float64,2}, 
                             para::Dict{ASCIIString, Any})
#function train_sgld_gibbs{T}(trainset::Array{T,2}, testset::Array{T,2}, n_users::Int64, n_items, n_elem_row, n_elem_col, P, Q, stepsz, sz_batch=3000, round_len=200, maxiter=100, burnin=100, itv_save=Inf, itv_test=1, maxsec=Inf)

    para["algo"] = "SGLD-Full-Gibbs"
    stepsz::Float64 = para["stepsz"] 
    sz_batch::Int64 = para["sz_batch"] 
    round_len::Int64 = para["round_len"]
    burnin::Int64 = para["burnin"]
    maxiter = para["maxiter"]
    maxsec = para["maxsec"] 
    itv_save = para["itv_save"]
    itv_test = para["itv_test"]
    
    println(para)
    @printf("Running %s (v0.04)\n", para["algo"])
    @assert itv_save > itv_test
    
    log_dir = "00_results_"para["algo"]
    cur_logfile = get_str_id2(para)

    # initialization
    #logs = Log(zeros(Int64,0), zeros(Float64,0), zeros(Float64,0))
    logs = Log2(zeros(Int64,0), zeros(Float64,0), zeros(Float64,0), para)
    const min_rate = float(minimum(trainset[:,3]))
    const max_rate = float(maximum(trainset[:,3]))
    const mean_rate = mean(trainset[:,3])
    const sz_trainset = size(trainset)[1]
    const sz_testset = size(testset)[1]
    const K = size(P,1)

    # initial params
    alpha = 2.0   # r_ui ~ N(Pu'Qi, alpha^inv)
    WI_p = eye(K)
    WI_q = eye(K)
    mu0_p = zeros(K)
    mu0_q = zeros(K)
    bt0_p = 2.0
    bt0_q = 2.0
    df_p = float(K)
    df_q = float(K)

    iter = 1        # iter
    ts = 0.0     # timestamp
    best_rmse = 100.0
    avg_cnt = 1  # counts number of averaged predicions
    avg_pred = zeros(Float64,sz_testset)


    while iter <= maxiter && ts < maxsec
    
        tic()
        ########################################################################
        # sample hyper params
        mu_p, mu_q, Lam_P, Lam_Q = sample_hyper_full(P, Q, n_items, n_users, WI_p, WI_q, bt0_p, bt0_q, mu0_p, mu0_q, K)
        
        # sample P and Q
        P, Q  = sample_sgld_full_gibbs(trainset, P, Q, mu_p, mu_q, Lam_P, Lam_Q, alpha, stepsz, n_elem_row, n_elem_col, sz_batch, mean_rate, round_len)
        iter += 1
        ########################################################################
        ts += toq();

        # Compute RMSE
        if mod(iter, itv_test) == 0
            avg_rmse_t, cur_rmse_t, avg_pred = rmse_avg(testset, iter, P, Q, avg_pred, avg_cnt, min_rate, max_rate, mean_rate, burnin)
            if iter > burnin
                avg_cnt += 1
            end
            @printf("%d, %.4f (%.4f) (%.2fs) ", iter, avg_rmse_t, cur_rmse_t, ts)
            if iter > burnin && avg_rmse_t < best_rmse 
                best_rmse = avg_rmse_t
                @printf("(**)\n")
            else @printf("\n") 
            end
            push!(logs.iter, iter)
            push!(logs.ts, ts)
            push!(logs.rmse, avg_rmse_t)
            para["iter"] = iter; para["ts"] = ts; para["best"] = best_rmse
            logs.para = para
        end

        # Save logs file
        if mod(iter, itv_save) == 0
            @printf("saving loggings ... ")
            #new_logfile = get_str_id2(para)
            #save("./$log_dir/$new_logfile.jd", "logs", logs)
            #run(`rm -f ./$log_dir/$cur_logfile.jd`)
            para["iter"] = iter; para["ts"] = ts
            cur_logfile = save_logs3(para, logs, log_dir, cur_logfile)
            #cur_logfile = new_logfile
            @printf("done\n")
            if ts > maxsec
                break
            end
        end
    end
    logs, P, Q
end


function sample_hyper_full(P::Array{Float64,2}, Q::Array{Float64,2}, 
                           n_items::Int64, n_users::Int64,
                           WI_p::Array{Float64,2}, WI_q::Array{Float64,2},
                           bt0_p::Float64, bt0_q::Float64, mu0_p::Array{Float64,1}, 
                           mu0_q::Array{Float64,1}, K::Int64)
    ## sample item params
    const df_p = size(P,1)
    const df_q = df_p
    x_bar = vec(mean(Q,2))
    S_bar = cov(Q')
    WI_post = inv(inv(WI_q) + n_items/1 * S_bar + n_items * bt0_q * (mu0_q - x_bar) * (mu0_q - x_bar)'/(1 * (bt0_q + n_items)))
    WI_post = (WI_post + WI_post')/2
    df_qmost = df_q + n_items
    Lam_Q = rand(Wishart(df_qmost, WI_post))
    mu_temp = (bt0_q * mu0_q + n_items * x_bar) / (bt0_q + n_items)
    lam = chol(inv((bt0_q + n_items) * Lam_Q)); lam = lam'
    mu_q = lam * randn(K) + mu_temp

    ## sample user params
    x_bar = vec(mean(P,2))
    S_bar = cov(P')
    WI_post = inv(inv(WI_p) + n_users/1 * S_bar + n_users * bt0_p * (mu0_p - x_bar) * (mu0_p - x_bar)'/(1 * (bt0_p + n_users)))
    WI_post = (WI_post + WI_post')/2
    df_pmost = df_p + n_users
    Lam_P = rand(Wishart(df_pmost, WI_post))
    mu_temp = (bt0_p * mu0_p + n_users * x_bar) / (bt0_p + n_users)
    lam = chol(inv((bt0_p + n_users) * Lam_P)); lam = lam'
    mu_p = lam * randn(K) + mu_temp

    return mu_p, mu_q, Lam_P, Lam_Q
end



# sample by sgld
function sample_sgld_full_gibbs{T}(trainset::Array{T,2}, 
                     P::Array{Float64,2}, Q::Array{Float64,2}, 
                     mu_p::Array{Float64,1}, mu_q::Array{Float64,1}, 
                     Lam_P::Array{Float64,2}, Lam_Q::Array{Float64,2},
                     alpha::Float64, stepsz::Float64, n_elem_row::Array{Int64,1}, 
                     n_elem_col::Array{Int64,1}, sz_batch::Int64, mean_rate::Float64,
                     round_len::Int64=200)
    sz_trainset = size(trainset)[1]
    K = size(P,1)
    grad_sum_P = zeros(Float64, K, n_users)
    grad_sum_Q = zeros(Float64, K, n_items)
    const ka = alpha * (sz_trainset / sz_batch)
    const halfstepsz = 0.5 * stepsz
    const sqrtstepsz = sqrt(stepsz)
    # sample over a single round
    for iter in 1:round_len
        @inbounds batch = trainset[rand_samp(sz_trainset, sz_batch, "w_replacement"), :]
        fill!(grad_sum_P, 0.0)
        fill!(grad_sum_Q, 0.0)
        uu, ii, rr = batch[:,1], batch[:,2], batch[:,3]
        error = (rr - mean_rate) - vec(sum(P[:,uu] .* Q[:,ii],1))
        grad_Pu = broadcast(*, error', Q[:,ii])
        grad_Qi = broadcast(*, error', P[:,uu])
        for ix = 1:sz_batch
            @inbounds grad_sum_P[:,uu[ix]] += grad_Pu[:,ix]
            @inbounds grad_sum_Q[:,ii[ix]] += grad_Qi[:,ix]
        end
        Prior_P = - Lam_P * broadcast(-, P, mu_p)
        Prior_Q = - Lam_Q * broadcast(-, Q, mu_q)
        noise_P = randn(K, n_users)
        noise_Q = randn(K, n_items)
        @devec P = P .+ halfstepsz .* (ka .* grad_sum_P .+ Prior_P) .+ sqrtstepsz .* noise_P 
        @devec Q = Q .+ halfstepsz .* (ka .* grad_sum_Q .+ Prior_Q) .+ sqrtstepsz .* noise_Q    
    end
    return P, Q
end


#function rmse_avg{T}(testset::Array{T,2}, iter::Int64, P::Array{Float64,2}, Q::Array{Float64,2}, avg_pred::Array{Float64,1}, avg_cnt::Int64, min_rate::Float64, max_rate::Float64, mean_rate::Float64, burnin::Int64)
    #avg_rmse = 0.0
    #rr = testset[:,3]
    ## dot products
    #pred = vec(sum(P[:,int(testset[:,1])] .* Q[:,int(testset[:,2])], 1)) + mean_rate
    ## filter out of range predicts
    #pred[pred .< min_rate] = min_rate
    #pred[pred .> max_rate] = max_rate
    #if iter > burnin
        #avg_pred = (1 - 1/avg_cnt) * avg_pred + 1/avg_cnt * pred
        #avg_rmse = sqrt(sum(sqr(rr - avg_pred))/size(testset,1))
    #end
    ## compute RMSE
    #cur_rmse = sqrt(sum(sqr(rr - pred))/size(testset,1))
    #return avg_rmse, cur_rmse, avg_pred
#end


#function get_str_id(algo, iter::Int64, sz_batch::Int64, burnin::Int64, round_len::Int64, best_rmse::Float64)
    #now_str = replace(replace(string(now(Datetime.PST)), ":",""),"-","")[1:end-4]
    #now_str = "$now_str""_np"string(nprocs()-1)"_K"string(K)"_stsz"@sprintf("%.7f",stepsz)"_szbatch"string(sz_batch)"_roundlen"string(round_len)"_burnin"string(burnin)"_""$algo""_rmse"@sprintf("%.4f",best_rmse)"_t""$iter"
    #now_str = replace(now_str,".","_")
    #now_str
#end

