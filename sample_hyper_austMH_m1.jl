function sample_hyper_aust_m1!(md::Dict{ASCIIString,Any},
                               para::Dict{ASCIIString,Any},
                               ac_rates::Dict{ASCIIString,Float64},
                               cnt::Int64)

    acpt_mu_p, ndata_mu_p = austerity_mu!(md["P"], md["mu_p"], md["mu0_p"], md["bt0_p"], md["La_p_chol"], para["sz_batch"], para["eps_mu_p"], para["tthresh"], true)
 
    acpt_mu_q, ndata_mu_q = austerity_mu!(md["Q"], md["mu_q"], md["mu0_q"], md["bt0_q"], md["La_q_chol"], para["sz_batch"], para["eps_mu_q"], para["tthresh"], false)

    acpt_La_p, ndata_La_p = austerity_La!(md["La_p"], md["La_p_chol"], md["P"], md["mu_p"], md["mu0_p"], md["bt0_p"], para["sz_batch"], para["eps_La_p"], para["tthresh"]::Float64, true)
    
    acpt_La_q, ndata_La_q = austerity_La!(md["La_q"], md["La_q_chol"], md["Q"], md["mu_q"], md["mu0_q"], md["bt0_q"], para["sz_batch"], para["eps_La_q"], para["tthresh"]::Float64, true)

    ac_rates["mu_p"] = (1 - 1/cnt) * ac_rates["mu_p"] + 1/cnt * acpt_mu_p
    ac_rates["mu_q"] = (1 - 1/cnt) * ac_rates["mu_q"] + 1/cnt * acpt_mu_q
    ac_rates["La_p"] = (1 - 1/cnt) * ac_rates["La_p"] + 1/cnt * acpt_La_p
    ac_rates["La_q"] = (1 - 1/cnt) * ac_rates["La_q"] + 1/cnt * acpt_La_q

    #return acpt_mu_p, acpt_mu_q, acpt_LaP, acpt_La_Q
    return nothing
end


function austerity_mu!(U::Array{Float64,2}, mu::Array{Float64,1}, mu0::Array{Float64,1}, bt0::Float64, Lam_chol::Array{Float64,2}, sz_batch::Int64, step::Float64, ttest_thresh::Float64, isU::Bool)

    mu_ = mu + step * randn(size(mu,1))
    iS_muU_mu0_ = sqrt(bt0) * Lam_chol * (mu_ - mu0)
    iS_muU_mu0  = sqrt(bt0) * Lam_chol * (mu  - mu0)
    difflogp = - 0.5 * (iS_muU_mu0_' * iS_muU_mu0_)[1] + 0.5 * (iS_muU_mu0' * iS_muU_mu0)[1]
    M = float(size(U, 2));
    thresh_MH = (log(rand()) - difflogp) / M
    accept, ndata = AR_MH_approx(thresh_MH, U, mu_, mu, Lam_chol, Lam_chol, sz_batch, ttest_thresh, isU)
    if accept
        for i in 1:length(mu); mu[i] = mu_[i]; end
    end
    return accept, ndata

end



function austerity_La!(Lam::Array{Float64,2}, Lam_chol::Array{Float64,2}, U::Array{Float64,2}, mu_U::Array{Float64,1}, mu0_U::Array{Float64,1}, bt0::Float64, sz_batch::Int64, step::Float64, ttest_thresh::Float64, isU::Bool)

    K, N = size(U)
    dof = K
    Lam_ = rand(Wishart(K + 1 + 1/step, step * Lam))
    Lam_chol_ = chol(Lam_)

    iS_muU_mu0_ = sqrt(bt0) * Lam_chol_ * (mu_U - mu0_U)
    iS_muU_mu0  = sqrt(bt0) * Lam_chol  * (mu_U - mu0_U)

    logdetLambda  = 2 * sum(log(diag(Lam_chol )))
    logdetLambda_ = 2 * sum(log(diag(Lam_chol_)))

    difflogl2 =   0.5 * N * (logdetLambda_ - logdetLambda)
    difflogp1 = - 0.5 * (iS_muU_mu0_' * iS_muU_mu0_)[1] + 0.5 * (iS_muU_mu0' * iS_muU_mu0)[1] + 0.5 * (logdetLambda_ - logdetLambda)
    difflogp2 =   0.5 * (dof - K - 1) * logdetLambda_ - 0.5 * trace(Lam_) - 0.5 * (dof - K - 1) * logdetLambda + 0.5 * trace(Lam)

    sqrt_invLambda2_Lambda = Lam_chol  / Lam_chol_
    sqrt_invLambda_Lambda2 = Lam_chol_ / Lam_chol
    difftrace = sqrt_invLambda2_Lambda .* sqrt_invLambda2_Lambda - sqrt_invLambda2_Lambda .* sqrt_invLambda_Lambda2

    difflogtransprob = 0.5 * (K+1+2/step) * (logdetLambda - logdetLambda_) - 0.5 * sum(difftrace) / step
    thresh_MH = (log(rand()) - difflogl2 - difflogp1 - difflogp2 - difflogtransprob) / N

    accept, ndata = AR_MH_approx(thresh_MH, U, mu_U, mu_U, Lam_chol_, Lam_chol, sz_batch, ttest_thresh, isU)
    if accept
        for i=1:K, j=1:K
            Lam[i,j] = Lam_[i,j]
            Lam_chol[i,j] = Lam_chol_[i,j]
        end
    end

    return accept, ndata
end



function AR_MH_approx(thresh_MH::Float64, U::Array{Float64,2}, 
                      mu_::Array{Float64,1}, mu::Array{Float64,1}, 
                      Lam_chol_::Array{Float64,2}, Lam_chol::Array{Float64,2}, 
                      sz_batch::Int64, ttest_thresh::Float64, isU::Bool)
    N = size(U,2);
    randinds = randperm(N)
    sampinds = randinds[1:sz_batch]
    randinds = randinds[sz_batch+1:end]
    n = sz_batch

    iS_U_muU_ = Lam_chol_ * broadcast(-, U[:,sampinds], mu_)
    iS_U_muU  = Lam_chol  * broadcast(-, U[:,sampinds], mu)
    y = - 0.5 * vec(sum(iS_U_muU_ .^ 2, 1)) + 0.5 * vec(sum(iS_U_muU .^ 2, 1))
    mx = mean(y)
    mx2 = (y' * y)[1] / n

    done = false
    accept = false
    while done == false 
        
        sx = sqrt((n/(n-1)) * (mx2 - mx^2))
        s  = sx * sqrt(1 - n/N) / sqrt(n)
        #if typeof(s) == Array{Float64,2}
            #@bp
        #end
        tstat = abs((mx - thresh_MH) / s)

        if tstat > ttest_thresh
            accept = thresh_MH < mx
            done = true
        else
            nmb = min(sz_batch, N - n)
            sampinds = randinds[1:nmb] 
            randinds = randinds[nmb+1:end]
            newn = n + nmb
            
            iS_U_muU_ = Lam_chol_ * broadcast(-, U[:,sampinds], mu_)
            iS_U_muU  = Lam_chol_ * broadcast(-, U[:,sampinds], mu)
            y = - 0.5 * vec(sum(iS_U_muU_ .^ 2, 1)) + 0.5 * vec(sum(iS_U_muU .^ 2, 1))

            mx = (mx * n + sum(y)) / newn
            
            if newn == N
                accept = thresh_MH < mx
                done = true
            else
                sumy2 = (y' * y)[1]
                mx2 = (mx2 * n + sumy2) / newn
            end
            
            n = newn
        end
    end

    return accept, n 
end
