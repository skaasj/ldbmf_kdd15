include("./c_wrappers.jl")

function sample_PQ_sgld_m1!{T}(model::Dict{ASCIIString,Any},
                               trainset::Array{T,2},
                               para::Dict{ASCIIString,Any},
                               mean_rate::Float64,
                               inv_pr_pick_user::Array{Float64,1},
                               inv_pr_pick_item::Array{Float64,1},
                               eps_t::Float64
                               )
    sz_trainset = size(trainset)[1]
    K = para["K"]
    ka = model["prec_r"] * (sz_trainset / para["m"])
    #halfstepsz = 0.5 * para["eps"]
    #sqrtstepsz = sqrt(para["eps"])
    halfstepsz = 0.5 * eps_t
    sqrtstepsz = sqrt(eps_t)
    P = model["P"]
    Q = model["Q"]
    mu_p = model["mu_p"]
    mu_q = model["mu_q"]
    La_p = model["La_q"]
    La_q = model["La_p"]
    #prec_r = model["prec_r"]
    grad_sum_P = zeros(Float64, size(P))
    grad_sum_Q = zeros(Float64, size(Q))
    m = para["m"]               

    error = Array(Float64, m)
    
    for iter in 1:para["rndlen"]

        batch = trainset[rand_samp(sz_trainset, m, "w_replacement"), :]
        uu, ii, rr = batch[:,1], batch[:,2], batch[:,3]
        ux, ix = unique(uu), unique(ii)

        c_comp_error_m1!(error, rr, mean_rate, P, Q, uu, ii, m, K)
        c_comp_grad_sum_m1!(error, uu, ii, P, Q, grad_sum_P, grad_sum_Q, m, K)
        c_update_para_m1!(ux, P, grad_sum_P, mu_p, La_p, inv_pr_pick_user, ka, halfstepsz, sqrtstepsz, K, m)
        c_update_para_m1!(ix, Q, grad_sum_Q, mu_q, La_q, inv_pr_pick_item, ka, halfstepsz, sqrtstepsz, K, m)
        
        #error = (rr - mean_rate) - vec(sum(P[:,uu] .* Q[:,ii], 1))
        #comp_grad_sum!(error, uu, ii, P, Q, grad_sum_P, grad_sum_Q)

        ## sgld update
        #P[:,ux] += halfstepsz * (ka * grad_sum_P[:,ux] - broadcast(*, La_p * broadcast(-, P[:,ux], mu_p), inv_pr_pick_user[ux]')) + sqrtstepsz * randn(K, length(ux))
        #Q[:,ix] += halfstepsz * (ka * grad_sum_Q[:,ix] - broadcast(*, La_q * broadcast(-, Q[:,ix], mu_q), inv_pr_pick_item[ix]')) + sqrtstepsz * randn(K, length(ix)) 

        ## prepare for next update
        #grad_sum_P[:,ux] = fill!(grad_sum_P[:,ux], 0.0)
        #grad_sum_Q[:,ix] = fill!(grad_sum_Q[:,ix], 0.0)
    end
    return nothing
end


function comp_grad_sum!{T}(error::Array{Float64,1}, uu::Array{T,1}, ii::Array{T,1},
                           Ps::Array{Float64,2}, Qs::Array{Float64,2},
                           grad_sum_Ps::Array{Float64,2}, grad_sum_Qs::Array{Float64,2}
                           )
    uu_n, ii_n, err_n = 0, 0, 0
    for n = 1:length(error)
        @inbounds uu_n, ii_n, err_n = uu[n], ii[n], error[n]
        @inbounds grad_sum_Ps[:,uu_n] += err_n * Qs[:,ii_n]
        @inbounds grad_sum_Qs[:,ii_n] += err_n * Ps[:,uu_n]
    end
    return nothing
end


