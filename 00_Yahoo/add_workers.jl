n_proc_vel = 0
n_proc_inv = 8
n_proc_ben = 8

str_vel = "sungjia@velox.ics.uci.edu"
str_inv = "sungjia@invictus.ics.uci.edu"
str_ben = "sungjia@benedictus.ics.uci.edu"

addr_vel = [str_vel for i=1:n_proc_vel]
addr_inv = [str_inv for i=1:n_proc_inv]
addr_ben = [str_ben for i=1:n_proc_ben]

addr = vcat(addr_vel, addr_inv, addr_ben)
addprocs(addr; sshflags=`-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o LogLevel=ERROR`)
