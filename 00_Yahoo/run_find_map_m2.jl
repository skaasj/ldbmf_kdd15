include("../24_v0.07_dev/utils/types.jl")

# Initialize parmeter
srand(1)
println("Yahoo")

para = Dict{ASCIIString, Any}()
para["K"]       = 30
para["eps"]     = 1e-6
para["m"]       = 100000
para["rndlen"]  = 50
para["rP"]      = 2.0 
para["rQ"]      = 2.0  
para["rA"]      = 2.0 
para["rB"]      = 2.0 
para["bt0"]     = 300.0
########################
para["bthresh"] = 1.08
para["hthresh"] = 1.08
para["tg_rmse"] = 1.08
########################
para["mxiter"]  = 999999
para["mxsec"]   = 999999
para["iscale"]  = 0.0001
para["itvsv"]   = 200
para["itvtst"]  = 20
para["itvhyp"]  = 100      # sample hyper-params per 'itvhyp' rounds
para["submean"] = true

# model
model0 = Dict{ASCIIString,Any}()
model0["P"] = rand(para["K"], n_users) * para["iscale"]
model0["Q"] = rand(para["K"], n_items) * para["iscale"]
model0["A"] = randn(n_users) * 0.01
model0["B"] = randn(n_items) * 0.01
model0["al0"] = 1.0
model0["bt0"] = para["bt0"]   # important to supress inv_pr_pick_user
model0["prec_r"] = 2.0
model0["prec_P"] = ones(para["K"]) * para["rP"]
model0["prec_Q"] = ones(para["K"]) * para["rQ"]
model0["prec_A"] = para["rA"]
model0["prec_B"] = para["rB"]

# run SGD to find a MAP
include("../24_v0.07_dev/train_sgld_gibbs_m2.jl")
model = deepcopy(model0)
train_sgld_gibbs_m2(trainset, testset, model, para, trn_n_elem_row, trn_n_elem_col)

save("/scratch/DATA/YahooMusic/map_k30.jd", "model", model, "para", para)
