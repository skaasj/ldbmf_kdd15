include("../24_v0.07_dev/utils/types.jl")
include("../24_v0.07_dev/train_sgd_m2.jl")

# Initialize parmeter
srand(1)
para = Dict{ASCIIString, Any}()
para["K"] = 30
para["eps"] = 3e-4
para["iscale"] = 0.0001
para["rP"] = 0.01
para["rQ"] = 0.01
para["rA"] = 0.001
para["rB"] = 0.001
para["mom"] = 0.9
para["rndlen"] = 2000000
para["itvtst"] = 1
para["itvsv"] = 100
para["mxiter"] = Inf
para["mxsec"] = 9990000

# model
model0 = Dict{ASCIIString,Any}()
model0["P"] = rand(para["K"], n_users) * para["iscale"]
model0["Q"] = rand(para["K"], n_items) * para["iscale"]
model0["A"] = randn(n_users) * para["iscale"]
model0["B"] = randn(n_items) * para["iscale"]
model0["prec_P"] = ones(para["K"]) * para["rP"]
model0["prec_Q"] = ones(para["K"]) * para["rQ"] 
model0["prec_A"] = para["rA"] 
model0["prec_B"] = para["rB"] 

model = deepcopy(model0)
model = train_sgd_m2(trainset, testset, model, para)

