include("../24_v0.07_dev/utils/types.jl")
include("../24_v0.07_dev/train_sgd2_m2.jl")

# Initialize parmeter
srand(1)
println("Yahoo700M")

## params 
para = Dict{ASCIIString, Any}()
para["K"]       = 30
para["eps"]     = 1e-6
para["m"]       = 100000
para["rndlen"]  = 50
para["rP"]      = 1e-15
para["rQ"]      = 1e-15 
para["rA"]      = 1e-15
para["rB"]      = 1e-15
para["iscale"]  = 0.0001
para["mxiter"]  = 999999
para["mxsec"]   = 200000
para["itvsv"]   = 200
para["itvtst"]  = 1 
para["submean"] = false

# model
model0 = Dict{ASCIIString,Any}()
model0["P"] = rand(para["K"], n_users) * para["iscale"]
model0["Q"] = rand(para["K"], n_items) * para["iscale"]
model0["A"] = randn(n_users) * 0.01
model0["B"] = randn(n_items) * 0.01
model0["prec_r"] = 2.0
model0["prec_P"] = ones(para["K"]) * para["rP"]
model0["prec_Q"] = ones(para["K"]) * para["rQ"]
model0["prec_A"] = para["rA"]
model0["prec_B"] = para["rB"]

model = deepcopy(model0)
model = train_sgd2_m2(trainset, testset, model, para, trn_n_elem_row, trn_n_elem_col)

