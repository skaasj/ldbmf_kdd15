include("../utils/types.jl")
include("../train_sgld_gibbs_m1.jl")

# Initialize parmeter
srand(1)
println("Yahoo SGLD-Gibbs-M1")

## params 
para = Dict{ASCIIString, Any}()
para["K"]       = 100
para["m"]       = 100000
para["rndlen"]  = 50
para["init_La"] = 0.001
para["submean"] = true
para["mxiter"]  = 9999999
para["mxsec"]   = 9999999
para["iscale"]  = 0.01
para["itvsv"]   = 100
para["itvtst"]  = 20
para["itvhyp"]  = 50       # sample hyper-params per 'itvhyp' rounds
para["map"]     = true
para["tg_rmse"] = 1.08
para["bthresh"] = 1.08

para["eps"]     = 1.5e-6
para["tau0"]    = 500
para["kappa"]   = 0.51
f_stepsz(t)     = para["eps"] .* ((1 + t./para["tau0"]).^(-para["kappa"]))

# model
model0 = Dict{ASCIIString,Any}()
model0["P"]         = randn(para["K"], n_users) * para["iscale"]
model0["Q"]         = randn(para["K"], n_items) * para["iscale"]
model0["mu_p"]      = zeros(para["K"])
model0["mu_q"]      = zeros(para["K"])   
model0["mu0_p"]     = zeros(para["K"])
model0["mu0_q"]     = zeros(para["K"])
model0["La_p"]      = para["init_La"] * eye(para["K"])
model0["La_q"]      = para["init_La"] * eye(para["K"])
model0["La_p_chol"] = chol(model0["La_p"])
model0["La_q_chol"] = chol(model0["La_q"])
model0["bt0_p"]     = 2.0
model0["bt0_q"]     = 2.0
model0["prec_r"]    = 2.0

model = deepcopy(model0);
train_sgld_gibbs_m1(trainset, testset, model, para, trn_n_elem_row, trn_n_elem_col, f_stepsz);

