using HDF5, JLD
n_users  = 1823179
n_items  = 136736
trainset = load("/scratch/DATA/YahooMusic/141218_trainset.jld")["trainset"]
testset  = load("/scratch/DATA/YahooMusic/141218_testset.jld")["testset"]
trn_n_elem_row = load("/scratch/DATA/YahooMusic/141218_trn_n_elem_row.jld")["trn_n_elem_row"]
trn_n_elem_col = load("/scratch/DATA/YahooMusic/141218_trn_n_elem_col.jld")["trn_n_elem_col"]

#v = load("/scratch/DATA/YahooMusic/141225_y700m.jld")
#trainset        = v["trainset"];   
#testset        = v["testset"];
#n_users         = v["n_users"];
#n_items         = v["n_items"];
#trn_n_elem_row  = v["trn_n_elem_row"];
#trn_n_elem_col  = v["trn_n_elem_col"];
@printf("YahooMusic 700M loaded")
