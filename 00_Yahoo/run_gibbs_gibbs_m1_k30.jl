include("../utils/types.jl")
include("../train_gibbs_gibbs_m1.jl")

# Initialize parmeter
srand(1)
println("BPMF-m1 Yahoo")

## params 
para = Dict{ASCIIString, Any}()
para["K"] = 100
#para["eps"] = 0.002
#para["iscale"] = 0.1
#para["reg"] = 0.01
#para["init_La"] = 0.01
#para["mom"] = 0.9
#para["rndlen"] = 500000
para["itvtst"] = 1
para["itvsv"] = 1
para["mxiter"] = Inf
para["mxsec"] = 9999999



# run SGD to find a MAP
#model = deepcopy(model0)
#include("../train_sgd_m1.jl")
##para["mxiter"] = 20 #opt = 20
#para["mxiter"] = 100
#train_sgd_m1(trainset, testset, model, para)


include("../utils/data_loader.jl")
user_index, item_index = build_user_item_index_map(trainset, n_users, n_items)

## run BPMF
para["mxiter"] = 999999
para["bthresh"] = 0.95
para["bt0"] = 1.0
para["itv_gibbs"] = 1

#model = load("./map_K30_m1_6e2.jd")["model"];
#model = load("./map_ya_m1_k60.jd")["model"];
model = load("./map_ya_m1_k100.jd")["model"];

train_gibbs_gibbs_m1(trainset, testset, model, para, user_index, item_index)

