using Debug
using Datetime
using Devectorize
using Distributions
using NumericExtensions
include("./utils/types.jl")
include("./utils/utils.jl")
include("./utils/mf_common.jl")
include("./sample_PQ_sgld_m2.jl")
include("./sample_hyper_austMH_m2.jl")

function train_sgld_austMH_m2{T}(trainset::Array{T,2}, 
                                testset::Array{T,2}, 
                                model::Dict{ASCIIString,Any},
                                para::Dict{ASCIIString, Any},
                                aust_para::Dict{ASCIIString, Any},
                                n_elem_row::Array{Int64,1}, 
                                n_elem_col::Array{Int64,1} 
                                )
    para["algo"] = "SGLD_AUST_MH_M2"

    eps::Float64    = para["eps"] 
    m::Int64        = para["m"] 
    rndlen::Int64   = para["rndlen"]
    mxiter::Int64   = para["mxiter"]
    mxsec::Int64    = para["mxsec"] 
    itvsv::Int64    = para["itvsv"]
    itvtst::Int64   = para["itvtst"]
    itvhyp::Int64   = para["itvhyp"]
    n_users = size(model["P"],2)
    n_items = size(model["Q"],2)

    println(para)
    println(aust_para)
    @printf("Running %s\n", para["algo"])
    @assert itvsv >= itvtst
    log_dir = "00_results_"para["algo"]
    #log_dir = "/scratch/sungjin/00_projects/00_dbmf/004_neflix_train8020/00_results_"para["algo"]
    cur_logfile = get_str_id2(para)
    logs = Log2(zeros(Int64,0), zeros(0), zeros(0), zeros(0,0), zeros(0,0), para)

    # initialization
    min_rate    = float(minimum(trainset[:,3]))
    max_rate    = float(maximum(trainset[:,3]))
    mean_rate   = mean(trainset[:,3])
    sz_trainset = size(trainset)[1]
    sz_testset  = size(testset)[1]
    K           = size(model["P"],1)

    if rndlen > sz_trainset
        rndlen = sz_trainset
        para["rndlen"] = sz_trainset
        @printf("rndlen is truncated to sz_trainset\n")
    end

    #P_star, Q_star = model["P"], model["Q"]

    iter = 1     # iter
    avg_cnt = 1  # counts number of averaged predicions
    ts = 0.0     # timestamp
    best_rmse = 100.0
    avg_pred = zeros(Float64,sz_testset)

    inv_pr_pick_user = 1 ./ (1 - (1 - n_elem_row ./ size(trainset,1)) .^ para["m"])
    inv_pr_pick_item = 1 ./ (1 - (1 - n_elem_col ./ size(trainset,1)) .^ para["m"])
    
    ac_rates = Dict{ASCIIString,Any}()
    ac_rates["la_P"] = zeros(K)
    ac_rates["la_Q"] = zeros(K)
    ac_rates["la_A"] = 0.0
    ac_rates["la_B"] = 0.0
    cnt_hyp = 0
    
    is_burnin = true

    while iter <= mxiter && ts < mxsec
    
        tic()
        ######################################################################## 
        sample_PQ_sgld_m2!(model, trainset, para, mean_rate, inv_pr_pick_user, inv_pr_pick_item) 
        if rem(iter, itvhyp) == 0
            cnt_hyp += 1
            sample_hyper_austMH_m2!(model, ac_rates, aust_para["tthresh"], aust_para["sz_batch"], cnt_hyp)
            @printf("pr_P:%f, pr_Q:%f, pr_A:%1.1f, pr_B:%1.1f, pr_r:%1.1f\n", mean(model["prec_P"]), mean(model["prec_Q"]), model["prec_A"], model["prec_B"], model["prec_r"])
            println(ac_rates)
        end
        ########################################################################
        ts += toq();

        # Compute RMSE
        if mod(iter, itvtst) == 0
            avg_rmse_t, cur_rmse_t, avg_pred = rmse_avg(testset, iter, model["P"], model["Q"], model["A"], model["B"], avg_pred, avg_cnt, min_rate, max_rate, mean_rate, is_burnin)
            if !is_burnin
                avg_cnt += 1
            end
            @printf("%d, %.4f (%.4f) (%.2fs) ", iter, avg_rmse_t, cur_rmse_t, ts)
            if !is_burnin && avg_rmse_t < best_rmse 
                best_rmse = avg_rmse_t
                #P_star, Q_star = model["P"], model["Q"]
                @printf("(**)\n")
            else 
                @printf("\n") 
            end
            push!(logs.iter, iter)
            push!(logs.ts, ts)
            push!(logs.rmse, avg_rmse_t)
            para["iter"] = iter; para["ts"] = ts; para["best"] = best_rmse
            logs.para = para
            #logs.P_star, logs.Q_star = typeof(P_star)[], typeof(Q_star)[]
            #logs.P_star, logs.Q_star = P_star, Q_star
            if is_burnin == true
                is_burnin = (cur_rmse_t > para["bthresh"])
                if is_burnin == false
                    @printf("burnin finished\n")
                end
            end
        end
        # Save logs file
        if mod(iter, itvsv) == 0
            @printf("saving loggings ... ")
            para["iter"] = iter; 
            para["ts"]   = ts
            cur_logfile  = save_logs3(para, logs, log_dir, cur_logfile)
            @printf("done\n")
            if ts > mxsec
                break
            end
        end

        iter += 1

    end
end



#function sample_PQ_sgld_m2!{T}(trainset::Array{T,2},
                             #model::Dict{ASCIIString,Any},
                             #para::Dict{ASCIIString,Any},
                             #mean_rate::Float64,
                             #inv_pr_pick_user::Array{Float64,1},
                             #inv_pr_pick_item::Array{Float64,1}
                             #)
    #sz_trainset = size(trainset)[1]
    #K = para["K"]
    #ka = model["prec_r"] * (sz_trainset / para["m"])
    #halfstepsz = 0.5 * para["eps"]
    #sqrtstepsz = sqrt(para["eps"])
    #P = model["P"]
    #Q = model["Q"]
    #A = model["A"]
    #B = model["B"]
    #prec_r = model["prec_r"]
    #prec_P = model["prec_P"]
    #prec_Q = model["prec_Q"]
    #prec_A = model["prec_A"]
    #prec_B = model["prec_B"]
    #grad_sum_P = zeros(Float64, size(P))
    #grad_sum_Q = zeros(Float64, size(Q))
    #grad_sum_A = zeros(Float64, size(A))
    #grad_sum_B = zeros(Float64, size(B))
    #m = para["m"]


    #for iter in 1:para["rndlen"]

        #batch = trainset[rand_samp(sz_trainset, m, "w_replacement"), :]
        
        #uu, ii, rr = batch[:,1], batch[:,2], batch[:,3]
        #ux, ix = unique(uu), unique(ii)
        
        #error = (rr - mean_rate) - (vec(sum(P[:,uu] .* Q[:,ii], 1)) + A[uu] + B[ii])
        #comp_grad_sum!(error, uu, ii, P, Q, A, B, grad_sum_P, grad_sum_Q, grad_sum_A, grad_sum_B)

        ##P[:,ux] += halfstepsz * (ka * grad_sum_P[:,ux] - (prec_P .* P[:,ux]))
                ##+  sqrtstepsz * randn(K,length(ux))
        ##Q[:,ix] += halfstepsz * (ka * grad_sum_Q[:,ix] - (prec_Q .* Q[:,ix])) 
                ##+  sqrtstepsz * randn(K,length(ix))

        ## compute gradinet of the prior
        #P[:,ux] += halfstepsz * (ka * grad_sum_P[:,ux] - (prec_P * inv_pr_pick_user[ux]') .* P[:,ux])
                #+  sqrtstepsz * randn(K,length(ux)) 
        #Q[:,ix] += halfstepsz * (ka * grad_sum_Q[:,ix] - (prec_Q * inv_pr_pick_item[ix]') .* Q[:,ix]) 
                #+  sqrtstepsz * randn(K,length(ix)) 
        #A[ux]   += halfstepsz * (ka * grad_sum_A[ux] - prec_A * (inv_pr_pick_user[ux] .* A[ux])) 
                #+  sqrtstepsz * randn(length(ux))
        #B[ix]   += halfstepsz * (ka * grad_sum_B[ix] - prec_B * (inv_pr_pick_item[ix] .* B[ix]))
                #+  sqrtstepsz * randn(length(ix))

        ## prepare for next update
        #grad_sum_P[:,ux] = fill!(grad_sum_P[:,ux], 0.0)
        #grad_sum_Q[:,ix] = fill!(grad_sum_Q[:,ix], 0.0)
        #grad_sum_A[ux]   = fill!(grad_sum_A[ux],   0.0)
        #grad_sum_B[ix]   = fill!(grad_sum_B[ix],   0.0)
    #end
    #return nothing
#end


#function comp_grad_sum!{T}(error::Array{Float64,1},
                           #uu::Array{T,1},ii::Array{T,1},
                           #Ps::Array{Float64,2}, Qs::Array{Float64,2},
                           #As::Array{Float64,1}, Bs::Array{Float64,1},
                           #grad_sum_Ps::Array{Float64,2}, grad_sum_Qs::Array{Float64,2},
                           #grad_sum_As::Array{Float64,1}, grad_sum_Bs::Array{Float64,1}
                           #)
    #uu_n, ii_n, err_n = 0, 0, 0
    #for n = 1:length(error)
        #@inbounds uu_n, ii_n, err_n = uu[n], ii[n], error[n]
        #@inbounds grad_sum_Ps[:,uu_n] += err_n * Qs[:,ii_n]
        #@inbounds grad_sum_Qs[:,ii_n] += err_n * Ps[:,uu_n]
        #@inbounds grad_sum_As[uu_n]   += err_n
        #@inbounds grad_sum_Bs[ii_n]   += err_n
    #end
    #return nothing
#end


#function sample_hyper!(model::Dict{ASCIIString,Any})
 
    #P = model["P"]
    #Q = model["Q"]
    #A = model["A"]
    #B = model["B"]

    #K, n_users = size(P)
    #n_items = size(Q,2)
 
    #sse_P = sum(sqr(broadcast(-, P, mean(P,2))), 2)
    #sse_Q = sum(sqr(broadcast(-, Q, mean(Q,2))), 2)
    #beta_P = model["bt0"] + 0.5 * sse_P
    #beta_Q = model["bt0"] + 0.5 * sse_Q
    #for k in 1:K 
        #@inbounds model["prec_P"][k] = 1/rand(InverseGamma(model["al0"] + 0.5 * n_users, beta_P[k]))
        #@inbounds model["prec_Q"][k] = 1/rand(InverseGamma(model["al0"] + 0.5 * n_items, beta_Q[k]))
    #end
    
    ### sample bias precisions
    #sse_A = sum(sqr(A - mean(A)))
    #sse_B = sum(sqr(B - mean(B)))
    #beta_A = model["bt0"] + 0.5 * sse_A
    #beta_B = model["bt0"] + 0.5 * sse_B
    #model["prec_A"]  = 1/rand(InverseGamma(model["al0"] + 0.5 * n_users, beta_A))
    #model["prec_B"]  = 1/rand(InverseGamma(model["al0"] + 0.5 * n_items, beta_B))
    
    #return nothing
#end



function compute_sse{T}(dataset::Array{T,2}, model::Dict{ASCIIString,Any})
    uu, ii = dataset[:,1], dataset[:,2]
    pred = vec(sum(model["P"][:,uu] .* model["Q"][:,ii], 1)) + model["A"][uu] + model["B"][ii]
    sse = sum(sqr(dataset[:,3] - pred))
    return sse
end


