#include <julia.h>

int main(int argc, char *argv[])
{
    /* required: setup the julia context */
    jl_init(NULL);
    JL_SET_STACK_BASE;
    
    /* run julia commands */
    jl_eval_string("print(sqrt(2.0))");

    /*
     *jl_atexit_hook();
     */
    return 0;
}
