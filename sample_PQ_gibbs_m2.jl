include("./c_wrappers.jl")

function sample_PQ_gibbs_m2!{T}(model::Dict{ASCIIString,Any}, trainset::Array{T,2}, 
                                u_idx::Array{Array{Int64,1},1}, i_idx::Array{Array{Int64,1},1}, 
                                mean_rate::Float64, itv_gibbs::Int64)
    P::Array{Float64,2}    = model["P"]
    Q::Array{Float64,2}    = model["Q"]
    A::Array{Float64,1}    = model["A"]
    B::Array{Float64,1}    = model["B"]
    al::Float64            = model["prec_r"]
    la_P::Array{Float64,2} = diagm(model["prec_P"])
    la_Q::Array{Float64,2} = diagm(model["prec_Q"])
    la_A::Float64          = model["prec_A"]
    la_B::Float64          = model["prec_B"]

    n_users = length(A)
    n_items = length(B)
    uu = trainset[:,1]
    ii = trainset[:,2]
    sz_trainset = size(trainset,1)
    K    = size(P,1)
    R    = trainset[:,3] - mean_rate
    C    = Array(Float64, sz_trainset)
    XX   = Array(Float64, sz_trainset)
    R_AB = Array(Float64, sz_trainset)

    for gibbs = 1:itv_gibbs
        
        #@time R_AB = R - A[uu] - B[ii]
        c_comp_R_AB_bpmf!(R_AB, R, A, B, uu, ii);  #**** this has a bug ****
        update_P!(trainset, P, Q, u_idx, R_AB, al, la_P, K)
        update_Q!(trainset, P, Q, i_idx, R_AB, al, la_Q, K)
        
        #@time C  = R - vec(sum(P[:,trainset[:,1]] .* Q[:,trainset[:,2]], 1))  # this requires very large memory
        c_comp_C_bpmf!(C, R, P, Q, uu, ii)
        
        c_comp_XX_bpmf!(XX, C, B, ii)
        #@time XX = C - B[ii]
        update_A!(A, XX, u_idx, al, la_A)
        
        c_comp_XX_bpmf!(XX, C, A, uu)
        #@time XX = C - A[uu]
        update_B!(B, XX, i_idx, al, la_B)     
        
        #R_AB = R - A[trainset[:,1]] - B[trainset[:,2]]
        ##@time c_comp_R_AB_bpmf!(R_AB, R, A, B, uu, ii);
        #for u = 1:n_users
            #@inbounds Qu = Q[:,trainset[u_idx[u],:][:,2]]
            #@inbounds covar = inv(la_P + al * Qu * Qu')
            #@inbounds mu = covar * (al * Qu * R_AB[u_idx[u]])
            #@inbounds P[:,u] = chol(covar)' * randn(K) + mu
        #end 
        ## sample Qi
        #for i = 1:n_items
            #@inbounds Pi = P[:,trainset[i_idx[i],:][:,1]]
            #@inbounds covar = inv(la_Q + al * Pi * Pi')
            #@inbounds mu = covar * (al * Pi * R_AB[i_idx[i]])
            #@inbounds Q[:,i] = chol(covar)' * randn(K) + mu
        #end
        ##sample Au
        ##C  = R - vec(sum(P[:,trainset[:,1]] .* Q[:,trainset[:,2]], 1))
        #@time c_comp_C_bpmf!(C, R, P, Q, uu, ii)
        #XX = C - B[trainset[:,2]]
        #for u = 1:n_users
            #@inbounds va = 1 / (la_A + al * length(u_idx[u]))
            #@inbounds mu = va * al * sum(XX[u_idx[u]])
            #@inbounds A[u] = mu + sqrt(va) * randn()
        #end
        ##sample Bi
        #XX = C - A[trainset[:,1]]
        #for i = 1:n_items
            #@inbounds va = 1 / (la_B + al * length(i_idx[i]))
            #@inbounds mu = va * al * sum(XX[i_idx[i]])
            #@inbounds B[i] = mu + sqrt(va) * randn()
        #end
    end
    return nothing
end


function update_P!{T}(trainset::Array{T,2}, P::Array{Float64,2}, Q::Array{Float64,2}, 
                       u_idx::Array{Array{Int64,1},1}, R_AB::Array{Float64,1}, 
                       al::Float64, la_P::Array{Float64,2}, K::Int64)
    n_users = size(P,2)
    for u = 1:n_users
        #@inbounds u_ratings = trainset[u_idx[u],:]
        @inbounds Qu = Q[:,trainset[u_idx[u],:][:,2]]
        @inbounds covar = inv(la_P + al * Qu * Qu')
        @inbounds mu = covar * (al * Qu * R_AB[u_idx[u]])
        @inbounds P[:,u] = chol(covar)' * randn(K) + mu
    end 
    return nothing
end

function update_Q!{T}(trainset::Array{T,2}, P::Array{Float64,2}, Q::Array{Float64,2}, 
                       i_idx::Array{Array{Int64,1},1}, R_AB::Array{Float64,1}, 
                       al::Float64, la_Q::Array{Float64,2}, K::Int64)
    n_items = size(Q,2)
    for i = 1:n_items
        #@inbounds i_ratings = trainset[i_idx[i],:]
        @inbounds Pi = P[:,trainset[i_idx[i],:][:,1]]
        @inbounds covar = inv(la_Q + al * Pi * Pi')
        @inbounds mu = covar * (al * Pi * R_AB[i_idx[i]])
        @inbounds Q[:,i] = chol(covar)' * randn(K) + mu
    end
    return nothing
end


function update_A!(A::Array{Float64,1}, XX::Array{Float64,1}, u_idx::Array{Array{Int64,1},1},
                    al::Float64, la_A::Float64)
    for u = 1:length(A)
        @inbounds va = 1 / (la_A + al * length(u_idx[u]))
        @inbounds mu = va * al * sum(XX[u_idx[u]])
        @inbounds A[u] = mu + sqrt(va) * randn()
    end
    return nothing
end


function update_B!(B::Array{Float64,1}, XX::Array{Float64,1}, i_idx::Array{Array{Int64,1},1},
                   al::Float64, la_B::Float64)
    for i = 1:length(B)
        @inbounds va = 1 / (la_B + al * length(i_idx[i]))
        @inbounds mu = va * al * sum(XX[i_idx[i]])
        @inbounds B[i] = mu + sqrt(va) * randn()
    end
    return nothing
end


#function update_P2!{T}(trainset::Array{T,2}, P::Array{Float64,2}, Q::Array{Float64,2}, 
                       #u_idx::Array{Array{Int64,1},1}, R_AB::Array{Float64,1}, 
                       #al::Float64, la_P::Array{Float64,2}, K::Int64)
    #n_users = size(P,2)
    #Prec = Array(Float64,K,K)
    #mu = Array(Float64,K)

    #for u = 1:n_users
        ##@inbounds u_ratings = trainset[u_idx[u],:]
        #@inbounds Qu = Q[:,trainset[u_idx[u],:][:,2]]
        #@inbounds comp_Prec!(Prec, la_P, al, Qu, K, n_users)
        #@inbounds covar = inv(Prec)
        ##@inbounds covar = inv(la_P + al * Qu * Qu')
        #@inbounds mu = covar * (al * Qu * R_AB[u_idx[u]])
        #@inbounds P[:,u] = chol(covar)' * randn(K) + mu
    #end 
    #return nothing
#end


#function comp_Prec!(Prec, la_P, al, Qu, K, D)
    #for k1 = 1:K
        #for k2 = 1:K
            #a = 0.0
            #for i = 1:D
                #@inbounds a += Qu[k1,i] * Qu[k2,i]
            #end
            #@inbounds Prec[k1,k2] = la_P[k1,k2] + al * a
        #end
    #end
#end


