include("../../../24_v0.07_dev_64/utils/types.jl")
using HDF5, JLD

issemilogx = true

# bpmf
file = "20150113T153735_ts810_6_tg_rmse0_86_mxiter999999_maptrue_kappa0_51_la0_p2_0_submeantrue_best0_8600_iscale0_01_tau050_itvsv100_algoSGD-m1-MAP_mxsec999999_itvtst10_itvhyp30_iter270_eps9_0e-6_m50000_la0_q2_0_K60_rndlen25.jd"
logs_map_k60 = load(file)["logs"]

file = "20150116T165059_ts107195_3_mxiter999999_itv_gibbs1_best0_8145_bthresh0_95_itvsv1_algoG-G-1_mxsec9990000_itvtst1_iter7_K60.jd"
logs_gibbs_k60 = load(file)["logs"]

# SGLD
file = "20150115T084059_ts79903_3_mxiter9999999_mapfalse_submeantrue_rP2_0_best0_8087_bthresh0_86_iscale0_01_itvsv100_rA2_0_algoS-G-2_mxsec9999999_itvtst20_rQ2_0_itvhyp100_iter10300_eps8_0e-6_bt0500_0_m100000_rB2_0_K60_rndlen20.jd"
logs_sgld_k60 = load(file)["logs"]

# D-SGLD
file = "20150114T193211_C9_ts13803_9_mxiter999999_t2200_rP2_0_best0_8075_bthresh0_86_iscale0_01_itvsv100_rA2_0_algoDS-G-2_itvtst20_rQ2_0_itvhyp100_eps8_0e-6_S9_bt0500_0_m50000_aug0_0_rB2_0_K60_rndlen30.jd"
logs_dsgldc_k60 = load(file)["logs"]

# SGD
file = "20150131T213336_ka0_51_eps06_0e-6_tau10_ts2496_9_tg_rmse0_0_mxiter9999999_maptrue_submeantrue_rP2_0_best0_8447_bthresh0_0_iscale0_01_itvsv100_rA2_0_algoSGD-m2-MAP_mxsec9999999_itvtst20_rQ2_0_itvhyp50_iter400_bt0300_0_m50000_rB2_0_K60_rndlen50.jd"
logs_sgd_k60 = load(file)["logs"]

# DSGD
file = "20150131T204610_ka0_51_C1_eps01_0e-6_tau10_ts1452_2_mxiter999999_t200_submeantrue_rP2_0_best0_8428_iscale0_01_itvsv100_rA2_0_ncb9_algoDSGD-2_iter200_itvtst10_rQ2_0_S9_m50000_rB2_0_K60_nrb9_rndlen50.jd"
logs_dsgd_k60 = load(file)["logs"]


###############################
# PyPlot must be included after loading HDF5 and JLD due to a bug in v0.3x
###############################
using PyPlot
clf()

hold(true)

############################################
# D-SGLD 9x1
avg_idx = find(logs_dsgldc_k60["ts"] .> logs_dsgldc_k60["burnin_ts"])
rmse = vcat(logs_dsgldc_k60["cur_rmse"][1:avg_idx[1]-1], logs_dsgldc_k60["avg_rmse"][avg_idx])
if issemilogx
    semilogx(logs_dsgldc_k60["ts"], rmse, "b-", linewidth=3, label="DSGLD-C (9x1)") 
else
    plot(logs_dsgldc_k60["ts"], rmse, "b-", linewidth=3, label="DSGLD-C (9x1)")  
end

############################################
# DSGD
#idx = [0.0:100000]
if issemilogx
    semilogx(logs_dsgd_k60["ts"][1:end], logs_dsgd_k60["rmse"][1:end], "y-", linewidth=3, label="DSGD (9x9)")
else
    plot(logs_dsgd_k60["ts"][1:end], logs_dsgd_k60["rmse"][1:end], "y-", linewidth=3, label="DSGD (9x9)")
end

############################################
# SGLD 
avg_idx = find(logs_sgld_k60["ts"] .> logs_sgld_k60["burnin_ts"])
rmse = vcat(logs_sgld_k60["cur_rmse"][1:avg_idx[1]-1], logs_sgld_k60["avg_rmse"][avg_idx])
if issemilogx
    semilogx(logs_sgld_k60["ts"], rmse, "g-", linewidth=3, label="SGLD") 
else
    plot(logs_sgld_k60["ts"], rmse, "g-", linewidth=3, label="SGLD") 
end
############################################
# BPMF
bpmf_ts = logs_map_k60["ts"][end] + logs_gibbs_k60["ts"]
bpmf_rmse = logs_gibbs_k60["avg_rmse"]
if issemilogx
    semilogx(bpmf_ts, bpmf_rmse, "k-", marker="", markersize=10, markeredgewidth=2, linewidth=3, label="Gibbs") 
else
    plot(bpmf_ts, bpmf_rmse, "k-", marker="", markersize=10, markeredgewidth=2, linewidth=3, label="Gibbs") 
end

############################################
# SGD
idx = [0.0:100000]
if issemilogx
    #semilogx(logs_sgd_k60["ts"][1:1:end], logs_sgd_k60["cur_rmse"][1:1:end], "r-", linewidth=3, label="SGD")
    semilogx(idx, ones(length(idx)) * 0.8447, "k--", linewidth=3, label="SGD")
else
    #plot(logs_sgd_k60["ts"][1:1:end], logs_sgd_k60["cur_rmse"][1:1:end], "r-", linewidth=3, label="SGD")
    plot(idx, ones(length(idx)) * 0.8447, "k--", linewidth=3, label="SGD")
end


##############################################################

fontsize = 21
plt.rc("font", family="serif", size=fontsize)
xlabel("Sec.", size=fontsize)
ylabel("RMSE", size=fontsize)
if issemilogx 
    ylim([0.800, 0.93])
    #yticks([0.81:0.05: 0.93])
    xlim([3*10^1, 100000])
    legend(fancybox=true, framealpha=0.7, fontsize=17)
else
    ylim([0.81, 0.866])
    xlim([10^1, 10500])
    xticks([0:2000:10000])
    yticks([0.81:0.01:0.865])
    legend(fancybox=true, framealpha=0.7, fontsize=17)
end
grid(true)

if issemilogx
    savefig("net8020_k60_decr_logx.png", format="png", bbox_inches="tight")
    savefig("net8020_k60_decr_logx.eps", format="eps", bbox_inches="tight")
    savefig("net8020_k60_decr_logx.pdf", format="pdf", bbox_inches="tight")
else
    savefig("net8020_k60_decr_linx.png", format="png", bbox_inches="tight")
    savefig("net8020_k60_decr_linx.eps", format="eps", bbox_inches="tight")
    savefig("net8020_k60_decr_linx.pdf", format="pdf", bbox_inches="tight")
end



