include("../../27_v0.08_dev/utils/types.jl")
using HDF5, JLD

issemilogx = false
#######################
# bpmf
file = "./k30/20150109T114754_ts584_8_mxiter999999_tg_rmse0_85_maptrue_la0_p0_001_submeantrue_best100_0000_bthresh0_85_iscale0_01_itvsv200_algoSGD-m1-MAP_mxsec200000_itvtst10_itvhyp30_iter380_eps8_0e-6_m50000_la0_q0_001_K30_rndlen20.jd"
logs_map_m1_k30 = load(file)["logs"]

file = "./k30/20150109T190809_ts25844_3_mxiter999999_itv_gibbs1_best0_8125_bthresh0_95_itvsv1_algoG-G-1_mxsec9990000_itvtst1_iter40_K30.jd"
logs_bpmf_m1_k30 = load(file)["logs"]

#######################
# SGLD
file = "./k30/20150111T222359_ts12715_2_mxiter9999999_mapfalse_submeantrue_rP2_0_best0_8151_bthresh0_85_iscale0_01_itvsv200_rA2_0_algoS-G-2_mxsec9999999_itvtst20_rQ2_0_itvhyp50_iter3800_eps9_0e-6_bt0300_0_m50000_rB2_0_K30_rndlen30.jd"
logs_sgld_m2_k30 = load(file)["logs"]

file = "./k60/20150114T153722_ts16380_1_mxiter9999999_mapfalse_submeantrue_rP2_0_best0_8112_bthresh0_86_iscale0_01_itvsv100_rA2_0_algoS-G-2_mxsec9999999_itvtst20_rQ2_0_itvhyp100_iter2800_eps8_0e-6_bt0500_0_m50000_rB2_0_K60_rndlen30.jd"
logs_sgld_m2_k60 = load(file)["logs"]

file = "./k100/20150114T155938_ts3420_8_mxiter9999999_mapfalse_submeantrue_rP2_0_best0_8309_bthresh0_86_iscale0_01_itvsv100_rA2_0_algoS-G-2_mxsec9999999_itvtst20_rQ2_0_itvhyp100_iter400_eps8_0e-6_bt0700_0_m50000_rB2_0_K100_rndlen30.jd"
logs_sgld_m2_k100 = load(file)["logs"]

#######################
# D-SGLD
#file = "20150109T230543_C5_ts2555_9_mxiter999999_t1000_rP2_0_best0_8206_bthresh0_97_iscale0_01_itvsv200_rA2_0_algoDS-G-2_G1_itvtst20_rQ2_0_itvhyp50_eps9_0e-6_S10_bt0300_0_m50000_aug0_0_rB2_0_K30_rndlen25.jd"
#logs_dsgld_m2_c5s10 = load(file)["logs"]
#file = "20150111T234111_C10_ts2466_6_mxiter999999_t800_rP2_0_best0_8144_bthresh0_85_iscale0_01_itvsv100_rA2_0_algoDS-G-2_itvtst20_rQ2_0_itvhyp50_eps9_0e-6_S10_bt0300_0_m50000_aug0_0_rB2_0_K30_rndlen25.jd"

file = "./k30/20150112T180037_C9_ts4393_7_mxiter999999_t1500_rP2_0_best0_8126_bthresh0_85_iscale0_01_itvsv100_rA2_0_algoDS-G-2_itvtst20_rQ2_0_itvhyp200_eps9_0e-6_S9_bt01_0_m50000_aug0_0_rB2_0_K30_rndlen25.jd"
logs_dsgld_m2_c9s9_k30 = load(file)["logs"]

file = "./k60/20150113T201110_C9_ts6097_4_mxiter999999_t800_rP2_0_best0_8095_bthresh0_86_iscale0_01_itvsv100_rA2_0_algoDS-G-2_itvtst20_rQ2_0_itvhyp100_eps7_0e-6_S9_bt0300_0_m50000_aug0_0_rB2_0_K60_rndlen40.jd"
logs_dsgld_m2_c9s9_k60 = load(file)["logs"]

file = "./k100/20150114T142414_C9_ts18233_6_mxiter999999_t1500_rP2_0_best0_8067_bthresh0_86_iscale0_01_itvsv100_rA2_0_algoDS-G-2_itvtst20_rQ2_0_itvhyp100_eps6_0e-6_S9_bt0500_0_m50000_aug0_0_rB2_0_K100_rndlen40.jd"
logs_dsgld_m2_c9s9_k100 = load(file)["logs"]


#######################
# DD-SGLD
file = "./k30/20150112T153707_C3_ts10371_1_mxiter999999_t1200_submeantrue_rP2_0_best0_8137_bthresh0_85_iscale0_1_itvsv200_rA2_0_ncb3_algoDDS-G-2_G1_itvtst10_rQ2_0_itvhyp100_eps3_0e-6_S9_bt0300_0_m50000_aug0_0_rB2_0_K30_nrb3_rndlen50.jd"
logs_ddsgld_m2_3x3_k30 = load(file)["logs"]

#######################
# SGD
#file = "20150111T222902_ts4442_8_tg_rmse0_1_mxiter999999_maptrue_kappa0_51_submeantrue_rP2_0_best0_8421_iscale0_01_tau050_itvsv100_rA2_0_algoSGD-m2-MAP_mxsec200000_itvtst10_rQ2_0_itvhyp10_iter2000_eps9_0e-6_bt0300_0_m50000_rB2_0_K30_rndlen25.jd"
file = "./k30/20150111T203807_ts3728_1_tg_rmse0_1_mxiter999999_maptrue_submeantrue_rP2_0_best100_0000_iscale0_01_itvsv100_rA2_0_algoSGD-MAP_mxsec200000_itvtst20_rQ2_0_itvhyp10_iter900_eps3_0e-6_bt0300_0_m50000_rB2_0_K30_rndlen50.jd"
logs_sgd_m2_k30 = load(file)["logs"]

file = "./k60/20150113T143802_ts1139_5_tg_rmse0_1_mxiter999999_maptrue_kappa0_51_submeantrue_rP10_0_best0_8520_iscale0_01_tau050_itvsv100_rA2_0_algoSGD-m2-MAP_mxsec200000_itvtst10_rQ2_0_itvhyp10_iter300_eps9_0e-6_bt0300_0_m50000_rB2_0_K60_rndlen25.jd"
logs_sgd_m2_k60 = load(file)["logs"]

file = "./k100/20150113T150405_ts1013_5_tg_rmse0_1_mxiter999999_maptrue_kappa0_51_submeantrue_rP2_0_best0_8491_iscale0_01_tau050_itvsv100_rA2_0_algoSGD-m2-MAP_mxsec200000_itvtst10_rQ2_0_itvhyp10_iter200_eps9_0e-6_bt0300_0_m50000_rB2_0_K100_rndlen25.jd"
logs_sgd_m2_k100 = load(file)["logs"]

#######################
# DSGD
file = "./k30/20150112T161819_C1_ts2083_8_mxiter999999_kappa0_51_t400_submeantrue_rP10_0_best0_8485_iscale0_01_tau0150_itvsv100_rA2_0_ncb9_algoDSGD-2_iter400_itvtst10_rQ2_0_eps1_0e-6_S9_m50000_rB2_0_rndlen50_K30_nrb9.jd"
logs_dsgd_m2_k30 = load(file)["logs"]

file = "./k60/20150113T161653_C1_ts621_9_mxiter999999_kappa0_51_t100_submeantrue_rP2_0_best0_8458_iscale0_01_tau050_itvsv100_rA2_0_ncb9_algoDSGD-2_iter100_itvtst10_rQ2_0_eps1_0e-6_S9_m50000_rB2_0_rndlen50_K60_nrb9.jd"
logs_dsgd_m2_k60 = load(file)["logs"]

file = "./k100/20150113T164329_C1_ts1016_6_mxiter999999_kappa0_51_t100_submeantrue_rP2_0_best0_8424_iscale0_01_tau050_itvsv100_rA2_0_ncb9_algoDSGD-2_iter100_itvtst10_rQ2_0_eps1_0e-6_S9_m50000_rB2_0_rndlen50_K100_nrb9.jd"
logs_dsgd_m2_k100 = load(file)["logs"]


###############################
# PyPlot must be included after loading HDF5 and JLD due to a bug in v0.3x
###############################
using PyPlot
clf()

hold(true)

############################################
# DD-SGLD 3x3
#brn_idx = find(logs_ddsgld_m2_3x3_k30["ts"] .<= logs_ddsgld_m2_3x3_k30["burnin_ts"])
#avg_idx = find(logs_ddsgld_m2_3x3_k30["ts"] .> logs_ddsgld_m2_3x3_k30["burnin_ts"])
##ec2_brn_ts = cumsum(ones(length(brn_idx)) * 120)
##ec2_avg_ts = ec2_brn_ts[end] + cumsum(ones(length(avg_idx)) * 160)
##ec2_ts = vcat(ec2_brn_ts, ec2_avg_ts)
#rmse = vcat(logs_ddsgld_m2_3x3_k30["cur_rmse"][1:avg_idx[1]-1], logs_ddsgld_m2_3x3_k30["avg_rmse"][avg_idx])
#if issemilogx
    #semilogx(logs_ddsgld_m2_3x3_k30["ts"][avg_idx], logs_ddsgld_m2_3x3_k30["avg_rmse"][avg_idx], color="purple", linewidth=3, label="D-SGLD-3x3") 
#else
    #plot(logs_ddsgld_m2_3x3_k30["ts"][avg_idx], logs_ddsgld_m2_3x3_k30["avg_rmse"][avg_idx], color="purple", linewidth=3, label="D-SGLD-3x3") 
#end

############################################
# D-SGLD 10x1
brn_idx = find(logs_dsgld_m2_c9s9_k30["ts"] .< logs_dsgld_m2_c9s9_k30["burnin_ts"])
avg_idx = find(logs_dsgld_m2_c9s9_k30["ts"] .> logs_dsgld_m2_c9s9_k30["burnin_ts"])
if issemilogx
    semilogx(logs_dsgld_m2_c9s9_k30["ts"][brn_idx], logs_dsgld_m2_c9s9_k30["cur_rmse"][brn_idx], "b-", linewidth=3) 
    semilogx(logs_dsgld_m2_c9s9_k30["ts"][avg_idx], logs_dsgld_m2_c9s9_k30["avg_rmse"][avg_idx], "b-", linewidth=3, label="D-SGLD (k=30)") 
else
    plot(logs_dsgld_m2_c9s9_k30["ts"][brn_idx], logs_dsgld_m2_c9s9_k30["cur_rmse"][brn_idx], "b-", linewidth=3)  
    plot(logs_dsgld_m2_c9s9_k30["ts"][avg_idx], logs_dsgld_m2_c9s9_k30["avg_rmse"][avg_idx], "b-", linewidth=3, label="D-SGLD (k=30)")  
end

brn_idx = find(logs_dsgld_m2_c9s9_k60["ts"] .<= logs_dsgld_m2_c9s9_k60["burnin_ts"])
avg_idx = find(logs_dsgld_m2_c9s9_k60["ts"] .> logs_dsgld_m2_c9s9_k60["burnin_ts"])
if issemilogx
    semilogx(logs_dsgld_m2_c9s9_k60["ts"][brn_idx], logs_dsgld_m2_c9s9_k60["cur_rmse"][brn_idx], "b--", linewidth=3) 
    semilogx(logs_dsgld_m2_c9s9_k60["ts"][avg_idx], logs_dsgld_m2_c9s9_k60["avg_rmse"][avg_idx], "b--", linewidth=3, label="D-SGLD (k=60)") 
else
    plot(logs_dsgld_m2_c9s9_k60["ts"][brn_idx], logs_dsgld_m2_c9s9_k60["cur_rmse"][brn_idx], "b--", linewidth=3)  
    plot(logs_dsgld_m2_c9s9_k60["ts"][avg_idx], logs_dsgld_m2_c9s9_k60["avg_rmse"][avg_idx], "b--", linewidth=3, label="D-SGLD (k=60)")  
end

brn_idx = find(logs_dsgld_m2_c9s9_k100["ts"] .<= logs_dsgld_m2_c9s9_k100["burnin_ts"])
avg_idx = find(logs_dsgld_m2_c9s9_k100["ts"] .> logs_dsgld_m2_c9s9_k100["burnin_ts"])
if issemilogx
    semilogx(logs_dsgld_m2_c9s9_k100["ts"][brn_idx], logs_dsgld_m2_c9s9_k100["cur_rmse"][brn_idx], "b:", linewidth=3) 
    semilogx(logs_dsgld_m2_c9s9_k100["ts"][avg_idx], logs_dsgld_m2_c9s9_k100["avg_rmse"][avg_idx], "b:", linewidth=3, label="D-SGLD (k=100)") 
else
    plot(logs_dsgld_m2_c9s9_k100["ts"][brn_idx], logs_dsgld_m2_c9s9_k100["cur_rmse"][brn_idx], "b:", linewidth=3)  
    plot(logs_dsgld_m2_c9s9_k100["ts"][avg_idx], logs_dsgld_m2_c9s9_k100["avg_rmse"][avg_idx], "b:", linewidth=3, label="D-SGLD (k=100)")  
end


############################################
# SGLD 
avg_idx = find(logs_sgld_m2_k30["ts"] .> logs_sgld_m2_k30["burnin_ts"])
brn_idx = find(logs_sgld_m2_k30["ts"] .< logs_sgld_m2_k30["burnin_ts"])
#rmse = vcat(logs_sgld_m2_k30["cur_rmse"][1:avg_idx[1]-1], logs_sgld_m2_k30["avg_rmse"][avg_idx])
if issemilogx
    semilogx(logs_sgld_m2_k30["ts"][brn_idx], logs_sgld_m2_k30["cur_rmse"][brn_idx], "g-", linewidth=3) 
    semilogx(logs_sgld_m2_k30["ts"][avg_idx], logs_sgld_m2_k30["avg_rmse"][avg_idx], "g-", linewidth=3, label="SGLD (k=30)") 
else
    plot(logs_sgld_m2_k30["ts"][brn_idx], logs_sgld_m2_k30["cur_rmse"][brn_idx], "g-", linewidth=3) 
    plot(logs_sgld_m2_k30["ts"][avg_idx], logs_sgld_m2_k30["avg_rmse"][avg_idx], "g-", linewidth=3, label="SGLD (k=30)") 
end

avg_idx = find(logs_sgld_m2_k60["ts"] .> logs_sgld_m2_k60["burnin_ts"])
brn_idx = find(logs_sgld_m2_k60["ts"] .< logs_sgld_m2_k60["burnin_ts"])
if issemilogx
    semilogx(logs_sgld_m2_k60["ts"][brn_idx], logs_sgld_m2_k60["cur_rmse"][brn_idx], "g--", linewidth=3) 
    semilogx(logs_sgld_m2_k60["ts"][avg_idx], logs_sgld_m2_k60["avg_rmse"][avg_idx], "g--", linewidth=3, label="SGLD (k=60)") 
else
    plot(logs_sgld_m2_k60["ts"][brn_idx], logs_sgld_m2_k60["cur_rmse"][brn_idx], "g--", linewidth=3) 
    plot(logs_sgld_m2_k60["ts"][avg_idx], logs_sgld_m2_k60["avg_rmse"][avg_idx], "g--", linewidth=3, label="SGLD (k=60)") 
end

avg_idx = find(logs_sgld_m2_k100["ts"] .> logs_sgld_m2_k100["burnin_ts"])
brn_idx = find(logs_sgld_m2_k100["ts"] .< logs_sgld_m2_k100["burnin_ts"])
if issemilogx
    semilogx(logs_sgld_m2_k100["ts"][brn_idx], logs_sgld_m2_k100["cur_rmse"][brn_idx], "g:", linewidth=3) 
    semilogx(logs_sgld_m2_k100["ts"][avg_idx], logs_sgld_m2_k100["avg_rmse"][avg_idx], "g:", linewidth=3, label="SGLD (k=100)") 
else
    plot(logs_sgld_m2_k100["ts"][brn_idx], logs_sgld_m2_k100["cur_rmse"][brn_idx], "g:", linewidth=3) 
    plot(logs_sgld_m2_k100["ts"][avg_idx], logs_sgld_m2_k100["avg_rmse"][avg_idx], "g:", linewidth=3, label="SGLD (k=100)") 
end

############################################
# DSGD
if issemilogx
    semilogx(logs_dsgd_m2_k30["ts"][1:end], logs_dsgd_m2_k30["rmse"][1:end], "y-", linewidth=3, label="D-SGD (k=30)")
else
    plot(logs_dsgd_m2_k30["ts"][1:end], logs_dsgd_m2_k30["rmse"][1:end], "y-", linewidth=3, label="D-SGD (k=30)")
end

if issemilogx
    semilogx(logs_dsgd_m2_k60["ts"][1:end], logs_dsgd_m2_k60["rmse"][1:end], "y--", linewidth=3, label="D-SGD (k=60)")
else
    plot(logs_dsgd_m2_k60["ts"][1:end], logs_dsgd_m2_k60["rmse"][1:end], "y--", linewidth=3, label="D-SGD (k=60)")
end

if issemilogx
    semilogx(logs_dsgd_m2_k100["ts"][1:end], logs_dsgd_m2_k100["rmse"][1:end], "y:", linewidth=3, label="D-SGD (k=100)")
else
    plot(logs_dsgd_m2_k100["ts"][1:end], logs_dsgd_m2_k100["rmse"][1:end], "y:", linewidth=3, label="D-SGD (k=100)")
end

############################################
# SGD
if issemilogx
    semilogx(logs_sgd_m2_k30["ts"][1:1:end], logs_sgd_m2_k30["cur_rmse"][1:1:end], "r-", linewidth=3, label="SGD (k=30)")
else
    plot(logs_sgd_m2_k30["ts"][1:1:end], logs_sgd_m2_k30["cur_rmse"][1:1:end], "r-", linewidth=3, label="SGD (k=30)")
end

if issemilogx
    semilogx(logs_sgd_m2_k60["ts"][1:1:end], logs_sgd_m2_k60["cur_rmse"][1:1:end], "r--", linewidth=3, label="SGD (k=60)")
else
    plot(logs_sgd_m2_k60["ts"][1:1:end], logs_sgd_m2_k60["cur_rmse"][1:1:end], "r--", linewidth=3, label="SGD (k=60)")
end

if issemilogx
    semilogx(logs_sgd_m2_k100["ts"][1:1:end], logs_sgd_m2_k100["cur_rmse"][1:1:end], "r:", linewidth=3, label="SGD (k=100)")
else
    plot(logs_sgd_m2_k100["ts"][1:1:end], logs_sgd_m2_k100["cur_rmse"][1:1:end], "r:", linewidth=3, label="SGD (k=100)")
end


############################################
# BPMF
#bpmf_ts = vcat(logs_map_m1_k30["ts"], logs_map_m1_k30["ts"][end] + logs_bpmf_m1_k30["ts"])
#bpmf_rmse = vcat(logs_map_m1_k30["cur_rmse"], logs_bpmf_m1_k30["avg_rmse"])
bpmf_ts = logs_map_m1_k30["ts"][end] + logs_bpmf_m1_k30["ts"]
bpmf_rmse = logs_bpmf_m1_k30["avg_rmse"]
if issemilogx
    semilogx(bpmf_ts, bpmf_rmse, "k-", marker="", markersize=10, markeredgewidth=2, linewidth=3, label="BPMF (k=30)") 
else
    plot(bpmf_ts, bpmf_rmse, "k-", marker="", markersize=10, markeredgewidth=2, linewidth=3, label="BPMF (k=30)") 
end

##############################################################

fontsize = 23
plt.rc("font", family="serif", size=fontsize)
xlabel("Sec.", size=fontsize)
ylabel("RMSE", size=fontsize)
if issemilogx 
    ylim([0.80, 0.96])
    #yticks([0.81:0.05: 0.93])
    xlim([3*10^1, 50000])
    legend(fancybox=true, framealpha=0.7, fontsize=20)
else
    ylim([0.805, 0.867])
    xlim([10^1, 11500])
    xticks([0:2000:10000])
    yticks([0.805:0.01:0.865])
    legend(fancybox=true, framealpha=0.7, fontsize=20)
end
grid(true)

if issemilogx
    savefig("net8020_all_logx.png", format="png", bbox_inches="tight")
    savefig("net8020_all_logx.eps", format="eps", bbox_inches="tight")
    savefig("net8020_all_logx.pdf", format="pdf", bbox_inches="tight")
else
    savefig("net8020_all_linx.png", format="png", bbox_inches="tight")
    savefig("net8020_all_linx.eps", format="eps", bbox_inches="tight")
    savefig("net8020_all_linx.pdf", format="pdf", bbox_inches="tight")
end



