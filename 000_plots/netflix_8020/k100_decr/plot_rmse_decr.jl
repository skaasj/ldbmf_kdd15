include("../../../24_v0.07_dev_64/utils/types.jl")
using HDF5, JLD

issemilogx = true

# bpmf
file = "20150113T154928_ts460_1_tg_rmse0_865_mxiter999999_maptrue_kappa0_51_la0_p2_0_submeantrue_best0_8645_iscale0_01_tau050_itvsv100_algoSGD-m1-MAP_mxsec999999_itvtst10_itvhyp30_iter110_eps9_0e-6_m50000_la0_q2_0_K100_rndlen25.jd"
logs_map_k100 = load(file)["logs"]

file = "20150116T153046_ts102174_7_mxiter999999_itv_gibbs1_best0_8272_bthresh0_95_itvsv1_algoG-G-1_mxsec9990000_itvtst1_iter3_K100.jd"
logs_gibbs_k100 = load(file)["logs"]

# SGLD
file = "20150115T104106_ts64733_2_mxiter9999999_mapfalse_submeantrue_rP2_0_best0_8076_bthresh0_86_iscale0_01_itvsv100_rA2_0_algoS-G-2_mxsec9999999_itvtst20_rQ2_0_itvhyp100_iter6900_eps8_0e-6_bt0700_0_m50000_rB2_0_K100_rndlen30.jd"
logs_sgld_k100 = load(file)["logs"]

# D-SGLD
file = "20150115T104801_C9_ts61148_0_mxiter999999_t5000_rP2_0_best0_8043_bthresh0_86_iscale0_01_itvsv100_rA2_0_algoDS-G-2_itvtst20_rQ2_0_itvhyp100_eps6_0e-6_S9_bt0600_0_m50000_aug0_0_rB2_0_K100_rndlen40.jd"
logs_dsgldc_k100 = load(file)["logs"]

# SGD
file = "20150131T225105_ka0_51_eps06_0e-6_tau10_ts3920_7_tg_rmse0_0_mxiter9999999_maptrue_submeantrue_rP5_0_best0_8415_bthresh0_0_iscale0_01_itvsv100_rA5_0_algoSGD-m2-MAP_mxsec9999999_itvtst20_rQ5_0_itvhyp50_iter400_bt0300_0_m50000_rB5_0_K100_rndlen50.jd"
logs_sgd_k100 = load(file)["logs"]

# DSGD
file = "20150131T222210_ka0_51_C1_eps01_0e-6_tau10_ts2181_0_mxiter999999_t200_submeantrue_rP2_0_best0_8395_iscale0_01_itvsv100_rA2_0_ncb9_algoDSGD-2_iter200_itvtst10_rQ2_0_S9_m50000_rB2_0_K100_nrb9_rndlen50.jd"
logs_dsgd_k100 = load(file)["logs"]


###############################
# PyPlot must be included after loading HDF5 and JLD due to a bug in v0.3x
###############################
using PyPlot
clf()

hold(true)


############################################
# D-SGLD 9x1
avg_idx = find(logs_dsgldc_k100["ts"] .> logs_dsgldc_k100["burnin_ts"])
rmse = vcat(logs_dsgldc_k100["cur_rmse"][1:avg_idx[1]-1], logs_dsgldc_k100["avg_rmse"][avg_idx])
if issemilogx
    semilogx(logs_dsgldc_k100["ts"], rmse, "b-", linewidth=3, label="DSGLD-C (9x1)") 
else
    plot(logs_dsgldc_k100["ts"], rmse, "b-", linewidth=3, label="DSGLD-C (9x1)")  
end

############################################
# DSGD
#idx = [0.0:100000]
if issemilogx
    semilogx(logs_dsgd_k100["ts"][1:end], logs_dsgd_k100["rmse"][1:end], "y-", linewidth=3, label="DSGD (9x9)")
else
    plot(logs_dsgd_k100["ts"][1:end], logs_dsgd_k100["rmse"][1:end], "y-", linewidth=3, label="DSGD (9x9)")
end

############################################
# SGLD 
avg_idx = find(logs_sgld_k100["ts"] .> logs_sgld_k100["burnin_ts"])
rmse = vcat(logs_sgld_k100["cur_rmse"][1:avg_idx[1]-1], logs_sgld_k100["avg_rmse"][avg_idx])
if issemilogx
    semilogx(logs_sgld_k100["ts"], rmse, "g-", linewidth=3, label="SGLD") 
else
    plot(logs_sgld_k100["ts"], rmse, "g-", linewidth=3, label="SGLD") 
end
############################################
# BPMF
bpmf_ts = logs_map_k100["ts"][end] + logs_gibbs_k100["ts"]
bpmf_rmse = logs_gibbs_k100["avg_rmse"]
if issemilogx
    semilogx(bpmf_ts, bpmf_rmse, "k-", marker="", markersize=10, markeredgewidth=2, linewidth=3, label="Gibbs") 
else
    plot(bpmf_ts, bpmf_rmse, "k-", marker="", markersize=10, markeredgewidth=2, linewidth=3, label="Gibbs") 
end

############################################
# SGD
idx = [0.0:200000]
if issemilogx
    #semilogx(logs_sgd_k100["ts"][1:1:end], logs_sgd_k100["cur_rmse"][1:1:end], "r-", linewidth=3, label="SGD")
    semilogx(idx, ones(length(idx)) * 0.8415, "k--", linewidth=3, label="SGD")
else
    #plot(logs_sgd_k100["ts"][1:1:end], logs_sgd_k100["cur_rmse"][1:1:end], "r-", linewidth=3, label="SGD")
    plot(idx, ones(length(idx)) * 0.8415, "k--", linewidth=3, label="SGD")
end


##############################################################

fontsize = 21
plt.rc("font", family="serif", size=fontsize)
xlabel("Sec.", size=fontsize)
ylabel("RMSE", size=fontsize)
if issemilogx 
    ylim([0.800, 0.93])
    #yticks([0.81:0.05: 0.93])
    xlim([3*10^1, 200000])
    legend(fancybox=true, framealpha=0.7, fontsize=17)
else
    ylim([0.81, 0.866])
    xlim([10^1, 10500])
    xticks([0:2000:10000])
    yticks([0.81:0.01:0.865])
    legend(fancybox=true, framealpha=0.7, fontsize=17)
end
grid(true)

if issemilogx
    savefig("net8020_k100_decr_logx.png", format="png", bbox_inches="tight")
    savefig("net8020_k100_decr_logx.eps", format="eps", bbox_inches="tight")
    savefig("net8020_k100_decr_logx.pdf", format="pdf", bbox_inches="tight")
else
    savefig("net8020_k100_decr_linx.png", format="png", bbox_inches="tight")
    savefig("net8020_k100_decr_linx.eps", format="eps", bbox_inches="tight")
    savefig("net8020_k100_decr_linx.pdf", format="pdf", bbox_inches="tight")
end



