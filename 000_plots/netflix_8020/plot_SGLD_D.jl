include("../../27_v0.08_dev/utils/types.jl")
using HDF5, JLD

issemilogx = false

#######################
# SGLD
file = "./k30/20150111T222359_ts12715_2_mxiter9999999_mapfalse_submeantrue_rP2_0_best0_8151_bthresh0_85_iscale0_01_itvsv200_rA2_0_algoS-G-2_mxsec9999999_itvtst20_rQ2_0_itvhyp50_iter3800_eps9_0e-6_bt0300_0_m50000_rB2_0_K30_rndlen30.jd"
logs_sgld_m2_k30 = load(file)["logs"]

file = "./k60/20150114T215306_ts36775_1_mxiter9999999_mapfalse_submeantrue_rP2_0_best0_8090_bthresh0_86_iscale0_01_itvsv100_rA2_0_algoS-G-2_mxsec9999999_itvtst20_rQ2_0_itvhyp100_iter6100_eps8_0e-6_bt0500_0_m50000_rB2_0_K60_rndlen30.jd"
logs_sgld_m2_k60 = load(file)["logs"]

file = "./k100/20150115T104106_ts64733_2_mxiter9999999_mapfalse_submeantrue_rP2_0_best0_8076_bthresh0_86_iscale0_01_itvsv100_rA2_0_algoS-G-2_mxsec9999999_itvtst20_rQ2_0_itvhyp100_iter6900_eps8_0e-6_bt0700_0_m50000_rB2_0_K100_rndlen30.jd"
logs_sgld_m2_k100 = load(file)["logs"]

#######################
# D-SGLD
file = "./k30/20150112T180037_C9_ts4393_7_mxiter999999_t1500_rP2_0_best0_8126_bthresh0_85_iscale0_01_itvsv100_rA2_0_algoDS-G-2_itvtst20_rQ2_0_itvhyp200_eps9_0e-6_S9_bt01_0_m50000_aug0_0_rB2_0_K30_rndlen25.jd"
logs_dsgld_m2_c9s9_k30 = load(file)["logs"]

file = "./k60/20150113T201110_C9_ts6097_4_mxiter999999_t800_rP2_0_best0_8095_bthresh0_86_iscale0_01_itvsv100_rA2_0_algoDS-G-2_itvtst20_rQ2_0_itvhyp100_eps7_0e-6_S9_bt0300_0_m50000_aug0_0_rB2_0_K60_rndlen40.jd"
logs_dsgld_m2_c9s9_k60 = load(file)["logs"]

file = "./k100/20150114T142414_C9_ts18233_6_mxiter999999_t1500_rP2_0_best0_8067_bthresh0_86_iscale0_01_itvsv100_rA2_0_algoDS-G-2_itvtst20_rQ2_0_itvhyp100_eps6_0e-6_S9_bt0500_0_m50000_aug0_0_rB2_0_K100_rndlen40.jd"
logs_dsgld_m2_c9s9_k100 = load(file)["logs"]


#######################
# DD-SGLD
file = "./k30/20150112T153707_C3_ts10371_1_mxiter999999_t1200_submeantrue_rP2_0_best0_8137_bthresh0_85_iscale0_1_itvsv200_rA2_0_ncb3_algoDDS-G-2_G1_itvtst10_rQ2_0_itvhyp100_eps3_0e-6_S9_bt0300_0_m50000_aug0_0_rB2_0_K30_nrb3_rndlen50.jd"
logs_ddsgld_m2_3x3_k30 = load(file)["logs"]

file = "./k60/20150116T104836_C3_ts67128_8_mxiter999999_t3600_submeantrue_rP2_0_best0_8087_bthresh0_85_iscale0_01_itvsv200_rA2_0_ncb3_algoDDS-G-2_itvtst10_rQ2_0_itvhyp100_eps3_0e-6_S9_bt0600_0_m50000_aug0_0_rB2_0_K60_nrb3_rndlen70.jd"
logs_ddsgld_m2_3x3_k60 = load(file)["logs"]

file = "./k100/20150116T102951_C3_ts64596_5_mxiter999999_t2400_submeantrue_rP2_0_best0_8094_bthresh0_85_iscale0_01_itvsv200_rA2_0_ncb3_algoDDS-G-2_G1_itvtst10_rQ2_0_itvhyp100_eps3_0e-6_S9_bt0700_0_m50000_aug0_0_rB2_0_K100_nrb3_rndlen70.jd"
logs_ddsgld_m2_3x3_k100 = load(file)["logs"]

###############################
# PyPlot must be included after loading HDF5 and JLD due to a bug in v0.3x
###############################
using PyPlot
clf()

hold(true)

############################################
# DD-SGLD 3x3
brn_idx = find(logs_ddsgld_m2_3x3_k30["ts"] .<= logs_ddsgld_m2_3x3_k30["burnin_ts"])
avg_idx = find(logs_ddsgld_m2_3x3_k30["ts"] .> logs_ddsgld_m2_3x3_k30["burnin_ts"])
#rmse = vcat(logs_ddsgld_m2_3x3_k30["cur_rmse"][1:avg_idx[1]-1], logs_ddsgld_m2_3x3_k30["avg_rmse"][avg_idx])
if issemilogx
    semilogx(logs_ddsgld_m2_3x3_k30["ts"][brn_idx], logs_ddsgld_m2_3x3_k30["cur_rmse"][brn_idx], color="purple", linewidth=3) 
    semilogx(logs_ddsgld_m2_3x3_k30["ts"][avg_idx], logs_ddsgld_m2_3x3_k30["avg_rmse"][avg_idx], color="purple", linewidth=3, label="D-SGLD-3x3 (k=30)") 
else
    plot(logs_ddsgld_m2_3x3_k30["ts"][brn_idx], logs_ddsgld_m2_3x3_k30["cur_rmse"][brn_idx], color="purple", linewidth=3) 
    plot(logs_ddsgld_m2_3x3_k30["ts"][avg_idx], logs_ddsgld_m2_3x3_k30["avg_rmse"][avg_idx], color="purple", linewidth=3, label="D-SGLD-3x3 (k=30)") 
end

brn_idx = find(logs_ddsgld_m2_3x3_k60["ts"] .<= logs_ddsgld_m2_3x3_k60["burnin_ts"])
avg_idx = find(logs_ddsgld_m2_3x3_k60["ts"] .> logs_ddsgld_m2_3x3_k60["burnin_ts"])
if issemilogx
    semilogx(logs_ddsgld_m2_3x3_k60["ts"][brn_idx], logs_ddsgld_m2_3x3_k60["cur_rmse"][brn_idx], color="purple", "--", linewidth=3) 
    semilogx(logs_ddsgld_m2_3x3_k60["ts"][avg_idx], logs_ddsgld_m2_3x3_k60["avg_rmse"][avg_idx], color="purple", "--", linewidth=3, label="D-SGLD-3x3 (k=60)") 
else
    plot(logs_ddsgld_m2_3x3_k60["ts"][brn_idx], logs_ddsgld_m2_3x3_k60["cur_rmse"][brn_idx], color="purple", "--", linewidth=3) 
    plot(logs_ddsgld_m2_3x3_k60["ts"][avg_idx], logs_ddsgld_m2_3x3_k60["avg_rmse"][avg_idx], color="purple", "--", linewidth=3, label="D-SGLD-3x3 (k=60)") 
end

brn_idx = find(logs_ddsgld_m2_3x3_k100["ts"] .<= logs_ddsgld_m2_3x3_k100["burnin_ts"])
avg_idx = find(logs_ddsgld_m2_3x3_k100["ts"] .> logs_ddsgld_m2_3x3_k100["burnin_ts"])
if issemilogx
    semilogx(logs_ddsgld_m2_3x3_k100["ts"][brn_idx], logs_ddsgld_m2_3x3_k100["cur_rmse"][brn_idx], color="purple", ":", linewidth=3) 
    semilogx(logs_ddsgld_m2_3x3_k100["ts"][avg_idx], logs_ddsgld_m2_3x3_k100["avg_rmse"][avg_idx], color="purple", ":", linewidth=3, label="D-SGLD-3x3 (k=100)") 
else
    plot(logs_ddsgld_m2_3x3_k100["ts"][brn_idx], logs_ddsgld_m2_3x3_k100["cur_rmse"][brn_idx], color="purple", ":", linewidth=3) 
    plot(logs_ddsgld_m2_3x3_k100["ts"][avg_idx], logs_ddsgld_m2_3x3_k100["avg_rmse"][avg_idx], color="purple", ":", linewidth=3, label="D-SGLD-3x3 (k=100)") 
end


############################################
# D-SGLD 10x1
brn_idx = find(logs_dsgld_m2_c9s9_k30["ts"] .<= logs_dsgld_m2_c9s9_k30["burnin_ts"])
avg_idx = find(logs_dsgld_m2_c9s9_k30["ts"] .> logs_dsgld_m2_c9s9_k30["burnin_ts"])
if issemilogx
    semilogx(logs_dsgld_m2_c9s9_k30["ts"][brn_idx], logs_dsgld_m2_c9s9_k30["cur_rmse"][brn_idx], "b-", linewidth=3) 
    semilogx(logs_dsgld_m2_c9s9_k30["ts"][avg_idx], logs_dsgld_m2_c9s9_k30["avg_rmse"][avg_idx], "b-", linewidth=3, label="D-SGLD (k=30)") 
else
    plot(logs_dsgld_m2_c9s9_k30["ts"][brn_idx], logs_dsgld_m2_c9s9_k30["cur_rmse"][brn_idx], "b-", linewidth=3)  
    plot(logs_dsgld_m2_c9s9_k30["ts"][avg_idx], logs_dsgld_m2_c9s9_k30["avg_rmse"][avg_idx], "b-", linewidth=3, label="D-SGLD (k=30)")  
end

brn_idx = find(logs_dsgld_m2_c9s9_k60["ts"] .<= logs_dsgld_m2_c9s9_k60["burnin_ts"])
avg_idx = find(logs_dsgld_m2_c9s9_k60["ts"] .> logs_dsgld_m2_c9s9_k60["burnin_ts"])
if issemilogx
    semilogx(logs_dsgld_m2_c9s9_k60["ts"][brn_idx], logs_dsgld_m2_c9s9_k60["cur_rmse"][brn_idx], "b--", linewidth=3) 
    semilogx(logs_dsgld_m2_c9s9_k60["ts"][avg_idx], logs_dsgld_m2_c9s9_k60["avg_rmse"][avg_idx], "b--", linewidth=3, label="D-SGLD (k=60)") 
else
    plot(logs_dsgld_m2_c9s9_k60["ts"][brn_idx], logs_dsgld_m2_c9s9_k60["cur_rmse"][brn_idx], "b--", linewidth=3)  
    plot(logs_dsgld_m2_c9s9_k60["ts"][avg_idx], logs_dsgld_m2_c9s9_k60["avg_rmse"][avg_idx], "b--", linewidth=3, label="D-SGLD (k=60)")  
end

brn_idx = find(logs_dsgld_m2_c9s9_k100["ts"] .<= logs_dsgld_m2_c9s9_k100["burnin_ts"])
avg_idx = find(logs_dsgld_m2_c9s9_k100["ts"] .> logs_dsgld_m2_c9s9_k100["burnin_ts"])
if issemilogx
    semilogx(logs_dsgld_m2_c9s9_k100["ts"][brn_idx], logs_dsgld_m2_c9s9_k100["cur_rmse"][brn_idx], "b:", linewidth=3) 
    semilogx(logs_dsgld_m2_c9s9_k100["ts"][avg_idx], logs_dsgld_m2_c9s9_k100["avg_rmse"][avg_idx], "b:", linewidth=3, label="D-SGLD (k=100)") 
else
    plot(logs_dsgld_m2_c9s9_k100["ts"][brn_idx], logs_dsgld_m2_c9s9_k100["cur_rmse"][brn_idx], "b:", linewidth=3)  
    plot(logs_dsgld_m2_c9s9_k100["ts"][avg_idx], logs_dsgld_m2_c9s9_k100["avg_rmse"][avg_idx], "b:", linewidth=3, label="D-SGLD (k=100)")  
end


############################################
# SGLD 
#avg_idx = find(logs_sgld_m2_k30["ts"] .> logs_sgld_m2_k30["burnin_ts"])
#brn_idx = find(logs_sgld_m2_k30["ts"] .<= logs_sgld_m2_k30["burnin_ts"])
##rmse = vcat(logs_sgld_m2_k30["cur_rmse"][1:avg_idx[1]-1], logs_sgld_m2_k30["avg_rmse"][avg_idx])
#if issemilogx
    #semilogx(logs_sgld_m2_k30["ts"][brn_idx], logs_sgld_m2_k30["cur_rmse"][brn_idx], "g-", linewidth=3) 
    #semilogx(logs_sgld_m2_k30["ts"][avg_idx], logs_sgld_m2_k30["avg_rmse"][avg_idx], "g-", linewidth=3, label="SGLD (k=30)") 
#else
    #plot(logs_sgld_m2_k30["ts"][brn_idx], logs_sgld_m2_k30["cur_rmse"][brn_idx], "g-", linewidth=3) 
    #plot(logs_sgld_m2_k30["ts"][avg_idx], logs_sgld_m2_k30["avg_rmse"][avg_idx], "g-", linewidth=3, label="SGLD (k=30)") 
#end

#avg_idx = find(logs_sgld_m2_k60["ts"] .> logs_sgld_m2_k60["burnin_ts"])
#brn_idx = find(logs_sgld_m2_k60["ts"] .<= logs_sgld_m2_k60["burnin_ts"])
#if issemilogx
    #semilogx(logs_sgld_m2_k60["ts"][brn_idx], logs_sgld_m2_k60["cur_rmse"][brn_idx], "g--", linewidth=3) 
    #semilogx(logs_sgld_m2_k60["ts"][avg_idx], logs_sgld_m2_k60["avg_rmse"][avg_idx], "g--", linewidth=3, label="SGLD (k=60)") 
#else
    #plot(logs_sgld_m2_k60["ts"][brn_idx], logs_sgld_m2_k60["cur_rmse"][brn_idx], "g--", linewidth=3) 
    #plot(logs_sgld_m2_k60["ts"][avg_idx], logs_sgld_m2_k60["avg_rmse"][avg_idx], "g--", linewidth=3, label="SGLD (k=60)") 
#end

#avg_idx = find(logs_sgld_m2_k100["ts"] .> logs_sgld_m2_k100["burnin_ts"])
#brn_idx = find(logs_sgld_m2_k100["ts"] .<= logs_sgld_m2_k100["burnin_ts"])
#if issemilogx
    #semilogx(logs_sgld_m2_k100["ts"][brn_idx], logs_sgld_m2_k100["cur_rmse"][brn_idx], "g:", linewidth=3) 
    #semilogx(logs_sgld_m2_k100["ts"][avg_idx], logs_sgld_m2_k100["avg_rmse"][avg_idx], "g:", linewidth=3, label="SGLD (k=100)") 
#else
    #plot(logs_sgld_m2_k100["ts"][brn_idx], logs_sgld_m2_k100["cur_rmse"][brn_idx], "g:", linewidth=3) 
    #plot(logs_sgld_m2_k100["ts"][avg_idx], logs_sgld_m2_k100["avg_rmse"][avg_idx], "g:", linewidth=3, label="SGLD (k=100)") 
#end


##############################################################

fontsize = 21
plt.rc("font", family="serif", size=fontsize)
xlabel("Sec.", size=fontsize)
ylabel("RMSE", size=fontsize)
if issemilogx 
    ylim([0.80, 0.96])
    #yticks([0.81:0.05: 0.93])
    xlim([3*10^1, 50000])
    legend(fancybox=true, framealpha=0.7, fontsize=18)
else
    ylim([0.805, 0.867])
    xlim([10^1, 11500])
    xticks([0:2000:10000])
    yticks([0.805:0.01:0.865])
    legend(fancybox=true, framealpha=0.7, fontsize=18)
end
grid(true)

if issemilogx
    savefig("net8020_all_SGLD_logx.png", format="png", bbox_inches="tight")
    savefig("net8020_all_SGLD_logx.eps", format="eps", bbox_inches="tight")
    savefig("net8020_all_SGLD_logx.pdf", format="pdf", bbox_inches="tight")
else
    savefig("net8020_all_SGLD_linx.png", format="png", bbox_inches="tight")
    savefig("net8020_all_SGLD_linx.eps", format="eps", bbox_inches="tight")
    savefig("net8020_all_SGLD_linx.pdf", format="pdf", bbox_inches="tight")
end



