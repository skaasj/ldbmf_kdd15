include("../../27_v0.08_dev/utils/types.jl")

###############################
# PyPlot must be included after loading HDF5 and JLD due to a bug in v0.3x
###############################
using PyPlot
clf()
#tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),  
             #(44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),  
             #(148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),  
             #(227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),  
             #(188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]  

hold(true)

#colors.ColorConverter.colors["aaa"] = tableau20[1]

D = [30,60,100]
plot(D, [0.8421, 0.8447, 0.8415], "r-+", linewidth=3, markersize=10, markeredgewidth=3, label="SGD")
plot(D, [0.8462, 0.8428, 0.8395], "y-+", linewidth=3, markersize=10, markeredgewidth=3, label="DSGD" )
plot(D, [0.8143, 0.8097, 0.8082], "g-+", linewidth=3, markersize=10, markeredgewidth=3, label="SGLD")
plot(D, [0.8126, 0.8074, 0.8043], "b-+", linewidth=3, markersize=10, markeredgewidth=3, label="DSGLD-C")
plot(D, [0.8118, 0.8259, 0.8339], "k-+", linewidth=3, markersize=10, markeredgewidth=3, label="Gibbs")

##############################################################
fontsize = 21
plt.rc("font", family="serif", size=fontsize)
xlabel("D", size=fontsize)
ylabel("RMSE", size=fontsize)

ylim([0.80, 0.850])
xlim([20, 110])
xticks([30,60,100])
#yticks([0.805:0.01:0.865])
legend(fancybox=true, framealpha=0.7, fontsize=17, loc=6)

grid(true)

#if issemilogx
    #savefig("net8020_all_logx.png", format="png", bbox_inches="tight")
    #savefig("net8020_all_logx.eps", format="eps", bbox_inches="tight")
    #savefig("net8020_all_logx.pdf", format="pdf", bbox_inches="tight")
#else
    savefig("net8020_D.png", format="png", bbox_inches="tight")
    savefig("net8020_D.eps", format="eps", bbox_inches="tight")
    savefig("net8020_D.pdf", format="pdf", bbox_inches="tight")
#end



