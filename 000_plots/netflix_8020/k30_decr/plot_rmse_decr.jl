include("../../../24_v0.07_dev_64/utils/types.jl")
using HDF5, JLD

issemilogx = false

# bpmf
file = "20150109T114754_ts584_8_mxiter999999_tg_rmse0_85_maptrue_la0_p0_001_submeantrue_best100_0000_bthresh0_85_iscale0_01_itvsv200_algoSGD-m1-MAP_mxsec200000_itvtst10_itvhyp30_iter380_eps8_0e-6_m50000_la0_q0_001_K30_rndlen20.jd"
logs_map_k30 = load(file)["logs"]

#file = "20150109T190809_ts25844_3_mxiter999999_itv_gibbs1_best0_8125_bthresh0_95_itvsv1_algoG-G-1_mxsec9990000_itvtst1_iter40_K30.jd"
file = "20150206T090932_ts150784_5_mxiter999999_itv_gibbs1_best0_8118_bthresh0_95_itvsv1_algoG-G-1_mxsec9990000_itvtst1_iter305_K30.jd"
logs_gibbs_k30 = load(file)["logs"]

# SGLD
file = "20150131T164739_ka0_51_eps09_0e-6_tau1000_ts51896_6_tg_rmse0_0_mxiter9999999_mapfalse_submeantrue_rP2_0_best0_8143_bthresh0_85_iscale0_01_itvsv100_rA2_0_algoS-G-2_mxsec9999999_itvtst20_rQ2_0_itvhyp50_iter5100_bt0300_0_m50000_rB2_0_K30_rndlen50.jd"
logs_sgld_k30 = load(file)["logs"]

# D-SGLD
file = "20150131T163251_ka0_51_C9_eps09_0e-6_tau1000_ts47214_3_mxiter999999_t4100_rP2_0_best0_8128_bthresh0_85_iscale0_01_itvsv100_rA2_0_algoDS-G-2_itvtst10_rQ2_0_itvhyp50_S9_bt0300_0_m50000_aug0_0_rB2_0_K30_rndlen50.jd"
logs_dsgldc_k30 = load(file)["logs"]

# DD-SGLD
file = "20150131T162651_ka0_51_C3_eps03_0e-6_tau500_ts48959_5_mxiter999999_t3400_submeantrue_rP2_0_best0_8143_bthresh0_85_iscale0_01_itvsv100_rA2_0_ncb3_algoDDS-G-2_G1_itvtst10_rQ2_0_itvhyp50_S9_bt0300_0_m50000_aug0_0_rB2_0_K30_nrb3_rndlen50.jd"
logs_dsglds_k30 = load(file)["logs"]

# SGD
file = "20150131T184103_ka0_51_eps09_0e-6_tau50_ts3953_2_tg_rmse0_0_mxiter9999999_maptrue_submeantrue_rP2_0_best0_8421_bthresh0_0_iscale0_01_itvsv100_rA2_0_algoSGD-m2-MAP_mxsec9999999_itvtst20_rQ2_0_itvhyp50_iter1000_bt0300_0_m50000_rB2_0_K30_rndlen50.jd"
logs_sgd_k30 = load(file)["logs"]

# DSGD
file = "20150131T221154_ka0_51_C1_eps01_0e-6_tau10_ts1256_7_mxiter999999_t300_submeantrue_rP2_0_best0_8462_iscale0_01_itvsv100_rA2_0_ncb9_algoDSGD-2_iter300_itvtst10_rQ2_0_S9_m50000_rB2_0_K30_nrb9_rndlen50.jd"
logs_dsgd_k30 = load(file)["logs"]


###############################
# PyPlot must be included after loading HDF5 and JLD due to a bug in v0.3x
###############################
using PyPlot
clf()

hold(true)

############################################
# D-SGLD 3x3
brn_idx = find(logs_dsglds_k30["ts"] .<= logs_dsglds_k30["burnin_ts"])
avg_idx = find(logs_dsglds_k30["ts"] .> logs_dsglds_k30["burnin_ts"])
rmse = vcat(logs_dsglds_k30["cur_rmse"][1:avg_idx[1]-1], logs_dsglds_k30["avg_rmse"][avg_idx])
if issemilogx
    semilogx(logs_dsglds_k30["ts"], rmse, color="red", linewidth=3, label="DSGLD-S (3x3)") 
else
    plot(logs_dsglds_k30["ts"], rmse, color="red", linewidth=3, label="DSGLD-S (3x3)") 
end

############################################
# D-SGLD 9x1
avg_idx = find(logs_dsgldc_k30["ts"] .> logs_dsgldc_k30["burnin_ts"])
rmse = vcat(logs_dsgldc_k30["cur_rmse"][1:avg_idx[1]-1], logs_dsgldc_k30["avg_rmse"][avg_idx])
if issemilogx
    semilogx(logs_dsgldc_k30["ts"], rmse, "b-", linewidth=3, label="DSGLD-C (9x1)") 
else
    plot(logs_dsgldc_k30["ts"], rmse, "b-", linewidth=3, label="DSGLD-C (9x1)")  
end

############################################
# DSGD
#idx = [0.0:100000]
if issemilogx
    semilogx(logs_dsgd_k30["ts"][1:end], logs_dsgd_k30["rmse"][1:end], "y-", linewidth=3, label="DSGD (9x9)")
else
    plot(logs_dsgd_k30["ts"][1:end], logs_dsgd_k30["rmse"][1:end], "y-", linewidth=3, label="DSGD (9x9)")
end

############################################
# SGLD 
avg_idx = find(logs_sgld_k30["ts"] .> logs_sgld_k30["burnin_ts"])
rmse = vcat(logs_sgld_k30["cur_rmse"][1:avg_idx[1]-1], logs_sgld_k30["avg_rmse"][avg_idx])
if issemilogx
    semilogx(logs_sgld_k30["ts"], rmse, "g-", linewidth=3, label="SGLD") 
else
    plot(logs_sgld_k30["ts"], rmse, "g-", linewidth=3, label="SGLD") 
end
############################################
# BPMF
bpmf_ts = logs_map_k30["ts"][end] + logs_gibbs_k30["ts"]
bpmf_rmse = logs_gibbs_k30["avg_rmse"]
if issemilogx
    semilogx(bpmf_ts, bpmf_rmse, "k-", marker="", markersize=10, markeredgewidth=2, linewidth=3, label="Gibbs") 
else
    plot(bpmf_ts, bpmf_rmse, "k-", marker="", markersize=10, markeredgewidth=2, linewidth=3, label="Gibbs") 
end

############################################
# SGD
idx = [0.0:100000]
if issemilogx
    #semilogx(logs_sgd_k30["ts"][1:1:end], logs_sgd_k30["cur_rmse"][1:1:end], "r-", linewidth=3, label="SGD")
    semilogx(idx, ones(length(idx)) * 0.8421, "k--", linewidth=3, label="SGD")
else
    #plot(logs_sgd_k30["ts"][1:1:end], logs_sgd_k30["cur_rmse"][1:1:end], "r-", linewidth=3, label="SGD")
    plot(idx, ones(length(idx)) * 0.8421, "k--", linewidth=3, label="SGD")
end


##############################################################

fontsize = 21
plt.rc("font", family="serif", size=fontsize)
xlabel("Sec.", size=fontsize)
ylabel("RMSE", size=fontsize)
if issemilogx 
    ylim([0.805, 0.93])
    #yticks([0.81:0.05: 0.93])
    xlim([3*10^1, 30000])
    legend(fancybox=true, framealpha=0.7, fontsize=17)
else
    ylim([0.81, 0.866])
    xlim([10^1, 10500])
    xticks([0:2000:10000])
    yticks([0.81:0.01:0.865])
    legend(fancybox=true, framealpha=0.7, fontsize=17)
end
grid(true)

if issemilogx
    savefig("net8020_k30_decr_logx.png", format="png", bbox_inches="tight")
    savefig("net8020_k30_decr_logx.eps", format="eps", bbox_inches="tight")
    savefig("net8020_k30_decr_logx.pdf", format="pdf", bbox_inches="tight")
else
    savefig("net8020_k30_decr_linx.png", format="png", bbox_inches="tight")
    savefig("net8020_k30_decr_linx.eps", format="eps", bbox_inches="tight")
    savefig("net8020_k30_decr_linx.pdf", format="pdf", bbox_inches="tight")
end



