include("../../../24_v0.07_dev_64/utils/types.jl")
using HDF5, JLD

issemilogx = false

file = "20141227T093335_ts78540_5_mxiter999999_hthresh1_08_rP2_0_submeantrue_bthresh1_08_iscale0_0001_best1_0451_itvsv200_rA2_0_algoS-G-2_mxsec999999_itvtst10_rQ2_0_itvhyp50_iter5000_eps1_0e-6_bt0300_0_m100000_rB2_0_K30_rndlen50.jd"
logs_sgld_m2_30k = load(file)["logs"]

file = "20150108T135738_ts188870_6_mxiter999999_mapfalse_hthresh1_06_rP2_0_submeantrue_bthresh1_06_iscale0_0001_best1_0385_itvsv200_rA2_0_algoS-G-2_mxsec999999_itvtst10_rQ2_0_itvhyp50_iter4400_eps1_0e-6_bt0300_0_m100000_rB2_0_K100_rndlen50.jd"
logs_sgld_m2_100k = load(file)["logs"]

file = "../K30/20150104T200910_C4_ts82335_9_mxiter999999_t3600_submeantrue_rP2_0_best1_0387_bthresh1_08_iscale0_0001_itvsv200_rA2_0_ncb4_algoDDS-G-2_G1_itvtst10_rQ2_0_itvhyp50_eps1_0e-6_S16_bt0300_0_m100000_aug0_0_rB2_0_K30_nrb4_rndlen50.jd"
logs_dsgld_m2_k30 = load(file)["logs"]

file = "../K30/20150106T234823_C1_ts10318_3_mxiter999999_t800_submeantrue_rP40_0_best1_0616_iscale0_0001_itvsv200_rA1_0_ncb16_algoDSGD-2_iter800_itvtst10_rQ3_0_eps3_0e-7_S16_m100000_rB2_0_K30_nrb16_rndlen50.jd"
logs_dsgd_m2_k30 = load(file)["logs"]

###############################
# PyPlot must be included after loading HDF5 and JLD due to a bug in v0.3x
###############################
using PyPlot
clf()

hold(true)
############################################
# SGLD 
avg_idx = find(logs_sgld_m2_30k["ts"] .> logs_sgld_m2_30k["burnin_ts"])
rmse = vcat(logs_sgld_m2_30k["cur_rmse"][1:avg_idx[1]-1], logs_sgld_m2_30k["avg_rmse"][avg_idx])
if issemilogx
    semilogx(logs_sgld_m2_30k["ts"], rmse, "b-", linewidth=2, label="SGLD (K=30)") 
else
    plot(logs_sgld_m2_30k["ts"], rmse, "b-", linewidth=2, label="SGLD (K=30)") 
end

avg_idx = find(logs_sgld_m2_100k["ts"] .> logs_sgld_m2_100k["burnin_ts"])
rmse = vcat(logs_sgld_m2_100k["cur_rmse"][1:avg_idx[1]-1], logs_sgld_m2_100k["avg_rmse"][avg_idx])
if issemilogx
    semilogx(logs_sgld_m2_100k["ts"], rmse, "k-", linewidth=2, label="SGLD (K=100)") 
else
    plot(logs_sgld_m2_100k["ts"], rmse, "k-", linewidth=2, label="SGLD (K=100)") 
end


#############################################
## D-SGLD 
#brn_idx = find(logs_dsgld_m2_k30["ts"] .<= logs_dsgld_m2_k30["burnin_ts"])
#avg_idx = find(logs_dsgld_m2_k30["ts"] .> logs_dsgld_m2_k30["burnin_ts"])
#ec2_brn_ts = cumsum(ones(length(brn_idx)) * 120)
#ec2_avg_ts = ec2_brn_ts[end] + cumsum(ones(length(avg_idx)) * 160)
#ec2_ts = vcat(ec2_brn_ts, ec2_avg_ts)
#rmse = vcat(logs_dsgld_m2_k30["cur_rmse"][1:avg_idx[1]-1], logs_dsgld_m2_k30["avg_rmse"][avg_idx])
#if issemilogx
    #semilogx(logs_dsgld_m2_k30["ts"], rmse, "b-", linewidth=2, label="D-SGLD (S=16, C=4)") 
    ##semilogx(ec2_ts, rmse, "b-", linewidth=2, label="DBMF-EC2 (S=16, C=4)") 
#else
    #plot(logs_dsgld_m2_k30["ts"], rmse, "b-", linewidth=2, label="D-SGLD (S=16, C=4)") 
    ##plot(ec2_ts, rmse, "b-", linewidth=2, label="DBMF-EC2 (S=16, C=4)") 
#end


#############################################
## DSGD
#idx = [0.0:100000]
#if issemilogx
    ##semilogx(idx, ones(length(idx)) * 1.0615, "k-", linewidth=2, label="SGD")
    #semilogx(logs_dsgd_m2_k30["ts"][1:end], logs_dsgd_m2_k30["rmse"][1:end], "r-", linewidth=2, label="D-SGD (S=16)")
#else
    ##plot(idx, ones(length(idx)) * 1.0615, "k-", linewidth=2, label="SGD")
    #plot(logs_dsgd_m2_k30["ts"][1:end], logs_dsgd_m2_k30["rmse"][1:end], "r-", linewidth=2, label="D-SGD (S=16)")
#end


#############################################
## SGD
#idx = [0.0:100000]
#if issemilogx
    ##semilogx(idx, ones(length(idx)) * 1.0615, "k-", linewidth=2, label="SGD")
    #semilogx(logs_sgd["ts"][1:5:end], logs_sgd["cur_rmse"][1:5:end], "r--", linewidth=2, label="SGD")
#else
    ##plot(idx, ones(length(idx)) * 1.0615, "k-", linewidth=2, label="SGD")
    #plot(logs_sgd["ts"][1:20:end], logs_sgd["cur_rmse"][1:20:end], "r--", linewidth=2, label="SGD")
#end

plt.rc("font", family="serif")

#title(r"\textbf{Movielens 1M}")
title("Yahoo Music Ratings", size=15)
xlabel("Sec.", size=15)
ylabel("RMSE", size=15)
if issemilogx 
    ylim([1.03, 1.45])
    xlim([10^2, 200000])
    legend()
else
    ylim([1.035, 1.08])
    xlim([10^1, 200000])
    legend()
    #legend(loc=7)
end
grid(true)

if issemilogx
    savefig("pp_logx.png", format="png", bbox_inches="tight")
    savefig("pp_logx.eps", format="eps", bbox_inches="tight")
    savefig("pp_logx.pdf", format="pdf", bbox_inches="tight")
else
    savefig("pp.png", format="png", bbox_inches="tight")
    savefig("pp.eps", format="eps", bbox_inches="tight")
    savefig("pp.pdf", format="pdf", bbox_inches="tight")
end



