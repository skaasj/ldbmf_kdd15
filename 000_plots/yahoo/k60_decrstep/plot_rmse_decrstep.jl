include("../../../24_v0.07_dev_64/utils/types.jl")
using HDF5, JLD

issemilogx = true


file = "20150126T081011_ka0_51_eps01_5e-6_tau500_ts33286_2_tg_rmse0_0_mxiter999999_maptrue_submeantrue_rP2_0_best1_0548_bthresh0_0_iscale0_0001_itvsv100_rA2_0_algoSGD-m2-MAP_mxsec999999_itvtst10_rQ2_0_itvhyp50_iter2000_bt0300_0_m100000_rB2_0_K60_rndlen50.jd"
logs_sgd_k60 = load(file)["logs"]

file = "20150120T082301_ts203664_0_tg_rmse1_09_mxiter999999_mapfalse_submeantrue_rP2_0_best1_0350_bthresh1_075_iscale0_001_itvsv100_rA2_0_algoS-G-2_mxsec999999_itvtst10_rQ2_0_itvhyp50_iter8100_eps1_0e-6_bt0300_0_m100000_rB2_0_K60_rndlen50.jd"
logs_sgld_k60 = load(file)["logs"]

file = "20150122T105957_C4_ts224893_7_mxiter999999_t5000_submeantrue_rP2_0_best1_0264_bthresh1_08_iscale0_0001_itvsv100_rA2_0_ncb4_algoDDS-G-2_G1_itvtst10_rQ2_0_itvhyp50_eps1_0e-6_S16_bt0500_0_m100000_aug0_0_rB2_0_K60_nrb4_rndlen50.jd"
logs_dsglds_k60 = load(file)["logs"]

file = "20150126T083414_ka0_51_C1_eps03_0e-7_tau100_ts33694_0_mxiter999999_t1400_submeantrue_rP2_0_best1_0588_iscale0_0001_itvsv100_rA2_0_ncb16_algoDSGD-2_iter1400_itvtst10_rQ2_0_S16_m100000_rB2_0_K60_nrb16_rndlen50.jd"
logs_dsgd_k60 = load(file)["logs"]

# Gibbs
file = "20150122T182716_ts8243_5_mxiter9999999_tg_rmse1_07_init_La0_001_maptrue_kappa0_51_submeantrue_best1_0699_bthresh1_09_iscale0_01_tau050_itvsv100_algoSGD-m1-MAP_mxsec9999999_itvtst20_itvhyp50_iter560_eps5_0e-6_m100000_K60_rndlen50.jd"
logs_map_k60 = load(file)["logs"]

file = "20150125T171546_ts249408_5_mxiter999999_itv_gibbs1_best1_0342_bthresh0_95_itvsv1_algoG-G-1_mxsec9999999_itvtst1_iter12_bt01_0_K60.jd"
logs_gibbs_k60 = load(file)["logs"]


###############################
# PyPlot must be included after loading HDF5 and JLD due to a bug in v0.3x
###############################
using PyPlot
clf()

############################################
# D-SGLD 
brn_idx = find(logs_dsglds_k60["ts"] .<= logs_dsglds_k60["burnin_ts"])
avg_idx = find(logs_dsglds_k60["ts"] .> logs_dsglds_k60["burnin_ts"])
rmse = vcat(logs_dsglds_k60["cur_rmse"][1:avg_idx[1]-1], logs_dsglds_k60["avg_rmse"][avg_idx])
if issemilogx
    semilogx(logs_dsglds_k60["ts"], rmse, color="red", "-", linewidth=3, label="DSGLD-S (4x4)") 
else
    plot(logs_dsglds_k60["ts"], rmse, color="red", "-", linewidth=3, label="DSGLD-S (4x4)") 
end

hold(true)

############################################
# DSGD
idx = [0.0:100000]
if issemilogx
    semilogx(logs_dsgd_k60["ts"][1:end], logs_dsgd_k60["rmse"][1:end], color="y", linewidth=3, label="DSGD (16x16)")
else
    plot(logs_dsgd_k60["ts"][1:end], logs_dsgd_k60["rmse"][1:end], color="y", linewidth=3, label="DSGD (16x16)")
end

############################################
# SGLD 
avg_idx = find(logs_sgld_k60["ts"] .> logs_sgld_k60["burnin_ts"])
rmse = vcat(logs_sgld_k60["cur_rmse"][1:avg_idx[1]-1], logs_sgld_k60["avg_rmse"][avg_idx])
if issemilogx
    semilogx(logs_sgld_k60["ts"], rmse, "g-", linewidth=3, label="SGLD") 
else
    plot(logs_sgld_k60["ts"], rmse, "g-", linewidth=3, label="SGLD") 
end


############################################
# BPMF
bpmf_ts = logs_map_k60["ts"][end] + logs_gibbs_k60["ts"]
bpmf_rmse = logs_gibbs_k60["avg_rmse"]
if issemilogx
    semilogx(bpmf_ts, bpmf_rmse, "k-", marker="", markersize=10, markeredgewidth=2, linewidth=3, label="Gibbs") 
else
    plot(bpmf_ts, bpmf_rmse, "k-", marker="", markersize=10, markeredgewidth=2, linewidth=3, label="Gibbs") 
end

############################################
# SGD
idx = [0.0:200000]
if issemilogx
    semilogx(idx, ones(length(idx)) * 1.0578, "--k", linewidth=3, label="SGD")
    #semilogx(logs_sgd_k60["ts"][1:5:end], logs_sgd_k60["cur_rmse"][1:5:end], "r-", linewidth=3, label="SGD")
else
    plot(idx, ones(length(idx)) * 1.0578, "--k", linewidth=3, label="SGD")
    #plot(logs_sgd_k60["ts"][1:20:end], logs_sgd_k60["cur_rmse"][1:20:end], "r-", linewidth=3, label="SGD")
end


##################################
fontsize = 21
plt.rc("font", family="serif", size=fontsize)
xlabel("Sec.", size=fontsize)
ylabel("RMSE", size=fontsize)
if issemilogx 
    ylim([1.025, 1.45])
    yticks([1.025:0.1:1.45])
    xlim([10^2, 200000])
    legend(fancybox=true, framealpha=0.8, fontsize=17)
else
    ylim([1.035, 1.09])
    xlim([10^1, 106500])
    legend(fancybox=true, framealpha=0.8, fontsize=17)
    xticks([0:20000:105000])
    #legend(loc=7)
end
grid(true)

if issemilogx
    savefig("yahoo_k60_decr_logx.png", format="png", bbox_inches="tight")
    savefig("yahoo_k60_decr_logx.eps", format="eps", bbox_inches="tight")
    savefig("yahoo_k60_decr_logx.pdf", format="pdf", bbox_inches="tight")
else
    savefig("yahoo_k60_decr_linx.png", format="png", bbox_inches="tight")
    savefig("yahoo_k60_decr_linx.eps", format="eps", bbox_inches="tight")
    savefig("yahoo_k60_decr_linx.pdf", format="pdf", bbox_inches="tight")
end



