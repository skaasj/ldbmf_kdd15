include("../../../24_v0.07_dev_64/utils/types.jl")
using HDF5, JLD

issemilogx = true

file = "20150211T172828_ka0_51_eps01_5e-6_tau500_ts28766_2_tg_rmse0_0_mxiter999999_maptrue_submeantrue_rP2_0_best1_0567_bthresh0_0_iscale0_0001_itvsv100_rA2_0_algoSGD-m2-MAP_mxsec999999_itvtst10_rQ2_0_itvhyp50_iter1600_bt0300_0_m100000_rB2_0_K100_rndlen50.jd"
logs_sgd_k100 = load(file)["logs"]

file = "20150214T202106_ka0_51_eps01_5e-6_tau2000_ts506086_2_tg_rmse0_0_mxiter999999_mapfalse_submeantrue_rP2_0_best1_0303_bthresh1_075_iscale0_0001_itvsv100_rA2_0_algoS-G-2_mxsec999999_itvtst10_rQ2_0_itvhyp50_iter12400_bt0300_0_m100000_rB2_0_K100_rndlen50.jd"
logs_sgld_k100 = load(file)["logs"]

file = "20150213T121853_ka0_51_C4_eps01_5e-6_tau1500_ts393424_8_mxiter999999_t6200_submeantrue_rP2_0_best1_0225_bthresh1_08_iscale0_0001_itvsv100_rA2_0_ncb4_algoDDS-G-2_itvtst10_rQ2_0_itvhyp50_S16_bt0300_0_m100000_aug0_0_rB2_0_K100_nrb4_rndlen50.jd"
logs_dsglds_k100 = load(file)["logs"]

file = "20150210T182626_ka0_51_C1_eps03_0e-7_tau100_ts8335_3_mxiter999999_t400_submeantrue_rP2_0_best1_0631_iscale0_0001_itvsv100_rA2_0_ncb16_algoDSGD-2_iter400_itvtst10_rQ2_0_S16_m100000_rB2_0_K100_nrb16_rndlen50.jd"
logs_dsgd_k100 = load(file)["logs"]

# BPMF
file = "20150210T182059_ts11799_5_mxiter9999999_tg_rmse1_08_init_La0_001_maptrue_kappa0_51_submeantrue_best1_0786_iscale0_01_bthresh1_08_tau0500_itvsv100_algoSGD-m1-MAP_mxsec9999999_itvhyp50_itvtst20_iter500_eps1_5e-6_m100000_K100_rndlen50.jd"
logs_map_k100 = load(file)["logs"]

file = "20150216T033732_ts445394_3_mxiter999999_itv_gibbs1_best1_0292_bthresh0_95_itvsv1_algoG-G-1_mxsec9999999_itvtst1_iter13_bt01_0_K100.jd"
logs_gibbs_k100 = load(file)["logs"]


###############################
# PyPlot must be included after loading HDF5 and JLD due to a bug in v0.3x
###############################
using PyPlot
clf()

############################################
# D-SGLD 
brn_idx = find(logs_dsglds_k100["ts"] .<= logs_dsglds_k100["burnin_ts"])
avg_idx = find(logs_dsglds_k100["ts"] .> logs_dsglds_k100["burnin_ts"])
rmse = vcat(logs_dsglds_k100["cur_rmse"][1:avg_idx[1]-1], logs_dsglds_k100["avg_rmse"][avg_idx])
if issemilogx
    semilogx(logs_dsglds_k100["ts"], rmse, color="red", "-", linewidth=3, label="DSGLD-S (4x4)") 
else
    plot(logs_dsglds_k100["ts"], rmse, color="red", "-", linewidth=3, label="DSGLD-S (4x4)") 
end

hold(true)
############################################
# DSGD
idx = [0.0:100000]
if issemilogx
    semilogx(logs_dsgd_k100["ts"][1:end], logs_dsgd_k100["rmse"][1:end], color="y", linewidth=3, label="DSGD (16x16)")
else
    plot(logs_dsgd_k100["ts"][1:end], logs_dsgd_k100["rmse"][1:end], color="y", linewidth=3, label="DSGD (16x16)")
end

############################################
# SGLD 
avg_idx = find(logs_sgld_k100["ts"] .> logs_sgld_k100["burnin_ts"])
rmse = vcat(logs_sgld_k100["cur_rmse"][1:avg_idx[1]-1], logs_sgld_k100["avg_rmse"][avg_idx])
if issemilogx
    semilogx(logs_sgld_k100["ts"], rmse, "g-", linewidth=3, label="SGLD") 
else
    plot(logs_sgld_k100["ts"], rmse, "g-", linewidth=3, label="SGLD") 
end


############################################
# BPMF
bpmf_ts = logs_map_k100["ts"][end] + logs_gibbs_k100["ts"]
bpmf_rmse = logs_gibbs_k100["avg_rmse"]
if issemilogx
    semilogx(bpmf_ts, bpmf_rmse, "k-", marker="", markersize=10, markeredgewidth=2, linewidth=3, label="Gibbs") 
else
    plot(bpmf_ts, bpmf_rmse, "k-", marker="", markersize=10, markeredgewidth=2, linewidth=3, label="Gibbs") 
end

############################################
# SGD
idx = [0.0:400000]
if issemilogx
    semilogx(idx, ones(length(idx)) * 1.0567, "--k", linewidth=3, label="SGD")
    #semilogx(logs_sgd_k100["ts"][1:5:end], logs_sgd_k100["cur_rmse"][1:5:end], "r-", linewidth=3, label="SGD")
else
    plot(idx, ones(length(idx)) * 1.0567, "--k", linewidth=3, label="SGD")
    #plot(logs_sgd_k100["ts"][1:20:end], logs_sgd_k100["cur_rmse"][1:20:end], "r-", linewidth=3, label="SGD")
end


##################################
fontsize = 21
plt.rc("font", family="serif", size=fontsize)
xlabel("Sec.", size=fontsize)
ylabel("RMSE", size=fontsize)
if issemilogx 
    ylim([1.02, 1.43])
    yticks([1.02:0.1:1.43])
    xlim([10^2, 500000])
    legend(fancybox=true, framealpha=0.8, fontsize=17)
else
    ylim([1.035, 1.09])
    xlim([10^1, 106500])
    legend(fancybox=true, framealpha=0.8, fontsize=17)
    xticks([0:20000:105000])
    #legend(loc=7)
end
grid(true)

if issemilogx
    savefig("yahoo_k100_decr_logx.png", format="png", bbox_inches="tight")
    savefig("yahoo_k100_decr_logx.eps", format="eps", bbox_inches="tight")
    savefig("yahoo_k100_decr_logx.pdf", format="pdf", bbox_inches="tight")
else
    savefig("yahoo_k100_decr_linx.png", format="png", bbox_inches="tight")
    savefig("yahoo_k100_decr_linx.eps", format="eps", bbox_inches="tight")
    savefig("yahoo_k100_decr_linx.pdf", format="pdf", bbox_inches="tight")
end



