include("../../../24_v0.07_dev_64/utils/types.jl")
using HDF5, JLD

issemilogx = false

#file = "20150127T215716_ka0_51_C16_eps09_0e-7_tau1500_ts183377_2_mxiter999999_t6900_matavgfalse_rP2_0_best1_0403_bthresh1_08_iscale0_0001_itvsv100_rA2_0_algoDS-G-2_itvtst10_rQ2_0_itvhyp50_S16_bt0300_0_m100000_aug0_0_rB2_0_K30_rndlen50.jd"
#logs_dsgldc_k30 = load(file)["logs"]


file = "20150124T021609_ka0_51_eps01_5e-6_tau500_ts30519_4_tg_rmse0_0_mxiter999999_maptrue_submeantrue_rP2_0_best1_0578_bthresh0_0_iscale0_0001_itvsv100_rA2_0_algoSGD-m2-MAP_mxsec999999_itvtst10_rQ2_0_itvhyp50_iter3000_bt0300_0_m100000_rB2_0_K30_rndlen50.jd"
logs_sgd_k30 = load(file)["logs"]

file = "20150124T225616_ka0_51_eps01_5e-6_tau1000_ts105966_3_tg_rmse0_0_mxiter999999_mapfalse_submeantrue_rP2_0_best1_0447_bthresh1_08_iscale0_0001_itvsv100_rA2_0_algoS-G-2_mxsec999999_itvtst10_rQ2_0_itvhyp50_iter6500_bt0300_0_m100000_rB2_0_K30_rndlen50.jd"
logs_sgld_k30 = load(file)["logs"]


#file = "20150125T223725_ka0_51_C4_eps01_5e-6_tau500_ts109739_1_mxiter999999_t3000_submeantrue_rP2_0_best1_0387_bthresh1_08_iscale0_0001_itvsv100_rA2_0_ncb4_algoDDS-G-2_itvtst10_rQ2_0_itvhyp50_S16_bt0300_0_m100000_aug0_0_rB2_0_K30_nrb4_rndlen50.jd"
#lgos_dsglds_k30 = load(file)["logs"]


#file = "20150123T164834_ka0_51_C1_eps03_0e-7_tau100_ts9327_3_mxiter999999_t600_submeantrue_rP2_0_best1_0576_iscale0_0001_itvsv100_rA2_0_ncb16_algoDSGD-2_iter600_itvtst10_rQ2_0_S16_m100000_rB2_0_K30_nrb16_rndlen50.jd"
#logs_dsgd_k30 = load(file)["logs"]

# BPMF
file = "20141228T193651_ts8166_2_tg_rmse1_08_mxiter999999_hthresh1_08_rP2_0_submeantrue_bthresh1_08_iscale0_0001_best100_0000_itvsv200_rA2_0_algoS-G-2_mxsec999999_itvtst20_rQ2_0_itvhyp100_iter780_eps1_0e-6_bt0300_0_m100000_rB2_0_K30_rndlen50.jd"
logs_map_k30 = load(file)["logs"]

#file = "20150109T112605_best1_0496_mxiter9999999_itv_gibbs1_itvsv1_algoG-G-2_mxsec9999999_K30_itvtst1_iter14_ts148933_8.jd"
#file = "20150112T074857_ts78876_1_mxiter999999_itv_gibbs1_best1_0467_bthresh0_95_itvsv1_algoG-G-1_mxsec9999999_itvtst1_iter8_bt01_0_K30.jd"
file = "20150113T134541_ts186631_4_mxiter999999_itv_gibbs1_best1_0428_bthresh0_95_itvsv1_algoG-G-1_mxsec9999999_itvtst1_iter19_bt01_0_K30.jd"
logs_gibbs_k30 = load(file)["logs"]


###############################
# PyPlot must be included after loading HDF5 and JLD due to a bug in v0.3x
###############################
using PyPlot
clf()

############################################
# D-SGLD 
#brn_idx = find(lgos_dsglds_k30["ts"] .<= lgos_dsglds_k30["burnin_ts"])
#avg_idx = find(lgos_dsglds_k30["ts"] .> lgos_dsglds_k30["burnin_ts"])
#rmse = vcat(lgos_dsglds_k30["cur_rmse"][1:avg_idx[1]-1], lgos_dsglds_k30["avg_rmse"][avg_idx])
#if issemilogx
    #semilogx(lgos_dsglds_k30["ts"], rmse, color="red", "-", linewidth=3, label="DSGLD-S (4x4)") 
#else
    #plot(lgos_dsglds_k30["ts"], rmse, color="red", "-", linewidth=3, label="DSGLD-S (4x4)") 
#end

hold(true)
############################################
# DSGLD S=16, C=16
#avg_idx = find(logs_dsgldc_k30["ts"] .> logs_dsgldc_k30["burnin_ts"])
#rmse = vcat(logs_dsgldc_k30["cur_rmse"][1:avg_idx[1]-1], logs_dsgldc_k30["avg_rmse"][avg_idx])

#if issemilogx 
    #semilogx(logs_dsgldc_k30["ts"], rmse, "b-", linewidth=3, label="DSGLD-C (16x1)")
#else 
    #plot(logs_dsgldc_k30["ts"], rmse, "b-", linewidth=3, label="DSGLD-C (16x1)")
#end

############################################
# DSGD
#idx = [0.0:100000]
#if issemilogx
    #semilogx(logs_dsgd_k30["ts"][1:end], logs_dsgd_k30["rmse"][1:end], color="y", linewidth=3, label="DSGD (16x16)")
#else
    #plot(logs_dsgd_k30["ts"][1:end], logs_dsgd_k30["rmse"][1:end], color="y", linewidth=3, label="DSGD (16x16)")
#end

############################################
# SGLD 
avg_idx = find(logs_sgld_k30["ts"] .> logs_sgld_k30["burnin_ts"])
rmse = vcat(logs_sgld_k30["cur_rmse"][1:avg_idx[1]-1], logs_sgld_k30["avg_rmse"][avg_idx])
if issemilogx
    semilogx(logs_sgld_k30["ts"], rmse, "g-", linewidth=3, label="SGLD") 
else
    plot(logs_sgld_k30["ts"], rmse, "g-", linewidth=3, label="SGLD") 
end


############################################
# BPMF
bpmf_ts = logs_map_k30["ts"][end] + logs_gibbs_k30["ts"]
bpmf_rmse = logs_gibbs_k30["avg_rmse"]
if issemilogx
    semilogx(bpmf_ts, bpmf_rmse, "k-", marker="", markersize=10, markeredgewidth=2, linewidth=3, label="Gibbs") 
else
    plot(bpmf_ts, bpmf_rmse, "k-", marker="", markersize=10, markeredgewidth=2, linewidth=3, label="Gibbs") 
end

############################################
# SGD
idx = [0.0:200000]
if issemilogx
    #semilogx(idx, ones(length(idx)) * 1.0578, "--k", linewidth=3, label="SGD")
    semilogx(logs_sgd_k30["ts"][1:5:end], logs_sgd_k30["cur_rmse"][1:5:end], "r-", linewidth=3, label="SGD")
else
    #plot(idx, ones(length(idx)) * 1.0578, "--k", linewidth=3, label="SGD")
    plot(logs_sgd_k30["ts"][1:20:end], logs_sgd_k30["cur_rmse"][1:20:end], "r-", linewidth=3, label="SGD")
end


##################################
fontsize = 21
plt.rc("font", family="serif", size=fontsize)
xlabel("Sec.", size=fontsize)
ylabel("RMSE", size=fontsize)
if issemilogx 
    ylim([1.03, 1.45])
    yticks([1.03:0.1:1.45])
    xlim([10^2, 150000])
    legend(fancybox=true, framealpha=0.8, fontsize=17)
else
    ylim([1.035, 1.09])
    xlim([10^1, 106500])
    legend(fancybox=true, framealpha=0.8, fontsize=17)
    xticks([0:20000:105000])
    #legend(loc=7)
end
grid(true)

if issemilogx
    savefig("yahoo_k30_sgd_sgld_logx.png", format="png", bbox_inches="tight")
    savefig("yahoo_k30_sgd_sgld_logx.eps", format="eps", bbox_inches="tight")
    savefig("yahoo_k30_sgd_sgld_logx.pdf", format="pdf", bbox_inches="tight")
else
    savefig("yahoo_k30_sgd_sgld_linx.png", format="png", bbox_inches="tight")
    savefig("yahoo_k30_sgd_sgld_linx.eps", format="eps", bbox_inches="tight")
    savefig("yahoo_k30_sgd_sgld_linx.pdf", format="pdf", bbox_inches="tight")
end



