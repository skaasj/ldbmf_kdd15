function sample_hyper_gibbs_m1!(model::Dict{ASCIIString,Any})

    P::Array{Float64,2} = model["P"]
    Q::Array{Float64,2} = model["Q"]
    bt0_p::Float64 = model["bt0_p"]
    bt0_q::Float64 = model["bt0_q"]
    mu0_p::Array{Float64,1} = model["mu0_p"]
    mu0_q::Array{Float64,1} = model["mu0_q"]
    K, n_users = size(P)
    n_items = size(Q,2)
    W0 = eye(K)   # for numerical stability of model 1
    df = float(K)

    ## sample item params
    x_bar    = vec(mean(Q,2))
    S_bar    = cov(Q')
    W_post   = inv(inv(W0) + n_items * S_bar + n_items * bt0_q * ((mu0_q - x_bar) * (mu0_q - x_bar)')/((bt0_q + n_items)))
    W_post   = (W_post + W_post')/2
    Lam_Q    = rand(Wishart(df + n_items, W_post))
    mu0_star = (bt0_q * mu0_q + n_items * x_bar) / (bt0_q + n_items)
    lam      = chol(inv((bt0_q + n_items) * Lam_Q))'
    
    model["mu_q"] = lam * randn(K) + mu0_star
    model["La_q"] = Lam_Q

    ## sample user params
    x_bar    = vec(mean(P,2))
    S_bar    = cov(P')
    W_post   = inv(inv(W0) + n_users * S_bar + n_users * bt0_p * ((mu0_p - x_bar) * (mu0_p - x_bar)')/((bt0_p + n_users)))
    W_post   = (W_post + W_post')/2
    Lam_P    = rand(Wishart(df + n_users, W_post))
    mu0_star = (bt0_p * mu0_p + n_users * x_bar) / (bt0_p + n_users)
    lam      = chol(inv((bt0_p + n_users) * Lam_P))'

    model["mu_p"] = lam * randn(K) + mu0_star
    model["La_p"] = Lam_P

    return nothing
end

#function sample_hyper_full(P::Array{Float64,2}, Q::Array{Float64,2}, 
                           #n_items::Int64, n_users::Int64,
                           #WI_p::Array{Float64,2}, WI_q::Array{Float64,2},
                           #bt0_p::Float64, bt0_q::Float64, mu0_p::Array{Float64,1}, 
                           #mu0_q::Array{Float64,1}, K::Int64)
    ### sample item params
    #df = size(P,1)
    #df = df
    #x_bar = vec(mean(Q,2))
    #S_bar = cov(Q')
    #W_post = inv(inv(WI_q) + n_items/1 * S_bar + n_items * bt0_q * (mu0_q - x_bar) * (mu0_q - x_bar)'/(1 * (bt0_q + n_items)))
    #W_post = (W_post + W_post')/2
    #df_qmost = df + n_items
    #Lam_Q = rand(Wishart(float(df_qmost), W_post))
    #mu_temp = (bt0_q * mu0_q + n_items * x_bar) / (bt0_q + n_items)
    #lam = chol(inv((bt0_q + n_items) * Lam_Q)); lam = lam'
    #mu_q = lam * randn(K) + mu_temp

    ### sample user params
    #x_bar = vec(mean(P,2))
    #S_bar = cov(P')
    #W_post = inv(inv(WI_p) + n_users/1 * S_bar + n_users * bt0_p * (mu0_p - x_bar) * (mu0_p - x_bar)'/(1 * (bt0_p + n_users)))
    #W_post = (W_post + W_post')/2
    #df_pmost = df + n_users
    #Lam_P = rand(Wishart(float(df_pmost), W_post))
    #mu_temp = (bt0_p * mu0_p + n_users * x_bar) / (bt0_p + n_users)
    #lam = chol(inv((bt0_p + n_users) * Lam_P)); lam = lam'
    #mu_p = lam * randn(K) + mu_temp

    #return mu_p, mu_q, Lam_P, Lam_Q
#end
